package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.PackageService;

import java.util.List;

public class PackagePricelistAdapter extends  RecyclerView.Adapter<PackagePricelistAdapter.ProductViewHolder>{

    private Context context;
    private List<Package> packages;
    private PackageService packageService;

    UserRole userRole;

    public PackagePricelistAdapter(Context context, List<Package> packages, PackageService packageService, UserRole userRole) {
        this.context = context;
        this.packages = packages;
        this.packageService = packageService;
        this.userRole = userRole;


        Log.i("Frag","Napravio adapter");
    }

    @NonNull
    @Override
    public PackagePricelistAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pricelist, parent, false);
        return new PackagePricelistAdapter.ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PackagePricelistAdapter.ProductViewHolder holder, int position) {
        Package product = packages.get(position);
        holder.bind(product);
    }
    @Override
    public int getItemCount() {
        return packages.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        ImageButton editPackageBtn;
        TextView packageName, packagePrice, packageDiscount, packageDiscountPrice, packageOridinalNumber;
        private Package currentService;
        private int currentIndex;
        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            editPackageBtn = itemView.findViewById(R.id.pricelist_item_edit_btn);

            packageOridinalNumber = itemView.findViewById(R.id.pricelist_item_oridinalNumber);
            packageName = itemView.findViewById(R.id.pricelist_item_name);
            packagePrice = itemView.findViewById(R.id.pricelist_item_price);
            packageDiscount = itemView.findViewById(R.id.pricelist_item_discount);
            packageDiscountPrice = itemView.findViewById(R.id.pricelist_item_newPrice);


            editPackageBtn.setOnClickListener(v -> {
                if(userRole == UserRole.OWNER ){

                    editProductPopup(currentService);

                }else{
                    Toast.makeText(context, "Your have no permission for this action", Toast.LENGTH_SHORT).show();
                }

            });
        }

        public void bind(Package service) {
            currentService = service;
            currentIndex = 0;
            boolean ind = service != null;
            currentIndex = packages.indexOf(service);
            currentIndex++;
            Log.i("binder",ind? "nije null": "null je");
            packageName.setText(service.getName());
            packageOridinalNumber.setText(String.valueOf(currentIndex));
            packagePrice.setText(String.valueOf(service.getPrice()));
            packageDiscount.setText(String.valueOf(service.getDiscount()));
            double finalprice = service.getPrice() - service.getDiscount()*service.getPrice()/100;
            packageDiscountPrice.setText(String.valueOf(finalprice));

        }

        @SuppressLint({"NotifyDataSetChanged"})
        private void editProductPopup(Package service) {
            @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.edit_pricelist, null);

            // Create a PopupWindow
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            boolean focusable = true;

            PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
            EditText price = popupView.findViewById(R.id.edit_pricelist_price);
            price.setVisibility(View.GONE);
            price.setText(String.valueOf(service.getPrice()));

            EditText discount = popupView.findViewById(R.id.edit_pricelist_discount);
            discount.setText(String.valueOf(service.getDiscount()));


            //show window
            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

            Button saveButton = popupView.findViewById(R.id.buttonSave_edit_pricelist);
            Button cancelButton = popupView.findViewById(R.id.buttonCancel_edit_pricelist);

            saveButton.setOnClickListener(v -> {
                service.setDiscount(Double.parseDouble(discount.getText().toString()));

                packageService.update(service);
                notifyDataSetChanged();
                popupWindow.dismiss();

            });

            cancelButton.setOnClickListener(v -> {

                popupWindow.dismiss();

            });
        }


    }
}
