package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ProductPricelistAdapter;
import com.example.eventplanner.database.repository.ProductRepository;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.ProductService;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PricelistProductsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PricelistProductsFragment extends Fragment {

    private static final String ARG_USER_ROLE = "user_role";
    private static final String ARG_COMPANY_ID = "company_id";

    private RecyclerView recyclerView;
    private ProductPricelistAdapter productAdapter;
    private List<Product> productList;
    private ProductService productService;
    private String companyId;
    private String userRole;


    public PricelistProductsFragment() {
        // Required empty public constructor
    }


    public static PricelistProductsFragment newInstance(String userRole, String companyId) {
        PricelistProductsFragment fragment = new PricelistProductsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USER_ROLE, userRole);
        args.putString(ARG_COMPANY_ID, companyId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userRole = getArguments().getString(ARG_USER_ROLE);
            companyId = getArguments().getString(ARG_COMPANY_ID);
        }

        ProductRepository productRepository = new ProductRepository();
        productService = new ProductService(productRepository);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pricelist_products, container, false);

        recyclerView = view.findViewById(R.id.product_pricelist);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        // Call the method to initialize the RecyclerView
        initializeRecyclerView();

        return view;
    }

    private void initializeRecyclerView(){

        productService.getUndeletedOfPUP(companyId ,new ProductService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Product> objects) {
                productList = new ArrayList<>(objects);
                if(userRole.equals("OWNER")){
                    productAdapter = new ProductPricelistAdapter(getContext(), productList,productService, UserRole.OWNER);
                }else{
                    productAdapter = new ProductPricelistAdapter(getContext(), productList,productService, UserRole.EMPLOYEE);
                }

                recyclerView.setAdapter(productAdapter);

            }

            @Override
            public void onFailure(String errorMessage) {}
        });
    }
}