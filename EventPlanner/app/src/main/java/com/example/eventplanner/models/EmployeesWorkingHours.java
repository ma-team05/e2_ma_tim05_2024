package com.example.eventplanner.models;

import java.util.Date;

public class EmployeesWorkingHours {
    private String id;
    private Date validFrom;
    private Date validTo;
    private TimeSchedule timeSchedule;
    private String employeeId;
    private Date timestamp;
    public EmployeesWorkingHours(){}
    public EmployeesWorkingHours(String id, Date validFrom, Date validTo, TimeSchedule schedule, String employeeId){
        this.id = id;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.timeSchedule = schedule;
        this.employeeId = employeeId;
        timestamp = new Date();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public TimeSchedule getTimeSchedule() {
        return timeSchedule;
    }

    public void setTimeSchedule(TimeSchedule timeSchedule) {
        this.timeSchedule = timeSchedule;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
