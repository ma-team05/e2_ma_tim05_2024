package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.CompanyRepository;
import com.example.eventplanner.models.Company;
import com.example.eventplanner.models.Employee;
import com.google.android.gms.tasks.Task;

public class CompanyService {
    private final CompanyRepository repository;

    public CompanyService() {
        this.repository = new CompanyRepository();
    }
    public Task<String> create(Company company) {
        return repository.save(company);
    }
    public Task<Company> get(String id){ return repository.get(id); }
    public Task<Company> getByName(String companyName) { return  repository.getByName(companyName); }

    public Task<Void> update(Company company){
        return repository.update(company);
    }
}