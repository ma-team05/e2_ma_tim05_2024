package com.example.eventplanner.models;

import java.util.Date;

public class Reservation {
    private String id;
    private String eventPlannerId;
    private User eventPlanner;
    private String employeeId;
    private Employee employee;
    private String serviceId;
    private Service service;
    private Date from;
    private Date to;
    private ReservationStatus status;
    private String packageId;
    private Date cancellationDeadline;
    private Boolean isGraded;
    private String eventId = "";

    public Reservation() {}

    public Reservation(User eventPlanner, Employee employee, Date from, Date to, Service service) {
        this.eventPlanner = eventPlanner;
        this.employee = employee;
        this.serviceId = service.getId();
        this.from = from;
        this.to = to;
        this.packageId = null;
        this.isGraded = false;
        this.status = ReservationStatus.NEW;

        if (service.getCancelationDeadline() != null) {
            this.cancellationDeadline = service.getCancelationDeadline().toDate();
        } else {
            this.cancellationDeadline = new Date();
        }

        this.service = service;
    }
    public Reservation(User eventPlanner, Employee employee, Date from, Date to, Service service, String packageId) {
        this.eventPlanner = eventPlanner;
        this.employee = employee;
        this.serviceId = service.getId();
        this.from = from;
        this.to = to;
        this.packageId = packageId;
        this.isGraded = false;
        this.status = ReservationStatus.NEW;

        if (service.getCancelationDeadline() != null) {
            this.cancellationDeadline = service.getCancelationDeadline().toDate();
        } else {
            this.cancellationDeadline = new Date();
        }

        this.service = service;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getEventPlannerId() {
        return eventPlannerId;
    }
    public void setEventPlannerId(String eventPlannerId) {
        this.eventPlannerId = eventPlannerId;
    }
    public String getEmployeeId() {
        return employeeId;
    }
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
    public String getServiceId() {
        return serviceId;
    }
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
    public Date getFrom() {
        return from;
    }
    public void setFrom(Date from) {
        this.from = from;
    }
    public Date getTo() {
        return to;
    }
    public void setTo(Date to) {
        this.to = to;
    }
    public ReservationStatus getStatus() {
        return status;
    }
    public void setStatus(ReservationStatus status) {
        this.status = status;
    }
    public String getPackageId() {
        return packageId;
    }
    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }
    public Date getCancellationDeadline() {
        return cancellationDeadline;
    }
    public void setCancellationDeadline(Date cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }
    public User getEventPlanner() {
        return eventPlanner;
    }
    public void setEventPlanner(User eventPlanner) {
        this.eventPlanner = eventPlanner;
    }
    public Employee getEmployee() {
        return employee;
    }
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public Service getService() {
        return service;
    }
    public void setService(Service service) {
        this.service = service;
    }
    public Boolean getGraded() {
        return isGraded;
    }
    public void setGraded(Boolean graded) {
        isGraded = graded;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}