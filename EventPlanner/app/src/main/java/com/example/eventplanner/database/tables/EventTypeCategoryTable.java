package com.example.eventplanner.database.tables;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class EventTypeCategoryTable {
    private EventTypeCategoryTable() {}

    public static final class Table implements BaseColumns {
        public static final String TABLE_NAME = "event_type_category";
        public static final String COLUMN_EVENT_TYPE_ID = "event_type_id";
        public static final String COLUMN_CATEGORY_ID = "category_id";
    }

    public static final class DDL {
        public static final String CREATE_TABLE = "CREATE TABLE " +
                Table.TABLE_NAME + " (" +
                Table.COLUMN_EVENT_TYPE_ID + " INTEGER NOT NULL, " +
                Table.COLUMN_CATEGORY_ID + " INTEGER NOT NULL, " +
                "FOREIGN KEY(" + Table.COLUMN_EVENT_TYPE_ID + ") REFERENCES " +
                EventTypeTable.Table.TABLE_NAME + "(" + EventTypeTable.Table._ID + "), " +
                "FOREIGN KEY(" + Table.COLUMN_CATEGORY_ID + ") REFERENCES " +
                CategoryTable.Table.TABLE_NAME + "(" + CategoryTable.Table._ID + "), " +
                "PRIMARY KEY(" + Table.COLUMN_EVENT_TYPE_ID + ", " + Table.COLUMN_CATEGORY_ID + ")" +
                ");";

        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " +
                Table.TABLE_NAME;
    }

    public static final class DML {
        public static void insertData(SQLiteDatabase db) {
            ContentValues values = new ContentValues();

            values.put(Table.COLUMN_EVENT_TYPE_ID, 2);
            values.put(Table.COLUMN_CATEGORY_ID, 1);
            db.insert(Table.TABLE_NAME, null, values);

            values.clear();
            values.put(Table.COLUMN_EVENT_TYPE_ID, 3);
            values.put(Table.COLUMN_CATEGORY_ID, 4);
            db.insert(Table.TABLE_NAME, null, values);
        }
    }
}
