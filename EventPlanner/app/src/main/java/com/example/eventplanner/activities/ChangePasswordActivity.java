package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.MainActivity;
import com.example.eventplanner.R;
import com.example.eventplanner.activities.admins_actions.LoggedInAdminActivity;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.User;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.UserService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangePasswordActivity extends AppCompatActivity {

    private UserService userService = new UserService();
    private EmployeeService employeeService = new EmployeeService();
    private OwnerService ownerService = new OwnerService();
    private User user;

    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_change_password);

        Intent intent = getIntent();
        email = intent.getStringExtra("EMAIL");

        userService.getByEmail(email).addOnCompleteListener(this, task->{
            if(task.isSuccessful()){
                user = task.getResult();

                Button edit = findViewById(R.id.edit);
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        validateFields();
                    }
                });
            }
        });
    }

    private boolean validateFields() {
        boolean isValid = true;

        EditText oldET, newET, repeatET;

        oldET = findViewById(R.id.old_password);
        newET = findViewById(R.id.new_password);
        repeatET = findViewById(R.id.repeat_password);

        String old = oldET.getText().toString().trim();
        String newP = newET.getText().toString().trim();
        String repeat = repeatET.getText().toString().trim();

        if (TextUtils.isEmpty(old)) {
            oldET.setError("Please enter the old password");
            oldET.requestFocus();
            isValid = false;
        }
        else if(!old.equals(user.getPassword())) {
            oldET.setError("Invalid old password");
            oldET.requestFocus();
            isValid = false;
        }

        if (TextUtils.isEmpty(newP)) {
            newET.setError("Please enter the new password");
            newET.requestFocus();
            isValid = false;
        }

        if (TextUtils.isEmpty(repeat)) {
            repeatET.setError("Please repeat the new password");
            repeatET.requestFocus();
            isValid = false;
        }

        if (!TextUtils.isEmpty(newP) && !TextUtils.isEmpty(repeat) && !newP.equals(repeat)) {
            newET.setError("New password and Repeat password must match");
            newET.requestFocus();
            repeatET.setError("New password and Repeat password must match");
            repeatET.requestFocus();
            isValid = false;
        }

        if(isValid) {
            editEvent();
        }

        return isValid;
    }

    private void editEvent() {
        EditText firstNameEditText, lastNameEditText, phoneEditText, addressEditText;

        EditText oldET, newET;

        oldET = findViewById(R.id.old_password);
        newET = findViewById(R.id.new_password);

        String old = oldET.getText().toString().trim();
        String newP = newET.getText().toString().trim();

        user.setPassword(newP);

        userService.update(user);

        if(user.getRole().equals(UserRole.EMPLOYEE)) {
            employeeService.getAll().addOnCompleteListener(employeeTask -> {
                if (employeeTask.isSuccessful()) {
                    Employee employee = employeeTask.getResult().stream().filter(e -> e.getEmail().equals(email)).findFirst().orElse(null);
                    employee.setPassword(newP);
                    employeeService.update(employee);
                }
            });
        }
        else if(user.getRole().equals(UserRole.OWNER)) {
            ownerService.getAll().addOnCompleteListener(employeeTask -> {
                if (employeeTask.isSuccessful()) {
                    Owner owner = employeeTask.getResult().stream().filter(o -> o.getEmail().equals(email)).findFirst().orElse(null);
                    owner.setPassword(newP);
                    ownerService.update(owner);
                }
            });
        }

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(email, old)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser currentUser = mAuth.getCurrentUser();
                        currentUser.updatePassword(newP)
                                .addOnCompleteListener(task1 -> {
                                    if (task.isSuccessful()) {
                                        String eventDetails = "Password changed";
                                        Toast.makeText(this, eventDetails, Toast.LENGTH_LONG).show();

                                        Intent intent = new Intent(this, UserProfileActivity.class);
                                        intent.putExtra("email", email);
                                        intent.putExtra("EMAIL", email);
                                        intent.putExtra("ForReport", "no");
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);

                                        finish();
                                    } else {
                                        Log.e("ChangePassword", "Password update failed.", task.getException());
                                    }
                                });
                    }
                    else {
                        Log.e("ChangePassword", "Password update failed.", task.getException());
                    }

                });
    }
}