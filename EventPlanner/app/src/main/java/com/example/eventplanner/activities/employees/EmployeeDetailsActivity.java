package com.example.eventplanner.activities.employees;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EmployeeEventAdapter;
import com.example.eventplanner.adapters.WorkingHoursAdapter;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.EmployeesWorkingHours;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.EventStatus;
import com.example.eventplanner.models.TimeSchedule;
import com.example.eventplanner.models.User;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.EmployeesWorkingHoursService;
import com.example.eventplanner.services.EventService;
import com.example.eventplanner.services.UserService;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class EmployeeDetailsActivity extends AppCompatActivity {

    private List<EmployeesWorkingHours> workingHoursList = new ArrayList<>();
    private boolean viewForOwner;
    private TimeSchedule newSchedule = new TimeSchedule();
    private Employee employee;
    private EmployeeService service;
    private Button addEventButton;
    private RecyclerView recyclerView;
    private List<Event> eventList;
    private EmployeeEventAdapter eventAdapter;
    private WorkingHoursAdapter workingHoursAdapter;
    private TextInputEditText datePickerEditText;
    private TextInputEditText weekPickerEditText;
    private TextInputEditText datePickerEditText2;
    private Button openDatePickerButton;
    private Button openWeekPickerButton;
    private Button openDatePickerButton2;
    private Button addButton;
    private Button deactivateButton;

    @Override
    protected void onResume(){
        super.onResume();
        if(employee!=null)
            initializeRecyclerView();
    }

    private void showDatePicker(TextInputEditText datePicker, boolean week) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                (view, year1, monthOfYear, dayOfMonth1) -> {
                    if (!week) {
                        datePicker.setText(String.format(Locale.getDefault(), "%04d-%02d-%02d", year1, monthOfYear + 1, dayOfMonth1));
                    } else {
                        Calendar selectedDate = Calendar.getInstance();
                        selectedDate.set(year1, monthOfYear, dayOfMonth1);

                        int selectedDayOfWeek = selectedDate.get(Calendar.DAY_OF_WEEK);
                        int firstDayOfWeek = selectedDate.getFirstDayOfWeek();

                        int diff = selectedDayOfWeek - firstDayOfWeek;
                        if (diff < 0) {
                            diff += 7; // Ensure positive value
                        }

                        Calendar firstDayOfWeekCalendar = (Calendar) selectedDate.clone();
                        firstDayOfWeekCalendar.add(Calendar.DAY_OF_MONTH, -diff);

                        Calendar lastDayOfWeekCalendar = (Calendar) firstDayOfWeekCalendar.clone();
                        lastDayOfWeekCalendar.add(Calendar.DAY_OF_MONTH, 6); // Last day of the week

                        String startDate = String.format(Locale.getDefault(), "%04d-%02d-%02d",
                                firstDayOfWeekCalendar.get(Calendar.YEAR),
                                firstDayOfWeekCalendar.get(Calendar.MONTH) + 1,
                                firstDayOfWeekCalendar.get(Calendar.DAY_OF_MONTH));

                        String endDate = String.format(Locale.getDefault(), "%04d-%02d-%02d",
                                lastDayOfWeekCalendar.get(Calendar.YEAR),
                                lastDayOfWeekCalendar.get(Calendar.MONTH) + 1,
                                lastDayOfWeekCalendar.get(Calendar.DAY_OF_MONTH));

                        datePicker.setText(startDate + " - " + endDate);
                        initializeRecyclerView();
                        addButton.setVisibility(View.VISIBLE);
                    }
                },
                year,
                month,
                dayOfMonth
        );
        datePickerDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        service = new EmployeeService(new EmployeeRepository());
        setContentView(R.layout.activity_employee_details);
        addButton = findViewById(R.id.addEventButton);
        Button cancel = findViewById(R.id.cancelButton);
        Intent intent = getIntent();
        viewForOwner = intent.getBooleanExtra("viewForOwner", false);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button save = findViewById(R.id.saveButton);
        if(!viewForOwner)
            save.setText("Take event");
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewForOwner)
                    saveWorkingHours();
                else
                    takeEvent();
            }
        });
        configureDatePickers();
        bindEmployee();
    }
    private void takeEvent(){
        if(eventAdapter==null){
            Toast.makeText(this, "Select week for your events schedule!", Toast.LENGTH_SHORT).show();
            return;
        }
        int index = eventAdapter.selectedItem;

        if(index==RecyclerView.NO_POSITION){
            Toast.makeText(this, "Please select the event first!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(eventList==null){
            Toast.makeText(this, "Empty list of events!", Toast.LENGTH_SHORT).show();
            return;
        }
        Event event = eventList.get(index);
        if(event.getStatus()==EventStatus.TAKEN){
            Toast.makeText(this, "Event is already taken!", Toast.LENGTH_SHORT).show();
            return;
        }
        EventService eventService = new EventService(new EventRepository());
        eventService.take(event);
        initializeRecyclerView();
    }
    private void saveWorkingHours(){
        String validFromString = datePickerEditText.getText().toString();
        String validToString = datePickerEditText2.getText().toString();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date validFrom = null, validTo = null;
        try {
            validFrom = format.parse(validFromString);
            validTo = format.parse(validToString);
        } catch (ParseException e) {
            Toast.makeText(this, "Nothing to change!", Toast.LENGTH_SHORT).show();
            return;
        }
        EventService eventService = new EventService(new EventRepository());
        Date finalValidFrom = validFrom;
        Date finalValidTo = validTo;
        eventService.getAllByEmployee(employee.getId(), new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                for(Event e: events){
                    if(e.getDate().after(finalValidFrom) && e.getDate().before(finalValidTo)){
                        Toast.makeText(EmployeeDetailsActivity.this, "There are events in this period scheduled for this employee!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                EmployeesWorkingHours newWorkingHours = new EmployeesWorkingHours("", finalValidFrom, finalValidTo, newSchedule, employee.getId());
                EmployeesWorkingHoursService workingHoursService = new EmployeesWorkingHoursService();
                workingHoursService.create(newWorkingHours);
                Toast.makeText(EmployeeDetailsActivity.this, "You successfully rescheduled working hours of the employee!", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(String errorMessage) {
                Toast.makeText(EmployeeDetailsActivity.this, "Error loading employees events! Try again later.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void configureDatePickers(){
        datePickerEditText = findViewById(R.id.datePickerEditText);
        openDatePickerButton = findViewById(R.id.openDatePickerButton);
        datePickerEditText2 = findViewById(R.id.datePickerEditText2);
        openDatePickerButton2 = findViewById(R.id.openDatePickerButton2);
        weekPickerEditText = findViewById(R.id.weekPickerEditText);
        openWeekPickerButton = findViewById(R.id.openWeekPickerButton);

        openDatePickerButton.setOnClickListener(v -> showDatePicker(datePickerEditText, false));
        openDatePickerButton2.setOnClickListener(v -> showDatePicker(datePickerEditText2, false));
        openWeekPickerButton.setOnClickListener(v -> showDatePicker(weekPickerEditText, true));
    }
    private void bindEmployee(){
        ImageView imageView = findViewById(R.id.imageViewDetails);
        TextView textViewName = findViewById(R.id.textViewNameDetails);
        TextView textViewEmail = findViewById(R.id.textViewEmailDetails);
        TextView textViewPhone = findViewById(R.id.textViewPhoneDetails);
        TextView textViewAddress = findViewById(R.id.textViewAddressDetails);
        Intent intent = getIntent();
        String employeeId = intent.getStringExtra("employeeId");
        viewForOwner = intent.getBooleanExtra("viewForOwner", false);
        service.get(employeeId).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                employee = task.getResult();
                Glide.with(this)
                        .load(employee.getPicture())
                        .placeholder(R.drawable.default_employee_image)
                        .error(R.drawable.default_employee_image)
                        .into(imageView);

                textViewName.setText(employee.getFirstName() + " " + employee.getLastName());
                textViewEmail.setText("Email: " + employee.getEmail());
                textViewPhone.setText("Phone: " + employee.getPhone());
                textViewAddress.setText("Address: " + employee.getAddress());
                initializeRecyclerWorkingHours();
                monitorWorkingHoursChanges();
                setUtilities();
                deactivateButton = findViewById(R.id.deactivateButton);
                deactivateButton.setText(employee.isActivated()?"Deactivate":"Activate");
                deactivateButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deactivate();
                    }
                });
                addEventButton = findViewById(R.id.addEventButton);
                addEventButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(v.getContext(), AddEventToScheduleActivity.class);
                        String dates = weekPickerEditText.getText().toString();
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        String start, end;
                        start = dates.split(" ")[0];
                        end = dates.split(" ")[2];
                        intent.putExtra("start", start);
                        intent.putExtra("end", end);
                        intent.putExtra("employeeId", employee.getId());
                        intent.putExtra("viewForOwner", viewForOwner);
                        v.getContext().startActivity(intent);
                    }
                });
            } else {
                Exception exception = task.getException();
            }
        });
    }
    private void deactivate(){
        boolean deactivating = employee.isActivated();
        employee.setActivated(!deactivating);
        service.update(employee).addOnCompleteListener(task -> {
            UserService userService = new UserService();
            userService.getByEmail(employee.getEmail()).addOnCompleteListener(task1 -> {
                if(task1.isSuccessful()) {
                    User user = task1.getResult();
                    user.setActivated(!deactivating);
                    userService.update(user).addOnCompleteListener(task2 -> {
                        if(deactivating){
                            deactivateButton.setText("Activate");
                            return;
                        }
                        FirebaseAuth.getInstance().createUserWithEmailAndPassword(employee.getEmail(), employee.getPassword())
                                .addOnCompleteListener(task3 -> {
                                    if (task3.isSuccessful()) {
                                        sendPasswordResetEmail();
                                        deactivateButton.setText("Deactivate");
                                    }
                                });
                    });
                }
            });
        });
    }

    private void monitorWorkingHoursChanges(){
        List<View> timeRangePickers = new ArrayList<>(Arrays.asList(
                findViewById(R.id.timeRangePicker11),
                findViewById(R.id.timeRangePicker12),
                findViewById(R.id.timeRangePicker13),
                findViewById(R.id.timeRangePicker14),
                findViewById(R.id.timeRangePicker15)
        ));
        for(View timeRangePicker : timeRangePickers){
            TimePicker startTimePicker = timeRangePicker.findViewById(R.id.startTimePicker);
            TimePicker endTimePicker = timeRangePicker.findViewById(R.id.endTimePicker);
            startTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> onTimePickerChanged());
            endTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> onTimePickerChanged());
        }
    }
    private void setUtilities(){
        Spinner spinner = findViewById(R.id.spinner);

        List<String> options = new ArrayList<>();
        options.add("Utilities");
        options.add("\uD83C\uDF89  Birthdays");
        options.add("\uD83D\uDC70\uD83C\uDFFC Weddings");
        options.add("\uD83D\uDC68\uD83C\uDFFC\u200D\uD83C\uDF93 Graduation");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                options
        ){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                if (position == 0) {
                    TextView textView = (TextView) view;
                    textView.setTypeface(null, Typeface.ITALIC);
                }

                return view;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                // Make the first item bold in the dropdown list
                if (position == 0) {
                    TextView textView = (TextView) view;
                    textView.setTypeface(null, Typeface.BOLD);
                }

                return view;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(0, false);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinner.setSelection(0);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing
            }
        });
    }

    private void onTimePickerChanged() {
        RelativeLayout relativeLayout = findViewById(R.id.relativeLayout);
        relativeLayout.setVisibility(View.VISIBLE);
        setTimeSchedule();
    }
    private void setTimeSchedule(){
        List<TimePicker> timeRangePickers = new ArrayList<>(Arrays.asList(
                findViewById(R.id.timeRangePicker11).findViewById(R.id.startTimePicker),
                findViewById(R.id.timeRangePicker11).findViewById(R.id.endTimePicker),
                findViewById(R.id.timeRangePicker12).findViewById(R.id.startTimePicker),
                findViewById(R.id.timeRangePicker12).findViewById(R.id.endTimePicker),
                findViewById(R.id.timeRangePicker13).findViewById(R.id.startTimePicker),
                findViewById(R.id.timeRangePicker13).findViewById(R.id.endTimePicker),
                findViewById(R.id.timeRangePicker14).findViewById(R.id.startTimePicker),
                findViewById(R.id.timeRangePicker14).findViewById(R.id.endTimePicker),
                findViewById(R.id.timeRangePicker15).findViewById(R.id.startTimePicker),
                findViewById(R.id.timeRangePicker15).findViewById(R.id.endTimePicker)
        ));
        newSchedule.mondayStart = timeRangePickers.get(0).getHour() + " : " + timeRangePickers.get(0).getMinute();
        newSchedule.mondayFinish = timeRangePickers.get(1).getHour() + " : " + timeRangePickers.get(1).getMinute();
        newSchedule.tuesdayStart = timeRangePickers.get(2).getHour() + " : " + timeRangePickers.get(2).getMinute();
        newSchedule.tuesdayFinish = timeRangePickers.get(3).getHour() + " : " + timeRangePickers.get(3).getMinute();
        newSchedule.wednesdayStart = timeRangePickers.get(4).getHour() + " : " + timeRangePickers.get(4).getMinute();
        newSchedule.wednesdayFinish = timeRangePickers.get(5).getHour() + " : " + timeRangePickers.get(5).getMinute();
        newSchedule.thursdayStart = timeRangePickers.get(6).getHour() + " : " + timeRangePickers.get(6).getMinute();
        newSchedule.thursdayFinish = timeRangePickers.get(7).getHour() + " : " + timeRangePickers.get(7).getMinute();
        newSchedule.fridayStart = timeRangePickers.get(8).getHour() + " : " + timeRangePickers.get(8).getMinute();
        newSchedule.fridayFinish = timeRangePickers.get(9).getHour() + " : " + timeRangePickers.get(9).getMinute();
    }

    private void setTimePickers(){
        List<TimePicker> timeRangePickers = new ArrayList<>(Arrays.asList(
                findViewById(R.id.timeRangePicker11).findViewById(R.id.startTimePicker),
                findViewById(R.id.timeRangePicker11).findViewById(R.id.endTimePicker),
                findViewById(R.id.timeRangePicker12).findViewById(R.id.startTimePicker),
                findViewById(R.id.timeRangePicker12).findViewById(R.id.endTimePicker),
                findViewById(R.id.timeRangePicker13).findViewById(R.id.startTimePicker),
                findViewById(R.id.timeRangePicker13).findViewById(R.id.endTimePicker),
                findViewById(R.id.timeRangePicker14).findViewById(R.id.startTimePicker),
                findViewById(R.id.timeRangePicker14).findViewById(R.id.endTimePicker),
                findViewById(R.id.timeRangePicker15).findViewById(R.id.startTimePicker),
                findViewById(R.id.timeRangePicker15).findViewById(R.id.endTimePicker)
        ));
        timeRangePickers.get(0).setIs24HourView(true);
        timeRangePickers.get(1).setIs24HourView(true);
        timeRangePickers.get(2).setIs24HourView(true);
        timeRangePickers.get(3).setIs24HourView(true);
        timeRangePickers.get(4).setIs24HourView(true);
        timeRangePickers.get(5).setIs24HourView(true);
        timeRangePickers.get(6).setIs24HourView(true);
        timeRangePickers.get(7).setIs24HourView(true);
        timeRangePickers.get(8).setIs24HourView(true);
        timeRangePickers.get(9).setIs24HourView(true);
        timeRangePickers.get(0).setHour(Integer.parseInt(employee.getTimeSchedule().mondayStart.split(" ")[0]));
        timeRangePickers.get(0).setMinute(Integer.parseInt(employee.getTimeSchedule().mondayStart.split(" ")[2]));
        timeRangePickers.get(1).setHour(Integer.parseInt(employee.getTimeSchedule().mondayFinish.split(" ")[0]));
        timeRangePickers.get(1).setMinute(Integer.parseInt(employee.getTimeSchedule().mondayFinish.split(" ")[2]));
        timeRangePickers.get(2).setHour(Integer.parseInt(employee.getTimeSchedule().tuesdayStart.split(" ")[0]));
        timeRangePickers.get(2).setMinute(Integer.parseInt(employee.getTimeSchedule().tuesdayStart.split(" ")[2]));
        timeRangePickers.get(3).setHour(Integer.parseInt(employee.getTimeSchedule().tuesdayFinish.split(" ")[0]));
        timeRangePickers.get(3).setMinute(Integer.parseInt(employee.getTimeSchedule().tuesdayFinish.split(" ")[2]));
        timeRangePickers.get(4).setHour(Integer.parseInt(employee.getTimeSchedule().wednesdayStart.split(" ")[0]));
        timeRangePickers.get(4).setMinute(Integer.parseInt(employee.getTimeSchedule().wednesdayStart.split(" ")[2]));
        timeRangePickers.get(5).setHour(Integer.parseInt(employee.getTimeSchedule().wednesdayFinish.split(" ")[0]));
        timeRangePickers.get(5).setMinute(Integer.parseInt(employee.getTimeSchedule().wednesdayFinish.split(" ")[2]));
        timeRangePickers.get(6).setHour(Integer.parseInt(employee.getTimeSchedule().thursdayStart.split(" ")[0]));
        timeRangePickers.get(6).setMinute(Integer.parseInt(employee.getTimeSchedule().thursdayStart.split(" ")[2]));
        timeRangePickers.get(7).setHour(Integer.parseInt(employee.getTimeSchedule().thursdayFinish.split(" ")[0]));
        timeRangePickers.get(7).setMinute(Integer.parseInt(employee.getTimeSchedule().thursdayFinish.split(" ")[2]));
        timeRangePickers.get(8).setHour(Integer.parseInt(employee.getTimeSchedule().fridayStart.split(" ")[0]));
        timeRangePickers.get(8).setMinute(Integer.parseInt(employee.getTimeSchedule().fridayStart.split(" ")[2]));
        timeRangePickers.get(9).setHour(Integer.parseInt(employee.getTimeSchedule().fridayFinish.split(" ")[0]));
        timeRangePickers.get(9).setMinute(Integer.parseInt(employee.getTimeSchedule().fridayFinish.split(" ")[2]));
    }
    private void initializeRecyclerWorkingHours(){
        RecyclerView rec = findViewById(R.id.workingHoursRecyclerView);
        LinearLayout lay = findViewById(R.id.timePickersLayout);
        if(viewForOwner){
            rec.setVisibility(View.GONE);
            setTimePickers();
            return;
        }
        lay.setVisibility(View.GONE);
        EmployeesWorkingHoursService workingHoursService = new EmployeesWorkingHoursService();
        if(employee==null) return;
        EmployeesWorkingHours regularWorkingHours = new EmployeesWorkingHours();
        regularWorkingHours.setTimeSchedule(employee.getTimeSchedule());
        regularWorkingHours.setValidFrom(null);
        regularWorkingHours.setValidTo(null);
        workingHoursList = new ArrayList<>();
        workingHoursList.add(regularWorkingHours);
        workingHoursService.getAllByEmployee(employee.getId()).addOnCompleteListener(task->{
            if(task.isSuccessful()){
                if(task.getResult()!=null){
                    for(EmployeesWorkingHours e: task.getResult())
                        workingHoursList.add(e);
                    workingHoursAdapter = new WorkingHoursAdapter(workingHoursList);
                    rec.setAdapter(workingHoursAdapter);
                }
            }
        });
    }
    private void initializeRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.eventsRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this); // Use LinearLayoutManager for vertical list
        recyclerView.setLayoutManager(layoutManager);
        EventService eventService = new EventService(new EventRepository());
        String dates = weekPickerEditText.getText().toString();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date start = new Date(), end = new Date();
        try{
            start = format.parse(dates.split(" ")[0]);
        }
        catch(Exception e){}
        try{
            end = format.parse(dates.split(" ")[2]);
        }
        catch(Exception e){}
        eventService.getAllByEmployeeInWeek(employee.getId(), start, end, new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                eventList = events;
                eventAdapter = new EmployeeEventAdapter(events);
                recyclerView.setAdapter(eventAdapter);
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });
    }
    private void sendPasswordResetEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            FirebaseAuth.getInstance().sendPasswordResetEmail(user.getEmail())
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Toast.makeText(this, "Password reset email sent. Please check your email to reset your password and verify your account.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, "Failed to send password reset email", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }
}
