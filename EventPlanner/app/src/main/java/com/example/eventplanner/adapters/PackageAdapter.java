package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.PackageService;

import java.util.Calendar;
import java.util.List;

public class PackageAdapter extends  RecyclerView.Adapter<PackageAdapter.PackageViewHolder> {
    private Context context;
    private List<Package> packageList;
    Package package1;

    LinearLayout layoutPackage;

    PackageService packageService;
    UserRole userRole;

    public PackageAdapter(Context context,List<Package> packageList, UserRole userRole) {
        this.context = context;
        this.packageList = packageList;
        packageService = new PackageService(new PackageRepository());
        this.userRole = userRole;

    }

    @NonNull
    @Override
    public PackageAdapter.PackageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_package, parent, false);
        return new PackageAdapter.PackageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PackageAdapter.PackageViewHolder holder, int position) {
        Package product = packageList.get(position);
        holder.bind(product);
    }

    @Override
    public int getItemCount() {
        return packageList.size();
    }

    public class PackageViewHolder extends RecyclerView.ViewHolder {

        ImageButton btPrevious, btNext, editPackageBtn, deletePackageBtn;
        ImageSwitcher packageImageSwitcher;
        TextView packageName, packageCategory, packageSubcategory, packageDescription,
                packagePrice, packageDiscount, packageDiscountPrice, packageAvailability, packageVisibility,  packageEventType,
                packageReserveIn, packageCancelIn, packageConfirm, packageProducts, packageServices;
        private Package currentPackage;
        private int currentIndex;
        public PackageViewHolder(@NonNull View itemView) {
            super(itemView);

            btPrevious = itemView.findViewById(R.id.bt_previous);
            btNext = itemView.findViewById(R.id.bt_next);
            editPackageBtn = itemView.findViewById(R.id.edit_package_btn);
            deletePackageBtn = itemView.findViewById(R.id.delete_package_btn);
            packageImageSwitcher = itemView.findViewById(R.id.package_image_switcher);
            packageImageSwitcher.setFactory(() -> {
                ImageView imageView = new ImageView(itemView.getContext());
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                return imageView;
            });
            packageName = itemView.findViewById(R.id.package_name);
            packageCategory = itemView.findViewById(R.id.package_category);
            packageSubcategory = itemView.findViewById(R.id.package_subcategory);
            packageDescription = itemView.findViewById(R.id.package_description);
            packagePrice = itemView.findViewById(R.id.package_price);
            packageDiscount = itemView.findViewById(R.id.package_discount);
            packageDiscountPrice = itemView.findViewById(R.id.package_discount_price);
            packageAvailability = itemView.findViewById(R.id.package_available);
            packageVisibility = itemView.findViewById(R.id.package_visibility);
            packageEventType = itemView.findViewById(R.id.package_event_type);
            packageReserveIn = itemView.findViewById(R.id.package_res_deadline);
            packageCancelIn = itemView.findViewById(R.id.package_cancel_in);
            packageConfirm = itemView.findViewById(R.id.package_confirmation);
            packageProducts = itemView.findViewById(R.id.package_products);
            packageServices = itemView.findViewById(R.id.package_services);
            LinearLayout service_layout = itemView.findViewById(R.id.layout_package_solution_helper);

            if(currentPackage != null){
                if(currentPackage.getServices().isEmpty()){
                    service_layout.setVisibility(View.GONE);
                }else{
                    service_layout.setVisibility(View.VISIBLE);
                }
            }


            btNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //int currentIndex = packageImageSwitcher.getDisplayedChild();
                    if (currentIndex < currentPackage.getImages().size() - 1) {
                        currentIndex++;
                        packageImageSwitcher.setImageResource(currentPackage.getImages().get(currentIndex));
                    }
                }
            });

            btPrevious.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //int currentIndex = packageImageSwitcher.getDisplayedChild();
                    if (currentIndex > 0) {
                        currentIndex--;
                        packageImageSwitcher.setImageResource(currentPackage.getImages().get(currentIndex));
                    }
                }
            });

            editPackageBtn.setOnClickListener(v -> {
                if(userRole == UserRole.OWNER){
                    EditPackagePopup();
                }else{
                    Toast.makeText(context, "You have no permission for this action", Toast.LENGTH_SHORT).show();
                }

            });
            deletePackageBtn.setOnClickListener(v -> {
                if(userRole == UserRole.OWNER){
                    showDeleteConfirmationDialog(currentPackage);
                }else{
                    Toast.makeText(context, "You have no permission for this action", Toast.LENGTH_SHORT).show();
                }

            });
        }

        public void bind(Package aPackage) {  // Changed to Service
            currentPackage = aPackage;
            currentIndex = 0;
            package1 = aPackage;
            boolean ind = aPackage != null;
            Log.i("binder", ind ? "nije null" : "null je");
            packageName.setText(aPackage.getName());
            packageCategory.setText(aPackage.getCategory());
            packageSubcategory.setText(aPackage.getSubcategoriesString());
            packageDescription.setText(aPackage.getDescription());
            packagePrice.setText(String.valueOf(aPackage.getPrice()));
            packageDiscount.setText(String.valueOf(aPackage.getDiscount()));
            packageDiscountPrice.setText(String.valueOf(aPackage.getPrice()*(100 -aPackage.getDiscount())/100));

            packageEventType.setText(aPackage.getEventTypes());
            packageAvailability.setText(aPackage.isAvailable() ? "Yes" : "No");
            packageVisibility.setText(aPackage.isVisible() ? "Yes" : "No");
            packageProducts.setText(aPackage.getProductsString());
            packageServices.setText(aPackage.getServicesString());

            //for time stamp
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(currentPackage.getReservationDeadline().getSeconds()*1000);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append((cal.get(Calendar.MONTH ) + 1)%12).append("m ");
            stringBuilder.append(cal.get(Calendar.DAY_OF_MONTH)).append("d ");
            packageReserveIn.setText(stringBuilder.toString());

            cal.setTimeInMillis(currentPackage.getCancellationDeadline().getSeconds()*1000);
            stringBuilder.setLength(0);
            stringBuilder.append((cal.get(Calendar.MONTH ) + 1)%12).append("m ");  // da moze 0 -- max 11 mjeseci cekanja sta znam
            stringBuilder.append(cal.get(Calendar.DAY_OF_MONTH )).append("d ");
            packageCancelIn.setText(stringBuilder.toString());



            if(currentPackage.getConfirmationType() == Service.ConfirmationType.AUTOMATIC){
                packageConfirm.setText("Automatic");
            }else{
                packageConfirm.setText("By hand");
            }




            //serviceDuration, serviceReserveIn, serviceCancelIn, serviceConfirm
            if (aPackage.getImages() != null && !aPackage.getImages().isEmpty()) {
                packageImageSwitcher.setImageResource(aPackage.getImages().get(0));
            } else {
                packageImageSwitcher.setImageResource(R.drawable.default_image);
            }
        }
    }

    private void showDeleteConfirmationDialog(Package package1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Delete Product");
        builder.setMessage("Are you sure you want to delete this product?");

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //delete operation here
                package1.setDeleted(true);
                packageService.deleteLogically(package1);
                packageList.remove(package1);
                notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @SuppressLint({"NotifyDataSetChanged"})
    private void EditPackagePopup() {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.package_edit_form, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.buttonSave_edit_package);
        Button cancelButton = popupView.findViewById(R.id.buttonCancel_edit_package);
        Button productButton = popupView.findViewById(R.id.package_buttonProducts_edit);
        Button serviceButton = popupView.findViewById(R.id.package_buttonServices_edit);

        layoutPackage = popupView.findViewById(R.id.package_help_layout);
        //inicijalna vidljivost paketa
        if(package1.getServices().isEmpty()){
            layoutPackage.setVisibility(View.GONE);
        }else{
            layoutPackage.setVisibility(View.VISIBLE);
        }




        saveButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });

        productButton.setOnClickListener(v->{
            AddProductsPopup();
        });
        serviceButton.setOnClickListener(v->{
            AddServicesPopup(package1);

        });

        cancelButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });
    }

    @SuppressLint({"NotifyDataSetChanged"})
    private void AddProductsPopup() {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.package_product_popup, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.package_product_add);

        saveButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });


    }

    @SuppressLint({"NotifyDataSetChanged"})
    private void AddServicesPopup(Package package1) {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.package_service_popup, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.package_service_add);
        CheckBox checkboxService1 = popupView.findViewById(R.id.checkbox_service1);
        CheckBox checkboxService2 = popupView.findViewById(R.id.checkbox_service2);
        CheckBox checkboxService3 = popupView.findViewById(R.id.checkbox_service3);

        saveButton.setOnClickListener(v -> {
            //package1.getServices().clear();

            if (checkboxService1.isChecked()) {
                //package1.getServices().add("Service 1");
            }
            if (checkboxService2.isChecked()) {
                //package1.getServices().add("Service 2");
            }
            if (checkboxService3.isChecked()) {
                //package1.getServices().add("Service 3");
            }

            popupWindow.dismiss();

        });




    }


}
