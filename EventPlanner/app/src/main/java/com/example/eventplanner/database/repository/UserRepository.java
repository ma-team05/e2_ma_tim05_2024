package com.example.eventplanner.database.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.Grade;
import com.example.eventplanner.models.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.example.eventplanner.models.UserRole;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserRepository {
    private final CollectionReference usersCollection;

    public UserRepository() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        usersCollection = db.collection("users");
    }
    public Task<Void> update(User user){
        DocumentReference userRef = usersCollection.document(user.getId());
        return userRef.set(user);
    }

    public Task<String> save(User user) {
        DocumentReference newUserRef = usersCollection.document();
        user.setId(newUserRef.getId());
        return newUserRef.set(user)
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        return newUserRef.getId();
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }

    public Task<String> saveFavouriteServices(User user) {
        usersCollection
                .add(user)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });

        DocumentReference newUserRef = usersCollection.document();
        return newUserRef.set(user)
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        return newUserRef.getId();
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }

    public Task<User> getByEmail(String email){
        return usersCollection.get()
                .continueWith(task -> {
                    for (DocumentSnapshot document : task.getResult()) {
                        User user = document.toObject(User.class);
                        assert user != null;
                        if(user.getEmail().equals(email))
                            return user;
                    }
                    return null;
                });
    }
    public Task<User> get(String userId){
        DocumentReference gradeRef = usersCollection.document(userId);
        return gradeRef.get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            return document.toObject(User.class);
                        } else {
                            throw new Exception("User with ID " + userId + " not found");
                        }
                    } else {
                        throw task.getException(); // Propagate the exception
                    }
                });
    }

    public Task<List<User>> getNonActivatedOwners() {
        return usersCollection
                .whereEqualTo("activated", false)
                .whereEqualTo("role", UserRole.OWNER)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        List<User> nonActivatedUsers = new ArrayList<>();
                        for (DocumentSnapshot document : task.getResult()) {
                            User user = document.toObject(User.class);
                            nonActivatedUsers.add(user);
                        }
                        return nonActivatedUsers;
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }
}
