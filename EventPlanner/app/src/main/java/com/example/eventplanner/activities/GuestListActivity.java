package com.example.eventplanner.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.BudgetItem;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.Guest;
import com.example.eventplanner.services.EventService;
import com.example.eventplanner.services.PDFReportService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class GuestListActivity extends AppCompatActivity {

    private EventService eventService;
    private PDFReportService reportService;
    private Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_guest_list);

        eventService = new EventService(new EventRepository());
        reportService = new PDFReportService();

        LinearLayout parentLayout = findViewById(R.id.main);

        int marginBetweenItems = getResources().getDimensionPixelSize(R.dimen.margin_between_items);

        String eventId = (String) getIntent().getSerializableExtra("EVENT");
        eventService.getById(eventId, new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                GuestListActivity.this.event = events.get(0);

                for (int i = 0; i < GuestListActivity.this.event.getGuests().size(); i++) {
                    final String guestId = GuestListActivity.this.event.getGuests().get(i).getFullName();

                    LinearLayout itemLayout = new LinearLayout(GuestListActivity.this);
                    itemLayout.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    ));
                    itemLayout.setOrientation(LinearLayout.VERTICAL);

                    TextView itemNameTextView = new TextView(GuestListActivity.this);
                    itemNameTextView.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    ));
                    itemNameTextView.setText(GuestListActivity.this.event.getGuests().get(i).getFullName() + " (" + GuestListActivity.this.event.getGuests().get(i).getAgeAsString()+ ")");
                    itemLayout.addView(itemNameTextView);

                    TextView invitedTextView = new TextView(GuestListActivity.this);
                    invitedTextView.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    ));
                    invitedTextView.setText("Invited: " + (GuestListActivity.this.event.getGuests().get(i).isInvited() ? ("Yes" + " (" +  (GuestListActivity.this.event.getGuests().get(i).isAcceptedInvitation() ? "accepted)" : "not accepted)")) : "No"));
                    itemLayout.addView(invitedTextView);

                    String specialRequests = GuestListActivity.this.event.getGuests().get(i).getSpecialRequests();
                    if (specialRequests.equals("Vegetarian + Vegan")) {
                        specialRequests = "Vegan";
                    }
                    if(!specialRequests.isEmpty()) {
                        TextView specialRequestsTextView = new TextView(GuestListActivity.this);
                        specialRequestsTextView.setLayoutParams(new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                        ));
                        specialRequestsTextView.setText("Special requests: " + specialRequests);
                        itemLayout.addView(specialRequestsTextView);
                    }

                    Button editButton = new Button(GuestListActivity.this);
                    LinearLayout.LayoutParams editParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    );
                    editParams.setMargins(0, marginBetweenItems / 4, 0, 0);
                    editParams.gravity = Gravity.CENTER_HORIZONTAL;
                    editButton.setLayoutParams(editParams);
                    editButton.setText("Edit");
                    editButton.setTextColor(Color.WHITE);
                    editButton.setBackgroundColor(ContextCompat.getColor(GuestListActivity.this, R.color.purple_btn));
                    editButton.setOnClickListener(v -> {
                        Intent editIntent = new Intent(GuestListActivity.this, EditGuestActivity.class);
                        editIntent.putExtra("EVENT", event.getId());
                        editIntent.putExtra("GUEST", guestId);
                        GuestListActivity.this.startActivity(editIntent);
                    });
                    itemLayout.addView(editButton);

                    Button deleteButton = new Button(GuestListActivity.this);
                    LinearLayout.LayoutParams deleteParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    );
                    deleteParams.setMargins(0, marginBetweenItems / 4, 0, 0);
                    deleteParams.gravity = Gravity.CENTER_HORIZONTAL;
                    deleteButton.setLayoutParams(deleteParams);
                    deleteButton.setText("Delete");
                    deleteButton.setTextColor(Color.WHITE);
                    deleteButton.setBackgroundColor(ContextCompat.getColor(GuestListActivity.this, R.color.purple_btn));
                    deleteButton.setOnClickListener(v -> {
                        removeGuest(guestId);
                    });
                    itemLayout.addView(deleteButton);

                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) itemLayout.getLayoutParams();
                    params.setMargins(0, 0, 0, marginBetweenItems);
                    itemLayout.setLayoutParams(params);

                    parentLayout.addView(itemLayout);
                }
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });

        FloatingActionButton addButton = findViewById(R.id.fab);
        addButton.setOnClickListener(v -> {
            Intent creationIntent = new Intent(GuestListActivity.this, GuestCreationActivity.class);
            creationIntent.putExtra("EVENT", event.getId());
            GuestListActivity.this.startActivity(creationIntent);
        });

        FloatingActionButton fileButton = findViewById(R.id.file);
        fileButton.setOnClickListener(v -> {
            showDownloadDialog();
        });
    }

    private void removeGuest(String guestId) {
        Guest guest = event.getGuests()
                .stream()
                .filter(g -> g.getFullName().equals(guestId))
                .findFirst()
                .orElse(null);

        showDeleteConfirmationDialog(guest);
    }

    private void showDeleteConfirmationDialog(Guest guest) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to remove this guest?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        event.removeGuest(guest);
                        eventService.update(event);
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    private void showDownloadDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to download the guest list (PDF)?")
                .setTitle("Download guest list")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        reportService.generateGuestListReport(GuestListActivity.this, event);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}