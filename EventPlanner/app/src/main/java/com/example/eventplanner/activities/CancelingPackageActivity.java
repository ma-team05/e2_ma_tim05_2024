package com.example.eventplanner.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;

public class CancelingPackageActivity extends AppCompatActivity {
    private String packageId;
    private String services;
    private TextView servicestextView;
    private Button buttonOK;
    private Button buttonCancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_canceling_package);
        Intent intent = getIntent();
        packageId = intent.getStringExtra("packageId");
        services = intent.getStringExtra("services");
        servicestextView = findViewById(R.id.services);
        servicestextView.setText(services);
        buttonOK =findViewById(R.id.buttonOK);
        buttonOK.setOnClickListener(v -> {
            Intent resultIntent = new Intent();
            resultIntent.putExtra("packageId", packageId);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        });

        buttonCancel =findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(v->{
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, resultIntent);
            finish();
        });
    }
}