package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.OwnerRepository;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Owner;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;

import java.util.List;

public class OwnerService {
    private final OwnerRepository ownerRepository;

    public OwnerService() {
        ownerRepository = new OwnerRepository();
    }

    public Task<Void> create(Owner owner, String userId, String companyId) {
        return ownerRepository.save(owner, userId, companyId);
    }
    public Task<Owner> get(String id){ return ownerRepository.getById(id); }
    public Task<Void> update(Owner owner){
        return ownerRepository.update(owner);
    }

    public Task<List<Owner>> getAll() {
        return ownerRepository.getAll();
    }

    public Task<Owner> getByComopanyId(String id){ return ownerRepository.getByCompanyId(id); }
}
