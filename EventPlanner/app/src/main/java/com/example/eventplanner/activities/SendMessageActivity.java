package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Chat;
import com.example.eventplanner.models.ChatMessage;
import com.example.eventplanner.models.Notification;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.ChatService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.UserService;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class SendMessageActivity extends AppCompatActivity {

    private String email;
    private String userId;
    private String recepientEmail;
    private Chat chat;

    private ChatService chatService = new ChatService();
    private NotificationService notificationService = new NotificationService();
    private UserService userService = new UserService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_send_message);

        Intent intent = getIntent();
        email = intent.getStringExtra("EMAIL");
        recepientEmail = intent.getStringExtra("RECEPIENT_EMAIL");
        userId = intent.getStringExtra("userId");
        String chatId = intent.getStringExtra("CHAT");

        chatService.getAll(new ChatService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<com.example.eventplanner.models.Chat> chats) {
                chat = chats.stream().filter(c -> c.getId().equals(chatId)).findFirst().orElse(null);
                bind();
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });

        Button createButton = findViewById(R.id.createButton);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields();
            }
        });
    }

    private void bind() {
        TextView recepient;

        recepient = findViewById(R.id.recepient);

        recepient.setText("To: " + recepientEmail);
    }

    private boolean validateFields() {
        boolean isValid = true;

        EditText messageEditText;
        String message;

        messageEditText = findViewById(R.id.message);
        message = messageEditText.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            messageEditText.setError("Please enter a message");
            messageEditText.requestFocus();
            isValid = false;
        }

        if(isValid) {
            startChat();
        }

        return isValid;
    }

    private void startChat() {
        EditText messageEditText;
        String message;

        messageEditText = findViewById(R.id.message);
        message = messageEditText.getText().toString().trim();

        ChatMessage chatMessage = new ChatMessage();

        chatMessage.setFrom(email);
        chatMessage.setTo(recepientEmail);
        chatMessage.setMessage(message);
        chatMessage.setDate(new Date());
        chatMessage.setReadStatus(false);

        chat.addMessage(chatMessage);
        if(chat.getParticipant1().equals(chatMessage.getFrom())) {
            chat.setReadAllMessages2(false);
        }
        else {
            chat.setReadAllMessages1(false);
        }
        chatService.update(chat, true);

        Notification notification = new Notification();
        notification.setUserId(recepientEmail);
        notification.setDescription("You received a message from: " + email);
        notification.setRead(false);
        notification.setSent(true);
        notification.setType(NotificationType.CHAT);

        userService.getByEmail(recepientEmail).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                User recepient = task.getResult();
                notificationService.add(recepient.getId(), "You received a message from: " + email, NotificationType.CHAT);
            }

            String eventDetails = "Message sent:\n";
            Toast.makeText(this, eventDetails, Toast.LENGTH_LONG).show();

            Intent intent = new Intent(this, ChatActivity.class);
            intent.putExtra("email", email);
            intent.putExtra("CHAT", chat.getId());
            intent.putExtra("userId", userId);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

            finish();
        });
    }
}