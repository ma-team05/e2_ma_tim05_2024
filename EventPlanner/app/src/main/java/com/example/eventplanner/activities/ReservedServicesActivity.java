package com.example.eventplanner.activities;

import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.BudgetItem;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.Reservation;
import com.example.eventplanner.services.EventService;
import com.example.eventplanner.services.ReservationService;

import java.util.List;
import java.util.stream.Collectors;

public class ReservedServicesActivity extends AppCompatActivity {

    private Event event;
    private List<Reservation> reservations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservated_services);

        EventService eventService = new EventService(new EventRepository());
        ReservationService reservationService = new ReservationService();

        reservationService.getAll().addOnCompleteListener(task->{
            if(task.isSuccessful()){
                reservations = task.getResult();

                LinearLayout parentLayout = findViewById(R.id.main);

                int marginBetweenItems = getResources().getDimensionPixelSize(R.dimen.margin_between_items);

                String eventId = (String) getIntent().getSerializableExtra("EVENT");
                String budgetItemId = (String) getIntent().getSerializableExtra("BUDGET_ITEM");
                eventService.getById(eventId, new EventService.OnDataFetchListener() {
                    @Override
                    public void onSuccess(List<Event> events) {
                        ReservedServicesActivity.this.event = events.get(0);

                        BudgetItem budgetItem = ReservedServicesActivity.this.event.getBudgetItems()
                                .stream()
                                .filter(item -> item.getName().equals(budgetItemId))
                                .findFirst()
                                .orElse(null);

                        LinearLayout itemLayout = new LinearLayout(ReservedServicesActivity.this);
                        itemLayout.setLayoutParams(new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                        ));
                        itemLayout.setOrientation(LinearLayout.VERTICAL);

                        for (int i = 0; i < budgetItem.getProducts().size(); i++) {
                            TextView itemNameTextView = new TextView(ReservedServicesActivity.this);
                            itemNameTextView.setLayoutParams(new LinearLayout.LayoutParams(
                                    ViewGroup.LayoutParams.WRAP_CONTENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT
                            ));
                            itemNameTextView.setText(budgetItem.getProducts().get(i).getName() + " (Product)");
                            itemLayout.addView(itemNameTextView);

                            TextView itemAmmountTextView = new TextView(ReservedServicesActivity.this);
                            LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT
                            );
                            viewParams.setMargins(0, 0, 0, marginBetweenItems / 2);
                            viewParams.gravity = Gravity.CENTER_HORIZONTAL;
                            itemAmmountTextView.setLayoutParams(viewParams);
                            itemAmmountTextView.setText("Ammount: " + budgetItem.getProducts().get(i).getPrice());
                            itemLayout.addView(itemAmmountTextView);
                        }
                        budgetItem.getCategory();
                        for (Reservation reservation : reservations) {
                            String eventId = reservation.getEventId();
                            String category = reservation.getService().getCategory();
                            String subcategory = reservation.getService().getSubCategory();
                            String itemCategory = budgetItem.getCategory().getName();
                            boolean eventValid = eventId.equals(event.getId());
                            boolean categoryValid = category.equals(itemCategory);
                            boolean subcategoryValid = subcategory.equals(itemCategory);
                            if((!categoryValid && !subcategoryValid) || !eventValid) {
                                continue;
                            }

                            TextView itemNameTextView = new TextView(ReservedServicesActivity.this);
                            itemNameTextView.setLayoutParams(new LinearLayout.LayoutParams(
                                    ViewGroup.LayoutParams.WRAP_CONTENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT
                            ));
                            itemNameTextView.setText(reservation.getService().getName() + " (Service)");
                            itemLayout.addView(itemNameTextView);

                            TextView itemAmmountTextView = new TextView(ReservedServicesActivity.this);
                            LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT
                            );
                            viewParams.setMargins(0, 0, 0, marginBetweenItems / 2);
                            viewParams.gravity = Gravity.CENTER_HORIZONTAL;
                            itemAmmountTextView.setLayoutParams(viewParams);
                            itemAmmountTextView.setText("Ammount: " + reservation.getService().getPrice());
                            itemLayout.addView(itemAmmountTextView);
                        }

                        parentLayout.addView(itemLayout);
                    }

                    @Override
                    public void onFailure(String errorMessage) {
                    }
                });
            }
        });
    }
}