package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.GradeReportAdapter;
import com.example.eventplanner.models.GradeReport;
import com.example.eventplanner.models.ReportStatus;
import com.example.eventplanner.models.Reservation;
import com.example.eventplanner.models.ReservationStatus;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.GradeReportService;
import com.example.eventplanner.services.OwnerService;

import java.util.ArrayList;
import java.util.List;

public class GradeReportsActivity extends AppCompatActivity {
    private GradeReportService reportService = new GradeReportService();
    private OwnerService ownerService = new OwnerService();
    private List<GradeReport> reports = new ArrayList<>();
    private GradeReportAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_grade_reports);
        setUpRecyclerView();
        fetchData();
    }

    private void setUpRecyclerView() {
        RecyclerView rv = findViewById(R.id.reportsRecyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adapter = new GradeReportAdapter(new ArrayList<>(), this); // Initialize with the empty list
        rv.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            if (data != null) {
                String reason = data.getStringExtra("reason");
                String reportId = data.getStringExtra("reportId");
                GradeReportService reportService = new GradeReportService();
                reportService.getById(reportId).addOnCompleteListener(t->{
                    if(t.isSuccessful()){
                        reportService.RejectGradeReport(t.getResult(), reason).addOnCompleteListener(task->{
                            if(task.isSuccessful()){
                                reports.clear();
                                fetchData();
                            }
                        });
                    }
                });
            }
        }
    }
    private List<GradeReport> copy(List<GradeReport> input){
        List<GradeReport> ret = new ArrayList<>();
        for(GradeReport gr: input){
            ret.add(gr);
        }
        return ret;
    }
    private void fetchData() {
        reportService.getAll().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<GradeReport> fetchedReports = task.getResult();
                for (GradeReport gr : fetchedReports) {
                    ownerService.get(gr.getOwnerId()).addOnCompleteListener(task2 -> {
                        if (task2.isSuccessful()) {
                            gr.owner = task2.getResult();
                            reports.add(gr);
                            updateRecyclerView();
                        }
                    });
                }
            } else {
                Log.e("GradeReportsActivity", "Error fetching reports: ", task.getException());
            }
        });
    }

    private void updateRecyclerView() {
        adapter.updateGradeReports(reports);
        Log.d("GradeReportsActivity", "RecyclerView updated with adapter and " + reports.size() + " grade reports.");
    }
}
