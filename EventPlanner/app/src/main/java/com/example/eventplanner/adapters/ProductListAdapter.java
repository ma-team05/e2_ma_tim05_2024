package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.Product;

import java.util.List;
import com.example.eventplanner.R;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.EventTypeService;
import com.example.eventplanner.services.ProductService;

public class ProductListAdapter extends  RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>{

    private final Context context;
    private final List<Product> productList;
    private final ProductService productService;
    Spinner subcategorySpinner;

    UserRole userRole;

    public ProductListAdapter(Context context,List<Product> productList, ProductService productService,UserRole userRole) {
        this.context = context;
        this.productList = productList;
        this.productService = productService;
        this.userRole = userRole;


        Log.i("Frag","Napravio adapter");
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_card, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = productList.get(position);
        holder.bind(product);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        ImageButton btPrevious, btNext, editProductBtn, deleteProductBtn;
        ImageSwitcher productImageSwitcher;
        TextView productName, productCategory, productSubcategory, productDescription,
                productPrice, productDiscount, productDiscountPrice, productAvailable, productVisibility, porductEventType;
        private Product currentProduct;
        private int currentIndex;
        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);

            btPrevious = itemView.findViewById(R.id.bt_previous);
            btNext = itemView.findViewById(R.id.bt_next);
            editProductBtn = itemView.findViewById(R.id.edit_product_btn);
            deleteProductBtn = itemView.findViewById(R.id.delete_product_btn);
            productImageSwitcher = itemView.findViewById(R.id.product_image_switcher);
            productImageSwitcher.setFactory(() -> {
                ImageView imageView = new ImageView(itemView.getContext());
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                return imageView;
            });
            productName = itemView.findViewById(R.id.product_name);
            productCategory = itemView.findViewById(R.id.product_category);
            productSubcategory = itemView.findViewById(R.id.product_subcategory);
            productDescription = itemView.findViewById(R.id.product_description);
            productPrice = itemView.findViewById(R.id.product_price);
            productDiscount = itemView.findViewById(R.id.product_discount);
            productDiscountPrice = itemView.findViewById(R.id.product_discount_price);
            productAvailable = itemView.findViewById(R.id.product_available);
            productVisibility = itemView.findViewById(R.id.product_visibility);
            porductEventType = itemView.findViewById(R.id.product_event_type);



            btNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //int currentIndex = productImageSwitcher.getDisplayedChild();
                    if (currentIndex < currentProduct.getImages().size() - 1) {
                        currentIndex++;
                        productImageSwitcher.setImageResource(currentProduct.getImages().get(currentIndex));
                    }
                }
            });

            btPrevious.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //int currentIndex = productImageSwitcher.getDisplayedChild();
                    if (currentIndex > 0) {
                        currentIndex--;

                        productImageSwitcher.setImageResource(currentProduct.getImages().get(currentIndex));
                    }
                }
            });

            editProductBtn.setOnClickListener(v -> {
                if(userRole == UserRole.OWNER ){
                    if(currentProduct.isPending()){
                        Toast.makeText(context, "Product creation is pending - new subcategory.", Toast.LENGTH_SHORT).show();
                    }else{
                        editProductPopup(currentProduct);
                    }

                }else{
                    Toast.makeText(context, "Your have no permission for this action", Toast.LENGTH_SHORT).show();
                }

            });
            deleteProductBtn.setOnClickListener(v -> {
                if(userRole == UserRole.OWNER ) {
                    if(currentProduct.isPending()){
                        Toast.makeText(context, "Product creation is pending - new subcategory.", Toast.LENGTH_SHORT).show();
                    }else{
                        showDeleteConfirmationDialog(currentProduct);
                    }

                }else{
                    Toast.makeText(context, "Your have no permission for this action", Toast.LENGTH_SHORT).show();
                }
            });
        }

        public void bind(Product product) {
            currentProduct = product;
            currentIndex = 0;
            boolean ind = product != null;
            Log.i("binder",ind? "nije null": "null je");
            assert product != null;
            productName.setText(product.getName());
            productCategory.setText(product.getCathegory());
            productSubcategory.setText(product.getSubCathegory());
            productDescription.setText(product.getDescription());
            productPrice.setText(String.valueOf(product.getPrice()));
            productDiscount.setText(String.valueOf(product.getDiscount()));
            productDiscountPrice.setText(String.valueOf(product.getPrice() - product.getDiscount()));
            porductEventType.setText(product.getEventType());
            productAvailable.setText(product.isAvailable() ? "Yes" : "No");
            productVisibility.setText(product.isVisible() ? "Yes" : "No");


            if (product.getImages() != null && !product.getImages().isEmpty()) {
                productImageSwitcher.setImageResource(product.getImages().get(0));
            } else {

                productImageSwitcher.setImageResource(R.drawable.default_image);
            }
        }

        @SuppressLint({"NotifyDataSetChanged"})
        private void editProductPopup(Product product) {
            @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.product_edit_form, null);

            // Create a PopupWindow
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            boolean focusable = true;
            PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

            //set data
            TextView category = popupView.findViewById(R.id.product_category_edit);
            category.setText(product.getCathegory());

            subcategorySpinner = popupView.findViewById(R.id.product_subcategory_edit);
            setSubcategorySpinnerData(product);

            Button eventTypesbutton = popupView.findViewById(R.id.product_event_type_edit);
            eventTypesbutton.setOnClickListener(v -> handleEventTypeSelection(product));

            EditText name = popupView.findViewById(R.id.edit_product_name);
            name.setText(product.getName());

            EditText description = popupView.findViewById(R.id.edit_product_description);
            description.setText(product.getDescription());

            EditText price = popupView.findViewById(R.id.edit_product_price);
            price.setText(String.valueOf(product.getPrice()));

            EditText discount = popupView.findViewById(R.id.edit_product_discount);
            discount.setText(String.valueOf(product.getDiscount()));

            RadioGroup available = popupView.findViewById(R.id.available_rg);
            if (product.isAvailable()) {
                available.check(R.id.available_yes);
            } else {
                available.check(R.id.available_no);
            }

            RadioButton availableYes = popupView.findViewById(R.id.available_yes);
            RadioButton availableNo = popupView.findViewById(R.id.available_no);

            RadioGroup visible = popupView.findViewById(R.id.visible_rg);
            if (product.isAvailable()) {
                visible.check(R.id.visible_yes);
            } else {
                visible.check(R.id.visible_no);
            }

            RadioButton visibleYes = popupView.findViewById(R.id.visible_yes);
            RadioButton visibleNo = popupView.findViewById(R.id.visible_no);

            //show window
            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

            Button saveButton = popupView.findViewById(R.id.buttonSave_edit_product);
            Button cancelButton = popupView.findViewById(R.id.buttonCancel_edit_product);

            saveButton.setOnClickListener(v -> {
            //preuzimanje vrijesnosti spinera
                product.setName(name.getText().toString());
                product.setDescription(description.getText().toString());
                product.setPrice(Double.parseDouble(price.getText().toString()));
                product.setDiscount(Integer.parseInt(discount.getText().toString()));

                //radio buttons
                int selectedRadioButtonId = available.getCheckedRadioButtonId();
                if(selectedRadioButtonId != -1){
                    if(selectedRadioButtonId == availableYes.getId()) {
                        product.setAvailable(true);
                    }
                    if(selectedRadioButtonId == availableNo.getId()){
                        product.setAvailable(false);
                    }
                }else{
                    Toast.makeText(context, "Select availability.", Toast.LENGTH_SHORT).show();
                }

                int selectedRadioButtonVisibleId = visible.getCheckedRadioButtonId();
                if(selectedRadioButtonVisibleId != -1){
                    if(selectedRadioButtonVisibleId == visibleYes.getId()) {
                        product.setVisible(true);
                    }
                    if(selectedRadioButtonVisibleId == visibleNo.getId()){
                        product.setVisible(false);
                    }
                }else{
                    Toast.makeText(context, "Select visibility.", Toast.LENGTH_SHORT).show();
                }

                Category selectedSubcategory = (Category)subcategorySpinner.getSelectedItem();
                product.setSubCathegory(selectedSubcategory.getName());
                product.setSubcategoryId(selectedSubcategory.getId());
                productService.update(product);
                notifyDataSetChanged();
                popupWindow.dismiss();

            });

            cancelButton.setOnClickListener(v -> {

                popupWindow.dismiss();

            });
        }

        private void showDeleteConfirmationDialog(Product product) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Delete Product");
            builder.setMessage("Are you sure you want to delete this product?");

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @SuppressLint("NotifyDataSetChanged")
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //delete operation here
                    product.setDeleted(true);
                    productService.deleteLogically(product);
                    productList.remove(product);
                    notifyDataSetChanged();
                    dialog.dismiss();
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

        private void setSubcategorySpinnerData(Product product){
            CategoryService categoryService = new CategoryService();
            categoryService.getById(product.getCategoryId()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Category category = task.getResult();

                    CategorySpinnerAdapter adapter = new CategorySpinnerAdapter(context, category.getSubcategories());
                    adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                    subcategorySpinner.setAdapter(adapter);
                    String selectedCategory = product.getSubCathegory();
                    int position = adapter.getPosition(findSubcategory(category.getSubcategories(), selectedCategory));
                    if (position != -1) {
                        subcategorySpinner.setSelection(position);
                    }


                } else {
                    // Handle the error
                    Log.e("ProductManagementActivity", "Error getting categories", task.getException());
                }
            });
        }

        private void handleEventTypeSelection(Product product){
            @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.event_type_helper_layout, null);
            EventTypeService eventTypeService = new EventTypeService();
            // Create a PopupWindow
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            boolean focusable = true;
            PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
            popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            // dodavanje checkboxova
            LinearLayout checkboxContainer = popupView.findViewById(R.id.checkbox_container);
            eventTypeService.getAll(new EventTypeService.OnDataFetchListener() {
                @Override
                public void onSuccess(List<EventType> objects) {
                    //dodavanje checkboxova hehe
                    for(EventType et: objects){
                        CheckBox checkBox = new CheckBox(context);
                        checkBox.setText(et.getName());
                        if(checkIfEventIsChecked(product, et)){
                            checkBox.setChecked(true);
                        }
                        checkboxContainer.addView(checkBox);
                    }
                }

                @Override
                public void onFailure(String errorMessage) {

                }
            });
            Button saveButton = popupView.findViewById(R.id.button_save_event_type);

            saveButton.setOnClickListener(v -> {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < checkboxContainer.getChildCount(); i++) {
                    View childView = checkboxContainer.getChildAt(i);
                    if (childView instanceof CheckBox) {
                        CheckBox checkBox = (CheckBox) childView;
                        if (checkBox.isChecked()) {
                            sb.append(checkBox.getText().toString()).append(" ");
                        }
                    }
                }
                //POSTAVLJANJE VRIJEDNOSTI!!!!!
                product.setEventType(sb.toString());

                popupWindow.dismiss();

            });


        }
        private boolean checkIfEventIsChecked(Product product, EventType eventType){
            return product.getEventType().contains(eventType.getName());
        }
    }

    private Category findSubcategory(List<Category> subcategories, String subcategoryName){
        for(Category sc: subcategories){
            if(sc.getName() != null && sc.getName().contains(subcategoryName)){
                return sc;
            }

        }
        return null;
    }


}
