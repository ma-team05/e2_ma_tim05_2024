package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.CategoryRepository;
import com.example.eventplanner.models.Category;
import com.google.android.gms.tasks.Task;

import java.util.List;

public class CategoryService {
    private final CategoryRepository repository;

    public CategoryService() {
        this.repository = new CategoryRepository();
    }

    public Task<String> create(Category category) {
        return repository.save(category);
    }
    public Task<Category> getById(String categoryId) {
        return repository.getById(categoryId);
    }
    public Task<Category> getByName(String categoryId) { return repository.getByName(categoryId); }
    public Task<List<Category>> getAll() { return repository.getAll(); }
    public Task<Void> update(Category category) {
        return repository.update(category);
    }
    public Task<Void> delete(String categoryId) {
        return repository.delete(categoryId);
    }
}
