package com.example.eventplanner.database.repository;

import android.util.Log;

import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.Service;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.List;

public class ServiceRepository {
    private final FirebaseFirestore db;
    public ServiceRepository(){
        db = FirebaseFirestore.getInstance();
    }

    public Task<Service> get(String serviceId) {
        DocumentReference serviceRef = db.collection("services").document(serviceId);
        return serviceRef.get().continueWith(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    return document.toObject(Service.class);
                } else {
                    Log.w("GET SERVICE", "No such document");
                    return null;
                }
            } else {
                Log.w("GET SERVICE", "get failed with ", task.getException());
                return null;
            }
        });
    }
    public interface OnDataFetchListener {
        void onSuccess(List<Service> events);
        void onFailure(String errorMessage);
    }

    public void getAll(final ServiceRepository.OnDataFetchListener listener){
        db.collection("services")

                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Service> productList = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Service product = documentSnapshot.toObject(Service.class);
                        productList.add(product);
                    }

                    processServicesWithCategories(productList);
                    listener.onSuccess(productList);
                })
                .addOnFailureListener(e -> {
                    Log.w("GET PRODUCTS FAILED", "Error getting products", e);
                    listener.onFailure(e.getMessage());
                });
    }

    public void create(Service service){
        db.collection("services")
                .add(service)
                .addOnSuccessListener(documentReference -> {
                    String serviceId = documentReference.getId();
                    service.setId(serviceId);
                    documentReference.update("id", serviceId)
                            .addOnSuccessListener(aVoid -> {
                                Log.d("Service UPDATE", "Document updated with product id: " + serviceId);
                            })
                            .addOnFailureListener(e -> {
                                Log.w("Service UPDATE FAILED", "Error updating document", e);
                            });
                })
                .addOnFailureListener(e ->{
                    Log.w("Service CREATION FAILED", "Error adding document", e);
                });

    }

    public void update(Service service){
        String productId = service.getId();
        if (productId != null) {

            DocumentReference productRef = db.collection("services").document(productId);

            productRef.set(service)
                    .addOnSuccessListener(aVoid -> {
                        Log.d("SERVICE UPDATE", "Product updated successfully: " + productId);
                    })
                    .addOnFailureListener(e -> {
                        Log.w("SERVICE UPDATE FAILED", "Error updating product: " + productId, e);
                    });
        } else {
            Log.w("SERVICE UPDATE FAILED", "Product ID is null");
        }

    }

    public void deleteLogically(Service service){
        String productId = service.getId();
        service.setDeleted(true); // logical deletition
        if (productId != null) {

            DocumentReference productRef = db.collection("services").document(productId);

            productRef.set(service)
                    .addOnSuccessListener(aVoid -> {
                        Log.d("SERVICE UPDATE", "Product updated successfully: " + productId);
                    })
                    .addOnFailureListener(e -> {
                        Log.w("SERVICE UPDATE FAILED", "Error updating product: " + productId, e);
                    });
        } else {
            Log.w("SERVICE UPDATE FAILED", "Product ID is null");
        }

    }

    public void getUndeletedOfPUP(String userId ,final OnDataFetchListener listener){
        db.collection("services")
                .whereEqualTo("companyId", userId)
                .whereEqualTo("deleted", false)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Service> serviceList = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Service service = documentSnapshot.toObject(Service.class);
                        serviceList.add(service);
                    }
                    // Process the list of products with categories here
                    processServicesWithCategories(serviceList);
                    listener.onSuccess(serviceList);
                })
                .addOnFailureListener(e -> {
                    Log.w("GET SERVICES FAILED", "Error getting products", e);
                    listener.onFailure(e.getMessage());
                });
    }

    private void processServicesWithCategories(List<Service> serviceList) {
        for (Service service : serviceList) {
            String categoryId = service.getCategoryId();
            if (categoryId != null) {

                db.collection("categories")
                        .document(categoryId)
                        .get()
                        .addOnSuccessListener(documentSnapshot -> {
                            if (documentSnapshot.exists()) {
                                Category category = documentSnapshot.toObject(Category.class);
                                // Set the category object to the product

                                service.setCategory(category.getName());
                                //find subcategory

                                Log.d("Service WITH CATEGORY", service.toString());
                            } else {
                                Log.w("GET CATEGORY FAILED", "Category document not found for product: " + service.getId());
                            }
                        })
                        .addOnFailureListener(e -> {
                            Log.w("GET CATEGORY FAILED", "Error getting category for product: " + service.getId(), e);
                        });
            } else {
                Log.w("GET CATEGORY FAILED", "Category ID is null for product: " + service.getId());
            }
        }
    }

    public void getUndeletedOfPUPByCategory(String companyId, String categoryId, final OnDataFetchListener listener){
        db.collection("services")
                .whereEqualTo("companyId", companyId)
                .whereEqualTo("categoryId", categoryId)
                .whereEqualTo("deleted", false)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Service> serviceList = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Service service = documentSnapshot.toObject(Service.class);
                        serviceList.add(service);
                    }
                    // Process the list of products with categories here
                    processServicesWithCategories(serviceList);
                    listener.onSuccess(serviceList);
                })
                .addOnFailureListener(e -> {
                    Log.w("GET SERVICES FAILED", "Error getting products", e);
                    listener.onFailure(e.getMessage());
                });
    }

    public void updateBatch(List<Service> updatedServices) {
        // Create a write batch
        WriteBatch batch = db.batch();

        for (Service pkg : updatedServices) {
            // Get the document reference for this package
            DocumentReference packageRef = db.collection("services").document(pkg.getId());

            // Update the package in the batch
            batch.set(packageRef, pkg);
        }

        // Commit the batch
        batch.commit().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d("BATCH", "Batch update successful!");
            } else {
                Log.w("BATCH", "Batch update failed.", task.getException());
            }
        });
    }
}
