package com.example.eventplanner.models;

import android.os.Parcel;
import android.os.Parcelable;


import com.google.firebase.Timestamp;

import java.util.List;

public class Service implements Parcelable {


    public enum ConfirmationType {AUTOMATIC,BYHAND}
    private String id;
    private String category;
    private String categoryId;
    private String subCategory;
    private String subcategoryId;
    private String name;
    private String description;
    private String specifities;
    private double price;
    private double discount;
    private List<Integer> images;

    private String eventType;
    private boolean visible;
    private boolean available;
    private boolean isAppointment;
    private String minDuration; // if not appointment
    private String maxDuration; // if not appointment
    private String appointmentDuration; //if appointment
    private Timestamp reservationDeadline;
    private Timestamp cancelationDeadline;
    private ConfirmationType confirmationType;

    private List<String> employees;

    private boolean deleted;
    private boolean pending;

    private String companyId;

    private boolean isFavourite;

    public Service(){}

    public Service(String category, String subCategory, String name, String description, String specifities, double price, double discount, List<Integer> images, String eventType, boolean visible, boolean available, boolean isAppointment, String minDuration, String maxDuration, String appointmentDuration, Timestamp reservationDeadline, Timestamp cancelationDeadline, ConfirmationType confirmationType, List<String> employees) {
        this.category = category;
        this.subCategory = subCategory;
        this.name = name;
        this.description = description;
        this.specifities = specifities;
        this.price = price;
        this.discount = discount;
        this.images = images;
        this.eventType = eventType;
        this.visible = visible;
        this.available = available;
        this.isAppointment = isAppointment;
        this.minDuration = minDuration;
        this.maxDuration = maxDuration;
        this.appointmentDuration = appointmentDuration;
        this.reservationDeadline = reservationDeadline;
        this.cancelationDeadline = cancelationDeadline;
        this.confirmationType = confirmationType;
        this.employees = employees;
        this.isFavourite = false;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    //PROVJERITI OVDJE!!!!!!!!!!!!!!!11
    protected Service(Parcel in) {
        category = in.readString();
        subCategory = in.readString();
        name = in.readString();
        description = in.readString();
        specifities = in.readString();
        price = in.readDouble();
        discount = in.readDouble();
        eventType = in.readString();
        visible = in.readByte() != 0;
        available = in.readByte() != 0;
        isAppointment = in.readByte() != 0;
        employees = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category);
        dest.writeString(subCategory);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(specifities);
        dest.writeDouble(price);
        dest.writeDouble(discount);
        dest.writeString(eventType);
        dest.writeByte((byte) (visible ? 1 : 0));
        dest.writeByte((byte) (available ? 1 : 0));
        dest.writeByte((byte) (isAppointment ? 1 : 0));
        dest.writeStringList(employees);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Service> CREATOR = new Creator<Service>() {
        @Override
        public Service createFromParcel(Parcel in) {
            return new Service(in);
        }

        @Override
        public Service[] newArray(int size) {
            return new Service[size];
        }
    };

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpecifities() {
        return specifities;
    }

    public void setSpecifities(String specifities) {
        this.specifities = specifities;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public List<Integer> getImages() {
        return images;
    }

    public void setImages(List<Integer> images) {
        this.images = images;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isAppointment() {
        return isAppointment;
    }

    public void setAppointment(boolean appointment) {
        isAppointment = appointment;
    }

    public String getMinDuration() {
        return minDuration;
    }

    public void setMinDuration(String minDuration) {
        this.minDuration = minDuration;
    }

    public String getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(String maxDuration) {
        this.maxDuration = maxDuration;
    }

    public String getAppointmentDuration() {
        return appointmentDuration;
    }

    public void setAppointmentDuration(String appointmentDuration) {
        this.appointmentDuration = appointmentDuration;
    }

    public Timestamp getReservationDeadline() {
        return reservationDeadline;
    }

    public void setReservationDeadline(Timestamp reservationDeadline) {
        this.reservationDeadline = reservationDeadline;
    }

    public Timestamp getCancelationDeadline() {
        return cancelationDeadline;
    }

    public void setCancelationDeadline(Timestamp cancelationDeadline) {
        this.cancelationDeadline = cancelationDeadline;
    }

    public ConfirmationType getConfirmationType() {
        return confirmationType;
    }

    public void setConfirmationType(ConfirmationType confirmationType) {
        this.confirmationType = confirmationType;
    }

    public List<String> getEmployees() {
        return employees;
    }

    public void setEmployees(List<String> employees) {
        this.employees = employees;
    }

    public String getEmployeesAsString(){
        StringBuilder builder = new StringBuilder();
        List<String> employees = getEmployees();
        if (employees != null) {
            for (int i=0; i< employees.size(); i++) {
                builder.append(employees.get(i));
                if(i != employees.size() -1){
                    builder.append(", ");
                }
            }
        }
        return builder.toString();
    }


    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }
}
