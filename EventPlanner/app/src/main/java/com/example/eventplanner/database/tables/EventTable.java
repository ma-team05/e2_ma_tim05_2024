package com.example.eventplanner.database.tables;

import android.provider.BaseColumns;

public class EventTable {
    private EventTable() {
    }

    public static final class Table implements BaseColumns {
        public static final String TABLE_NAME = "events";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_MAX_GUESTS = "max_guests";
        public static final String COLUMN_OPEN_TO_PUBLIC = "open_to_public";
        public static final String COLUMN_CITY = "city";
        public static final String COLUMN_MAX_DISTANCE = "max_distance";
        public static final String COLUMN_DATE = "date";

        public static final String COLUMN_EVENT_TYPE_ID = "event_type_id";
    }

    public static final class DDL {
        public static final String CREATE_TABLE = "CREATE TABLE " +
                Table.TABLE_NAME + " (" +
                Table._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Table.COLUMN_NAME + " TEXT NOT NULL, " +
                Table.COLUMN_DESCRIPTION + " TEXT NOT NULL, " +
                Table.COLUMN_MAX_GUESTS + " INTEGER NOT NULL, " +
                Table.COLUMN_OPEN_TO_PUBLIC + " INTEGER NOT NULL, " +
                Table.COLUMN_CITY + " TEXT NOT NULL, " +
                Table.COLUMN_MAX_DISTANCE + " REAL NOT NULL, " +
                Table.COLUMN_DATE + " TEXT NOT NULL, " +
                Table.COLUMN_EVENT_TYPE_ID + " INTEGER NOT NULL, " +
                "FOREIGN KEY(" + EventTypeTable.Table._ID + ") REFERENCES " +
                CategoryTable.Table.TABLE_NAME + "(" + CategoryTable.Table._ID + ")" +
                ");";


        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " +
                Table.TABLE_NAME;

    }
}