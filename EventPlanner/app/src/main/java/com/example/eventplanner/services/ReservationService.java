package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.ReservationRepository;
import com.example.eventplanner.models.Reservation;
import com.google.android.gms.tasks.Task;

import java.util.List;

public class ReservationService {
    private final ReservationRepository repository;
    public ReservationService(){
        repository = new ReservationRepository();
    }
    public Task<Void> save(Reservation reservation){
        return repository.saveReservation(reservation);
    }
    public Task<Void> update(Reservation reservation){
        return repository.updateReservation(reservation);
    }
    public Task<List<Reservation>> getAll(){
        return repository.getAll();
    }
    public Task<List<Reservation>> getAllByEmployee(String employeeId){
        return repository.getAllByEmployee(employeeId);
    }
    public Task<List<Reservation>> getAllByEventPlanner(String eventPlannerId){
        return repository.getAllByEventPlanner(eventPlannerId);
    }
    public Task<Reservation> getById(String id){
        return repository.getById(id);
    }

    public Task<List<Reservation>> getActiveByCompanyId(String companyId){
        return repository.getActiveAllByCompanyId(companyId);
    }
    public Task<List<Reservation>> getActiveByEventPlanner(String eventPlannerId){
        return repository.getActiveAllByEventPlanner(eventPlannerId);
    }

    public void updateBatch(List<Reservation> reservations){
        repository.updateBatchOfEmployees(reservations);
    }

}
