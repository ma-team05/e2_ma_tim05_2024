package com.example.eventplanner.models;

import java.util.Date;

public class Grade {
    private String id;
    private String companyId;
    private int rating;
    private String comment;
    private Date createdOn;
    private String userId;

    public Grade(){}
    public Grade(String id, String companyId, int rating, String comment, Date createdOn, String userId) {
        this.id = id;
        this.companyId = companyId;
        this.rating = rating;
        this.comment = comment;
        this.createdOn = createdOn;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
