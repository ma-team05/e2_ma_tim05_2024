package com.example.eventplanner.models;

public enum GuestAge {
    AGE_0_3,
    AGE_3_10,
    AGE_10_18,
    AGE_18_30,
    AGE_30_50,
    AGE_50_70,
    AGE_70_PLUS,
}
