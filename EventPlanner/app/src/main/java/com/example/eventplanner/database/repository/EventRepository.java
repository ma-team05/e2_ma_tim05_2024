package com.example.eventplanner.database.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.eventplanner.models.Event;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class EventRepository {
    private final FirebaseFirestore db;
    private final String collection;

    public EventRepository() {
        db = FirebaseFirestore.getInstance();
        collection = "events";
    }

    public interface OnDataFetchListener {
        void onSuccess(List<Event> events);
        void onFailure(String errorMessage);
    }

    public void save(Event event) {
        db.collection(collection)
            .add(event)
            .addOnSuccessListener(documentReference -> Log.d("EVENT CREATION SUCCESS", "DocumentSnapshot added with ID: " + documentReference.getId()))
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.w("EVENT CREATION FAILED", "Error adding document", e);
                }
            });
    }
    public Task<List<Event>> getAll(){
        return db.collection(collection).get()
                .continueWith(task -> {
                    List<Event> events = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Event event = document.toObject(Event.class);
                        events.add(event);
                    }
                    return events;
                });
    }
    public void getAll(final OnDataFetchListener listener) {
        db.collection(collection).get().addOnSuccessListener(queryDocumentSnapshots -> {
            List<Event> objects = new ArrayList<>();
            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                Event event = documentSnapshot.toObject(Event.class);
                event.setId(documentSnapshot.getId());
                objects.add(event);
                //documentSnapshot.getReference().delete();
            }
            listener.onSuccess(objects);
        });
    }

    public void getAllUnassigned(final OnDataFetchListener listener) {
        db.collection(collection).get().addOnSuccessListener(queryDocumentSnapshots -> {
            List<Event> objects = new ArrayList<>();
            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                Event event = documentSnapshot.toObject(Event.class);
                event.setId(documentSnapshot.getId());
                if(event.getEmployeeId()==null)
                    objects.add(event);
            }
            listener.onSuccess(objects);
        });
    }

    public void getAllUnassignedInWeek(Date start, Date end, final OnDataFetchListener listener) {
        db.collection(collection).get().addOnSuccessListener(queryDocumentSnapshots -> {
            List<Event> objects = new ArrayList<>();
            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                Event event = documentSnapshot.toObject(Event.class);
                event.setId(documentSnapshot.getId());
                if(event.getEmployeeId()==null && event.getDate().after(start) && event.getDate().before(end))
                    objects.add(event);
            }
            listener.onSuccess(objects);
        });
    }

    public void getAllByEmployee(String employeeId, final OnDataFetchListener listener) {
        db.collection(collection).get().addOnSuccessListener(queryDocumentSnapshots -> {
            List<Event> objects = new ArrayList<>();
            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                Event event = documentSnapshot.toObject(Event.class);
                event.setId(documentSnapshot.getId());
                if(event.getEmployeeId()==null) continue;
                if(event.getEmployeeId().equals(employeeId))
                    objects.add(event);
            }
            listener.onSuccess(objects);
        });
    }

    public void getAllByEmployeeInWeek(String employeeId, Date start, Date end, final OnDataFetchListener listener) {
        db.collection(collection).get().addOnSuccessListener(queryDocumentSnapshots -> {
            List<Event> objects = new ArrayList<>();
            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                Event event = documentSnapshot.toObject(Event.class);
                event.setId(documentSnapshot.getId());
                if(event.getEmployeeId()==null) continue;
                if(event.getEmployeeId().equals(employeeId) && event.getDate().after(start) && event.getDate().before(end))
                    objects.add(event);
            }
            listener.onSuccess(objects);
        });
    }

    public void getById(String eventId, final OnDataFetchListener listener) {
        db.collection(collection)
                .document(eventId)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        Event event = documentSnapshot.toObject(Event.class);
                        event.setId(documentSnapshot.getId());
                        listener.onSuccess(Collections.singletonList(event));
                    } else {
                        listener.onSuccess(Collections.emptyList());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        listener.onFailure("Failed to retrieve event with ID: " + eventId);
                    }
                });
    }

    public void update(Event event) {
        db.collection(collection)
                .document(event.getId())
                .set(event)
                .addOnSuccessListener(aVoid -> Log.d("EVENT UPDATE SUCCESS", "Event updated successfully with ID: " + event.getId()))
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("EVENT UPDATE FAILED", "Error updating event with ID: " + event.getId(), e);
                    }
                });
    }

    public void delete(String eventId) {
        db.collection(collection)
                .document(eventId)
                .delete()
                .addOnSuccessListener(aVoid -> Log.d("EVENT DELETION SUCCESS", "Event deleted successfully with ID: " + eventId))
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("EVENT DELETION FAILED", "Error deleting event with ID: " + eventId, e);
                    }
                });
    }

    public void deleteAll() {
        db.collection(collection).get().addOnSuccessListener(queryDocumentSnapshots -> {
            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                documentSnapshot.getReference().delete();
            }
        });
    }
}
