package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.EventStatus;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.Guest;
import com.example.eventplanner.models.GuestAge;
import com.example.eventplanner.services.EventService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class GuestCreationActivity extends AppCompatActivity {

    private EventService eventService;

    private Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_guest_creation);

        eventService = new EventService(new EventRepository());

        Intent intent = getIntent();
        String eventId = intent.getStringExtra("EVENT");

        eventService.getById(eventId, new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                GuestCreationActivity.this.event = events.get(0);
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });

        List<String> ages = new ArrayList<>();
        ages.add("Select age");
        ages.add("Age: 0-3");
        ages.add("Age: 3-10");
        ages.add("Age: 10-18");
        ages.add("Age: 18-30");
        ages.add("Age: 30-50");
        ages.add("Age: 50-70");
        ages.add("Age: 70+");

        Spinner ageSpinner = findViewById(R.id.age);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(GuestCreationActivity.this, android.R.layout.simple_spinner_item, ages);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ageSpinner.setAdapter(adapter);

        Button createButton = findViewById(R.id.createButton);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields();
            }
        });

        Switch invitedSwitch = findViewById(R.id.invitedSwitch);
        TextView acceptedLabel = findViewById(R.id.accepted);
        Switch acceptedSwitch = findViewById(R.id.acceptedSwitch);
        Switch veganSwitch = findViewById(R.id.veganSwitch);
        TextView vegetarianLabel = findViewById(R.id.vegetarian);
        Switch vegetarianSwitch = findViewById(R.id.vegetarianSwitch);

        acceptedLabel.setVisibility(View.GONE);
        acceptedSwitch.setVisibility(View.GONE);
        invitedSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            acceptedLabel.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            acceptedSwitch.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        });

        veganSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            vegetarianLabel.setVisibility(!isChecked ? View.VISIBLE : View.GONE);
            vegetarianSwitch.setVisibility(!isChecked ? View.VISIBLE : View.GONE);
        });
    }

    private boolean validateFields() {
        boolean isValid = true;

        EditText nameEditText;
        Spinner ageSpinner;
        String field;
        String eventName;
        String evenType;

        nameEditText = findViewById(R.id.name);
        field = nameEditText.getText().toString().trim();
        eventName = field;
        if (TextUtils.isEmpty(field)) {
            nameEditText.setError("Please enter a full name");
            nameEditText.requestFocus();
            isValid = false;
        }

        ageSpinner = findViewById(R.id.age);
        field = ageSpinner.getSelectedItem().toString().trim();
        String eventType = field;
        if (field.equals("Select age")) {
            Toast.makeText(this, "Please select age", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if(isValid) {
            createGuest();
        }

        return isValid;
    }

    private void createGuest() {
        EditText nameEditText = findViewById(R.id.name);
        Spinner ageSpinner = findViewById(R.id.age);
        Switch invitedSwitch = findViewById(R.id.invitedSwitch);
        Switch acceptedInviteSwitch = findViewById(R.id.acceptedSwitch);
        Switch vegetarianSwitch = findViewById(R.id.vegetarianSwitch);
        Switch veganSwitcch = findViewById(R.id.veganSwitch);

        String name = nameEditText.getText().toString().trim();
        String ageString = ageSpinner.getSelectedItem().toString();
        GuestAge age = GuestAge.AGE_0_3;
        switch(ageString) {
            case "Age: 0-3":
                age = GuestAge.AGE_0_3;
                break;
            case "Age: 3-10":
                age = GuestAge.AGE_3_10;
                break;
            case "Age: 10-18":
                age = GuestAge.AGE_10_18;
                break;
            case "Age: 18-30":
                age = GuestAge.AGE_18_30;
                break;
            case "Age: 30-50":
                age = GuestAge.AGE_30_50;
                break;
            case "Age: 50-70":
                age = GuestAge.AGE_50_70;
                break;
            case "Age: 70+":
                age = GuestAge.AGE_70_PLUS;
                break;
        }
        boolean invited = invitedSwitch.isChecked();
        boolean accepted = acceptedInviteSwitch.isChecked();
        boolean vegetarian = vegetarianSwitch.isChecked();
        boolean vegan = veganSwitcch.isChecked();
        String specialRequests = "";
        if(vegetarian && vegan) {
            specialRequests = "Vegetarian + Vegan";
        }
        else if(vegetarian) {
            specialRequests = "Vegetarian";
        }
        else if(vegan) {
            specialRequests = "Vegan";
        }

        Guest guest = new Guest();
        guest.setFullName(name);
        guest.setAge(age);
        guest.setInvited(invited);
        guest.setAcceptedInvitation(accepted);
        guest.setSpecialRequests(specialRequests);

        event.addGuest(guest);
        eventService.update(event);

        String guestDetails = "Guest added:\n" +
                "Full name: " + guest.getFullName();
        Toast.makeText(this, guestDetails, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, GuestListActivity.class);
        intent.putExtra("EVENT", event.getId());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        finish();
    }
}