package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.example.eventplanner.models.ChatMessage;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Notification;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.services.ChatService;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.OwnerService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StartChatEmployeeActivity extends AppCompatActivity {

    private String email;
    private String recepientEmail;
    private EmployeeService employeeService = new EmployeeService(new EmployeeRepository());
    private ChatService chatService = new ChatService();
    private NotificationService notificationService = new NotificationService();
    private List<Employee> employees;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_start_chat_employee);

        Intent intent = getIntent();
        email = intent.getStringExtra("EMAIL");
        String companyId = intent.getStringExtra("COMPANY");

        employeeService.getAll().addOnCompleteListener(employeeTask -> {
            if (employeeTask.isSuccessful()) {
                employees = employeeTask.getResult();
                bind();
            }
        });

        Button createButton = findViewById(R.id.createButton);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields();
            }
        });
    }

    private void bind() {
        Spinner employeeSpinner = findViewById(R.id.employee);

        List<String> empolyeeStrings = new ArrayList<>();
        empolyeeStrings.add("Select an employee");
        for(Employee employee : employees) {
            empolyeeStrings.add("To: " + employee.getEmail());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(StartChatEmployeeActivity.this, android.R.layout.simple_spinner_item, empolyeeStrings);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        employeeSpinner.setAdapter(adapter);
    }

    private boolean validateFields() {
        boolean isValid = true;

        Spinner employeeSpinner;
        String employeeEmail;

        EditText messageEditText;
        String message;

        employeeSpinner = findViewById(R.id.employee);
        employeeEmail = employeeSpinner.getSelectedItem().toString().trim();
        if (employeeEmail.equals("Select an employee")) {
            Toast.makeText(this, "Please select an employee", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        messageEditText = findViewById(R.id.message);
        message = messageEditText.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            messageEditText.setError("Please enter a message");
            messageEditText.requestFocus();
            isValid = false;
        }

        if(isValid) {
            startChat();
        }

        return isValid;
    }

    private void startChat() {
        EditText messageEditText;
        Spinner employeeSpinner;
        String message;
        String employeeEmail;

        messageEditText = findViewById(R.id.message);
        message = messageEditText.getText().toString().trim();

        employeeSpinner = findViewById(R.id.employee);
        employeeEmail = employeeSpinner.getSelectedItem().toString().trim().substring(3);

        ChatMessage chatMessage = new ChatMessage();

        chatMessage.setFrom(email);
        chatMessage.setTo(employeeEmail);
        chatMessage.setMessage(message);
        chatMessage.setDate(new Date());
        chatMessage.setReadStatus(false);

        chatService.start(chatMessage);

        Notification notification = new Notification();
        notification.setUserId(recepientEmail);
        notification.setDescription("You received a message from: " + email);
        notification.setRead(false);
        notification.setSent(true);
        notification.setType(NotificationType.CHAT);

        Employee employee = employees.stream().filter(e -> e.getEmail().equals(recepientEmail)).findFirst().orElse(null);
        notificationService.add(employee.getId(), "You received a message from: " + email, NotificationType.CHAT);

        String eventDetails = "Message sent:\n";
        Toast.makeText(this, eventDetails, Toast.LENGTH_LONG).show();

        finish();
    }
}