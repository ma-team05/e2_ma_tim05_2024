package com.example.eventplanner.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.eventplanner.R;
import com.example.eventplanner.activities.employees.EmployeeDetailsActivity;
import com.example.eventplanner.models.Employee;

import java.util.List;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder> {

    private List<Employee> employeeList;

    public EmployeeAdapter(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder holder, int position) {
        Employee employee = employeeList.get(position);
        holder.bind(employee);
    }
    @NonNull
    @Override
    public EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_employee, parent, false);
        return new EmployeeViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return employeeList.size();
    }

    static class EmployeeViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textViewName;
        private TextView textViewEmail;
        private TextView textViewPhone;
        private TextView textViewAddress;
        private Button detailsButton; // Add details button

        public EmployeeViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewEmail = itemView.findViewById(R.id.textViewEmail);
            textViewPhone = itemView.findViewById(R.id.textViewPhone);
            textViewAddress = itemView.findViewById(R.id.textViewAddress);
            detailsButton = itemView.findViewById(R.id.detailsButton); // Initialize details button
        }

        public void bind(Employee employee) {
            // Load image using Glide
            Glide.with(itemView)
                    .load(employee.getPicture())  // Assuming employee.Picture is the path to the image
                    .placeholder(R.drawable.default_employee_image) // Placeholder image while loading
                    .error(R.drawable.default_employee_image) // Error image if loading fails
                    .into(imageView);

            textViewName.setText(employee.getFirstName() + " " + employee.getLastName());
            textViewEmail.setText("Email: " + employee.getEmail());
            textViewPhone.setText("Phone: " + employee.getPhone());
            textViewAddress.setText("Address: " + employee.getAddress());
            detailsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), EmployeeDetailsActivity.class);
                    intent.putExtra("employeeId", employee.getId());
                    intent.putExtra("viewForOwner", true);
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
