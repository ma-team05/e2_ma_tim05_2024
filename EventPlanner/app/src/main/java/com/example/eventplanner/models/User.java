package com.example.eventplanner.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class User {
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String address;
    private String picture;
    private UserRole role;
    private String password;
    private boolean activated;
    private boolean deactivated = false;
    private Date createdAt;

    private List<String> favouriteServices = new ArrayList<>();
    private List<String> favouriteProducts = new ArrayList<>();
    private List<String> favouritePackages = new ArrayList<>();

    public User(){}
    public User(String id, String firstName, String lastName, String email, String phone, String address, String picture, UserRole role, String password, Boolean activated) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.picture = picture;
        this.role = role;
        this.password = password;
        this.activated = activated;
        this.createdAt = new Date();
    }
    public User(String firstName, String lastName, String email, String phone, String address, String picture, UserRole role, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.picture = picture;
        this.role = role;
        this.password = password;
        this.activated = true;
        this.createdAt = new Date();
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setPassword(String password) { this.password = password; }
    public String getPassword() {
        return password;
    }
    public void setRole(UserRole role) {
        this.role = role;
    }
    public UserRole getRole() {
        return role;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getPicture() {
        return picture;
    }
    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public List<String> getFavouriteServices() {
        return favouriteServices;
    }

    public void setFavouriteServices(List<String> favouriteServices) {
        this.favouriteServices = favouriteServices;
    }

    public void addFavouriteService(String service) {
        this.favouriteServices.add(service);
    }

    public void removeFavouriteService(String service) {
        this.favouriteServices = this.favouriteServices.stream().filter(s -> !s.equals(service)).collect(Collectors.toList());
    }

    public List<String> getFavouriteProducts() {
        return favouriteProducts;
    }

    public void setFavouriteProducts(List<String> favouriteProducts) {
        this.favouriteProducts = favouriteProducts;
    }

    public void addFavouriteProduct(String product) {
        this.favouriteProducts.add(product);
    }

    public void removeFavouriteProduct(String product) {
        this.favouriteProducts = this.favouriteProducts.stream().filter(s -> !s.equals(product)).collect(Collectors.toList());
    }

    public List<String> getFavouritePackages() {
        return favouritePackages;
    }

    public void setFavouritePackages(List<String> favouritePackages) {
        this.favouritePackages = favouritePackages;
    }

    public void addFavouritePackage(String servicePackage) {
        this.favouritePackages.add(servicePackage);
    }

    public void removeFavouritePackage(String servicePackage) {
        this.favouritePackages = this.favouritePackages.stream().filter(s -> !s.equals(servicePackage)).collect(Collectors.toList());
    }

    public boolean isDeactivated() {
        return deactivated;
    }

    public void setDeactivated(boolean deactivated) {
        this.deactivated = deactivated;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }
}