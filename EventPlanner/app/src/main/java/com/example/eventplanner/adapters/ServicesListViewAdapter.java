package com.example.eventplanner.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.ServiceDetailsActivity;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.UserService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ServicesListViewAdapter  extends BaseAdapter {
    private Context context;
    private List<Service> serviceList;
    private User user;

    private UserService userService = new UserService();

    private int currentIndex;

    public ServicesListViewAdapter(Context context,List<Service> serviceList, User user) {
        this.context = context;
        this.serviceList = serviceList;
        this.user = user;


        Log.i("Frag","Napravio adapter");
    }
    @Override
    public int getCount() {
        return serviceList.size();
    }

    @Override
    public Object getItem(int position) {
        return serviceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderOk holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_service_result, parent, false);


                  //  serviceDuration, serviceReserveIn, serviceCancelIn, serviceConfirm, serviceEmployees, serviceSpecifities;
            holder = new ViewHolderOk();

            holder.btPrevious = convertView.findViewById(R.id.bt_previous);
            holder.btNext = convertView.findViewById(R.id.bt_next);
            holder.btnFav = convertView.findViewById(R.id.btn_fav);
            holder.btnUnFav = convertView.findViewById(R.id.btn_unfav);
            holder.btnInfo = convertView.findViewById(R.id.btn_info);
            holder.solutionImageSwitcher = convertView.findViewById(R.id.solution_image_switcher);
            holder.serviceName = convertView.findViewById(R.id.solution_name);
            holder.serviceCategory = convertView.findViewById(R.id.solution_category);
            holder.serviceSubcategory = convertView.findViewById(R.id.solution_subcategory);
            holder.serviceDescription = convertView.findViewById(R.id.solution_description);
            holder.servicePrice = convertView.findViewById(R.id.solution_price);
            holder.serviceDiscount = convertView.findViewById(R.id.solution_discount);
            holder.serviceDiscountPrice = convertView.findViewById(R.id.solution_discount_price);
            holder.serviceAvailable = convertView.findViewById(R.id.solution_available);
            holder.serviceVisibility = convertView.findViewById(R.id.solution_visibility);
            holder.serviceEventType = convertView.findViewById(R.id.solution_event_type);
            holder.serviceDuration = convertView.findViewById(R.id.solution_duration);
            holder.serviceReserveIn = convertView.findViewById(R.id.solution_res_deadline);
            holder.serviceCancelIn = convertView.findViewById(R.id.solution_cancel_in);
            holder.serviceConfirm = convertView.findViewById(R.id.solution_confirmation);
            holder.serviceEmployees = convertView.findViewById(R.id.solution_employees);
            holder.serviceSpecifities = convertView.findViewById(R.id.solution_specifities);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderOk) convertView.getTag();
        }

        holder.solutionImageSwitcher.setFactory(() -> {
            ImageView imageView = new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            return imageView;
        });

        holder.serviceName.setText(serviceList.get(position).getName());
        holder.serviceCategory.setText(serviceList.get(position).getCategory());
        holder.serviceSubcategory.setText(serviceList.get(position).getSubCategory());
        holder.serviceDescription.setText(serviceList.get(position).getDescription());
        holder.servicePrice.setText(String.valueOf(serviceList.get(position).getPrice()));
        holder.serviceDiscount.setText(String.valueOf(serviceList.get(position).getDiscount()));

        holder.serviceEventType.setText(serviceList.get(position).getEventType());
        holder.serviceAvailable.setText(serviceList.get(position).isAvailable() ? "Yes" : "No");
        holder.serviceVisibility.setText(serviceList.get(position).isVisible() ? "Yes" : "No");
        holder.serviceEmployees.setText(serviceList.get(position).getEmployeesAsString());

        if(serviceList.get(position).isAppointment()){
            holder.serviceDuration.setText(serviceList.get(position).getAppointmentDuration() + "h");
        }else{
            StringBuilder builder = new StringBuilder();
            builder.append("min ").append(serviceList.get(position).getMinDuration()).append("h ").append("max ").append(serviceList.get(position).getMaxDuration()).append("h ");
            holder.serviceDuration.setText(builder.toString());
        }
        //for time stamp

        Calendar cal = Calendar.getInstance();

// Reservation deadline
        Date reservationDate = serviceList.get(position).getReservationDeadline().toDate();
        cal.setTimeInMillis(reservationDate.getTime());
        StringBuilder reservationBuilder = new StringBuilder();
        reservationBuilder.append(cal.get(Calendar.MONTH)).append("m "); // Adding 1 to get 1-based month
        reservationBuilder.append(cal.get(Calendar.DAY_OF_MONTH)).append("d ");
        holder.serviceReserveIn.setText(reservationBuilder.toString());

// Cancellation deadline
        Date cancellationDate = serviceList.get(position).getCancelationDeadline().toDate();
        cal.setTimeInMillis(cancellationDate.getTime());
        StringBuilder cancellationBuilder = new StringBuilder();
        cancellationBuilder.append(cal.get(Calendar.MONTH) ).append("m "); // Adding 1 to get 1-based month
        cancellationBuilder.append(cal.get(Calendar.DAY_OF_MONTH)).append("d ");
        holder.serviceCancelIn.setText(cancellationBuilder.toString());

        if(serviceList.get(position).getSpecifities().isEmpty()){
            holder.serviceSpecifities.setText("/");
        }else{
            holder.serviceSpecifities.setText(serviceList.get(position).getSpecifities());
        }

        if(serviceList.get(position).getConfirmationType() == Service.ConfirmationType.AUTOMATIC){
            holder.serviceConfirm.setText("Automatic");
        }else{
            holder.serviceConfirm.setText("By hand");
        }

        if(serviceList.get(position).isAppointment()){
            double finalprice = serviceList.get(position).getPrice() - serviceList.get(position).getDiscount();
            double hours = Double.parseDouble(serviceList.get(position).getAppointmentDuration());
            finalprice = finalprice*hours;
            holder.serviceDiscountPrice.setText(String.valueOf(finalprice));
        }else{
            holder.serviceDiscountPrice.setText("will be calculated");
        }


        //serviceDuration, serviceReserveIn, serviceCancelIn, serviceConfirm
        if (serviceList.get(position).getImages() != null && !serviceList.get(position).getImages().isEmpty()) {
            holder.solutionImageSwitcher.setImageResource(serviceList.get(position).getImages().get(0));
        } else {
            holder.solutionImageSwitcher.setImageResource(R.drawable.default_image);
        }

        if (user.getFavouriteServices().contains(serviceList.get(position).getId())) {
            holder.btnUnFav.setVisibility(View.VISIBLE);
            holder.btnFav.setVisibility(View.GONE);
        } else {
            holder.btnFav.setVisibility(View.VISIBLE);
            holder.btnUnFav.setVisibility(View.GONE);
        }

        holder.btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int currentIndex = solutionImageSwitcher.getDisplayedChild();
                if (currentIndex < serviceList.get(position).getImages().size() - 1) {
                    currentIndex++;
                    holder.solutionImageSwitcher.setImageResource(serviceList.get(position).getImages().get(currentIndex));
                }
            }
        });

        holder.btPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int currentIndex = solutionImageSwitcher.getDisplayedChild();
                if (currentIndex > 0) {
                    currentIndex--;
                    holder.solutionImageSwitcher.setImageResource(serviceList.get(position).getImages().get(currentIndex));
                }
            }
        });

        holder.btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.addFavouriteService(serviceList.get(position).getId());
                userService.update(user);
                holder.btnFav.setVisibility(View.GONE);
                holder.btnUnFav.setVisibility(View.VISIBLE);
            }
        });

        holder.btnUnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.removeFavouriteService(serviceList.get(position).getId());
                userService.update(user);
                holder.btnFav.setVisibility(View.VISIBLE);
                holder.btnUnFav.setVisibility(View.GONE);
            }
        });

        holder.btnInfo.setOnClickListener(v -> {
            Intent createIntent = new Intent(context, ServiceDetailsActivity.class);
            createIntent.putExtra("EMAIL", user.getEmail());
            createIntent.putExtra("SERVICE", serviceList.get(position).getId());
            context.startActivity(createIntent);
        });

        return convertView;
    }

    static class ViewHolderOk {
        ImageButton btPrevious, btNext, btnFav, btnUnFav, btnInfo;
        ImageSwitcher solutionImageSwitcher;
        TextView serviceName, serviceCategory, serviceSubcategory, serviceDescription,
                servicePrice, serviceDiscount, serviceDiscountPrice, serviceAvailable, serviceVisibility, serviceEventType,
                serviceDuration, serviceReserveIn, serviceCancelIn, serviceConfirm, serviceEmployees, serviceSpecifities;
    }
}
