package com.example.eventplanner.services;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Build;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.example.eventplanner.MainActivity;
import com.example.eventplanner.R;
import com.example.eventplanner.activities.LoggedInUserActivity;
import com.example.eventplanner.models.Notification;
import com.example.eventplanner.models.NotificationType;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.List;
import java.util.stream.Collectors;

public class NotificationListenerService extends Service {
    private Handler handler;
    private Runnable runnable;
    private final NotificationService notificationService = new NotificationService();
    private String userId;

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                Log.d("NOTIFICATION LISTENER: ", "Listening for notifications.");
                checkForNotifications();
                handler.postDelayed(this, 5000);
            }
        };
        handler.post(runnable);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        userId = intent.getStringExtra("NOTIFICATIONS_USER_ID");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void checkForNotifications() {
        notificationService.getNotificationsByUser(userId).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<com.example.eventplanner.models.Notification> notifications = task.getResult();
                notifications = notifications.stream().filter(n -> n.getType().equals(NotificationType.CHAT)).collect(Collectors.toList());
                if (notifications != null) {
                    for (Notification notification : notifications) {
                        if (!notification.isSent()) {
                            sendNotification(notification);
                            notification.setSent(true);
                            notificationService.update(notification);
                        }
                    }
                }
            }
        });
    }

    private void sendNotification(Notification notification) {
        FirebaseMessaging.getInstance().subscribeToTopic("notification")
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        triggerLocalNotification(notification);
                    }
                });
    }

    private void triggerLocalNotification(Notification notification) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "event_planner_channel";

        NotificationChannel channel = new NotificationChannel(channelId, "Event Planner Notification", NotificationManager.IMPORTANCE_DEFAULT);
        notificationManager.createNotificationChannel(channel);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setContentTitle(notification.getType().toString())
                .setContentText(notification.getDescription())
                .setSmallIcon(R.drawable.notification_icon)
                .setAutoCancel(true)
                .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE));

        notificationManager.notify(0, notificationBuilder.build());
    }
}
