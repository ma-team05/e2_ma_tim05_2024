package com.example.eventplanner.database.repository;

import android.util.Log;

import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.Report;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class ReportRepository {
    private FirebaseFirestore db;
    public ReportRepository(){
        db = FirebaseFirestore.getInstance();
    }

    public void create(Report report){
        db.collection("reports")
                .add(report)
                .addOnSuccessListener(documentReference -> {
                    String reportId = documentReference.getId();
                    report.setId(reportId);
                    documentReference.update("id", reportId)
                            .addOnSuccessListener(aVoid -> {
                                Log.d("REPORT UPDATE", "Document updated with report id: " + reportId);
                            })
                            .addOnFailureListener(e -> {
                                Log.w("REPORT UPDATE FAILED", "Error updating document", e);
                            });
                })
                .addOnFailureListener(e ->{
                    Log.w("REPORT CREATION FAILED", "Error adding document", e);
                });

    }

    public void update(Report report){
        String reportId = report.getId();
        if (reportId != null) {

            DocumentReference productRef = db.collection("reports").document(reportId);

            productRef.set(report)
                    .addOnSuccessListener(aVoid -> {
                        Log.d("REPORT UPDATE", "Product updated successfully: " + reportId);
                    })
                    .addOnFailureListener(e -> {
                        Log.w("REPORT UPDATE FAILED", "Error updating report: " + reportId, e);
                    });
        } else {
            Log.w("REPORT UPDATE FAILED", "REPORT ID is null");
        }

    }

    public interface OnDataFetchListener {
        void onSuccess(List<Report> reports);
        void onFailure(String errorMessage);
    }

    public void getAll(final OnDataFetchListener listener){
        db.collection("reports")

                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Report> productList = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Report product = documentSnapshot.toObject(Report.class);
                        productList.add(product);
                    }
                    listener.onSuccess(productList);
                })
                .addOnFailureListener(e -> {
                    Log.w("GET REPORTS FAILED", "Error getting products", e);
                    listener.onFailure(e.getMessage());
                });
    }
    public void getExisting(String reporterId, String reportedId,final OnDataFetchListener listener){
        db.collection("reports")
                .whereEqualTo("reporterId", reporterId)
                .whereEqualTo("reportedId", reportedId)
                .whereEqualTo("reportType", Report.REPORT_TYPE.Reported)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Report> productList = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Report product = documentSnapshot.toObject(Report.class);
                        productList.add(product);
                    }
                    listener.onSuccess(productList);
                })
                .addOnFailureListener(e -> {
                    Log.w("GET REPORTS FAILED", "Error getting products", e);
                    listener.onFailure(e.getMessage());
                });
    }


}
