package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.ChatRepository;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.Chat;
import com.example.eventplanner.models.ChatMessage;
import com.example.eventplanner.models.Event;

import java.util.List;

public class ChatService {
    private ChatRepository repository;

    public ChatService() {
        this.repository = new ChatRepository();
    }

    public interface OnDataFetchListener {
        void onSuccess(List<Chat> objects);
        void onFailure(String errorMessage);
    }

    public void start(ChatMessage message) {
        repository.getAll(new ChatRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Chat> events) {
                Chat existingChat = events.stream().filter(c -> (c.getParticipant1().equals(message.getFrom()) && c.getParticipant2().equals(message.getTo())) ||  (c.getParticipant1().equals(message.getTo()) && c.getParticipant2().equals(message.getFrom()))).findFirst().orElse(null);
                if(existingChat == null) {
                    Chat chat = new Chat();
                    chat.setParticipant1(message.getFrom());
                    chat.setParticipant2(message.getTo());
                    chat.setReadAllMessages1(true);
                    chat.setReadAllMessages2(false);
                    chat.addMessage(message);
                    repository.save(chat);
                }
                else {
                    existingChat.addMessage(message);
                    if(existingChat.getParticipant1().equals(message.getFrom())) {
                        existingChat.setReadAllMessages2(false);
                    }
                    else {
                        existingChat.setReadAllMessages1(false);
                    }
                    repository.update(existingChat, true);
                }
            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });
    }

    public void getAll(final ChatService.OnDataFetchListener listener) {
        repository.getAll(new ChatRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Chat> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void update(Chat chat, boolean updateDate) {
        repository.update(chat, updateDate);
    }
}
