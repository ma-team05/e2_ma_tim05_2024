package com.example.eventplanner.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Company;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.Report;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.ReportService;
import com.example.eventplanner.services.UserService;
import com.google.firebase.Timestamp;

import java.util.List;

public class CompanyDetailsOwnerActivity extends AppCompatActivity {private User user;
    private Company company;
    private UserService userService = new UserService();
    private CompanyService companyService = new CompanyService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_company_details_owner);

        Intent intent = getIntent();
        String email = intent.getStringExtra("EMAIL");
        String companyId = intent.getStringExtra("COMPANY");
        userService.getByEmail(email).addOnCompleteListener(this, task->{
            if(task.isSuccessful()){
                user = task.getResult();
            }

            companyService.get(companyId).addOnCompleteListener(companyTask -> {
                if (companyTask.isSuccessful()) {
                    company = companyTask.getResult();
                    bind();
                }
            });
        });

        Button editButton = findViewById(R.id.edit);

        editButton.setOnClickListener(v -> {
            Intent createIntent = new Intent(this, EditCompanyActivity.class);
            createIntent.putExtra("EMAIL", email);
            createIntent.putExtra("email", email);
            createIntent.putExtra("COMPANY", companyId);
            this.startActivity(createIntent);
        });
    }

    private void bind() {
        TextView name, description, type, email, phone, categories, eventTypes, address;

        name = findViewById(R.id.name);
        description = findViewById(R.id.description);
        type = findViewById(R.id.type);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        categories = findViewById(R.id.categories);
        eventTypes = findViewById(R.id.eventTypes);
        address = findViewById(R.id.address);

        name.setText("Name: " + company.getName());
        description.setText("Description: " + company.getDescription());
        type.setText("Type: " + company.getCompanyType().toLowerCase());
        email.setText("Email: " + company.getEmail());
        phone.setText("Phone number: " + company.getPhoneNumber());
        address.setText("Address: " + company.getAddress());

        String categoriesString = "";
        for(Category category : company.getCategories()) {
            categoriesString += ", " + category.getName();
        }
        if(!categoriesString.isEmpty()) {
            categoriesString = categoriesString.substring(2);
        }
        categories.setText("Categories: " + categoriesString);

        String eventTypesString = "";
        for(EventType eventType : company.getEventTypes()) {
            eventTypesString += ", " + eventType.getName();
        }
        if(!eventTypesString.isEmpty()) {
            eventTypesString = eventTypesString.substring(2);
        }
        eventTypes.setText("Event types: " + eventTypesString);
    }

}