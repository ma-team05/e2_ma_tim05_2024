package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.EventTypeRepository;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.User;
import com.google.android.gms.tasks.Task;

import java.util.List;

public class EventTypeService {
    private final EventTypeRepository eventTypeRepository;

    public EventTypeService() {
        eventTypeRepository = new EventTypeRepository();
    }

    public interface OnDataFetchListener {
        void onSuccess(List<EventType> objects);
        void onFailure(String errorMessage);
    }
    public Task<String> save(EventType eventType) {
        return eventTypeRepository.save(eventType);
    }
    public Task<Void> update(EventType eventType) {
        return eventTypeRepository.update(eventType);
    }

    public void getAll(final EventTypeService.OnDataFetchListener listener) {
        eventTypeRepository.getAll(new EventTypeRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<EventType> eventTypes) {
                listener.onSuccess(eventTypes);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }
}