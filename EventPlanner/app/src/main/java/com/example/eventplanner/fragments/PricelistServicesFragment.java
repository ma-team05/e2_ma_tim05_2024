package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ServicePricetlistAdapter;
import com.example.eventplanner.database.repository.ServiceRepository;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.ServiceService;

import java.util.ArrayList;
import java.util.List;


public class PricelistServicesFragment extends Fragment {

    private static final String ARG_USER_ROLE = "user_role";
    private static final String ARG_COMPANY_ID = "company_id";
    private RecyclerView recyclerView;
    private ServicePricetlistAdapter serviceAdapter;
    private List<Service> serviceList;
    private ServiceService serviceService;
    private String companyId;
    private String userRole;

    public PricelistServicesFragment() {
        // Required empty public constructor
    }

    public static PricelistServicesFragment newInstance(String userRole, String companyId) {
        PricelistServicesFragment fragment = new PricelistServicesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USER_ROLE, userRole);
        args.putString(ARG_COMPANY_ID, companyId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userRole = getArguments().getString(ARG_USER_ROLE);
            companyId = getArguments().getString(ARG_COMPANY_ID);
        }

        ServiceRepository productRepository = new ServiceRepository();
        serviceService = new ServiceService(productRepository);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_pricelist, container, false);
        recyclerView = view.findViewById(R.id.pricelist_services);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        // Call the method to initialize the RecyclerView
        initializeRecyclerView();

        return view;
    }

    private void initializeRecyclerView(){

        serviceService.getUndeletedOfPUP(companyId ,new ServiceService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Service> events) {
                serviceList = new ArrayList<>(events);
                if(userRole.equals("OWNER")){
                    serviceAdapter = new ServicePricetlistAdapter(getContext(), serviceList, serviceService, UserRole.OWNER);
                }else{
                    serviceAdapter = new ServicePricetlistAdapter(getContext(), serviceList, serviceService, UserRole.EMPLOYEE);
                }

                recyclerView.setAdapter(serviceAdapter);

            }

            @Override
            public void onFailure(String errorMessage) {}
        });
    }
}