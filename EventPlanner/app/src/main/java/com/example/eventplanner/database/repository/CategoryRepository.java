package com.example.eventplanner.database.repository;

import com.example.eventplanner.models.Category;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CategoryRepository {
    private final FirebaseFirestore db;
    private final String collection;

    public CategoryRepository() {
        db = FirebaseFirestore.getInstance();
        collection = "categories";
    }
    public Task<Category> getByName(String categoryName) {
        return getAll().continueWith(task -> {
            if (task.isSuccessful()) {
                List<Category> categories = task.getResult();
                for (Category category : categories) {
                    if (category.getName().equals(categoryName)) {
                        return category;
                    }
                }
            }
            throw new RuntimeException("Category not found: " + categoryName);
        });
    }

    public interface OnDataFetchListener {
        void onSuccess(List<Category> categories);
        void onFailure(String errorMessage);
    }

    public Task<Void> update(Category category) {
        return db.collection(collection).document(category.getId()).set(category);
    }

    public Task<String> save(Category category) {
        DocumentReference newCategoryRef = db.collection(collection).document();
        category.setId(newCategoryRef.getId());
        return newCategoryRef.set(category)
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        return newCategoryRef.getId();
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }
    public Task<Void> delete(String categoryId) {
        return db.collection(collection).document(categoryId).delete();
    }
    public void deleteAll() {
        db.collection(collection).get().addOnSuccessListener(queryDocumentSnapshots -> {
            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                documentSnapshot.getReference().delete();
            }
        });
    }
    public Task<Category> getById(String categoryId) {
        DocumentReference docRef = db.collection("categories").document(categoryId);
        return docRef.get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            return document.toObject(Category.class);
                        } else {
                            return null;
                        }
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }

    public Task<List<Category>> getAll() {
        return db.collection(collection).get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        List<Category> categories = new ArrayList<>();
                        for (QueryDocumentSnapshot documentSnapshot : querySnapshot) {
                            Category category = documentSnapshot.toObject(Category.class);
                            category.setId(documentSnapshot.getId());
                            categories.add(category);
                        }
                        return categories;
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }
}
