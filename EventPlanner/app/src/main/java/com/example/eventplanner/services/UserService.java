package com.example.eventplanner.services;


import com.example.eventplanner.database.repository.UserRepository;
import com.example.eventplanner.models.User;
import com.google.android.gms.tasks.Task;

import java.util.List;

public class UserService {
    private final UserRepository userRepository;
    public UserService() {
        userRepository = new UserRepository();
    }
    public Task<String> save(User user) {
        return userRepository.save(user);
    }

    public Task<String> saveFavouriteServices(User user) {
        return userRepository.saveFavouriteServices(user);
    }
    public Task<Void> update(User user){return userRepository.update(user);}
    public Task<User> getByEmail(String email){
        return userRepository.getByEmail(email);
    }
    public Task<User> get(String userId){return userRepository.get(userId);}
    public Task<List<User>> getNonActivatedUsers() {
        return userRepository.getNonActivatedOwners();
    }
}