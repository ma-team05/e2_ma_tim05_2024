package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.employees.EmployeeDetailsActivity;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.User;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.UserService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EditProfileActivity extends AppCompatActivity {

    private UserService userService = new UserService();
    private EmployeeService employeeService = new EmployeeService();
    private OwnerService ownerService = new OwnerService();
    private User user;

    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_edit_profile);

        Intent intent = getIntent();
        email = intent.getStringExtra("EMAIL");

        userService.getByEmail(email).addOnCompleteListener(this, task->{
            if(task.isSuccessful()){
                user = task.getResult();
                bind();

                Button edit = findViewById(R.id.edit);
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        validateFields();
                    }
                });
            }
        });
    }

    private void bind() {
        EditText firstName, lastName, phone, address;

        firstName = findViewById(R.id.first_name);
        lastName = findViewById(R.id.last_name);
        phone = findViewById(R.id.phone);
        address = findViewById(R.id.address);

        firstName.setText(user.getFirstName());
        lastName.setText(user.getLastName());
        phone.setText(user.getPhone());
        address.setText(user.getAddress());
    }

    private boolean validateFields() {
        boolean isValid = true;

        EditText firstNameEditText, lastNameEditText, phoneEditText, addressEditText;

        firstNameEditText = findViewById(R.id.first_name);
        lastNameEditText = findViewById(R.id.last_name);
        phoneEditText = findViewById(R.id.phone);
        addressEditText = findViewById(R.id.address);

        String firstName = firstNameEditText.getText().toString().trim();
        String lastName = lastNameEditText.getText().toString().trim();
        String address = addressEditText.getText().toString().trim();
        String phone = phoneEditText.getText().toString();

        if (TextUtils.isEmpty(firstName)) {
            firstNameEditText.setError("Please enter a first name");
            firstNameEditText.requestFocus();
            isValid = false;
        }

        if (TextUtils.isEmpty(lastName)) {
            lastNameEditText.setError("Please enter a last name");
            lastNameEditText.requestFocus();
            isValid = false;
        }

        if (TextUtils.isEmpty(phone)) {
            phoneEditText.setError("Please enter a phone number");
            phoneEditText.requestFocus();
            isValid = false;
        }

        if (TextUtils.isEmpty(address)) {
            addressEditText.setError("Please enter an address");
            addressEditText.requestFocus();
            isValid = false;
        }

        if(isValid) {
            editEvent();
        }

        return isValid;
    }

    private void editEvent() {
        EditText firstNameEditText, lastNameEditText, phoneEditText, addressEditText;

        firstNameEditText = findViewById(R.id.first_name);
        lastNameEditText = findViewById(R.id.last_name);
        phoneEditText = findViewById(R.id.phone);
        addressEditText = findViewById(R.id.address);

        String firstName = firstNameEditText.getText().toString().trim();
        String lastName = lastNameEditText.getText().toString().trim();
        String address = addressEditText.getText().toString().trim();
        String phone = phoneEditText.getText().toString();

        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setAddress(address);
        user.setPhone(phone);

        userService.update(user);

        if(user.getRole().equals(UserRole.EMPLOYEE)) {
            employeeService.getAll().addOnCompleteListener(employeeTask -> {
                if (employeeTask.isSuccessful()) {
                    Employee employee = employeeTask.getResult().stream().filter(e -> e.getEmail().equals(email)).findFirst().orElse(null);
                    employee.setFirstName(firstName);
                    employee.setLastName(lastName);
                    employee.setAddress(address);
                    employee.setPhone(phone);
                    employeeService.update(employee);
                }
            });
        }
        else if(user.getRole().equals(UserRole.OWNER)) {
            ownerService.getAll().addOnCompleteListener(employeeTask -> {
                if (employeeTask.isSuccessful()) {
                    Owner owner = employeeTask.getResult().stream().filter(o -> o.getEmail().equals(email)).findFirst().orElse(null);
                    owner.setFirstName(firstName);
                    owner.setLastName(lastName);
                    owner.setAddress(address);
                    owner.setPhone(phone);
                    ownerService.update(owner);
                }
            });
        }

        String eventDetails = "Profile updated";
        Toast.makeText(this, eventDetails, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, UserProfileActivity.class);
        intent.putExtra("email", email);
        intent.putExtra("EMAIL", email);
        intent.putExtra("ForReport", "no");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        finish();
    }
}