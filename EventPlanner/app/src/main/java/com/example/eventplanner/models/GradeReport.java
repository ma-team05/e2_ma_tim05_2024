package com.example.eventplanner.models;

import java.util.Date;

public class GradeReport {
    private String id;
    private String gradeId;
    private String reason;
    private String rejectionReason;
    private Date createdOn;
    private ReportStatus status;
    private String ownerId;
    private Grade grade;
    public Owner owner;
    public GradeReport(){}
    public GradeReport(String id, String gradeId, String reason, String rejectionReason, Date createdOn, ReportStatus status, String ownerId, Grade grade) {
        this.id = id;
        this.gradeId = gradeId;
        this.reason = reason;
        this.rejectionReason = rejectionReason;
        this.createdOn = createdOn;
        this.status = status;
        this.ownerId = ownerId;
        this.grade = grade;
    }
    public Grade getGrade() {
        return grade;
    }
    public void setGrade(Grade grade) {
        this.grade = grade;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public ReportStatus getStatus() {
        return status;
    }

    public void setStatus(ReportStatus status) {
        this.status = status;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }
}
