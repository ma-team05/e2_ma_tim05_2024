package com.example.eventplanner.activities.reservations;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ReservationAdapter;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.Reservation;
import com.example.eventplanner.models.ReservationStatus;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.ReservationService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ReservationsActivity extends AppCompatActivity {
    private ReservationAdapter adapter;
    private String status = "ALL";
    private List<Reservation> reservations = new ArrayList<>();
    private UserRole userRole;
    private String userId;
    private final ReservationService service = new ReservationService();
    private List<Reservation> searchedReservations = new ArrayList<>();
    private EditText searchEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservations);
        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");
        userRole = UserRole.valueOf(intent.getStringExtra("userRole"));
        //mockReservations();
        searchedReservations.addAll(reservations);

        adapter = new ReservationAdapter(copy(reservations), this, userId, userRole);
        setSpinner();

        View searchBox = findViewById(R.id.searchBox);
        Button searchButton = findViewById(R.id.searchButton);
        searchEditText = findViewById(R.id.searchEditText);
        searchBox.setVisibility((userRole == UserRole.OWNER || userRole == UserRole.EMPLOYEE) ? View.VISIBLE : View.GONE);
        searchButton.setOnClickListener(v -> search());

        RecyclerView rv = findViewById(R.id.reservationsRecyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);
        loadReservations();
    }
    private void search() {
        String input = searchEditText.getText().toString().toLowerCase();
        searchedReservations.clear();
        for (Reservation r : reservations) {
            if (userRole == UserRole.OWNER) {
                if (r.getEmployee().getFirstName().toLowerCase().contains(input) ||
                        r.getEmployee().getLastName().toLowerCase().contains(input) ||
                        r.getEventPlanner().getFirstName().toLowerCase().contains(input) ||
                        r.getEventPlanner().getLastName().toLowerCase().contains(input) ||
                        r.getService().getName().toLowerCase().contains(input) || input=="") {
                    if (status.equals("ALL") || r.getStatus() == ReservationStatus.valueOf(status))
                        searchedReservations.add(r);
                }
            }
            if (userRole == UserRole.EMPLOYEE) {
                if (r.getEventPlanner().getFirstName().toLowerCase().contains(input) ||
                        r.getEventPlanner().getLastName().toLowerCase().contains(input) ||
                        r.getService().getName().toLowerCase().contains(input) || input=="") {
                    if (status.equals("ALL") || r.getStatus() == ReservationStatus.valueOf(status))
                        searchedReservations.add(r);
                }
            }
        }
        adapter.updateReservation(copy(searchedReservations));
    }


    private void loadReservations(){
        if(userRole==UserRole.OWNER){
            service.getAll().addOnCompleteListener(task->{
               if(task.isSuccessful()){
                   task.getResult().sort((o1, o2) -> {
                       String s1 = o1.getPackageId() == null ? "" : o1.getPackageId();
                       String s2 = o2.getPackageId() == null ? "" : o2.getPackageId();
                       return s1.compareTo(s2);
                   });
                   reservations = task.getResult();
                   searchedReservations = copy(reservations);
                   adapter.updateReservation(copy(reservations));
               }
            });
        }
        else if(userRole==UserRole.EMPLOYEE){
            service.getAllByEmployee(userId).addOnCompleteListener(task->{
                if(task.isSuccessful()){
                    task.getResult().sort((o1, o2) -> {
                        String s1 = o1.getPackageId() == null ? "" : o1.getPackageId();
                        String s2 = o2.getPackageId() == null ? "" : o2.getPackageId();
                        return s1.compareTo(s2);
                    });
                    reservations = task.getResult();
                    searchedReservations = copy(reservations);
                    adapter.updateReservation(copy(reservations));
                }
            });
        }
        else if(userRole==UserRole.EVENT_PLANNER){
            service.getAllByEventPlanner(userId).addOnCompleteListener(task->{
                if(task.isSuccessful()){
                    task.getResult().sort((o1, o2) -> {
                        String s1 = o1.getPackageId() == null ? "" : o1.getPackageId();
                        String s2 = o2.getPackageId() == null ? "" : o2.getPackageId();
                        return s1.compareTo(s2);
                    });
                    reservations = task.getResult();
                    searchedReservations = copy(reservations);
                    adapter.updateReservation(copy(reservations));
                }
            });
        }
    }
    private void setSpinner() {
        Spinner statusSpinner = findViewById(R.id.statusSpinner);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.reservation_status_array, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statusSpinner.setAdapter(spinnerAdapter);
        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedStatus = parent.getItemAtPosition(position).toString();
                handleStatusSelection(selectedStatus);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            if (data != null) {
                String packageId = data.getStringExtra("packageId");
                for(Reservation r: reservations){
                    if(r.getPackageId()==null)continue;
                    if(r.getPackageId().equals(packageId)){
                        r.setStatus(userRole==UserRole.EVENT_PLANNER ? ReservationStatus.CANCELED_BY_EVENT_PLANNER : ReservationStatus.CANCELED_BY_PUP);
                        service.update(r);
                        if(userRole==UserRole.EVENT_PLANNER)
                            new NotificationService().add(r.getEmployee().getId(), "Event planner canceled their reservation package.", NotificationType.RESERVATION);
                        else
                            new NotificationService().add(r.getEventPlanner().getId(), "Your reservation package has been canceled, you are able to leave a grade for company.", NotificationType.RESERVATION);
                    }
                }
                adapter.updateReservation(copy(reservations));
            }
        }
        if (requestCode == 2 && resultCode == RESULT_OK) {
            if (data != null) {
                String reservationId = data.getStringExtra("reservationId");
                for(Reservation r: reservations){
                    if(r.getId().equals(reservationId)){
                        r.setGraded(true);
                        service.update(r);
                        new OwnerService().getByComopanyId(r.getEmployee().getCompanyId()).addOnCompleteListener(t->{
                            if(t.isSuccessful()){
                                new NotificationService().add(t.getResult().getId(), "New grade for your company", NotificationType.GRADES);
                                adapter.updateReservation(copy(reservations));
                            }
                        });
                    }
                }


            }
        }
    }

    private void handleStatusSelection(String selectedStatus) {
        status = selectedStatus;
        Toast.makeText(this, "Currently showing reservations with status: " + selectedStatus, Toast.LENGTH_SHORT).show();
        if(selectedStatus.equals("ALL")){
            adapter.updateReservation(copy(searchedReservations));
            return;
        }
        ReservationStatus status = ReservationStatus.valueOf(selectedStatus);
        List<Reservation> res = new ArrayList<>();
        for(Reservation r: searchedReservations){
            if(r.getStatus()==status)
                res.add(r);
        }
        adapter.updateReservation(copy(res));
    }
    private List<Reservation> copy(List<Reservation> input){
        List<Reservation> ret = new ArrayList<>();
        for(Reservation r: input)
            ret.add(r);
        return ret;
    }
    public String getReservationsByPackage(String packageId){
        String ret = "";
        for(Reservation r: reservations){
            if(r.getPackageId()==null) continue;
            if(r.getPackageId().equals(packageId)) {
                ret += ret.equals("")?"":",";
                ret += r.getService().getName();
            }
        }
        return ret;
    }

    private Reservation mockReservation(String id, ReservationStatus status, String serviceName, String servicedescription, String serviceCategory, String employeeFirstName, String employeeLastName, String eventPlannerFirstName, String eventPlannerLastName, String packageId){
        Reservation r = new Reservation();
        r.setId(id);
        r.setCancellationDeadline(new Date());
        Date c = r.getCancellationDeadline();
        c.setMonth(6);
        r.setCancellationDeadline(c);
        r.setStatus(status);
        r.setFrom(new Date());
        r.setTo(new Date());
        r.setEmployeeId("12");
        r.setEventPlannerId("11");
        r.setPackageId(packageId);
        r.setServiceId("132");
        Service service2 = new Service();
        service2.setName(serviceName);
        service2.setDescription(servicedescription);
        service2.setCategory(serviceCategory);
        r.setService(service2);
        Employee employee = new Employee();
        employee.setFirstName(employeeFirstName);
        employee.setLastName(employeeLastName);
        employee.setCompanyId("QFVNleTxOAXdvrDCx7F5");
        r.setEmployee(employee);
        User user = new User();
        user.setFirstName(eventPlannerFirstName);
        user.setLastName(eventPlannerLastName);
        r.setEventPlanner(user);
        return r;
    }
    private void mockReservations(){
        reservations.clear();
        reservations.add(mockReservation("hgfchvjb",ReservationStatus.NEW, "Servis1", "OPIS servisa 1 koji je tu i najbolji servis", "Kategorija", "Milos", "Djuric", "Marko", "Jovanovic", "asgauysgd"));
        reservations.add(mockReservation("rstdfb",ReservationStatus.NEW, "Servis2", "OPIS servisa 2 koji je tu i najbolji servis", "Kategorija", "Milutin", "Djuric", "Mirko", "Jovic", null));
        reservations.add(mockReservation("retsyukbjv",ReservationStatus.FINISHED, "Servis3", "OPIS servisa 3 koji je tu i najbolji servis", "Kategorija", "Jovan", "Mikic", "Dusan", "Pilipovic", "joszmohnd3"));
        reservations.add(mockReservation("lkjcycjk",ReservationStatus.NEW, "Servis4", "OPIS servisa 4 koji je tu i najbolji servis", "Kategorija", "Slavoljub", "Simic", "Stefan", "Stefanovic", null));
        reservations.add(mockReservation("klovicuyets",ReservationStatus.NEW, "Servis5", "OPIS servisa 5 koji je tu i najbolji servis", "Kategorija", "Kornelije", "Knezevic", "Matija", "Karadzic", "asgauysgd"));
        reservations.add(mockReservation("oiuyoiuyctv",ReservationStatus.FINISHED, "Servis6", "OPIS servisa 6 koji je tu i najbolji servis", "Kategorija", "Jelena", "Stanimirovic", "Jelisaveta", "Vukovic", "asgauysgd"));
        reservations.add(mockReservation("iouytyrewret",ReservationStatus.NEW, "Servis7", "OPIS servisa 7 koji je tu i najbolji servis", "Kategorija", "Igor", "Stojisavljevic", "Sara", "Perazic", "ukxgfndri3l"));
        reservations.add(mockReservation("ywtdqw",ReservationStatus.FINISHED, "Servis8", "OPIS servisa 8 koji je tu i najbolji servis", "Kategorija", "Mihajlo", "Prole", "Ivan", "Pristajko", "asgauysgd"));
        reservations.add(mockReservation("rrrrrrrrrrrrrr",ReservationStatus.CANCELED_BY_PUP, "Servis9", "OPIS servisa 9 koji je tu i najbolji servis", "Kategorija", "Ksenija", "Djuricic", "Dusan", "Pilipovic", "lowxh"));
        reservations.add(mockReservation("oiituy",ReservationStatus.CANCELED_BY_EVENT_PLANNER, "Servis10", "OPIS servisa 10 koji je tu i najbolji servis", "Kategorija", "Simeon", "Stajic", "Marko", "Jovanovic", "asgauysgd"));
        reservations.add(mockReservation("gyuwiqbdbx",ReservationStatus.ACCEPTED, "Servis11", "OPIS servisa 11 koji je tu i najbolji servis", "Kategorija", "Nebojsa", "Viskovic", "Marko", "Jovanovic", "lowxh"));
        reservations.add(mockReservation("yefibwururrux",ReservationStatus.ACCEPTED, "Servis12", "OPIS servisa 12 koji je tu i najbolji servis", "Kategorija", "Marina", "Bercek", "Marko", "Jovanovic", "asgauysgd"));

        reservations.sort((o1, o2) -> {
            String s1 = o1.getPackageId() == null ? "" : o1.getPackageId();
            String s2 = o2.getPackageId() == null ? "" : o2.getPackageId();
            return s1.compareTo(s2);
        });
        //adapter.updateReservation(reservations);
    }
}