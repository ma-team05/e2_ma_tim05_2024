package com.example.eventplanner.database.repository;

import android.util.Log;

import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.services.PackageService;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PackageRepository {
    private final FirebaseFirestore db;

    public PackageRepository(){
        db = FirebaseFirestore.getInstance();
    }

    public Task<Package> get(String id) {
        DocumentReference packageRef = db.collection("packages").document(id);
        return packageRef.get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            return document.toObject(Package.class);
                        } else {
                            throw new Exception("Package with ID " + id + " not found");
                        }
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }

    public interface OnDataFetchListener {
        void onSuccess(List<Package> events);
        void onFailure(String errorMessage);
    }

    public void getAll(final PackageRepository.OnDataFetchListener listener){
        db.collection("packages")

                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Package> productList = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Package product = documentSnapshot.toObject(Package.class);
                        productList.add(product);
                    }
                    // Process the list of products with categories here
                    //processProductsWithCategories(productList);

                    listener.onSuccess(productList);
                })
                .addOnFailureListener(e -> {
                    Log.w("GET PRODUCTS FAILED", "Error getting products", e);
                    listener.onFailure(e.getMessage());
                });
    }

    public void create(Package package1){
        db.collection("packages")
                .add(package1)
                .addOnSuccessListener(documentReference -> {
                    String packageId = documentReference.getId();
                    package1.setId(packageId);
                    documentReference.update("id", packageId)
                            .addOnSuccessListener(aVoid -> {
                                Log.d("Package UPDATE", "Document updated with product id: " + packageId);
                            })
                            .addOnFailureListener(e -> {
                                Log.w("Package UPDATE FAILED", "Error updating document", e);
                            });
                })
                .addOnFailureListener(e ->{
                    Log.w("Package CREATION FAILED", "Error adding document", e);
                });

    }

    public void update(Package service){
        String packageId = service.getId();
        if (packageId != null) {

            DocumentReference productRef = db.collection("packages").document(packageId);

            productRef.set(service)
                    .addOnSuccessListener(aVoid -> {
                        Log.d("Package UPDATE", "Product updated successfully: " + packageId);
                    })
                    .addOnFailureListener(e -> {
                        Log.w("Package UPDATE FAILED", "Error updating product: " + packageId, e);
                    });
        } else {
            Log.w("Package UPDATE FAILED", "Product ID is null");
        }

    }

    public void deleteLogically(Package service){
        String packageId = service.getId();
        service.setDeleted(true); // logical deletition
        if (packageId != null) {

            DocumentReference productRef = db.collection("packages").document(packageId);

            productRef.set(service)
                    .addOnSuccessListener(aVoid -> {
                        Log.d("Package UPDATE", "Product updated successfully: " + packageId);
                    })
                    .addOnFailureListener(e -> {
                        Log.w("Package UPDATE FAILED", "Error updating product: " + packageId, e);
                    });
        } else {
            Log.w("Package UPDATE FAILED", "Product ID is null");
        }

    }

    public void getUndeletedOfPUP(String userId ,final OnDataFetchListener listener){
        db.collection("packages")
                .whereEqualTo("companyId", userId)
                .whereEqualTo("deleted", false)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Package> packagesList = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Package service = documentSnapshot.toObject(Package.class);
                        packagesList.add(service);
                    }
                    // Process the list of products with categories here
                    processPackagesWithCategories(packagesList);
                    listener.onSuccess(packagesList);
                })
                .addOnFailureListener(e -> {
                    Log.w("GET Products FAILED", "Error getting products", e);
                    listener.onFailure(e.getMessage());
                });
    }

    private void processPackagesWithCategories(List<Package> serviceList) {
        for (Package service : serviceList) {
            String categoryId = service.getCategoryId();
            if (categoryId != null) {

                db.collection("categories")
                        .document(categoryId)
                        .get()
                        .addOnSuccessListener(documentSnapshot -> {
                            if (documentSnapshot.exists()) {
                                Category category = documentSnapshot.toObject(Category.class);
                                // Set the category object to the product

                                assert category != null;
                                service.setCategory(category.getName());
                                //find subcategory

                                Log.d("Service WITH CATEGORY", service.toString());
                            } else {
                                Log.w("GET CATEGORY FAILED", "Category document not found for product: " + service.getId());
                            }
                        })
                        .addOnFailureListener(e -> {
                            Log.w("GET CATEGORY FAILED", "Error getting category for product: " + service.getId(), e);
                        });
            } else {
                Log.w("GET CATEGORY FAILED", "Category ID is null for product: " + service.getId());
            }
        }
    }

    public void getUndeletedWithProduct(String userId,String productId,final OnDataFetchListener listener){
        db.collection("packages")
                .whereEqualTo("companyId", userId)
                .whereEqualTo("deleted", false)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Package> packagesList = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Package pkg = documentSnapshot.toObject(Package.class);
                        // Check if the package contains the specified productId
                        if (pkg.getProductIds() != null && pkg.getProductIds().contains(productId)) {
                            packagesList.add(pkg);
                        }
                    }
                    // Process the list of products with categories here
                    processPackagesWithCategories(packagesList);
                    listener.onSuccess(packagesList);
                })
                .addOnFailureListener(e -> {
                    Log.w("GET Products FAILED", "Error getting products", e);
                    listener.onFailure(e.getMessage());
                });
    }

    public void getUndeletedWithService(String userId,String serviceId,final OnDataFetchListener listener){
        db.collection("packages")
                .whereEqualTo("companyId", userId)
                .whereEqualTo("deleted", false)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Package> packagesList = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Package pkg = documentSnapshot.toObject(Package.class);
                        // Check if the package contains the specified productId
                        if (pkg.getServiceIds() != null && pkg.getServiceIds().contains(serviceId)) {
                            packagesList.add(pkg);
                        }
                    }
                    // Process the list of products with categories here
                    processPackagesWithCategories(packagesList);
                    listener.onSuccess(packagesList);
                })
                .addOnFailureListener(e -> {
                    Log.w("GET Products FAILED", "Error getting products", e);
                    listener.onFailure(e.getMessage());
                });
    }

    public void updatePackagesInFirestore(List<Package> updatedPackages) {
        // Create a write batch
        WriteBatch batch = db.batch();

        for (Package pkg : updatedPackages) {
            // Get the document reference for this package
            DocumentReference packageRef = db.collection("packages").document(pkg.getId());

            // Update the package in the batch
            batch.set(packageRef, pkg);
        }

        // Commit the batch
        batch.commit().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d("BATCH", "Batch update successful!");
            } else {
                Log.w("BATCH", "Batch update failed.", task.getException());
            }
        });
    }
}
