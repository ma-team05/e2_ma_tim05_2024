package com.example.eventplanner.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.ReportingGradeActivity;
import com.example.eventplanner.models.Grade;
import com.example.eventplanner.models.GradeReport;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.GradeReportService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.UserService;

import java.util.List;

public class GradeAdapter extends RecyclerView.Adapter<GradeAdapter.GradeViewHolder> {

    private List<Grade> gradeList;
    private boolean isOwner;
    private String userId;
    private Activity parentActivity;

    public GradeAdapter(List<Grade> gradeList, boolean isOwner, String userId, Activity parentActivity) {
        this.gradeList = gradeList;
        this.isOwner = isOwner;
        this.userId = userId;
        this.parentActivity = parentActivity;
    }

    @NonNull
    @Override
    public GradeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grade, parent, false);
        return new GradeViewHolder(view, isOwner, userId);
    }

    @Override
    public void onBindViewHolder(@NonNull GradeViewHolder holder, int position) {
        Grade grade = gradeList.get(position);
        holder.bind(grade);
    }

    @Override
    public int getItemCount() {
        return gradeList.size();
    }

    public void updateGrades(List<Grade> newGrades) {
        gradeList.clear();
        gradeList.addAll(newGrades);
        notifyDataSetChanged();
    }

    class GradeViewHolder extends RecyclerView.ViewHolder {
        private GradeReportService gradeReportService = new GradeReportService();
        private UserService userService = new UserService();
        private CompanyService companyService = new CompanyService();
        private TextView ratingText;
        private TextView commentText;
        private TextView dateText;
        private TextView reportsText;
        private TextView nameText;
        private TextView companyName;
        private Button reportButton;
        private boolean isOwner;
        private String userId;

        public GradeViewHolder(@NonNull View itemView, boolean isOwner, String userId) {
            super(itemView);
            nameText = itemView.findViewById(R.id.nameText);
            ratingText = itemView.findViewById(R.id.ratingText);
            commentText = itemView.findViewById(R.id.commentText);
            dateText = itemView.findViewById(R.id.dateText);
            reportsText = itemView.findViewById(R.id.reportsText);
            reportButton = itemView.findViewById(R.id.reportButton);
            companyName = itemView.findViewById(R.id.companyName);
            this.isOwner = isOwner;
            this.userId = userId;
        }

        private String formatDate(String input){
            if (input == null || input.length() < 19) {
                throw new IllegalArgumentException("Input string must be at least 19 characters long.");
            }
            String part1 = input.substring(4, 11); // From 5th char (index 4) to 11th char (index 10)
            String part2 = input.substring(input.length() - 4); // Last 4 chars
            String part3 = input.substring(10, 19); // From 12th char (index 11) to 19th char (index 18)

            return part1 + part2 + part3;
        }
        public void bind(Grade grade) {
            ratingText.setText("Grade: " + String.valueOf(grade.getRating()));
            commentText.setText(grade.getComment());
            dateText.setText(formatDate(grade.getCreatedOn().toString()));
            userService.get(grade.getUserId()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    nameText.setText(task.getResult().getFirstName() + " " + task.getResult().getLastName());
                    gradeReportService.getAllByGrade(grade.getId()).addOnCompleteListener(task2 -> {
                        if (task2.isSuccessful()) {
                            List<GradeReport> reports = task2.getResult();
                            boolean visible = false;
                            reportsText.setText("Reports: " + reports.size());
                            if (isOwner) {
                                visible = true;
                                for (GradeReport report : reports) {
                                    if (report.getOwnerId().equals(userId)) {
                                        visible = false;
                                        break;
                                    }
                                }
                            }
                            reportButton.setVisibility(visible ? View.VISIBLE : View.GONE);

                            reportButton.setOnClickListener(v -> {
                                Intent intent = new Intent(itemView.getContext(), ReportingGradeActivity.class);
                                intent.putExtra("gradeId", grade.getId());
                                intent.putExtra("ownerId", userId);
                                parentActivity.startActivityForResult(intent, 1); // Request code 1
                            });
                            companyService.get(grade.getCompanyId()).addOnCompleteListener(task3->{
                               if(task3.isSuccessful())
                                   companyName.setText(task3.getResult().getName());
                            });
                        }
                    });
                }
            });
        }
    }
}
