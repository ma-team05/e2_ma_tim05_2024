package com.example.eventplanner.activities;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.EventActivity;
import com.example.eventplanner.services.EventService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EventActivitiesCreationActivity extends AppCompatActivity {

    private EventService eventService;

    private Event event;

    Date from;
    Date to;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_event_activities_creation);

        eventService = new EventService(new EventRepository());

        Intent intent = getIntent();
        String eventId = intent.getStringExtra("EVENT");

        eventService.getById(eventId, new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                EventActivitiesCreationActivity.this.event = events.get(0);
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });

        Button createButton = findViewById(R.id.createButton);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields();
            }
        });

        ImageButton startTimeButton = findViewById(R.id.timeButton);
        ImageButton endTimeButton = findViewById(R.id.endTimeButton);

        startTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog();
            }
        });

        endTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEndTimePickerDialog();
            }
        });
    }

    private void showTimePickerDialog() {
        // Get current time
        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        // Create TimePickerDialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        from = new Date();
                        from.setHours(hourOfDay);
                        from.setMinutes(minute);
                    }
                }, hour, minute, false);

        timePickerDialog.show();
    }

    private void showEndTimePickerDialog() {
        // Get current time
        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        // Create TimePickerDialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        to = new Date();
                        to.setHours(hourOfDay);
                        to.setMinutes(minute);
                    }
                }, hour, minute, false);

        timePickerDialog.show();
    }

    private boolean validateFields() {
        boolean isValid = true;

        EditText nameEditText, descriptionEditText, locationEditText;
        String field;

        nameEditText = findViewById(R.id.name);
        field = nameEditText.getText().toString().trim();
        if (TextUtils.isEmpty(field)) {
            nameEditText.setError("Please enter a name");
            nameEditText.requestFocus();
            isValid = false;
        }

        descriptionEditText = findViewById(R.id.description);
        field = descriptionEditText.getText().toString().trim();
        if (TextUtils.isEmpty(field)) {
            descriptionEditText.setError("Please enter a description");
            descriptionEditText.requestFocus();
            isValid = false;
        }

        locationEditText = findViewById(R.id.location);
        field = descriptionEditText.getText().toString().trim();
        if (TextUtils.isEmpty(field)) {
            locationEditText.setError("Please enter a location");
            locationEditText.requestFocus();
            isValid = false;
        }

        if(from == null) {
            Toast.makeText(this, "You must select start time", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(to == null) {
            Toast.makeText(this, "You must select end time", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(from.after(to)) {
            Toast.makeText(this, "Start time can't be after end time", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if(isValid) {
            createActivity();
        }

        return isValid;
    }

    private void createActivity() {
        EditText nameEditText = findViewById(R.id.name);
        EditText descriptionEditText = findViewById(R.id.description);
        EditText locationEditText = findViewById(R.id.location);

        String name = nameEditText.getText().toString().trim();
        String description = descriptionEditText.getText().toString().trim();
        String location = locationEditText.getText().toString().trim();

        EventActivity activity = new EventActivity();
        activity.setName(name);
        activity.setDescription(description);
        activity.setLocation(location);
        activity.setFrom(from);
        activity.setTo(to);

        event.addActivity(activity);
        eventService.update(event);

        String activityDetails = "Activity added:\n" +
                "Name: " + activity.getName();
        Toast.makeText(this, activityDetails, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, EventActivitiesActivity.class);
        intent.putExtra("EVENT", event.getId());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        finish();
    }
}