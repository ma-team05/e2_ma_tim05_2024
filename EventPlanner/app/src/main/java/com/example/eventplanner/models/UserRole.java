package com.example.eventplanner.models;

public enum UserRole {
    OWNER,
    EMPLOYEE,
    EVENT_PLANNER,
    ADMIN
}
