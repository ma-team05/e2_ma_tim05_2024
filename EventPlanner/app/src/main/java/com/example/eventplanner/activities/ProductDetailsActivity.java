package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.ProductRepository;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.ProductService;
import com.example.eventplanner.services.UserService;

import java.util.List;

public class ProductDetailsActivity extends AppCompatActivity {
    private User user;
    private Product product;
    private final ProductService productService = new ProductService(new ProductRepository());
    private final UserService userService = new UserService();
    private int currentIndex;
    private String selectedEventId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_product_details);

        Intent intent = getIntent();
        String email = intent.getStringExtra("EMAIL");
        String productId = intent.getStringExtra("PRODUCT");
        selectedEventId = (String) getIntent().getSerializableExtra("EVENT");
        userService.getByEmail(email).addOnCompleteListener(this, task->{
            if(task.isSuccessful()){
                user = task.getResult();
            }

            productService.getAll(new ProductService.OnDataFetchListener() {
                @Override
                public void onSuccess(List<Product> products) {
                    product = products.stream().filter(p -> p.getId().equals(productId)).findFirst().orElse(null);

                    bind();
                }

                @Override
                public void onFailure(String errorMessage) {
                }
            });
        });
    }

    public void bind() {
        currentIndex = 0;

        ImageButton btPrevious, btNext, btnFav, btnUnFav, btnCompanyInfo, btnChat, btnBuy;
        ImageSwitcher productImageSwitcher;
        TextView productName, productCategory, productSubcategory, productDescription,
                productPrice, productDiscount, productDiscountPrice, productAvailable, productVisibility, porductEventType;

        btPrevious = findViewById(R.id.bt_previous);
        btNext = findViewById(R.id.bt_next);
        btnFav = findViewById(R.id.btn_fav);
        btnUnFav = findViewById(R.id.btn_unfav);
        btnCompanyInfo = findViewById(R.id.btn_company_info);
        btnChat = findViewById(R.id.btn_chat);
        btnBuy = findViewById(R.id.btn_buy);
        productImageSwitcher = findViewById(R.id.product_image_switcher);
        productImageSwitcher.setFactory(() -> {
            ImageView imageView = new ImageView(ProductDetailsActivity.this);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            return imageView;
        });
        productName = findViewById(R.id.product_name);
        productCategory = findViewById(R.id.product_category);
        productSubcategory = findViewById(R.id.product_subcategory);
        productDescription = findViewById(R.id.product_description);
        productPrice = findViewById(R.id.product_price);
        productDiscount = findViewById(R.id.product_discount);
        productDiscountPrice = findViewById(R.id.product_discount_price);
        productAvailable = findViewById(R.id.product_available);
        productVisibility = findViewById(R.id.product_visibility);
        porductEventType = findViewById(R.id.product_event_type);

        productName.setText(product.getName());
        productCategory.setText(product.getCathegory());
        productSubcategory.setText(product.getSubCathegory());
        productDescription.setText(product.getDescription());
        productPrice.setText(String.valueOf(product.getPrice()));
        productDiscount.setText(String.valueOf(product.getDiscount()));
        productDiscountPrice.setText(String.valueOf(product.getPrice() - product.getDiscount()));
        porductEventType.setText(product.getEventType());
        productAvailable.setText(product.isAvailable() ? "Yes" : "No");
        productVisibility.setText(product.isVisible() ? "Yes" : "No");


        if (product.getImages() != null && !product.getImages().isEmpty()) {
            productImageSwitcher.setImageResource(product.getImages().get(0));
        } else {

            productImageSwitcher.setImageResource(R.drawable.default_image);
        }

        if (user.getFavouriteProducts().contains(product.getId())) {
            btnUnFav.setVisibility(View.VISIBLE);
            btnFav.setVisibility(View.GONE);
        } else {
            btnFav.setVisibility(View.VISIBLE);
            btnUnFav.setVisibility(View.GONE);
        }

        btNext.setOnClickListener(v -> {
            //int currentIndex = productImageSwitcher.getDisplayedChild();
            if (currentIndex < product.getImages().size() - 1) {
                currentIndex++;
                productImageSwitcher.setImageResource(product.getImages().get(currentIndex));
            }
        });

        btPrevious.setOnClickListener(v -> {

            //int currentIndex = productImageSwitcher.getDisplayedChild();
            if (currentIndex > 0) {
                currentIndex--;

                productImageSwitcher.setImageResource(product.getImages().get(currentIndex));
            }
        });

        btnFav.setOnClickListener(v -> {
            user.addFavouriteProduct(product.getId());
            userService.update(user);
            btnFav.setVisibility(View.GONE);
            btnUnFav.setVisibility(View.VISIBLE);
        });

        btnUnFav.setOnClickListener(v -> {
            user.removeFavouriteProduct(product.getId());
            userService.update(user);
            btnFav.setVisibility(View.VISIBLE);
            btnUnFav.setVisibility(View.GONE);
        });

        btnCompanyInfo.setOnClickListener(v -> {
            Intent createIntent = new Intent(ProductDetailsActivity.this, CompanyDetailsActivity.class);
            createIntent.putExtra("EMAIL", user.getEmail());
            createIntent.putExtra("COMPANY", product.getCompanyId());
            this.startActivity(createIntent);
        });

        btnChat.setOnClickListener(v -> {
            Intent createIntent = new Intent(ProductDetailsActivity.this, StartChatOwnerActivity.class);
            createIntent.putExtra("EMAIL", user.getEmail());
            createIntent.putExtra("email", user.getEmail());
            createIntent.putExtra("COMPANY", product.getCompanyId());
            this.startActivity(createIntent);
        });

        btnBuy.setOnClickListener(v -> {
            Intent createIntent = new Intent(ProductDetailsActivity.this, BuyProductActivity.class);
            createIntent.putExtra("EMAIL", user.getEmail());
            createIntent.putExtra("COMPANY", product.getCompanyId());
            createIntent.putExtra("EVENT", selectedEventId);
            createIntent.putExtra("PRODUCT_ID", product.getId());
            this.startActivity(createIntent);
        });

        if(!product.isAvailable()) {
            btnBuy.setVisibility(View.GONE);
        }

    }
}