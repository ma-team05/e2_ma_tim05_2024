package com.example.eventplanner.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.GradingCompanyActivity;
import com.example.eventplanner.activities.RejectingGradeReportActivity;
import com.example.eventplanner.activities.ReportingGradeActivity;
import com.example.eventplanner.activities.UserProfileActivity;
import com.example.eventplanner.models.Grade;
import com.example.eventplanner.models.GradeReport;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.ReportStatus;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.GradeReportService;
import com.example.eventplanner.services.GradeService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.UserService;

import java.util.HashMap;
import java.util.List;

public class GradeReportAdapter  extends RecyclerView.Adapter<GradeReportAdapter.GradeReportViewHolder> {

    private List<GradeReport> reports;
    private Activity parentActivity;
    public GradeReportAdapter(List<GradeReport> reports, Activity p) {
        this.reports = reports;
        this.parentActivity = p;
    }

    @NonNull
    @Override
    public GradeReportAdapter.GradeReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grade_report, parent, false);
        return new GradeReportAdapter.GradeReportViewHolder(view, parentActivity);
    }

    @Override
    public void onBindViewHolder(@NonNull GradeReportAdapter.GradeReportViewHolder holder, int position) {
        GradeReport grade = reports.get(position);
        holder.bind(grade);
    }

    @Override
    public int getItemCount() {
        return reports.size();
    }

    public void updateGradeReports(List<GradeReport> newGrades) {
        reports.clear();
        reports.addAll(newGrades);
        notifyDataSetChanged();
    }

    class GradeReportViewHolder extends RecyclerView.ViewHolder {

        private TextView gradeRating;
        private TextView gradeComment;
        private TextView dateText;
        private TextView statusText;
        private TextView reasonText;
        private TextView ownerText;
        private Button rejectButton;
        private Button acceptButton;
        private Activity parentActivity;

        public GradeReportViewHolder(@NonNull View itemView, Activity parentActivity) {
            super(itemView);
            this.parentActivity = parentActivity;
            rejectButton = itemView.findViewById(R.id.rejectButton);
            acceptButton = itemView.findViewById(R.id.acceptButton);
            gradeRating= itemView.findViewById(R.id.gradeRating);
            gradeComment= itemView.findViewById(R.id.gradeComment);
            statusText= itemView.findViewById(R.id.statusText);
            dateText= itemView.findViewById(R.id.dateText);
            ownerText = itemView.findViewById(R.id.ownerText);
            reasonText = itemView.findViewById(R.id.reasonText);
        }

        private String formatDate(String input){
            if (input == null || input.length() < 19) {
                throw new IllegalArgumentException("Input string must be at least 19 characters long.");
            }
            String part1 = input.substring(4, 11); // From 5th char (index 4) to 11th char (index 10)
            String part2 = input.substring(input.length() - 4); // Last 4 chars
            String part3 = input.substring(10, 19); // From 12th char (index 11) to 19th char (index 18)

            return part1 + part2 + part3;
        }
        public void bind(GradeReport report) {
            gradeRating.setText(String.valueOf(report.getGrade().getRating()));
            ownerText.setOnClickListener(v->showOwnersProfile(report.getOwnerId()));
            gradeComment.setText(report.getGrade().getComment());
            statusText.setText(String.valueOf(report.getStatus()));
            reasonText.setText(report.getReason());
            dateText.setText(formatDate(report.getCreatedOn().toString()));
            ownerText.setText(report.owner.getFirstName()+ " "+ report.owner.getLastName());
            acceptButton.setVisibility(report.getStatus()== ReportStatus.REPORTED ? View.VISIBLE : View.GONE);
            rejectButton.setVisibility(report.getStatus()== ReportStatus.REPORTED ? View.VISIBLE : View.GONE);
            acceptButton.setOnClickListener(v->acceptReport(report));
            rejectButton.setOnClickListener(v->rejectReport(report));
        }
        private void showOwnersProfile(String ownerId){
            UserService userService = new UserService();
            userService.get(ownerId).addOnCompleteListener(task->{
               if(task.isSuccessful()) {
                   Intent intent = new Intent(itemView.getContext(), UserProfileActivity.class);
                   intent.putExtra("EMAIL", task.getResult().getEmail());
                   intent.putExtra("ForReport", "no");
                   parentActivity.startActivity(intent);
               }
            });
        }
        private void rejectReport(GradeReport report){
            Intent intent = new Intent(itemView.getContext(), RejectingGradeReportActivity.class);
            intent.putExtra("reportId", report.getId());
            intent.putExtra("ownerId", report.getOwnerId());
            parentActivity.startActivityForResult(intent, 1);
        }
        private void acceptReport(GradeReport report){
            GradeReportService reportService = new GradeReportService();
            GradeService gradeService = new GradeService();
            reportService.AcceptGradeReport(report).addOnCompleteListener(task->{
               if(task.isSuccessful()){
                   gradeService.deleteGrade(report.getGradeId()).addOnCompleteListener(task2->{
                      report.setStatus(ReportStatus.ACCEPTED);
                      acceptButton.setVisibility(View.GONE);
                      rejectButton.setVisibility(View.GONE);
                      statusText.setText("ACCEPTED");
                   });
               }
            });
        }
    }
}

