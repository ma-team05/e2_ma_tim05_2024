package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.ProductRepository;
import com.example.eventplanner.database.repository.ReportRepository;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.Report;

import java.util.List;

public class ReportService {
    private ReportRepository reportRepository;

    public ReportService(){
        this.reportRepository = new ReportRepository();
    }

    public interface OnDataFetchListener {
        void onSuccess(List<Report> reports);
        void onFailure(String errorMessage);
    }

    public void create(Report report){

        reportRepository.create(report);
    }

    public void update(Report report){
        reportRepository.update(report);
    }

    public void getAll(final OnDataFetchListener listener){
        reportRepository.getAll(new ReportRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Report> reports) {
                listener.onSuccess(reports);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void getExisting(String reporterId, String reportedId, final OnDataFetchListener listener){
        reportRepository.getExisting(reporterId,reportedId,new ReportRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Report> reports) {
                listener.onSuccess(reports);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }


}
