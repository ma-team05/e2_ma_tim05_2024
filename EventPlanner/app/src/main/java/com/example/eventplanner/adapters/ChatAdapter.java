package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.ChatActivity;
import com.example.eventplanner.activities.EventActivitiesActivity;
import com.example.eventplanner.activities.ServiceDetailsActivity;
import com.example.eventplanner.models.Chat;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.UserService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder>{
    private Context context;
    private List<Chat> chatList;

    private String email;
    private String userId;


    public ChatAdapter(Context context,List<Chat> chatList, String email, String userId) {
        this.context = context;
        this.chatList = chatList;
        this.email = email;
        this.userId = userId;


        Log.i("Frag","Napravio adapter");
    }

    @NonNull
    @Override
    public ChatAdapter.ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_card, parent, false);
        return new ChatAdapter.ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatAdapter.ChatViewHolder holder, int position) {
        Chat chat = chatList.get(position);
        holder.bind(chat);
    }



    @Override
    public int getItemCount() {
        return chatList.size();
    }



    public class ChatViewHolder extends RecyclerView.ViewHolder {
        ImageButton openButton;
        TextView to;
        TextView toBold;
        private Chat currentChat;  // Changed to Service
        private int currentIndex;
        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);

            to = itemView.findViewById(R.id.textName);
            toBold = itemView.findViewById(R.id.textNameBold);
            openButton = itemView.findViewById(R.id.btnDelete);
        }

        public void bind(Chat chat) {  // Changed to Service
            currentChat = chat;
            boolean ind = chat != null;
            currentIndex = 0;
            Log.i("binder", ind ? "nije null" : "null je");
            boolean isParticipant1 =  chat.getParticipant1().equals(email);
            String recepient = isParticipant1 ? chat.getParticipant2() : chat.getParticipant1();

            boolean isRead = isParticipant1 ? chat.isReadAllMessages1() : chat.isReadAllMessages2();
            if(!isRead) {
                toBold.setText(recepient);
                to.setVisibility(View.GONE);
            }
            else {
                to.setText(recepient);
                toBold.setVisibility(View.GONE);
            }

            openButton.setOnClickListener(v -> {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("email", email);
                intent.putExtra("userId", userId);
                intent.putExtra("EMAIL", email);
                intent.putExtra("CHAT", currentChat.getId());
                context.startActivity(intent);
            });
        }
    }
}
