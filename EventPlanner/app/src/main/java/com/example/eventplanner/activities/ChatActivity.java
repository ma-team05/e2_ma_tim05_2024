package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ChatAdapter;
import com.example.eventplanner.adapters.ChatMessageAdapter;
import com.example.eventplanner.models.Chat;
import com.example.eventplanner.models.ChatMessage;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.ChatService;
import com.example.eventplanner.services.UserService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ChatActivity extends AppCompatActivity {
    private ChatService chatService = new ChatService();
    private ChatMessageAdapter chatMessageAdapter;

    private RecyclerView recyclerView;
    private String email;
    private String userId;
    private Chat chat;
    private UserService userService = new UserService();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_chat);

        Intent intent = getIntent();
        email = intent.getStringExtra("email");
        userId = intent.getStringExtra("userId");
        String chatId = intent.getStringExtra("CHAT");

        chatService.getAll(new ChatService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Chat> chats) {

                chat = chats.stream().filter(c -> c.getId().equals(chatId)).findFirst().orElse(null);
                chat.read(email);
                chatService.update(chat, false);

                TextView title = findViewById(R.id.title);
                String titleEmail = chat.getParticipant1().equals(email) ? chat.getParticipant2() : chat.getParticipant1();
                title.setText(titleEmail);

                initializeChatMessages(chat.getMessages());

                FloatingActionButton addButton = findViewById(R.id.fab);

                addButton.setOnClickListener(v -> {
                    Intent createIntent = new Intent(ChatActivity.this, SendMessageActivity.class);
                    createIntent.putExtra("EMAIL", email);
                    createIntent.putExtra("email", email);
                    createIntent.putExtra("CHAT", chatId);
                    createIntent.putExtra("userId", userId);

                    String recepient = chat.getParticipant1().equals(email) ? chat.getParticipant2() : chat.getParticipant1();
                    createIntent.putExtra("RECEPIENT_EMAIL", recepient);
                    ChatActivity.this.startActivity(createIntent);
                });

                userService.get(userId).addOnSuccessListener(user ->{
                    if(user.getRole() == UserRole.OWNER){
                        title.setOnClickListener(v ->{
                            Intent intent = new Intent(ChatActivity.this, UserProfileActivity.class);
                            intent.putExtra("EMAIL",titleEmail);
                            intent.putExtra("ForReport", "yes");
                            intent.putExtra("ReportingId", userId);
                            ChatActivity.this.startActivity(intent);
                        });
                    }
                });

            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });
    }

    private void initializeChatMessages(List<ChatMessage> messages){
        Collections.sort(messages, new Comparator<ChatMessage>() {
            @Override
            public int compare(ChatMessage m1, ChatMessage m2) {
                return m1.getDate().compareTo(m2.getDate());
            }
        });

        recyclerView = findViewById(R.id.chat_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        chatMessageAdapter = new ChatMessageAdapter(this, messages, email);
        recyclerView.setAdapter(chatMessageAdapter);

        if (!messages.isEmpty()) {
            recyclerView.scrollToPosition(messages.size() - 1);

        }
    }
}