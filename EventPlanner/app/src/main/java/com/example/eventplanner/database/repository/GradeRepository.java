package com.example.eventplanner.database.repository;

import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Grade;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class GradeRepository {
    private FirebaseFirestore db;
    private CollectionReference gradesCollection;
    public GradeRepository(){
        db = FirebaseFirestore.getInstance();
        gradesCollection = db.collection("grades");
    }
    public Task<Void> saveGrade(Grade grade){
        DocumentReference newGradeRef = gradesCollection.document();
        grade.setId(newGradeRef.getId());
        return newGradeRef.set(grade);
    }
    public Task<Void> deleteGrade(String gradeId){
        DocumentReference gradeRef = gradesCollection.document(gradeId);
        return gradeRef.delete();
    }
    public Task<List<Grade>> getAllByCompany(String companyId){
        return gradesCollection.get()
                .continueWith(task -> {
                    List<Grade> grades = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Grade grade = document.toObject(Grade.class);
                        if(grade.getCompanyId().equals(companyId))
                            grades.add(grade);
                    }
                    return grades;
                });
    }
    public Task<List<Grade>> getAllByEventPlanner(String userId){
        return gradesCollection.get()
                .continueWith(task -> {
                    List<Grade> grades = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Grade grade = document.toObject(Grade.class);
                        if(grade.getUserId().equals(userId))
                            grades.add(grade);
                    }
                    return grades;
                });
    }
    public Task<List<Grade>> getAll(){
        return gradesCollection.get()
                .continueWith(task -> {
                    List<Grade> grades = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Grade grade = document.toObject(Grade.class);
                        grades.add(grade);
                    }
                    return grades;
                });
    }
    public Task<Grade> getById(String id) {
        DocumentReference gradeRef = gradesCollection.document(id);
        return gradeRef.get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            return document.toObject(Grade.class);
                        } else {
                            throw new Exception("Grade with ID " + id + " not found");
                        }
                    } else {
                        throw task.getException(); // Propagate the exception
                    }
                });
    }

}
