package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.activities.BudgetPlanActivity;
import com.example.eventplanner.activities.EditEventActivity;
import com.example.eventplanner.activities.EventActivitiesActivity;
import com.example.eventplanner.activities.GuestListActivity;
import com.example.eventplanner.activities.ProductDetailsActivity;
import com.example.eventplanner.activities.ServiceDetailsActivity;
import com.example.eventplanner.activities.ServiceSearchActivity;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.example.eventplanner.R;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.UserService;

public class ProductResultsAdapter extends  RecyclerView.Adapter<ProductResultsAdapter.ProductViewHolder1>{

    private Context context;
    private List<Product> productList;
    private User user;

    private UserService userService = new UserService();

    private int currentIndex;

    private String selectedEventId = "";

    public ProductResultsAdapter(Context context,List<Product> productList, User user, String selectedEventId) {
        this.context = context;
        this.productList = productList;
        this.user = user;
        this.selectedEventId = selectedEventId;


        Log.i("Frag","Napravio adapter");
    }

    @NonNull
    @Override
    public ProductViewHolder1 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_card_result, parent, false);
        return new ProductViewHolder1(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder1 holder, int position) {
        Product product = productList.get(position);
        holder.bind(product);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ProductViewHolder1 extends RecyclerView.ViewHolder {

        ImageButton btPrevious, btNext, editProductBtn, deleteProductBtn, btnFav, btnUnFav, btnInfo;
        ImageSwitcher productImageSwitcher;
        TextView productName, productCategory, productSubcategory, productDescription,
                productPrice, productDiscount, productDiscountPrice, productAvailable, productVisibility, porductEventType;
        private Product currentProduct;
        public ProductViewHolder1(@NonNull View itemView) {
            super(itemView);

            btPrevious = itemView.findViewById(R.id.bt_previous);
            btNext = itemView.findViewById(R.id.bt_next);
            btnFav = itemView.findViewById(R.id.btn_fav);
            btnUnFav = itemView.findViewById(R.id.btn_unfav);
            btnInfo = itemView.findViewById(R.id.btn_info);
            editProductBtn = itemView.findViewById(R.id.edit_product_btn);
            deleteProductBtn = itemView.findViewById(R.id.delete_product_btn);
            productImageSwitcher = itemView.findViewById(R.id.product_image_switcher);
            productImageSwitcher.setFactory(() -> {
                ImageView imageView = new ImageView(itemView.getContext());
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                return imageView;
            });
            productName = itemView.findViewById(R.id.product_name);
            productCategory = itemView.findViewById(R.id.product_category);
            productSubcategory = itemView.findViewById(R.id.product_subcategory);
            productDescription = itemView.findViewById(R.id.product_description);
            productPrice = itemView.findViewById(R.id.product_price);
            productDiscount = itemView.findViewById(R.id.product_discount);
            productDiscountPrice = itemView.findViewById(R.id.product_discount_price);
            productAvailable = itemView.findViewById(R.id.product_available);
            productVisibility = itemView.findViewById(R.id.product_visibility);
            porductEventType = itemView.findViewById(R.id.product_event_type);

            btnFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        user.addFavouriteProduct(productList.get(position).getId());
                        userService.update(user);
                        btnFav.setVisibility(View.GONE);
                        btnUnFav.setVisibility(View.VISIBLE);
                    }
                }
            });

            btNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //int currentIndex = productImageSwitcher.getDisplayedChild();
                    if (currentIndex < currentProduct.getImages().size() - 1) {
                        currentIndex++;
                        productImageSwitcher.setImageResource(currentProduct.getImages().get(currentIndex));
                    }
                }
            });

            btPrevious.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //int currentIndex = productImageSwitcher.getDisplayedChild();
                    if (currentIndex > 0) {
                        currentIndex--;

                        productImageSwitcher.setImageResource(currentProduct.getImages().get(currentIndex));
                    }
                }
            });

            btnUnFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        user.removeFavouriteProduct(productList.get(position).getId());
                        userService.update(user);
                        btnFav.setVisibility(View.VISIBLE);
                        btnUnFav.setVisibility(View.GONE);
                    }
                }
            });

            btnInfo.setOnClickListener(v -> {
                int position = getAdapterPosition();
                Intent createIntent = new Intent(context, ProductDetailsActivity.class);
                createIntent.putExtra("EMAIL", user.getEmail());
                createIntent.putExtra("PRODUCT", productList.get(position).getId());
                createIntent.putExtra("EVENT", selectedEventId);
                context.startActivity(createIntent);
            });
        }

        public void bind(Product product) {
            currentProduct = product;
            boolean ind = product != null;
            currentIndex = 0;
            Log.i("binder",ind? "nije null": "null je");
            productName.setText(product.getName());
            productCategory.setText(product.getCathegory());
            productSubcategory.setText(product.getSubCathegory());
            productDescription.setText(product.getDescription());
            productPrice.setText(String.valueOf(product.getPrice()));
            productDiscount.setText(String.valueOf(product.getDiscount()));
            productDiscountPrice.setText(String.valueOf(product.getPrice() - product.getDiscount()));
            porductEventType.setText(product.getEventType());
            productAvailable.setText(product.isAvailable() ? "Yes" : "No");
            productVisibility.setText(product.isVisible() ? "Yes" : "No");


            if (product.getImages() != null && !product.getImages().isEmpty()) {
                productImageSwitcher.setImageResource(product.getImages().get(0));
            } else {

                productImageSwitcher.setImageResource(R.drawable.default_image);
            }

            if (user.getFavouriteProducts().contains(product.getId())) {
                btnUnFav.setVisibility(View.VISIBLE);
                btnFav.setVisibility(View.GONE);
            } else {
                btnFav.setVisibility(View.VISIBLE);
                btnUnFav.setVisibility(View.GONE);
            }
        }

        @SuppressLint({"NotifyDataSetChanged"})
        private void editProductPopup(Product product) {
            @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.product_edit_form, null);

            // Create a PopupWindow
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            boolean focusable = true;
            PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

            //set data
            TextView category = popupView.findViewById(R.id.product_category_edit);
            category.setText(product.getCathegory());

            Spinner subcategory = popupView.findViewById(R.id.product_subcategory_edit);
            String selectedCategory = product.getSubCathegory();  // Assuming getCategory() returns a string
            ArrayAdapter<String> adapter = (ArrayAdapter<String>) subcategory.getAdapter();
            if (adapter != null) {
                int position = adapter.getPosition(selectedCategory);
                if (position != -1) {
                    subcategory.setSelection(position);
                }
            }

            EditText eventType = popupView.findViewById(R.id.product_event_type_edit);
            eventType.setText(product.getEventType());

            EditText name = popupView.findViewById(R.id.edit_product_name);
            name.setText(product.getName());

            EditText description = popupView.findViewById(R.id.edit_product_description);
            description.setText(product.getDescription());

            EditText price = popupView.findViewById(R.id.edit_product_price);
            price.setText(String.valueOf(product.getPrice()));

            EditText discount = popupView.findViewById(R.id.edit_product_discount);
            discount.setText(String.valueOf(product.getDiscount()));

            RadioGroup available = popupView.findViewById(R.id.available_rg);
            if (product.isAvailable()) {
                available.check(R.id.available_yes);
            } else {
                available.check(R.id.available_no);
            }

            RadioGroup visible = popupView.findViewById(R.id.visible_rg);
            if (product.isAvailable()) {
                visible.check(R.id.visible_yes);
            } else {
                visible.check(R.id.visible_no);
            }

            //show window
            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

            Button saveButton = popupView.findViewById(R.id.buttonSave_edit_product);
            Button cancelButton = popupView.findViewById(R.id.buttonCancel_edit_product);

            saveButton.setOnClickListener(v -> {

                popupWindow.dismiss();

            });

            cancelButton.setOnClickListener(v -> {

                popupWindow.dismiss();

            });
        }

        private void showDeleteConfirmationDialog(Product product) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Delete Product");
            builder.setMessage("Are you sure you want to delete this product?");

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //delete operation here

                    dialog.dismiss();
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

}
