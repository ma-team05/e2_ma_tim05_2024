package com.example.eventplanner.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.BudgetPlanActivity;
import com.example.eventplanner.activities.EditEventActivity;
import com.example.eventplanner.activities.EventActivitiesActivity;
import com.example.eventplanner.activities.EventCreationActivity;
import com.example.eventplanner.activities.GuestListActivity;
import com.example.eventplanner.activities.ServiceSearchActivity;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.services.EventService;

import java.util.List;

public class EventAdapter extends BaseAdapter {
    private final List<Event> dataList;
    private final Context context;
    private final EventService eventService;
    private final String email;

    public EventAdapter(Context context, List<Event> dataList, String email) {
        this.context = context;
        this.dataList = dataList;
        this.eventService = new EventService(new EventRepository());
        this.email = email;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.event_card_layout, parent, false);
            holder = new ViewHolder();
            holder.textName = convertView.findViewById(R.id.textName);
            holder.btnEdit = convertView.findViewById(R.id.btnEdit);
            holder.btnDelete = convertView.findViewById(R.id.btnDelete);
            holder.btnBudget = convertView.findViewById(R.id.btnBudget);
            holder.btnServiceSearch = convertView.findViewById(R.id.btnBuy);
            holder.btnGuests = convertView.findViewById(R.id.btnGuests);
            holder.btnActivities = convertView.findViewById(R.id.btnActivities);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Event event = dataList.get(position);
        holder.textName.setText(event.getName());

        holder.btnEdit.setOnClickListener(v -> {
            Intent editIntent = new Intent(context, EditEventActivity.class);
            editIntent.putExtra("EVENT", event.getId());
            editIntent.putExtra("EMAIL", event.getUserEmail());
            context.startActivity(editIntent);
        });

        holder.btnDelete.setOnClickListener(v -> showDeleteConfirmationDialog(event));

        holder.btnBudget.setOnClickListener(v -> {
            Intent intent = new Intent(context, BudgetPlanActivity.class);
            intent.putExtra("EVENT", event.getId());
            intent.putExtra("EMAIL", event.getUserEmail());
            context.startActivity(intent);
        });

        holder.btnServiceSearch.setOnClickListener(v -> {
            Intent intent = new Intent(context, ServiceSearchActivity.class);
            intent.putExtra("EVENT", event.getId());
            intent.putExtra("email", email);
            context.startActivity(intent);
        });

        holder.btnGuests.setOnClickListener(v -> {
            Intent intent = new Intent(context, GuestListActivity.class);
            intent.putExtra("EVENT", event.getId());
            intent.putExtra("email", email);
            context.startActivity(intent);
        });

        holder.btnActivities.setOnClickListener(v -> {
            Intent intent = new Intent(context, EventActivitiesActivity.class);
            intent.putExtra("EVENT", event.getId());
            intent.putExtra("email", email);
            context.startActivity(intent);
        });

        return convertView;
    }

    private void showDeleteConfirmationDialog(Event event) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to delete this event?")
                .setPositiveButton("Yes", (dialog, id) -> deleteEvent(event))
                .setNegativeButton("No", (dialog, id) -> dialog.dismiss());
        builder.create().show();
    }

    private void deleteEvent(Event event) {
        eventService.delete(event.getId());
        Toast.makeText(context, "Event deleted", Toast.LENGTH_SHORT).show();
        // Remove the event from the list and update the UI
        dataList.remove(event);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView textName;
        ImageButton btnEdit, btnDelete, btnBudget, btnServiceSearch, btnGuests, btnActivities;
    }
}
