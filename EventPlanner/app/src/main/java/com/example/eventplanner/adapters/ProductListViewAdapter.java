package com.example.eventplanner.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.ProductDetailsActivity;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.UserService;

import java.util.List;

public class ProductListViewAdapter extends BaseAdapter {

    private Context context;
    private List<Product> productList;
    private User user;

    private UserService userService = new UserService();

    private int currentIndex;

    public ProductListViewAdapter(Context context,List<Product> productList, User user) {
        this.context = context;
        this.productList = productList;
        this.user = user;


        Log.i("Frag","Napravio adapter");
    }
    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.product_card_result, parent, false);
            holder = new ViewHolder();

            holder.btPrevious = convertView.findViewById(R.id.bt_previous);
            holder.btNext = convertView.findViewById(R.id.bt_next);
            holder.editProductBtn = convertView.findViewById(R.id.edit_product_btn);
            holder.deleteProductBtn = convertView.findViewById(R.id.delete_product_btn);
            holder.btnFav = convertView.findViewById(R.id.btn_fav);
            holder.btnUnFav = convertView.findViewById(R.id.btn_unfav);
            holder.btnInfo = convertView.findViewById(R.id.btn_info);
            holder.productImageSwitcher = convertView.findViewById(R.id.product_image_switcher);
            holder.productName = convertView.findViewById(R.id.product_name);
            holder.productCategory = convertView.findViewById(R.id.product_category);
            holder.productSubcategory = convertView.findViewById(R.id.product_subcategory);
            holder.productDescription = convertView.findViewById(R.id.product_description);
            holder.productPrice = convertView.findViewById(R.id.product_price);
            holder.productDiscount = convertView.findViewById(R.id.product_discount);
            holder.productDiscountPrice = convertView.findViewById(R.id.product_discount_price);
            holder.productAvailable = convertView.findViewById(R.id.product_available);
            holder.productVisibility = convertView.findViewById(R.id.product_visibility);
            holder.porductEventType = convertView.findViewById(R.id.product_event_type);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.productImageSwitcher.setFactory(() -> {
            ImageView imageView = new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            return imageView;
        });
        holder.productName = convertView.findViewById(R.id.product_name);
        holder.productCategory = convertView.findViewById(R.id.product_category);
        holder.productSubcategory = convertView.findViewById(R.id.product_subcategory);
        holder.productDescription = convertView.findViewById(R.id.product_description);
        holder.productPrice = convertView.findViewById(R.id.product_price);
        holder.productDiscount = convertView.findViewById(R.id.product_discount);
        holder.productDiscountPrice = convertView.findViewById(R.id.product_discount_price);
        holder.productAvailable = convertView.findViewById(R.id.product_available);
        holder.productVisibility = convertView.findViewById(R.id.product_visibility);
        holder.porductEventType = convertView.findViewById(R.id.product_event_type);

        holder.productName.setText(productList.get(position).getName());
        holder.productCategory.setText(productList.get(position).getCathegory());
        holder.productSubcategory.setText(productList.get(position).getSubCathegory());
        holder.productDescription.setText(productList.get(position).getDescription());
        holder.productPrice.setText(String.valueOf(productList.get(position).getPrice()));
        holder.productDiscount.setText(String.valueOf(productList.get(position).getDiscount()));
        holder.productDiscountPrice.setText(String.valueOf(productList.get(position).getPrice() - productList.get(position).getDiscount()));
        holder.porductEventType.setText(productList.get(position).getEventType());
        holder.productAvailable.setText(productList.get(position).isAvailable() ? "Yes" : "No");
        holder.productVisibility.setText(productList.get(position).isVisible() ? "Yes" : "No");


        if (productList.get(position).getImages() != null && !productList.get(position).getImages().isEmpty()) {
            holder.productImageSwitcher.setImageResource(productList.get(position).getImages().get(0));
        } else {

            holder.productImageSwitcher.setImageResource(R.drawable.default_image);
        }

        if (user.getFavouriteProducts().contains(productList.get(position).getId())) {
            holder.btnUnFav.setVisibility(View.VISIBLE);
            holder.btnFav.setVisibility(View.GONE);
        } else {
            holder.btnFav.setVisibility(View.VISIBLE);
            holder.btnUnFav.setVisibility(View.GONE);
        }

        holder.btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.addFavouriteProduct(productList.get(position).getId());
                userService.update(user);
                holder.btnFav.setVisibility(View.GONE);
                holder.btnUnFav.setVisibility(View.VISIBLE);
            }
        });

        holder.btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int currentIndex = productImageSwitcher.getDisplayedChild();
                if (currentIndex < productList.get(position).getImages().size() - 1) {
                    currentIndex++;
                    holder.productImageSwitcher.setImageResource(productList.get(position).getImages().get(currentIndex));
                }
            }
        });

        holder.btPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //int currentIndex = productImageSwitcher.getDisplayedChild();
                if (currentIndex > 0) {
                    currentIndex--;

                    holder.productImageSwitcher.setImageResource(productList.get(position).getImages().get(currentIndex));
                }
            }
        });

        holder.btnUnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.removeFavouriteProduct(productList.get(position).getId());
                userService.update(user);
                holder.btnFav.setVisibility(View.VISIBLE);
                holder.btnUnFav.setVisibility(View.GONE);
            }
        });

        holder.btnInfo.setOnClickListener(v -> {
            Intent createIntent = new Intent(context, ProductDetailsActivity.class);
            createIntent.putExtra("EMAIL", user.getEmail());
            createIntent.putExtra("PRODUCT", productList.get(position).getId());
            context.startActivity(createIntent);
        });

        return convertView;
    }

    static class ViewHolder {
        ImageButton btPrevious, btNext, editProductBtn, deleteProductBtn, btnFav, btnUnFav, btnInfo;
        ImageSwitcher productImageSwitcher;
        TextView productName, productCategory, productSubcategory, productDescription,
                productPrice, productDiscount, productDiscountPrice, productAvailable, productVisibility, porductEventType;
    }
}
