package com.example.eventplanner.activities.admins_actions;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.GradeReportsActivity;
import com.example.eventplanner.activities.LoggedInUserActivity;
import com.example.eventplanner.models.Notification;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.services.NotificationService;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.List;

public class LoggedInAdminActivity extends AppCompatActivity {
    private final NotificationService notificationService = new NotificationService();
    private Handler handler;
    private Runnable runnable;
    private static final int INTERVAL = 3000; // 3 seconds

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in_admin);

        handler = new Handler();
        runnable = () -> {
            checkAndSendNotifications();
            handler.postDelayed(runnable, INTERVAL);
        };
        handler.post(runnable);

        Button manageCategoriesProductsButton = findViewById(R.id.manageCategoriesProductsButton);
        manageCategoriesProductsButton.setOnClickListener(v -> startActivity(new Intent(LoggedInAdminActivity.this, CategoriesManagementActivity.class)));

        Button eventTypesButton = findViewById(R.id.buttonEventTypes);
        eventTypesButton.setOnClickListener(v -> startActivity(new Intent(LoggedInAdminActivity.this, EventTypesManagementActivity.class)));

        Button registrationRequestsButton = findViewById(R.id.buttonRegistrationRequests);
        registrationRequestsButton.setOnClickListener(v -> startActivity(new Intent(LoggedInAdminActivity.this, RegistrationRequestsActivity.class)));

        Button gradeReportsButton = findViewById(R.id.buttonGradeReports);
        gradeReportsButton.setOnClickListener(v-> startActivity(new Intent(LoggedInAdminActivity.this, GradeReportsActivity.class)));

        Button notificationsButton = findViewById(R.id.buttonNotifications);
        notificationsButton.setOnClickListener(v -> startActivity(new Intent(LoggedInAdminActivity.this, AdminsNotificationsActivity.class)));

        Button reportsButton = findViewById(R.id.buttonReports);
        reportsButton.setOnClickListener(v -> startActivity(new Intent(LoggedInAdminActivity.this, ReportManagementActivity.class)));
    }

    private void checkAndSendNotifications() {
        notificationService.getNotifications().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<Notification> notifications = task.getResult();
                if (notifications != null) {
                    for (Notification notification : notifications) {
                        if (!notification.isSent() && (notification.getType() == NotificationType.REGISTRATION || notification.getType() == NotificationType.ADMIN)) {
                            sendNotification(notification);
                            notification.setSent(true);
                            notificationService.update(notification);
                        }
                    }
                }
            } else {
                Exception e = task.getException();
                if (e != null) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void sendNotification(Notification notification) {
        FirebaseMessaging.getInstance().subscribeToTopic("notification")
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        triggerLocalNotification(notification);
                    }
                });
    }

    private void triggerLocalNotification(Notification notification) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "event_planner_channel";

        NotificationChannel channel = new NotificationChannel(channelId, "Event Planner Notification", NotificationManager.IMPORTANCE_DEFAULT);
        notificationManager.createNotificationChannel(channel);

        if(notification.getType()==NotificationType.REGISTRATION){
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                    .setContentTitle("New user registered!")
                    .setContentText("For more information, see the app.")
                    .setSmallIcon(R.drawable.notification_icon)
                    .setAutoCancel(true)
                    .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, LoggedInUserActivity.class), PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE));
            notificationManager.notify(0, notificationBuilder.build());
        }
        else {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                    .setContentTitle(notification.getType().toString())
                    .setContentText(notification.getDescription())
                    .setSmallIcon(R.drawable.notification_icon)
                    .setAutoCancel(true)
                    .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, LoggedInUserActivity.class), PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE));
            notificationManager.notify(0, notificationBuilder.build());
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }
}