package com.example.eventplanner.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Event {
    private String id;
    private String name;
    private String description;
    private int maxGuests;
    private boolean openToPublic;
    private String city;
    private String userEmail;
    private double maxDistance;
    private Date date;
    private EventType eventType;
    private EventStatus status;
    private Date from;
    private Date to;
    private String employeeId;
    private List<BudgetItem> budgetItems = new ArrayList<>();
    private List<Guest> guests = new ArrayList<>();
    private List<EventActivity> activities = new ArrayList<>();

    public Event() {
    }

    public Event(String id, String name, String description, int maxGuests, boolean openToPublic, String city, double maxDistance, Date date, EventType eventType) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.maxGuests = maxGuests;
        this.openToPublic = openToPublic;
        this.city = city;
        this.maxDistance = maxDistance;
        this.date = date;
        this.eventType = eventType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxGuests() {
        return maxGuests;
    }

    public void setMaxGuests(int maxGuests) {
        this.maxGuests = maxGuests;
    }

    public boolean isOpenToPublic() {
        return openToPublic;
    }

    public void setOpenToPublic(boolean openToPublic) {
        this.openToPublic = openToPublic;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(double maxDistance) {
        this.maxDistance = maxDistance;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public List<BudgetItem> getBudgetItems() {
        return budgetItems;
    }

    public void setBudgetItems(List<BudgetItem> budgetItems) {
        this.budgetItems = budgetItems;
    }

    public void addBudgetItem(BudgetItem item) {
        budgetItems.add(item);
    }

    public boolean removeBudgetItem(BudgetItem item) {
        if(!item.getReservations().isEmpty()) {
            return false;
        }
        budgetItems.remove(item);
        return true;
    }

    public EventStatus getStatus() {
        return status;
    }

    public void setStatus(EventStatus status) {
        this.status = status;
    }


    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public List<Guest> getGuests() {
        return guests;
    }

    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }

    public void addGuest(Guest guest) {
        this.guests.add(guest);
    }

    public void removeGuest(Guest guest) {
        this.guests = this.guests.stream().filter(g -> !g.getFullName().equals(guest.getFullName())).collect(Collectors.toList());
    }

    public List<EventActivity> getActivities() {
        return activities;
    }

    public void setActivities(List<EventActivity> activities) {
        this.activities = activities;
    }

    public void addActivity(EventActivity activity) {
        this.activities.add(activity);
    }

    public void removeActivity(EventActivity activity) {
        this.activities = this.activities.stream().filter(g -> !g.getName().equals(activity.getName())).collect(Collectors.toList());
    }
}
