package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.CancelingPackageActivity;
import com.example.eventplanner.activities.GradingCompanyActivity;
import com.example.eventplanner.activities.RejectingGradeReportActivity;
import com.example.eventplanner.activities.ReportingGradeActivity;
import com.example.eventplanner.activities.reservations.ReservationsActivity;
import com.example.eventplanner.activities.UserProfileActivity;
import com.example.eventplanner.activities.reservations.ReservationsActivity;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.Reservation;
import com.example.eventplanner.models.ReservationStatus;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.ReservationService;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ReservationAdapter extends RecyclerView.Adapter<ReservationAdapter.ReservationViewHolder> {
    private List<Reservation> reservations;
    private ReservationsActivity activity;
    private UserRole userRole;
    private String userId;
    private ReservationsActivity parentActivity;
    private Reservation currentReservation;

    public ReservationAdapter(List<Reservation> reservations, ReservationsActivity p, String userId, UserRole userRole) {
        this.reservations = reservations;
        this.parentActivity = p;
        this.userId = userId;
        this.userRole = userRole;
    }

    @NonNull
    @Override
    public ReservationAdapter.ReservationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reservation, parent, false);
        return new ReservationAdapter.ReservationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReservationAdapter.ReservationViewHolder holder, int position) {
        Reservation reservation = reservations.get(position);
        holder.bind(reservation);
    }

    @Override
    public int getItemCount() {
        return reservations.size();
    }

    public void updateReservation(List<Reservation> newReservations) {
        reservations.clear();
        reservations.addAll(newReservations);
        notifyDataSetChanged();
    }

    class ReservationViewHolder extends RecyclerView.ViewHolder {

        private TextView serviceName;
        private TextView serviceDescription;
        private TextView serviceCategory;
        private TextView reservationEmployee;
        private TextView reservationEventPlanner;
        private TextView reservationCancellationDeadline;
        private TextView reservationPackage;
        private TextView reservationAppointment;
        private TextView reservationStatus;
        private Button cancelButton;
        private Button gradeButton;
        private Button acceptButton;
        private Button rejectButton;
        private ReservationService service = new ReservationService();

        public ReservationViewHolder(@NonNull View itemView) {
            super(itemView);
            reservationAppointment = itemView.findViewById(R.id.reservationAppointment);
            reservationCancellationDeadline = itemView.findViewById(R.id.reservationCancellationDeadline);
            reservationPackage = itemView.findViewById(R.id.reservationPackage);
            reservationEmployee = itemView.findViewById(R.id.reservationEmployee);
            reservationEventPlanner = itemView.findViewById(R.id.reservationEventPlanner);
            reservationStatus = itemView.findViewById(R.id.reservationStatus);
            serviceCategory = itemView.findViewById(R.id.serviceCategory);
            serviceName = itemView.findViewById(R.id.serviceName);
            serviceDescription = itemView.findViewById(R.id.serviceDescription);
            acceptButton = itemView.findViewById(R.id.acceptButton);
            gradeButton = itemView.findViewById(R.id.gradeButton);
            cancelButton = itemView.findViewById(R.id.cancelButton);
            rejectButton = itemView.findViewById(R.id.rejectButton);

            reservationEventPlanner.setOnClickListener(v ->{
                if(userRole == UserRole.OWNER){
                    Intent intent = new Intent(itemView.getContext(), UserProfileActivity.class);
                    intent.putExtra("EMAIL", currentReservation.getEventPlanner().getEmail());
                    intent.putExtra("ForReport", "yes");
                    intent.putExtra("ReportingId", userId);
                    parentActivity.startActivityForResult(intent, 2);
                }
            });
        }

        @SuppressLint("SetTextI18n")
        public void bind(Reservation reservation) {
            currentReservation = reservation;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
            reservationAppointment.setText(sdf.format(reservation.getFrom()) + " - " + sdf.format(reservation.getTo()));
            reservationCancellationDeadline.setText("Cancellation deadline: " + sdf.format(reservation.getCancellationDeadline()));
            if (reservation.getPackageId() != null) {
                reservationPackage.setText("Package: " + reservation.getPackageId());
                reservationPackage.setVisibility(View.VISIBLE);
            } else {
                reservationPackage.setVisibility(View.GONE);
            }
            reservationEmployee.setText("Employee: " + reservation.getEmployee().getFirstName() + " " + reservation.getEmployee().getLastName());
            reservationEventPlanner.setText("Event Planner: " + reservation.getEventPlanner().getFirstName() + " " + reservation.getEventPlanner().getLastName());
            serviceCategory.setText("Category: " + reservation.getService().getCategory());
            serviceName.setText("Service: " + reservation.getService().getName());
            serviceDescription.setText("Description: " + reservation.getService().getDescription());
            reservationStatus.setText("Status: " + reservation.getStatus().toString());
            acceptButton.setVisibility((userRole == UserRole.EMPLOYEE && reservation.getStatus() == ReservationStatus.NEW) ? View.VISIBLE : View.GONE);
            rejectButton.setVisibility((userRole == UserRole.EMPLOYEE && reservation.getStatus() == ReservationStatus.NEW) ? View.VISIBLE : View.GONE);
            cancelButton.setVisibility(((userRole == UserRole.EMPLOYEE || userRole == UserRole.OWNER || userRole == UserRole.EVENT_PLANNER) && (reservation.getStatus() == ReservationStatus.NEW || reservation.getStatus() == ReservationStatus.ACCEPTED)) ? View.VISIBLE : View.GONE);
            acceptButton.setOnClickListener(v -> acceptReservation(reservation));
            cancelButton.setOnClickListener(v -> cancelReservation(reservation));
            rejectButton.setOnClickListener(v -> rejectReservation(reservation));
            gradeButton.setVisibility( (!reservation.getGraded() && userRole==UserRole.EVENT_PLANNER && (reservation.getStatus()==ReservationStatus.FINISHED || reservation.getStatus()==ReservationStatus.CANCELED_BY_PUP)) ? View.VISIBLE : View.GONE);
            gradeButton.setOnClickListener(v->gradeReservation(reservation));
        }
        private void gradeReservation(Reservation reservation){
            Date deadline = reservation.getTo();
            Instant currentInstant = deadline.toInstant();
            LocalDateTime localDateTime = LocalDateTime.ofInstant(currentInstant, ZoneId.systemDefault());
            localDateTime = localDateTime.plusDays(5);
            Instant futureInstant = localDateTime.toInstant(ZoneOffset.UTC);
            deadline = Date.from(futureInstant);
            if(deadline.before(new Date())){
                Toast.makeText(parentActivity, "Deadline for grading expired.", Toast.LENGTH_SHORT).show();
                return;
            }
            Intent intent = new Intent(itemView.getContext(), GradingCompanyActivity.class);
            intent.putExtra("companyId", reservation.getEmployee().getCompanyId());
            intent.putExtra("userId", userId);
            intent.putExtra("reservationId", reservation.getId());
            parentActivity.startActivityForResult(intent, 2);
        }

        private void acceptReservation(Reservation reservation) {
            reservation.setStatus(ReservationStatus.ACCEPTED);
            notifyDataSetChanged();
            service.update(reservation);
        }
        private void cancelReservation(Reservation reservation){
            if(reservation.getCancellationDeadline().before(new Date())){
                Toast.makeText(parentActivity, "Deadline for cancellation expired.", Toast.LENGTH_SHORT).show();
                return;
            }
            if(reservation.getPackageId()==null){
                reservation.setStatus(userRole==UserRole.EVENT_PLANNER?ReservationStatus.CANCELED_BY_EVENT_PLANNER:ReservationStatus.CANCELED_BY_PUP);
                notifyDataSetChanged();
                service.update(reservation);
                if(userRole==UserRole.EVENT_PLANNER)
                    new NotificationService().add(reservation.getEmployee().getId(), "Event planner canceled their reservation.", NotificationType.RESERVATION);
                else
                    new NotificationService().add(reservation.getEventPlanner().getId(), "Your reservation has been canceled, you are able to leave a grade for company.", NotificationType.RESERVATION);
            }
            else{
                String services = parentActivity.getReservationsByPackage(reservation.getPackageId());
                Intent intent = new Intent(itemView.getContext(), CancelingPackageActivity.class);
                intent.putExtra("services", services);
                intent.putExtra("packageId", reservation.getPackageId());
                parentActivity.startActivityForResult(intent, 1);
            }
        }

        private void rejectReservation(Reservation reservation) {
            reservation.setStatus(ReservationStatus.REJECTED);
            notifyDataSetChanged();
            service.update(reservation);
            new NotificationService().add(reservation.getEventPlanner().getId(), "Your reservation has been rejected by the PUP-Z.", NotificationType.RESERVATION);
        }
    }
}