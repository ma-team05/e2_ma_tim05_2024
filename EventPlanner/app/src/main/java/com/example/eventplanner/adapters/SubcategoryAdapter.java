package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.services.CategoryService;

import java.util.List;

public class SubcategoryAdapter extends RecyclerView.Adapter<SubcategoryAdapter.SubcategoryViewHolder> {

    private final List<Category> subcategories;
    private OnSubcategoryEditListener editListener;
    private final Context context;
    private final Category category;

    public SubcategoryAdapter(Context context, List<Category> subcategories, Category category) {
        this.context = context;
        this.subcategories = subcategories;
        this.category = category;
    }

    @NonNull
    @Override
    public SubcategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_subcategory, parent, false);
        return new SubcategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubcategoryViewHolder holder, int position) {
        Category subcategory = subcategories.get(position);
        holder.bind(subcategory);
    }

    @Override
    public int getItemCount() {
        return subcategories.size();
    }

    public void setOnSubcategoryEditListener(OnSubcategoryEditListener listener) {
        this.editListener = listener;
    }

    public interface OnSubcategoryEditListener {
        void onSubcategoryEdit(Category subcategory);

        void onAcceptClick(Category subcategoryRequest);
    }

    public class SubcategoryViewHolder extends RecyclerView.ViewHolder {
        private final TextView textViewName;
        private final TextView textViewDescription;

        public SubcategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            ImageView editIcon = itemView.findViewById(R.id.editIcon);
            ImageView deleteIcon = itemView.findViewById(R.id.deleteIcon);

            editIcon.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Category subcategory = subcategories.get(position);
                    if (editListener != null) {
                        editListener.onSubcategoryEdit(subcategory);
                    }
                }
            });

            deleteIcon.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Category subcategory = subcategories.get(position);
                    showDeleteConfirmationDialog(subcategory);
                }
            });
        }

        private void showDeleteConfirmationDialog(Category subcategory) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Confirm Deletion")
                    .setMessage("Are you sure you want to delete " + subcategory.getName() + "?")
                    .setPositiveButton("Delete", (dialog, which) -> {
                        deleteSubcategory(subcategory);
                        dialog.dismiss();
                    })
                    .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss())
                    .show();
        }

        @SuppressLint("NotifyDataSetChanged")
        private void deleteSubcategory(Category subcategory) {
            subcategories.remove(subcategory);
            category.deleteSubcategory(subcategory);
            CategoryService categoryService = new CategoryService();
            categoryService.update(category);
            notifyDataSetChanged();
        }

        public void bind(final Category subcategory) {
            textViewName.setText(subcategory.getName());
            textViewDescription.setText(subcategory.getDescription());
        }
    }
}
