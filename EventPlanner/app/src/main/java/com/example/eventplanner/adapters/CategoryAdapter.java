package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.activities.admins_actions.SubcategoriesManagementActivity;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.R;
import com.example.eventplanner.services.CategoryService;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private final List<Category> categories;
    private final Context context;

    public CategoryAdapter(Context context, List<Category> categories) {
        this.context = context;
        this.categories = categories;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category category = categories.get(position);
        holder.bind(category);
        holder.editIcon.setOnClickListener(v -> {
            // Pass the selected category to the activity for editing
            editCategory(category);
        });
    }

    private void editCategory(Category category) {
        // Notify the listener when the edit button is clicked
        if (editListener != null) {
            editListener.onCategoryEdit(category);
        }
    }

    public interface OnCategoryEditListener {
        void onCategoryEdit(Category category);
    }

    private OnCategoryEditListener editListener;

    public void setOnCategoryEditListener(OnCategoryEditListener listener) {
        this.editListener = listener;
    }
    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        private final TextView textViewName;
        private final TextView textViewDescription;
        private final ImageView editIcon;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            Button subcategoryButton = itemView.findViewById(R.id.subcategoryButton);
            editIcon = itemView.findViewById(R.id.editIcon);
            ImageView deleteIcon = itemView.findViewById(R.id.deleteIcon);

            editIcon.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Category category = categories.get(position);
                    editCategory(category);
                }
            });

            deleteIcon.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Category category = categories.get(position);
                    showDeleteConfirmationDialog(category);
                }
            });

            subcategoryButton.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Category category = categories.get(position);
                    Intent intent = new Intent(context, SubcategoriesManagementActivity.class);
                    intent.putExtra("category_id", category.getId());
                    context.startActivity(intent);
                }
            });
        }

        private void showDeleteConfirmationDialog(Category category) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Confirm Deletion")
                    .setMessage("Are you sure you want to delete " + category.getName() + "?")
                    .setPositiveButton("Delete", (dialog, which) -> {
                        deleteCategory(category);
                        dialog.dismiss();
                    })
                    .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss())
                    .show();
        }

        @SuppressLint("NotifyDataSetChanged")
        private void deleteCategory(Category category) {
            categories.remove(category);
            CategoryService categoryService = new CategoryService();
            categoryService.delete(category.getId());
            notifyDataSetChanged();
        }

        public void bind(final Category category) {
            textViewName.setText(category.getName());
            textViewDescription.setText(category.getDescription());
        }
    }
}