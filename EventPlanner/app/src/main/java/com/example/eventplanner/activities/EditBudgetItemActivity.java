package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.BudgetItem;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.services.EventService;

import java.util.ArrayList;
import java.util.List;

public class EditBudgetItemActivity extends AppCompatActivity {

    private EventService eventService;
    private Event event;

    private BudgetItem budgetItem;

    List<Category> subcategories = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_budget_item);

        eventService = new EventService(new EventRepository());

        String eventId = (String) getIntent().getSerializableExtra("EVENT");
        String budgetItemId = (String) getIntent().getSerializableExtra("BUDGET_ITEM");

        eventService.getById(eventId, new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                EditBudgetItemActivity.this.event = events.get(0);

                EditBudgetItemActivity.this.budgetItem = EditBudgetItemActivity.this.event.getBudgetItems()
                        .stream()
                        .filter(item -> item.getName().equals(budgetItemId))
                        .findFirst()
                        .orElse(null);

                EditText amountEditText = findViewById(R.id.ammount);
                amountEditText.setText(String.valueOf(budgetItem.getAmmount()));

            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });

        Button editButton = findViewById(R.id.createButton);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields();
            }
        });
    }

    private boolean validateFields() {
        boolean isValid = true;

        EditText eventNameEditText;
        String field;

        eventNameEditText = findViewById(R.id.ammount);
        field = eventNameEditText.getText().toString().trim();
        if (TextUtils.isEmpty(field)) {
            eventNameEditText.setError("Please enter the amount");
            eventNameEditText.requestFocus();
            isValid = false;
        } else {
            double maxGuests = Double.parseDouble(field);
            if (maxGuests < 1) {
                eventNameEditText.setError("Please enter a number bigger than 0 for amount");
                eventNameEditText.requestFocus();
                isValid = false;
            }
        }

        if(isValid) {
            editBudgetItem();
        }

        return isValid;
    }

    private void editBudgetItem() {
        EditText amountEditText = findViewById(R.id.ammount);

        double amount = Double.parseDouble(amountEditText.getText().toString().trim());

        budgetItem.setAmmount(amount);
        eventService.update(event);

        String eventDetails = "Budget item edited:\n" +
                "Name: " + budgetItem.getName() + "\n" +
                "Amount: " + budgetItem.getAmmount() + "\n" +
                "Category: " + budgetItem.getCategory().getName();
        Toast.makeText(this, eventDetails, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, BudgetPlanActivity.class);
        intent.putExtra("EVENT", event.getId());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        finish();
    }
}