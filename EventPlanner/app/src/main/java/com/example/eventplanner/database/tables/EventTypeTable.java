package com.example.eventplanner.database.tables;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class EventTypeTable {
    private EventTypeTable() {
    }

    public static final class Table implements BaseColumns {
        public static final String TABLE_NAME = "event_types";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_ACTIVATED = "activated";
    }

    public static final class DDL {
        public static final String CREATE_TABLE = "CREATE TABLE " +
                Table.TABLE_NAME + " (" +
                Table._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Table.COLUMN_NAME + " TEXT NOT NULL, " +
                Table.COLUMN_DESCRIPTION + " TEXT NOT NULL, " +
                Table.COLUMN_ACTIVATED + " INTEGER NOT NULL" +
                ");";

        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " +
                Table.TABLE_NAME;
    }

    public static final class DML {
        public static void insertData(SQLiteDatabase db) {
            ContentValues values = new ContentValues();

            values.put(Table.COLUMN_NAME, "Select an event type");
            values.put(Table.COLUMN_DESCRIPTION, "Select an event type");
            values.put(Table.COLUMN_ACTIVATED, 1);
            db.insert(Table.TABLE_NAME, null, values);

            values.clear();
            values.put(Table.COLUMN_NAME, "Event Type 1");
            values.put(Table.COLUMN_DESCRIPTION, "Description 1");
            values.put(Table.COLUMN_ACTIVATED, 1);
            db.insert(Table.TABLE_NAME, null, values);

            values.clear();
            values.put(Table.COLUMN_NAME, "Event Type 2");
            values.put(Table.COLUMN_DESCRIPTION, "Description 2");
            values.put(Table.COLUMN_ACTIVATED, 1);
            db.insert(Table.TABLE_NAME, null, values);

            values.clear();
            values.put(Table.COLUMN_NAME, "Other");
            values.put(Table.COLUMN_DESCRIPTION, "Other");
            values.put(Table.COLUMN_ACTIVATED, 1);
            db.insert(Table.TABLE_NAME, null, values);
        }
    }
}
