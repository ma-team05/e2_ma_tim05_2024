package com.example.eventplanner.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;


import com.google.firebase.Timestamp;

import java.util.HashMap;
import java.util.List;

public class Package implements Parcelable {
    private String id;
    private String companyId;
    private String name;
    private String category;
    private String categoryId;
    private List<String> subcategories;
    private String description;
    private double price;
    private double discount;
    private List<Integer> images;
    private boolean visible;
    private boolean available;
    private List<String> products;
    private List<String> services;
    private List<String> productIds;
    private List<String> serviceIds;
    HashMap<String,Product> productsHashMap;
    HashMap<String,Service> servicesHashMap;
    private String eventTypes;
    private Timestamp reservationDeadline;
    private Timestamp cancellationDeadline;
    private Service.ConfirmationType confirmationType;
    private boolean deleted;

    public Package(){}

    public Package(String name, String category, List<String> subcategories, String description, double price, double discount, List<Integer> images, boolean visible, boolean available, List<String> products, List<String> services, String eventTypes, Timestamp reservationDeadline, Timestamp cancellationDeadline, Service.ConfirmationType confirmationType) {
        this.name = name;
        this.category = category;
        this.subcategories = subcategories;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.images = images;
        this.visible = visible;
        this.available = available;
        this.products = products;
        this.services = services;
        this.eventTypes = eventTypes;
        this.reservationDeadline = reservationDeadline;
        this.cancellationDeadline = cancellationDeadline;
        this.confirmationType = confirmationType;
    }

    protected Package(Parcel in) {
        name = in.readString();
        category = in.readString();
        subcategories = in.createStringArrayList();
        description = in.readString();
        price = in.readDouble();
        discount = in.readDouble();
        visible = in.readByte() != 0;
        available = in.readByte() != 0;
        products = in.createStringArrayList();
        services = in.createStringArrayList();
        eventTypes = in.readString();
    }

    public static final Creator<Package> CREATOR = new Creator<Package>() {
        @Override
        public Package createFromParcel(Parcel in) {
            return new Package(in);
        }

        @Override
        public Package[] newArray(int size) {
            return new Package[size];
        }
    };

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }

    public List<String> getServiceIds() {
        return serviceIds;
    }

    public void setServiceIds(List<String> serviceIds) {
        this.serviceIds = serviceIds;
    }

    public HashMap<String, Product> getProductsHashMap() {
        return productsHashMap;
    }

    public void setProductsHashMap(HashMap<String, Product> productsHashMap) {
        this.productsHashMap = productsHashMap;
    }

    public HashMap<String, Service> getServicesHashMap() {
        return servicesHashMap;
    }

    public void setServicesHashMap(HashMap<String, Service> servicesHashMap) {
        this.servicesHashMap = servicesHashMap;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<String> subcategories) {
        this.subcategories = subcategories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public List<Integer> getImages() {
        return images;
    }

    public void setImages(List<Integer> images) {
        this.images = images;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public List<String> getProducts() {
        return products;
    }

    public String getProductsString(){
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i< products.size(); i++){
            builder.append(products.get(i));
            if(i < products.size()-1){
                builder.append(",");
            }
        }
        return builder.toString();
    }

    public String getServicesString(){
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i< services.size(); i++){
            builder.append(services.get(i));
            if(i < services.size()-1){
                builder.append(",");
            }
        }
        return builder.toString();
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }

    public String getEventTypes() {
        return eventTypes;
    }
    public String getSubcategoriesString() {
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i< subcategories.size(); i++){
            builder.append(subcategories.get(i));
            if(i < subcategories.size()-1){
                builder.append(",");
            }
        }
        return builder.toString();
    }

    public void setEventTypes(String eventTypes) {
        this.eventTypes = eventTypes;
    }

    public Timestamp getReservationDeadline() {
        return reservationDeadline;
    }

    public void setReservationDeadline(Timestamp reservationDeadline) {
        this.reservationDeadline = reservationDeadline;
    }

    public Timestamp getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(Timestamp cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public Service.ConfirmationType getConfirmationType() {
        return confirmationType;
    }

    public void setConfirmationType(Service.ConfirmationType confirmationType) {
        this.confirmationType = confirmationType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(category);
        dest.writeStringList(subcategories);
        dest.writeString(description);
        dest.writeDouble(price);
        dest.writeDouble(discount);
        dest.writeByte((byte) (visible ? 1 : 0));
        dest.writeByte((byte) (available ? 1 : 0));
        dest.writeStringList(products);
        dest.writeStringList(services);
        dest.writeString(eventTypes);
    }
}
