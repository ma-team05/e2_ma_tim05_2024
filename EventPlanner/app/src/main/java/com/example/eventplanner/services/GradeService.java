package com.example.eventplanner.services;

import com.example.eventplanner.adapters.EmployeeAdapter;
import com.example.eventplanner.database.repository.GradeRepository;
import com.example.eventplanner.models.Grade;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

public class GradeService {
    private GradeRepository repo;
    public GradeService(){
        repo = new GradeRepository();
    }

    public Task<Void> addGrade(Grade grade){
        return repo.saveGrade(grade);
    }
    public Task<Void> deleteGrade(String gradeId){
        return repo.deleteGrade(gradeId);
    }
    public Task<List<Grade>> getAllByCompany(String companyId){
        return repo.getAllByCompany(companyId);
    }

    public Task<List<Grade>> getAllByEventPlanner(String userId){
        return repo.getAllByEventPlanner(userId);
    }
    public Task<List<Grade>> getAll(){
        return repo.getAll();
    }
    public Task<Grade> getById(String id){
        return repo.getById(id);
    }
}
