package com.example.eventplanner.database.repository;

import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Owner;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OwnerRepository {
    private final CollectionReference ownersCollection;

    public OwnerRepository() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        ownersCollection = db.collection("owners");
    }

    public Task<Void> save(Owner owner, String userId, String companyId) {
        DocumentReference newOwnerRef = ownersCollection.document(userId);
        owner.setId(userId);
        owner.setCompanyId(companyId);
        return newOwnerRef.set(owner);
    }

    public Task<List<Owner>> getAll(){
        return ownersCollection.get()
                .continueWith(task -> {
                    List<Owner> owners = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Owner owner = document.toObject(Owner.class);
                        owners.add(owner);
                    }
                    return owners;
                });
    }

    public Task<Owner> getById(String id) {
        DocumentReference ownerRef = ownersCollection.document(id);
        return ownerRef.get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            return document.toObject(Owner.class);
                        } else {
                            throw new Exception("Owner with ID " + id + " not found");
                        }
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }

    public Task<Owner> getByCompanyId(String id) {
        DocumentReference ownerRef = ownersCollection.document(id);
        return ownersCollection.get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        List<Owner> ownerList = new ArrayList<>();
                        if (querySnapshot != null) {
                            for (DocumentSnapshot document : querySnapshot.getDocuments()) {
                                Owner owner = document.toObject(Owner.class);
                                ownerList.add(owner);
                            }
                        }
                        Owner owner = ownerList.stream().filter(o -> o .getCompanyId().equals(id)).findFirst().orElse(null);
                        return owner;
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }
    public Task<Void> update(Owner owner){
        DocumentReference employeeRef = ownersCollection.document(owner.getId());
        return employeeRef.set(owner);
    }
}