package com.example.eventplanner.database.repository;

import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Grade;
import com.example.eventplanner.models.GradeReport;
import com.example.eventplanner.models.Reservation;
import com.example.eventplanner.models.ReservationStatus;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ReservationRepository {
    private final CollectionReference reservationsCollection;
    public ReservationRepository(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        reservationsCollection = db.collection("reservations");
    }
    public Task<Void> saveReservation(Reservation reservation){
        DocumentReference newReservationRef = reservationsCollection.document();
        reservation.setId(newReservationRef.getId());
        return newReservationRef.set(reservation);
    }
    public Task<Void> updateReservation(Reservation reservation){
        DocumentReference gradeReservationRef = reservationsCollection.document(reservation.getId());
        return gradeReservationRef.set(reservation);
    }
    public Task<List<Reservation>> getAllByEmployee(String employeeId){
        return reservationsCollection.get()
                .continueWith(task -> {
                    List<Reservation> reservations = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Reservation reservation = document.toObject(Reservation.class);
                        assert reservation != null;
                        if(reservation.getEmployee().getId().equals(employeeId))
                            reservations.add(reservation);
                    }
                    return reservations;
                });
    }
    public Task<List<Reservation>> getAllByEventPlanner(String eventPlannerId){
        return reservationsCollection.get()
                .continueWith(task -> {
                    List<Reservation> reservations = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Reservation reservation = document.toObject(Reservation.class);
                        assert reservation != null;
                        if(reservation.getEventPlanner().getId().equals(eventPlannerId))
                            reservations.add(reservation);
                    }
                    return reservations;
                });
    }

    public Task<List<Reservation>> getAll(){
        return reservationsCollection.get()
                .continueWith(task -> {
                    List<Reservation> reservations = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Reservation reservation = document.toObject(Reservation.class);
                        reservations.add(reservation);
                    }
                    return reservations;
                });
    }
    public Task<Reservation> getById(String id) {
        DocumentReference reservationRef = reservationsCollection.document(id);
        return reservationRef.get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            return document.toObject(Reservation.class);
                        } else {
                            throw new Exception("Reservation with ID " + id + " not found");
                        }
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }
    public Task<List<Reservation>> getActiveAllByCompanyId(String companyId){
        return reservationsCollection
                .whereEqualTo("status", ReservationStatus.NEW)
                .get()
                .continueWith(task -> {
                    List<Reservation> reservations = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Reservation reservation = document.toObject(Reservation.class);
                        assert reservation != null;
                        if(reservation.getEmployee().getCompanyId().equals(companyId))
                            reservations.add(reservation);
                    }
                    return reservations;
                });
    }

    public Task<List<Reservation>> getActiveAllByEventPlanner(String eventPlannerId){
        return reservationsCollection
                .whereEqualTo("status", ReservationStatus.NEW)
                .get()
                .continueWith(task -> {
                    List<Reservation> reservations = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Reservation reservation = document.toObject(Reservation.class);
                        assert reservation != null;
                        if(reservation.getEventPlanner().getId().equals(eventPlannerId))
                            reservations.add(reservation);
                    }
                    return reservations;
                });
    }

    public void updateBatchOfEmployees(List<Reservation> reservations) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        WriteBatch batch = db.batch();

        for (Reservation pkg : reservations) {
            // Get the document reference for this package
            DocumentReference packageRef = db.collection("reservations").document(pkg.getId());

            // Update the package in the batch
            batch.set(packageRef, pkg);
        }

        batch.commit().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {

                // Batch update succeeded
                System.out.println("Batch update succeeded.");
            } else {
                // Batch update failed
                System.err.println("Batch update failed: " + task.getException());
            }
        });

    }

}
