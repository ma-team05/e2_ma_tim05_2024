package com.example.eventplanner.database.repository;

import com.example.eventplanner.models.Grade;
import com.example.eventplanner.models.GradeReport;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class GradeReportRepository {
    private FirebaseFirestore db;
    private CollectionReference gradesCollection;
    public GradeReportRepository(){
        db = FirebaseFirestore.getInstance();
        gradesCollection = db.collection("gradeReports");
    }
    public Task<Void> saveGradeReport(GradeReport gradeReport){
        DocumentReference newGradeReportRef = gradesCollection.document();
        gradeReport.setId(newGradeReportRef.getId());
        return newGradeReportRef.set(gradeReport);
    }
    public Task<Void> updateGradeReport(GradeReport gradeReport){
        DocumentReference gradeReportRef = gradesCollection.document(gradeReport.getId());
        return gradeReportRef.set(gradeReport);
    }
    public Task<List<GradeReport>> getAllByGrade(String gradeId){
        return gradesCollection.get()
                .continueWith(task -> {
                    List<GradeReport> grades = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        GradeReport grade = document.toObject(GradeReport.class);
                        if(grade.getGradeId().equals(gradeId))
                            grades.add(grade);
                    }
                    return grades;
                });
    }
    public Task<List<GradeReport>> getAll(){
        return gradesCollection.get()
                .continueWith(task -> {
                    List<GradeReport> grades = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        GradeReport grade = document.toObject(GradeReport.class);
                        grades.add(grade);
                    }
                    return grades;
                });
    }
    public Task<GradeReport> getById(String id) {
        DocumentReference gradeRef = gradesCollection.document(id);
        return gradeRef.get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            return document.toObject(GradeReport.class);
                        } else {
                            throw new Exception("Grade report with ID " + id + " not found");
                        }
                    } else {
                        throw task.getException(); // Propagate the exception
                    }
                });
    }

}
