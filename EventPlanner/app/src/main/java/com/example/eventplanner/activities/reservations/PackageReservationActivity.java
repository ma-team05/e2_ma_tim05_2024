package com.example.eventplanner.activities.reservations;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.PackageReservationServiceAdapter;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.services.EventService;
import com.example.eventplanner.services.PackageService;
import com.example.eventplanner.services.ServiceService;
import com.example.eventplanner.services.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


public class PackageReservationActivity extends AppCompatActivity {
    private static final String TAG = "PackageReservationActivity";
    private ListView listViewServices;

    private String selectedEventId = "";
    private Event selectedEvent;

    private EventService eventService;
    private List<Event> events;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_reservation);

        listViewServices = findViewById(R.id.listViewServices);
        Button btnSubmit = findViewById(R.id.btnSubmit);

        String packageId = getIntent().getStringExtra("PACKAGE_ID");
        String userId = getIntent().getStringExtra("USER_ID");
        String email = (String) getIntent().getSerializableExtra("EMAIL");
        selectedEventId = (String) getIntent().getSerializableExtra("EVENT");

        eventService = new EventService(new EventRepository());
        eventService.getAll(new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events1) {
                events = events1.stream().filter(e -> e.getUserEmail().equals(email)).collect(Collectors.toList());

                List<String> names = new ArrayList<>();
                names.add("Select an event");
                for (Event e : events) {
                    names.add("Selected event: " + e.getName());
                }

                Spinner eventSpinner = findViewById(R.id.event_spinner);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(PackageReservationActivity.this, android.R.layout.simple_spinner_item, names);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                eventSpinner.setAdapter(adapter);

                for (int i = 0; i < events.size(); i++) {
                    if (events.get(i).getId().equals(selectedEventId)) {
                        eventSpinner.setSelection(i + 1);
                        selectedEvent = events.get(i);
                        break;
                    } else {
                        eventSpinner.setSelection(0);
                        selectedEvent = null;
                    }
                }

                eventSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String eventName = eventSpinner.getSelectedItem().toString().trim();
                        if (eventName.equals("Select an event")) {
                            selectedEventId = "";
                            selectedEvent = null;
                        } else {
                            selectedEvent = events.stream().filter(e -> e.getName().equals(eventName.substring(16))).findFirst().orElse(null);
                            selectedEventId = selectedEvent.getId();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });

        fetchServicesForPackage(packageId, userId, btnSubmit);
    }

    private void fetchServicesForPackage(String packageId, String userId, Button btnSubmit) {
        PackageService packageService = new PackageService(new PackageRepository());
        ServiceService serviceService = new ServiceService();

        packageService.get(packageId).addOnSuccessListener(selectedPackage -> {
            if (selectedPackage != null) {
                List<Service> services = new ArrayList<>();
                List<String> serviceIds = selectedPackage.getServiceIds();
                AtomicInteger serviceCounter = new AtomicInteger(0);

                for (String serviceId : serviceIds) {
                    serviceService.getService(serviceId).addOnSuccessListener(service -> {
                        if (service != null) {
                            services.add(service);
                        }

                        if (serviceCounter.incrementAndGet() == serviceIds.size()) {
                            PackageReservationServiceAdapter adapter = new PackageReservationServiceAdapter(this, services);
                            listViewServices.setAdapter(adapter);

                            setButtonClickListener(btnSubmit, userId, adapter, packageId);
                        }
                    }).addOnFailureListener(e -> {
                        Log.e(TAG, "Error fetching service", e);
                    });
                }
            }
        }).addOnFailureListener(e -> {
            Log.e(TAG, "Error fetching package", e);
        });
    }

    private void setButtonClickListener(Button btnSubmit, String userId, PackageReservationServiceAdapter adapter, String packageId) {
        btnSubmit.setOnClickListener(v -> {
            if (adapter.areAllDatesSelected()) {
                UserService userService = new UserService();
                if (selectedEvent == null) {
                    Toast.makeText(this, "Please select an event", Toast.LENGTH_SHORT).show();
                    return;
                }
                userService.get(userId).addOnSuccessListener(user -> {
                    if (user != null) {
                        adapter.createReservations(user, packageId, selectedEvent.getId());
                    } else {
                        Log.d(TAG, "User not found");
                    }
                }).addOnFailureListener(e -> {
                    Log.e(TAG, "Error fetching user", e);
                });
            } else {
                Toast.makeText(this, "Please select start and end dates for all services", Toast.LENGTH_SHORT).show();
            }
        });
    }
}