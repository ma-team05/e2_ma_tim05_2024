package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ChatAdapter;
import com.example.eventplanner.adapters.EventAdapter;
import com.example.eventplanner.adapters.ProductResultsAdapter;
import com.example.eventplanner.models.Chat;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.services.ChatService;
import com.example.eventplanner.services.EventService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MyChatsActivity extends AppCompatActivity {
    private ChatService chatService = new ChatService();
    private ChatAdapter chatAdapter;

    private RecyclerView recyclerView;
    private String email;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_my_chats);

        Intent intent = getIntent();
        email = intent.getStringExtra("email");
        userId = intent.getStringExtra("userId");

        chatService.getAll(new ChatService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Chat> chats) {

                chats = chats.stream().filter(c -> c.getParticipant1().equals(email) || c.getParticipant2().equals(email)).collect(Collectors.toList());
                Collections.sort(chats, new Comparator<Chat>() {
                    @Override
                    public int compare(Chat c1, Chat c2) {
                        return c2.getLastUpdated().compareTo(c1.getLastUpdated());
                    }
                });

                initializeChats(chats);
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });
    }

    private void initializeChats(List<Chat> chats){
        recyclerView = findViewById(R.id.chat_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        chatAdapter = new ChatAdapter(this, chats, email, userId);
        recyclerView.setAdapter(chatAdapter);
    }
}