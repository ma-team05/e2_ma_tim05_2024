package com.example.eventplanner.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.firebase.Timestamp;

public class Report implements Parcelable {

    protected Report(Parcel in) {
        id = in.readString();
        reporterId = in.readString();
        reporterUsername = in.readString();
        reportedId = in.readString();
        reportedUsername = in.readString();
        reason = in.readString();
        isForCompany = in.readByte() != 0;
        reportTimestamp = in.readParcelable(Timestamp.class.getClassLoader());
    }

    public static final Creator<Report> CREATOR = new Creator<Report>() {
        @Override
        public Report createFromParcel(Parcel in) {
            return new Report(in);
        }

        @Override
        public Report[] newArray(int size) {
            return new Report[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(reporterId);
        dest.writeString(reporterUsername);
        dest.writeString(reportedId);
        dest.writeString(reportedUsername);
        dest.writeString(reason);
        dest.writeByte((byte) (isForCompany ? 1 : 0));
        dest.writeParcelable(reportTimestamp, flags);
    }

    public enum REPORT_TYPE {Reported,Accepted,Denied}
    private String id;
    private String reporterId;
    private String reporterUsername;
    private String reportedId;
    private String reportedUsername;
    private String reason;
    private boolean isForCompany;
    private Timestamp reportTimestamp;
    private REPORT_TYPE reportType;


    public Report(String id, String reporterId, String reporterUsername, String reportedId, String reportedUsername, String reason, boolean isForCompany, Timestamp reportTimestamp, REPORT_TYPE reportType) {
        this.id = id;
        this.reporterId = reporterId;
        this.reporterUsername = reporterUsername;
        this.reportedId = reportedId;
        this.reportedUsername = reportedUsername;
        this.reason = reason;
        this.isForCompany = isForCompany;
        this.reportTimestamp = reportTimestamp;
        this.reportType = reportType;
    }

    public Report(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReporterId() {
        return reporterId;
    }

    public void setReporterId(String reporterId) {
        this.reporterId = reporterId;
    }

    public String getReporterUsername() {
        return reporterUsername;
    }

    public void setReporterUsername(String reporterUsername) {
        this.reporterUsername = reporterUsername;
    }

    public String getReportedId() {
        return reportedId;
    }

    public void setReportedId(String reportedId) {
        this.reportedId = reportedId;
    }

    public String getReportedUsername() {
        return reportedUsername;
    }

    public void setReportedUsername(String reportedUsername) {
        this.reportedUsername = reportedUsername;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean isForCompany() {
        return isForCompany;
    }

    public void setForCompany(boolean forCompany) {
        isForCompany = forCompany;
    }

    public Timestamp getReportTimestamp() {
        return reportTimestamp;
    }

    public void setReportTimestamp(Timestamp reportTimestamp) {
        this.reportTimestamp = reportTimestamp;
    }

    public REPORT_TYPE getReportType() {
        return reportType;
    }

    public void setReportType(REPORT_TYPE reportType) {
        this.reportType = reportType;
    }
}
