package com.example.eventplanner.activities.admins_actions;

import android.os.Bundle;
import android.util.Log;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.ProductManagementActivity;
import com.example.eventplanner.adapters.ProductListAdapter;
import com.example.eventplanner.adapters.ReportListAdapter;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.Report;
import com.example.eventplanner.services.ProductService;
import com.example.eventplanner.services.ReportService;

import java.util.ArrayList;
import java.util.List;

public class ReportManagementActivity extends AppCompatActivity {
    private final ReportService reportService = new ReportService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_report_management);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        initializeRecyclerView();

    }

    private void initializeRecyclerView(){
        RecyclerView recyclerView = findViewById(R.id.report_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        reportService.getAll(new ReportService.OnDataFetchListener() {

            @Override
            public void onSuccess(List<Report> reports) {
                Log.i("BROJ REPORTOVA",String.valueOf(reports.size()));
                ReportListAdapter reportAdapter = new ReportListAdapter(ReportManagementActivity.this, reports,reportService,ReportManagementActivity.this);
                recyclerView.setAdapter(reportAdapter);
            }

            @Override
            public void onFailure(String errorMessage) {
                Log.i("FAILLL","NEKA GRESSKA");
            }
        });
    }
}