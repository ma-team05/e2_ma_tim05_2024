package com.example.eventplanner.activities.admins_actions;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.SubcategoryAdapter;
import com.example.eventplanner.adapters.SubcategoryRequestsAdapter;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.SubcategoryType;
import com.example.eventplanner.services.CategoryService;

import java.util.ArrayList;
import java.util.List;

public class SubcategoriesManagementActivity extends AppCompatActivity implements SubcategoryAdapter.OnSubcategoryEditListener {

    private List<Category> subcategories;
    private SubcategoryAdapter adapter;
    private Category category;

    private EditText subcategoryNameEditText;
    private EditText subcategoryDescriptionEditText;
    private Spinner spinnerSubcategoryType;
    private Button saveButton;

    private CategoryService categoryService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategories_management);

        RecyclerView recyclerView = findViewById(R.id.recyclerViewSubcategories);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        categoryService = new CategoryService();

        setCategory(recyclerView);
        setAdapters(recyclerView);
        setButtons();
    }

    @Override
    public void onAcceptClick(Category subcategoryRequest) {
        showPopup(subcategoryRequest, false);
    }

    @Override
    public void onSubcategoryEdit(Category category) {
        showPopup(category, true);
    }

    private void setButtons() {
        Button addButton = findViewById(R.id.buttonAddSubcategory);
        addButton.setOnClickListener(v -> showPopup(null, false));

        Button viewRequestsButton = findViewById(R.id.buttonViewRequests);
        viewRequestsButton.setOnClickListener(v -> showSubcategoryRequestsPopup());
    }

    private void setAdapters(RecyclerView recyclerView) {
        if (subcategories == null) {
            subcategories = new ArrayList<>();
        }
        adapter = new SubcategoryAdapter(this, subcategories, category);
        adapter.setOnSubcategoryEditListener(this);
        recyclerView.setAdapter(adapter);
    }

    private void setCategory(RecyclerView recyclerView) {
        String categoryId = getIntent().getStringExtra("category_id");
        CategoryService categoryService = new CategoryService();
        categoryService.getById(String.valueOf(categoryId))
                .addOnSuccessListener(category -> {
                    this.category = category;
                    subcategories = category.getSubcategories();
                    setAdapters(recyclerView);
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(this, "Failed to get category", Toast.LENGTH_SHORT).show();
                });
    }

    private void showPopup(Category subcategory, boolean isEditing) {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.popup_add_subcategory, null);

        PopupWindow popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.showAtLocation(popupView, 0, 0, 0);

        findElementsById(popupView);

        saveButton = popupView.findViewById(R.id.buttonSave);

        if (isEditing) {
            // Populate the fields with selected subcategory's information
            subcategoryNameEditText.setText(subcategory.getName());
            subcategoryDescriptionEditText.setText(subcategory.getDescription());
            spinnerSubcategoryType.setSelection(subcategory.getSubcategoryType().ordinal());
        } else {
            // Set the adapter for the spinner
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.subcategory_types, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerSubcategoryType.setAdapter(adapter);
        }

        saveButtonHandler(subcategory, isEditing, popupWindow);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void saveButtonHandler(Category subcategory, boolean isEditing, PopupWindow popupWindow) {
        saveButton.setOnClickListener(v -> {
            String subcategoryName = subcategoryNameEditText.getText().toString().trim();
            String subcategoryDescription = subcategoryDescriptionEditText.getText().toString().trim();
            SubcategoryType subcategoryType = SubcategoryType.values()[spinnerSubcategoryType.getSelectedItemPosition()];

            if (!subcategoryName.isEmpty() && !subcategoryDescription.isEmpty()) {
                if (isEditing) {
                    // Update existing subcategory

                    // Values before the change - from that i will find the values in database
                    String oldName = subcategory.getName();
                    String oldDescription = subcategory.getDescription();
                    SubcategoryType oldSubcategoryType = subcategory.getSubcategoryType();

                    // Values after the change
                    subcategory.setName(subcategoryName);
                    subcategory.setDescription(subcategoryDescription);
                    subcategory.setSubcategoryType(subcategoryType);
                    adapter.notifyDataSetChanged();

                    category.updateSubcategory(oldName, oldDescription, oldSubcategoryType, subcategory);
                } else {
                    // Add new subcategory to the parent category
                    Category newSubcategory = new Category(subcategoryName, subcategoryDescription, subcategoryType);
                    category.addSubcategory(newSubcategory);

                }
                categoryService.update(category);
                popupWindow.dismiss();
            } else {
                Toast.makeText(this, "Please fill both fields", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showSubcategoryRequestsPopup() {
        if (category != null && category.getId() != null) {
            View popupView = LayoutInflater.from(this).inflate(R.layout.popup_subcategory_requests, null);

            PopupWindow popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);
            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

            RecyclerView recyclerView = popupView.findViewById(R.id.recyclerViewSubcategoryRequests);

            List<Category> subcategoryRequests = category.getSubcategoriesRequests();
            SubcategoryRequestsAdapter adapter = new SubcategoryRequestsAdapter(subcategoryRequests, new SubcategoryRequestsAdapter.OnApproveRejectClickListener() {
                @Override
                public void onApproveClick(Category subcategoryRequest) {
                    category.approveSubcategoryRequest(subcategoryRequest);
                    categoryService.update(category);
                    popupWindow.dismiss();
                }

                @Override
                public void onRejectClick(Category subcategoryRequest) {
                    category.rejectSubcategoryRequest(subcategoryRequest);
                    categoryService.update(category);
                    popupWindow.dismiss();
                }
            });
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        } else {
            // Handle the case where category or its ID is null
            Toast.makeText(this, "Category ID is null", Toast.LENGTH_SHORT).show();
        }
    }

    private void findElementsById(View popupView) {
        subcategoryNameEditText = popupView.findViewById(R.id.editTextSubcategoryName);
        subcategoryDescriptionEditText = popupView.findViewById(R.id.editTextSubcategoryDescription);
        spinnerSubcategoryType = popupView.findViewById(R.id.spinnerSubcategoryType);
        saveButton = popupView.findViewById(R.id.buttonSave);
    }
}
