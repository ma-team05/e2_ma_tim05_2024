package com.example.eventplanner.activities.admins_actions;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.RegistrationRequestsAdapter;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.EmailService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.UserService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegistrationRequestsActivity extends AppCompatActivity implements RegistrationRequestsAdapter.OnRequestActionListener {

    private RecyclerView recyclerView;
    private RegistrationRequestsAdapter adapter;
    private UserService userService;
    private OwnerService ownerService;
    private CompanyService companyService;
    private Spinner spinnerCategories;
    private List<Category> categories;
    private CategoryService categoryService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_requests);

        recyclerView = findViewById(R.id.recyclerViewRegistrationRequests);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        SearchView searchView = findViewById(R.id.searchView);
        Button buttonFilterByDate = findViewById(R.id.buttonFilterByDate);
        spinnerCategories = findViewById(R.id.spinnerCategories);

        userService = new UserService();
        ownerService = new OwnerService();
        companyService = new CompanyService();
        categoryService = new CategoryService();

        fetchCategories();
        fetchNonActivatedUsers();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (adapter != null) {
                    adapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        buttonFilterByDate.setOnClickListener(v -> {
            if (adapter != null) {
                adapter.filterByDate();
            }
        });
    }

    private void fetchCategories() {
        categoryService.getAll().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                categories = task.getResult();
                populateCategoriesDropdown();
            } else {
                Log.e("RegistrationRequests", "Error fetching categories", task.getException());
            }
        });
    }

    private void populateCategoriesDropdown() {
        List<String> categoryNames = new ArrayList<>();
        for (Category category : categories) {
            categoryNames.add(category.getName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categoryNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategories.setAdapter(adapter);

        // Listener for category selection
        spinnerCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Category selectedCategory = categories.get(position);
                if (RegistrationRequestsActivity.this.adapter != null) {
                    RegistrationRequestsActivity.this.adapter.filterByCategory(selectedCategory);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing
            }
        });
    }

    private void fetchNonActivatedUsers() {
        userService.getNonActivatedUsers().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<User> nonActivatedUsers = task.getResult();
                Map<String, String> userCompanyMap = new HashMap<>();
                for (User user : nonActivatedUsers) {
                    ownerService.get(user.getId()).addOnCompleteListener(ownerTask -> {
                        if (ownerTask.isSuccessful()) {
                            String companyId = ownerTask.getResult().getCompanyId();
                            companyService.get(companyId).addOnCompleteListener(companyTask -> {
                                if (companyTask.isSuccessful()) {
                                    userCompanyMap.put(user.getId(), companyTask.getResult().getName());
                                    if (userCompanyMap.size() == nonActivatedUsers.size()) {
                                        // Initialize adapter only after fetching all company names
                                        adapter = new RegistrationRequestsAdapter(this, nonActivatedUsers, RegistrationRequestsActivity.this, userCompanyMap);
                                        recyclerView.setAdapter(adapter);
                                    }
                                }
                            });
                        }
                    });
                }
            } else {
                Log.e("RegistrationRequests", "Error fetching non-activated users", task.getException());
            }
        });
    }

    @Override
    public void onAccept(User user) {
        user.setActivated(true);
        userService.update(user).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                sendVerificationEmail(user);
                fetchNonActivatedUsers();
            } else {
                Log.e("RegistrationRequests", "Error updating user", task.getException());
            }
        });
    }

    @Override
    public void onReject(User user, String reason) {
        String subject = "Registration Request Denied";
        String messageBody = "Dear " + user.getFullName() + ",\n\n"
                + "We regret to inform you that your registration request has been denied.\n"
                + "Reason for denial: " + reason + "\n\n"
                + "If you have any further questions or concerns, please feel free to contact us.\n\n"
                + "Sincerely,\n"
                + "Event Planner Team";

        EmailService.sendEmail(user.getEmail(), subject, messageBody);
    }


    private void sendVerificationEmail(User givenUserClass) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser originalUser = auth.getCurrentUser();

        if (originalUser != null) {
            String originalUserEmail = originalUser.getEmail();

            // Assuming you have a method to get the original user's password from the user service
            userService.getByEmail(originalUserEmail).addOnCompleteListener(originalTask -> {
                if (originalTask.isSuccessful() && originalTask.getResult() != null) {
                    String originalUserPassword = originalTask.getResult().getPassword();

                    // Log out the original user
                    auth.signOut();

                    // Log into the given user's profile
                    auth.signInWithEmailAndPassword(givenUserClass.getEmail(), givenUserClass.getPassword())
                            .addOnCompleteListener(task -> {
                                if (task.isSuccessful()) {
                                    FirebaseUser givenUser = auth.getCurrentUser();
                                    if (givenUser != null) {
                                        givenUser.sendEmailVerification()
                                                .addOnCompleteListener(emailTask -> {
                                                    if (emailTask.isSuccessful()) {
                                                        Toast.makeText(this, "Verification email sent to given user", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        Toast.makeText(this, "Failed to send verification email to given user", Toast.LENGTH_SHORT).show();
                                                    }

                                                    // Log out the given user
                                                    auth.signOut();

                                                    // Log back in as the original user
                                                    auth.signInWithEmailAndPassword(originalUserEmail, originalUserPassword)
                                                            .addOnCompleteListener(originalSignInTask -> {
                                                                if (originalSignInTask.isSuccessful()) {
                                                                    Toast.makeText(this, "Logged back in as original user", Toast.LENGTH_SHORT).show();
                                                                } else {
                                                                    Toast.makeText(this, "Failed to log back in as original user", Toast.LENGTH_SHORT).show();
                                                                }
                                                            });
                                                });
                                    }
                                } else {
                                    Toast.makeText(this, "Failed to log in as the given user", Toast.LENGTH_SHORT).show();
                                    // Try to log back in as the original user in case of failure
                                    assert originalUserEmail != null;
                                    auth.signInWithEmailAndPassword(originalUserEmail, originalUserPassword)
                                            .addOnCompleteListener(originalSignInTask -> {
                                                if (originalSignInTask.isSuccessful()) {
                                                    Toast.makeText(this, "Logged back in as original user", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(this, "Failed to log back in as original user", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                }
                            });
                } else {
                    Toast.makeText(this, "Failed to get original user's password", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}