package com.example.eventplanner.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.EventActivity;
import com.example.eventplanner.models.Guest;
import com.example.eventplanner.services.EventService;
import com.example.eventplanner.services.PDFReportService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.List;

public class EventActivitiesActivity extends AppCompatActivity {
    private EventService eventService;
    private PDFReportService reportService;
    private Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_event_activities);

        eventService = new EventService(new EventRepository());
        reportService = new PDFReportService();

        int marginBetweenItems = getResources().getDimensionPixelSize(R.dimen.margin_between_items);
        LinearLayout parentLayout = findViewById(R.id.main);

        String eventId = (String) getIntent().getSerializableExtra("EVENT");

        eventService.getById(eventId, new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                EventActivitiesActivity.this.event = events.get(0);

                for (int i = 0; i < EventActivitiesActivity.this.event.getActivities().size(); i++) {
                    final String activityId = EventActivitiesActivity.this.event.getActivities().get(i).getName();

                    LinearLayout itemLayout = new LinearLayout(EventActivitiesActivity.this);
                    itemLayout.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    ));
                    itemLayout.setOrientation(LinearLayout.VERTICAL);

                    TextView itemNameTextView = new TextView(EventActivitiesActivity.this);
                    itemNameTextView.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    ));
                    SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
                    String from = formatter.format(EventActivitiesActivity.this.event.getActivities().get(i).getFrom());
                    String to = formatter.format(EventActivitiesActivity.this.event.getActivities().get(i).getTo());
                    itemNameTextView.setText(EventActivitiesActivity.this.event.getActivities().get(i).getName() + " (" + from + " - " + to + ")");
                    itemLayout.addView(itemNameTextView);

                    TextView descriptionTextView = new TextView(EventActivitiesActivity.this);
                    descriptionTextView.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    ));
                    descriptionTextView.setText("Description: " + (EventActivitiesActivity.this.event.getActivities().get(i).getDescription()));
                    itemLayout.addView(descriptionTextView);

                    TextView locationTextView = new TextView(EventActivitiesActivity.this);
                    locationTextView.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    ));
                    locationTextView.setText("Location: " + (EventActivitiesActivity.this.event.getActivities().get(i).getLocation()));
                    itemLayout.addView(locationTextView);

                    Button editButton = new Button(EventActivitiesActivity.this);
                    LinearLayout.LayoutParams editParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    );
                    editParams.setMargins(0, marginBetweenItems / 4, 0, 0);
                    editParams.gravity = Gravity.CENTER_HORIZONTAL;
                    editButton.setLayoutParams(editParams);
                    editButton.setText("Edit");
                    editButton.setTextColor(Color.WHITE);
                    editButton.setBackgroundColor(ContextCompat.getColor(EventActivitiesActivity.this, R.color.purple_btn));
                    editButton.setOnClickListener(v -> {
                        Intent editIntent = new Intent(EventActivitiesActivity.this, EditEventActivitiesActivity.class);
                        editIntent.putExtra("EVENT", event.getId());
                        editIntent.putExtra("ACTIVITY", activityId);
                        EventActivitiesActivity.this.startActivity(editIntent);
                    });
                    itemLayout.addView(editButton);

                    Button deleteButton = new Button(EventActivitiesActivity.this);
                    LinearLayout.LayoutParams deleteParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    );
                    deleteParams.setMargins(0, marginBetweenItems / 4, 0, 0);
                    deleteParams.gravity = Gravity.CENTER_HORIZONTAL;
                    deleteButton.setLayoutParams(deleteParams);
                    deleteButton.setText("Delete");
                    deleteButton.setTextColor(Color.WHITE);
                    deleteButton.setBackgroundColor(ContextCompat.getColor(EventActivitiesActivity.this, R.color.purple_btn));
                    deleteButton.setOnClickListener(v -> {
                        removeActivity(activityId);
                    });
                    itemLayout.addView(deleteButton);

                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) itemLayout.getLayoutParams();
                    params.setMargins(0, 0, 0, marginBetweenItems);
                    itemLayout.setLayoutParams(params);

                    parentLayout.addView(itemLayout);
                }
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });

        FloatingActionButton addButton = findViewById(R.id.fab);
        addButton.setOnClickListener(v -> {
            Intent creationIntent = new Intent(EventActivitiesActivity.this, EventActivitiesCreationActivity.class);
            creationIntent.putExtra("EVENT", event.getId());
            EventActivitiesActivity.this.startActivity(creationIntent);
        });

        FloatingActionButton fileButton = findViewById(R.id.file);
        fileButton.setOnClickListener(v -> {
            showDownloadDialog();
        });
    }

    private void removeActivity(String activityId) {
        EventActivity activitiy = event.getActivities()
                .stream()
                .filter(g -> g.getName().equals(activityId))
                .findFirst()
                .orElse(null);

        showDeleteConfirmationDialog(activitiy);
    }

    private void showDeleteConfirmationDialog(EventActivity activitiy) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to remove this activity?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        event.removeActivity(activitiy);
                        eventService.update(event);
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    private void showDownloadDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to download the activity list (PDF)?")
                .setTitle("Download activity list")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        reportService.generateActivityListReport(EventActivitiesActivity.this, event);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}