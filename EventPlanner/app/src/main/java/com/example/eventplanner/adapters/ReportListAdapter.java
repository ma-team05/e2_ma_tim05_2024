package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.UserProfileActivity;
import com.example.eventplanner.activities.admins_actions.ReportManagementActivity;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.database.repository.ProductRepository;
import com.example.eventplanner.database.repository.ServiceRepository;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.Report;
import com.example.eventplanner.models.Reservation;
import com.example.eventplanner.models.ReservationStatus;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.PackageService;
import com.example.eventplanner.services.ProductService;
import com.example.eventplanner.services.ReportService;
import com.example.eventplanner.services.ReservationService;
import com.example.eventplanner.services.ServiceService;
import com.example.eventplanner.services.UserService;
import com.google.firebase.Timestamp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ReportListAdapter extends  RecyclerView.Adapter<ReportListAdapter.ReportViewHolder>{
    private final Context context;
    private final List<Report> reports;
    private final ReportService reportService;
    private ReportManagementActivity parentActivity;
    private UserService userService = new UserService();
    private EmployeeService employeeService = new EmployeeService(new EmployeeRepository());
    private OwnerService ownerService = new OwnerService();
    private ReservationService reservationService = new ReservationService();
    private PackageService packageService = new PackageService(new PackageRepository());
    private ProductService productService = new ProductService(new ProductRepository());
    private ServiceService serviceService = new ServiceService(new ServiceRepository());

    private NotificationService notificationService = new NotificationService();

    public ReportListAdapter(Context context, List<Report> reports,ReportService reportService, ReportManagementActivity parentActivity) {
        this.context = context;
        this.reports = reports;
        this.reportService = reportService;
        this.parentActivity = parentActivity;
    }

    @NonNull
    @Override
    public ReportListAdapter.ReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_report_card, parent, false);
        return new ReportListAdapter.ReportViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportListAdapter.ReportViewHolder holder, int position) {
        Report report = reports.get(position);
        holder.bind(report);
    }
    @Override
    public int getItemCount() {
        return reports.size();
    }

    public class ReportViewHolder extends RecyclerView.ViewHolder {

        ImageButton approveButton, denyButton;
        TextView reportedUser, reportDate, reportStatus, ReportingUser;
        private Report currentReport;
        private int currentIndex;
        public ReportViewHolder(@NonNull View itemView) {
            super(itemView);
            approveButton = itemView.findViewById(R.id.report_card_approve);
            denyButton = itemView.findViewById(R.id.report_card_deny);

            ReportingUser = itemView.findViewById(R.id.report_card_reporting);
            reportedUser = itemView.findViewById(R.id.report_card_reported);
            reportDate = itemView.findViewById(R.id.report_card_date);
            reportStatus = itemView.findViewById(R.id.report_card_status);
            approveButton.setOnClickListener(v ->{
                approveReport(currentReport);
            });
            denyButton.setOnClickListener(v ->{
                denyReport(currentReport);
            });

            reportedUser.setOnClickListener(v ->{
                Intent intent = new Intent(itemView.getContext(), UserProfileActivity.class);
                intent.putExtra("EMAIL", currentReport.getReportedUsername());
                intent.putExtra("ForReport", "view");
                parentActivity.startActivityForResult(intent, 2);
            });

            ReportingUser.setOnClickListener(v ->{
                Intent intent = new Intent(itemView.getContext(), UserProfileActivity.class);
                intent.putExtra("EMAIL", currentReport.getReporterUsername());
                intent.putExtra("ForReport", "view");
                parentActivity.startActivityForResult(intent, 2);
            });


        }

        public void bind(Report report) {
            currentReport = report;
            currentIndex = 0;
            reportedUser.setText(report.getReportedUsername());
            ReportingUser.setText(report.getReporterUsername());
            Timestamp firebaseTimestamp = report.getReportTimestamp();
            Date date = firebaseTimestamp.toDate();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String formattedDate = dateFormat.format(date);
            reportDate.setText(formattedDate);
            reportStatus.setText(report.getReportType().name());

            if(report.getReportType() != Report.REPORT_TYPE.Reported){
                approveButton.setVisibility(View.GONE);
                denyButton.setVisibility(View.GONE);
            }

        }

        private void approveReport(Report report){
            report.setReportType(Report.REPORT_TYPE.Accepted);
            reportService.update(report);
            notifyDataSetChanged();

            if(report.isForCompany()){

                blockPup(report);
            }else{
                blockUser(report);
            }

        }

        private void denyReport(Report report){
            showPopUpForCancelling(report);

        }


    }

    private void blockPup(Report report){
        userService.getByEmail(report.getReportedUsername()).addOnSuccessListener(user ->{
            User owner = user;
            user.setActivated(false);
            userService.update(user);
            ownerService.get(owner.getId()).addOnSuccessListener(ownerObject ->{
                ownerObject.setActivated(false);
                ownerService.update(ownerObject);
                // block of the employees
                blockEmployees(ownerObject.getCompanyId(), report.getReportedUsername());
            });

        });
    }

    private void blockEmployees(String companyId, String reportedUsername){
        employeeService.getAllByCompany(companyId).addOnSuccessListener(employees ->{
            for(Employee e : employees){
                e.setActivated(false);
                userService.get(e.getId()).addOnSuccessListener( user ->{
                    user.setActivated(false);
                    userService.update(user);
                });
            }
            //update all employes in batch
            employeeService.updateBatch(employees);

        });
        cancelPuPReservations(companyId, reportedUsername);
    }

    private void blockUser(Report report){
        userService.getByEmail(report.getReportedUsername()).addOnSuccessListener(user ->{
            user.setActivated(false);
            userService.update(user);
            cancelUserReservations(user.getId(), report.getReportedUsername());

        });
    }

    private void cancelPuPReservations(String companyId, String reportedUsername){
        reservationService.getActiveByCompanyId(companyId).addOnSuccessListener( reservations ->{
            for(Reservation res: reservations){
                res.setStatus(ReservationStatus.CANCELED_BY_ADMIN);
                sendReservationsCancelationNotification(res.getEventPlanner().getId(),reportedUsername);
            }
            reservationService.updateBatch(reservations);



        });
        cancelAll(companyId);
    }

    private void cancelUserReservations(String eventPlannerId, String reportedUsername){
        reservationService.getActiveByEventPlanner(eventPlannerId).addOnSuccessListener( reservations ->{
            for(Reservation res: reservations){
                res.setStatus(ReservationStatus.CANCELED_BY_ADMIN);
                sendReservationsCancelationNotification(res.getEmployee().getId(),reportedUsername);
            }
            reservationService.updateBatch(reservations);
        });
    }

    private void cancelAll(String companyId){
        productService.getUndeletedOfPUP(companyId, new ProductService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Product> objects) {
                for(Product p : objects){
                    p.setAvailable(false);
                }
                productService.updateBatch(objects);
                cancelServices(companyId);
            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });

    }

    private void cancelServices(String companyId){
        serviceService.getUndeletedOfPUP(companyId, new ServiceService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Service> objects) {
                for(Service s : objects){
                    s.setAvailable(false);
                }
                serviceService.updateBatch(objects);
                calcelPackages(companyId);
            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });
    }

    private void calcelPackages(String companyId){
        packageService.getUndeletedOfPUP(companyId, new PackageService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Package> objects) {
                for(Package p : objects){
                    p.setAvailable(false);
                }
                packageService.updatePackagesInFirestore(objects);

            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });
    }

    private void sentDenialNotification(String reporterId, String reportedUsername, String reason){
        StringBuilder sb = new StringBuilder();
        sb.append("Your report of ").append(reportedUsername).append(". Reason: ").append(reason);
        notificationService.add(reporterId,sb.toString(), NotificationType.REPORT);
    }

    private void showPopUpForCancelling( Report report){
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.report_form, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;

        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        EditText reason = popupView.findViewById(R.id.report_reason);

        //show window
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.report_save_btn);
        Button cancelButton = popupView.findViewById(R.id.report_cancel_btn);

        saveButton.setOnClickListener(v -> {
            report.setReportType(Report.REPORT_TYPE.Denied);
            reportService.update(report);
            notifyDataSetChanged();
            sentDenialNotification(report.getReporterId(),report.getReportedUsername(),reason.getText().toString());
            popupWindow.dismiss();
        });

        cancelButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });
    }

    private void sendReservationsCancelationNotification(String userId, String reportedUsername){
        StringBuilder sb = new StringBuilder();
        sb.append("Reservation of user").append(reportedUsername).append(" has been canceled by admin because ").append(reportedUsername).append(" has been blocked.");
        notificationService.add(userId,sb.toString(),NotificationType.RESERVATION);

    }

}
