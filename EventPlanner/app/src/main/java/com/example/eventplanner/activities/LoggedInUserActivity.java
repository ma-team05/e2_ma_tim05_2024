package com.example.eventplanner.activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.employees.EmployeeDetailsActivity;
import com.example.eventplanner.activities.employees.EmployeesManagementActivity;
import com.example.eventplanner.activities.reservations.ReservationsActivity;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.example.eventplanner.models.Notification;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.User;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.UserService;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.List;

public class LoggedInUserActivity extends AppCompatActivity {

    private User loggedUser;
    private final UserService userService = new UserService();
    private final OwnerService ownerService = new OwnerService();
    private final EmployeeService employeeService = new EmployeeService(new EmployeeRepository());
    private final NotificationService notificationService = new NotificationService();
    private Handler handler;
    private Runnable runnable;
    private static final int INTERVAL = 3000;
    private String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in_user);

        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");
        String email = intent.getStringExtra("email");
        UserRole userRole = UserRole.values()[intent.getIntExtra("userRole", 0)];
        handler = new Handler();
        runnable = () -> {
            checkAndSendNotifications();
            handler.postDelayed(runnable, INTERVAL);
        };
        handler.post(runnable);
        setupButtons(userId, email, String.valueOf(userRole));
        handleUserRole(userId, email, userRole);
    }

    private void setupButtons(String userId, String email, String userRole) {
        setupButton(R.id.button_product_management, ProductManagementActivity.class, userId, userRole);
        setupButton(R.id.button_services_management, ServiceManagementActivity.class, userId, userRole);
        setupButton(R.id.add_event, MyEventsActivity.class, userId, userRole, email);
        setupButton(R.id.search_services, ServiceSearchActivity.class, userId, userRole, email);
        setupButton(R.id.button_packages_management, PackageManagementActivity.class, userId, userRole);
        setupButton(R.id.button_pricelist, PriceListActivity.class, userId, userRole);
        setupButton(R.id.button_chats, MyChatsActivity.class, userId, userRole, email);
        //setupButton(R.id.button_grade_company, GradingCompanyActivity.class, userId, userRole);
        setupButton(R.id.button_grades, GradesActivity.class, userId, userRole);
        setupButton(R.id.button_reservations, ReservationsActivity.class, userId, userRole);
        setupButton(R.id.profile, UserProfileActivity.class, userId, userRole, email);
        setupButton(R.id.notifications, NotificationsActivity.class, userId, userRole, email);
    }

    private void setupButton(int buttonId, Class<?> targetActivity, String userId, String userRole) {
        Button button = findViewById(buttonId);
        Intent intent = new Intent(this, targetActivity);
        intent.putExtra("userId", userId);
        intent.putExtra("userRole", userRole);
        button.setOnClickListener(v -> startActivity(intent));
    }

    private void setupButton(int buttonId, Class<?> targetActivity, String userId, String userRole, String email) {
        Button button = findViewById(buttonId);
        Intent intent = new Intent(this, targetActivity);
        intent.putExtra("userId", userId);
        intent.putExtra("userRole", userRole);
        intent.putExtra("email", email);
        intent.putExtra("EMAIL", email);
        intent.putExtra("EVENT", "");
        intent.putExtra("ForReport", "no");
        button.setOnClickListener(v -> startActivity(intent));
    }

    private void handleUserRole(String userId, String email, UserRole userRole) {
        switch (userRole) {
            case EMPLOYEE:
                handleEmployeeRole(userId);
                break;
            case OWNER:
                handleOwnerRole(userId);
                break;
            case EVENT_PLANNER:
                handleEventPlannerRole(email);
                break;
            default:
                break;
        }
    }

    private void handleEmployeeRole(String userId) {
        employeeService.get(userId).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                loggedUser = task.getResult();
                findViewById(R.id.button_employees_management).setVisibility(View.GONE);
                setupEmployeeProfileButton(loggedUser.getId(), false);
            }
        });
    }

    private void handleOwnerRole(String userId) {
        ownerService.get(userId).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                loggedUser = task.getResult();
                findViewById(R.id.button_employee_profile).setVisibility(View.GONE);
                String companyId = ((Owner) loggedUser).getCompanyId();
                setupButton(R.id.button_employees_management, EmployeesManagementActivity.class, companyId, null);
            }
        });
    }
    private void handleEventPlannerRole(String email) {
        userService.getByEmail(email).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                loggedUser = task.getResult();
                setupButton(R.id.add_event, MyEventsActivity.class, null, null, email);
                hideButtonsForEventPlanner();
            }
        });
    }
    private void setupEmployeeProfileButton(String employeeId, boolean viewForOwner) {
        Button button = findViewById(R.id.button_employee_profile);
        Intent intent = new Intent(this, EmployeeDetailsActivity.class);
        intent.putExtra("employeeId", employeeId);
        intent.putExtra("viewForOwner", viewForOwner);
        button.setOnClickListener(v -> startActivity(intent));
    }

    private void hideButtonsForEventPlanner() {
        findViewById(R.id.button_employees_management).setVisibility(View.GONE);
        findViewById(R.id.button_packages_management).setVisibility(View.GONE);
        findViewById(R.id.button_services_management).setVisibility(View.GONE);
        findViewById(R.id.button_product_management).setVisibility(View.GONE);
        findViewById(R.id.button).setVisibility(View.GONE);
        findViewById(R.id.button_employee_profile).setVisibility(View.GONE);
        findViewById(R.id.button_pricelist).setVisibility(View.GONE);
    }

    private void checkAndSendNotifications() {
        notificationService.getNotificationsByUser(userId).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<Notification> notifications = task.getResult();
                if (notifications != null) {
                    for (Notification notification : notifications) {
                        if (!notification.isSent()) {
                            sendNotification(notification);
                            notification.setSent(true);
                            notificationService.update(notification);
                        }
                    }
                }
            }
        });
    }

    private void sendNotification(Notification notification) {
        FirebaseMessaging.getInstance().subscribeToTopic("notification")
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        triggerLocalNotification(notification);
                    }
                });
    }

    private void triggerLocalNotification(Notification notification) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "event_planner_channel";

        NotificationChannel channel = new NotificationChannel(channelId, "Event Planner Notification", NotificationManager.IMPORTANCE_DEFAULT);
        notificationManager.createNotificationChannel(channel);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setContentTitle(notification.getType().toString())
                .setContentText(notification.getDescription())
                .setSmallIcon(R.drawable.notification_icon)
                .setAutoCancel(true)
                .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, LoggedInUserActivity.class), PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE));

        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }
}