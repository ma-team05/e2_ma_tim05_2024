package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.BudgetItem;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.services.EventService;

import java.util.ArrayList;
import java.util.List;

public class BudgetItemCreationActivity extends AppCompatActivity {
    private EventService eventService;
    private Event event;

    List<Category> subcategories = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_item_creation);

        eventService = new EventService(new EventRepository());

        String eventId = (String) getIntent().getSerializableExtra("EVENT");

        eventService.getById(eventId, new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                BudgetItemCreationActivity.this.event = events.get(0);

                for (Category category : BudgetItemCreationActivity.this.event.getEventType().getRecommendedServices()) {
                    BudgetItemCreationActivity.this.subcategories.addAll(category.getSubcategories());
                }

                List<String> names = new ArrayList<>();
                names.add("Select a subcategory");
                for (Category category : subcategories) {
                    names.add(category.getName());
                }

                Spinner typeSpinner = findViewById(R.id.type);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(BudgetItemCreationActivity.this, android.R.layout.simple_spinner_item, names);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                typeSpinner.setAdapter(adapter);
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });

        Button createButton = findViewById(R.id.createButton);
        createButton.setOnClickListener(v -> validateFields());
    }

    private boolean validateFields() {
        boolean isValid = true;

        EditText eventNameEditText;
        Spinner eventTypeSpinner;
        String field;
        String eventName;
        String evenType;

        eventNameEditText = findViewById(R.id.name);
        field = eventNameEditText.getText().toString().trim();
        eventName = field;
        if (TextUtils.isEmpty(field)) {
            eventNameEditText.setError("Please enter a name");
            eventNameEditText.requestFocus();
            isValid = false;
        }

        eventTypeSpinner = findViewById(R.id.type);
        field = eventTypeSpinner.getSelectedItem().toString().trim();
        String eventType = field;
        if (field.equals("Select a subcategory")) {
            Toast.makeText(this, "Please select a subcategory", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        eventNameEditText = findViewById(R.id.ammount);
        field = eventNameEditText.getText().toString().trim();
        if (TextUtils.isEmpty(field)) {
            eventNameEditText.setError("Please enter the amount");
            eventNameEditText.requestFocus();
            isValid = false;
        } else {
            double maxGuests = Double.parseDouble(field);
            if (maxGuests < 1) {
                eventNameEditText.setError("Please enter a number bigger than 0 for amount");
                eventNameEditText.requestFocus();
                isValid = false;
            }
        }

        if(isValid) {
            createBudgetItem();
        }

        return isValid;
    }

    private void createBudgetItem() {
        EditText nameEditText = findViewById(R.id.name);
        EditText amountEditText = findViewById(R.id.ammount);
        Spinner eventTypeSpinner = findViewById(R.id.type);

        String name = nameEditText.getText().toString().trim();
        int position = eventTypeSpinner.getSelectedItemPosition();
        Category subcategory = subcategories.get(position - 1);

        double amount = Double.parseDouble(amountEditText.getText().toString().trim());

        BudgetItem item = new BudgetItem();
        item.setName(name);
        item.setAmmount(amount);
        item.setCategory(subcategory);

        event.addBudgetItem(item);
        eventService.update(event);

        String eventDetails = "Budget item created:\n" +
                "Name: " + item.getName() + "\n" +
                "Amount: " + item.getAmmount() + "\n" +
                "Category: " + subcategory.getName();
        Toast.makeText(this, eventDetails, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, BudgetPlanActivity.class);
        intent.putExtra("EVENT", event.getId());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        finish();
    }
}