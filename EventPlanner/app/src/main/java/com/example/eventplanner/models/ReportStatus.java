package com.example.eventplanner.models;

public enum ReportStatus {
    REPORTED,
    ACCEPTED,
    REJECTED
}
