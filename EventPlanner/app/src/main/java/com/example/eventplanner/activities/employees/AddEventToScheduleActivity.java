package com.example.eventplanner.activities.employees;

import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EmployeeEventAdapter;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.EmployeesWorkingHours;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.EmployeesWorkingHoursService;
import com.example.eventplanner.services.EventService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddEventToScheduleActivity extends AppCompatActivity {

    private boolean viewForOwner;
    private EmployeeEventAdapter eventAdapter;
    private String employeeId;
    private Button addButton;
    private Employee employee;
    private Date start;
    private Date end;
    private List<Event> availableEvents = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_add_event_to_schedule);
        Intent intent = getIntent();
        viewForOwner = intent.getBooleanExtra("viewForOwner", true);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try{
            start = format.parse(intent.getStringExtra("start"));
        }
        catch(Exception e){}
        try{
            end = format.parse(intent.getStringExtra("end"));
        }
        catch(Exception e){}
        employeeId = intent.getStringExtra("employeeId");
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        EmployeeService service = new EmployeeService(new EmployeeRepository());
        service.get(employeeId).addOnCompleteListener(task->{
            if(task.isSuccessful()) {
                employee = task.getResult();
                loadEvents();
            }
        });
        addButton = findViewById(R.id.btnAddEvent);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assignToEmployee();
            }
        });
    }
    private void assignToEmployee(){
        int index = eventAdapter.selectedItem;
        if(index==RecyclerView.NO_POSITION){
            Toast.makeText(this, "Please select the event first.", Toast.LENGTH_SHORT).show();
            return;
        }
        Event event = availableEvents.get(index);
        EventService eventService = new EventService(new EventRepository());
        if(viewForOwner)
            eventService.assignToEmployee(event, employeeId);
        else
            eventService.assignAndTake(event, employeeId);
        finish();
    }

    private void loadEvents(){
        RecyclerView recyclerView = findViewById(R.id.eventsRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this); // Use LinearLayoutManager for vertical list
        recyclerView.setLayoutManager(layoutManager);
        EventService eventService = new EventService(new EventRepository());
        EmployeesWorkingHoursService workingHoursService = new EmployeesWorkingHoursService();
        workingHoursService.getAllByEmployee(employeeId).addOnCompleteListener(task->{
           if(task.isSuccessful()){
               List<EmployeesWorkingHours> schedules = task.getResult();
               eventService.getAllUnassignedInWeek(start, end, new EventService.OnDataFetchListener() {
                   @Override
                   public void onSuccess(List<Event> events) {
                       List<Event> unassignedEvents = events;
                       List<Event> pom = new ArrayList<>();
                       for(Event e: unassignedEvents){
                           Date newest = null;
                           EmployeesWorkingHours workinghours = new EmployeesWorkingHours();
                           for(EmployeesWorkingHours h: schedules){
                               if(!h.getValidTo().before(e.getDate())&& !h.getValidFrom().after(e.getDate())){
                                   if (workinghours==null){
                                       workinghours = h;
                                       newest = h.getTimestamp();
                                   }
                                   else if(newest.before(h.getTimestamp())){
                                       workinghours = h;
                                       newest = h.getTimestamp();
                                   }
                               }
                           }
                           if(newest==null){
                               workinghours.setTimeSchedule(employee.getTimeSchedule());
                           }

                           switch(e.getDate().getDay()){
                               case 1:{
                                   Date date = e.getDate();
                                   int year = date.getYear() + 1900;
                                   int month = date.getMonth();
                                   int day = date.getDate();
                                   int hour = Integer.parseInt(workinghours.getTimeSchedule().mondayStart.split(" ")[0]);
                                   int minute = Integer.parseInt(workinghours.getTimeSchedule().mondayStart.split(" ")[2]);
                                   int hourFinish = Integer.parseInt(workinghours.getTimeSchedule().mondayFinish.split(" ")[0]);
                                   int minuteFinish = Integer.parseInt(workinghours.getTimeSchedule().mondayFinish.split(" ")[2]);
                                   Calendar calendar = Calendar.getInstance();
                                   calendar.set(year, month, day, hour, minute);
                                   Date equivalentDate = calendar.getTime();
                                   calendar.set(year, month, day, hourFinish, minuteFinish);
                                   Date equivalentDateFinish = calendar.getTime();
                                   if(e.getFrom().after(equivalentDate)
                                    && e.getTo().before(equivalentDateFinish)
                                   ){
                                       pom.add(e);
                                   }
                                   break;
                               }
                               case 2:{
                                   Date date = e.getDate();
                                   int year = date.getYear() + 1900;
                                   int month = date.getMonth();
                                   int day = date.getDate();
                                   int hour = Integer.parseInt(workinghours.getTimeSchedule().tuesdayStart.split(" ")[0]);
                                   int minute = Integer.parseInt(workinghours.getTimeSchedule().tuesdayStart.split(" ")[2]);
                                   int hourFinish = Integer.parseInt(workinghours.getTimeSchedule().tuesdayFinish.split(" ")[0]);
                                   int minuteFinish = Integer.parseInt(workinghours.getTimeSchedule().tuesdayFinish.split(" ")[2]);
                                   Calendar calendar = Calendar.getInstance();
                                   calendar.set(year, month, day, hour, minute);
                                   Date equivalentDate = calendar.getTime();
                                   calendar.set(year, month, day, hourFinish, minuteFinish);
                                   Date equivalentDateFinish = calendar.getTime();
                                   if(e.getFrom().after(equivalentDate)
                                           && e.getTo().before(equivalentDateFinish)
                                   ){
                                       pom.add(e);
                                   }
                                   break;
                               }
                               case 3:{
                                   Date date = e.getDate();
                                   int year = date.getYear() + 1900;
                                   int month = date.getMonth();
                                   int day = date.getDate();
                                   int hour = Integer.parseInt(workinghours.getTimeSchedule().wednesdayStart.split(" ")[0]);
                                   int minute = Integer.parseInt(workinghours.getTimeSchedule().wednesdayStart.split(" ")[2]);
                                   int hourFinish = Integer.parseInt(workinghours.getTimeSchedule().wednesdayFinish.split(" ")[0]);
                                   int minuteFinish = Integer.parseInt(workinghours.getTimeSchedule().wednesdayFinish.split(" ")[2]);
                                   Calendar calendar = Calendar.getInstance();
                                   calendar.set(year, month, day, hour, minute);
                                   Date equivalentDate = calendar.getTime();
                                   calendar.set(year, month, day, hourFinish, minuteFinish);
                                   Date equivalentDateFinish = calendar.getTime();
                                   if(e.getFrom().after(equivalentDate)
                                           && e.getTo().before(equivalentDateFinish)
                                   ){
                                       pom.add(e);
                                   }
                                   break;
                               }
                               case 4:{
                                   Date date = e.getDate();
                                   int year = date.getYear() + 1900;
                                   int month = date.getMonth();
                                   int day = date.getDate();
                                   int hour = Integer.parseInt(workinghours.getTimeSchedule().thursdayStart.split(" ")[0]);
                                   int minute = Integer.parseInt(workinghours.getTimeSchedule().thursdayStart.split(" ")[2]);
                                   int hourFinish = Integer.parseInt(workinghours.getTimeSchedule().thursdayFinish.split(" ")[0]);
                                   int minuteFinish = Integer.parseInt(workinghours.getTimeSchedule().thursdayFinish.split(" ")[2]);
                                   Calendar calendar = Calendar.getInstance();
                                   calendar.set(year, month, day, hour, minute);
                                   Date equivalentDate = calendar.getTime();
                                   calendar.set(year, month, day, hourFinish, minuteFinish);
                                   Date equivalentDateFinish = calendar.getTime();
                                   if(e.getFrom().after(equivalentDate)
                                           && e.getTo().before(equivalentDateFinish)
                                   ){
                                       pom.add(e);
                                   }
                                   break;
                               }
                               case 5:{
                                   Date date = e.getDate();
                                   int year = date.getYear() + 1900;
                                   int month = date.getMonth();
                                   int day = date.getDate();
                                   int hour = Integer.parseInt(workinghours.getTimeSchedule().fridayStart.split(" ")[0]);
                                   int minute = Integer.parseInt(workinghours.getTimeSchedule().fridayStart.split(" ")[2]);
                                   int hourFinish = Integer.parseInt(workinghours.getTimeSchedule().fridayFinish.split(" ")[0]);
                                   int minuteFinish = Integer.parseInt(workinghours.getTimeSchedule().fridayFinish.split(" ")[2]);
                                   Calendar calendar = Calendar.getInstance();
                                   calendar.set(year, month, day, hour, minute);
                                   Date equivalentDate = calendar.getTime();
                                   calendar.set(year, month, day, hourFinish, minuteFinish);
                                   Date equivalentDateFinish = calendar.getTime();
                                   if(e.getFrom().after(equivalentDate)
                                           && e.getTo().before(equivalentDateFinish)
                                   ){
                                       pom.add(e);
                                   }
                                   break;
                               }
                               default:
                                   break;
                           }
                       }
                       List<Event> finalPom = pom;
                       eventService.getAllByEmployeeInWeek(employeeId, start, end, new EventService.OnDataFetchListener() {
                           @Override
                           public void onSuccess(List<Event> events) {
                               List<Event> employeesEvents = events;
                               for(Event e: finalPom){
                                   boolean overlap = false;
                                   for(Event v: employeesEvents){
                                       if(!(e.getFrom().after(v.getTo()) || e.getTo().before(v.getFrom()))){
                                           overlap = true;
                                           break;
                                       }
                                   }
                                   if(overlap) break;
                                   availableEvents.add(e);
                               }
                               eventAdapter = new EmployeeEventAdapter(availableEvents);
                               recyclerView.setAdapter(eventAdapter);
                           }

                           @Override
                           public void onFailure(String errorMessage) {
                           }
                       });
                   }

                   @Override
                   public void onFailure(String errorMessage) {
                   }
               });
           }
        });

    }
}