package com.example.eventplanner.models;

import java.util.ArrayList;
import java.util.List;

public class BudgetItem {
    private String id;
    private String eventId;
    private String name;
    private double ammount;
    private Category category;
    private List<ServiceReservation> reservations = new ArrayList<>();
    private List<Product> products = new ArrayList<>();

    public BudgetItem() {
    }

    public BudgetItem(String id, String eventId, String name, double ammount, Category category) {
        this.id = id;
        this.eventId = eventId;
        this.name = name;
        this.ammount = ammount;
        this.category = category;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmmount() {
        return ammount;
    }

    public void setAmmount(double ammount) {
        this.ammount = ammount;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public List<ServiceReservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<ServiceReservation> reservations) {
        this.reservations = reservations;
    }
    public void addReservation(ServiceReservation reservation) {
        reservations.add(reservation);
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
