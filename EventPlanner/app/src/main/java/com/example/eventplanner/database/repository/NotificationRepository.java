package com.example.eventplanner.database.repository;

import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Notification;
import com.example.eventplanner.models.Reservation;
import com.example.eventplanner.models.User;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class NotificationRepository {
    private final CollectionReference notificationCollection;

    public NotificationRepository() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        notificationCollection = db.collection("notification");
    }

    public Task<Void> update(Notification notification){
        DocumentReference notificationRef = notificationCollection.document(notification.getId());
        return notificationRef.set(notification);
    }

    public Task<String> save(Notification notification) {
        DocumentReference newNotificationRef = notificationCollection.document();
        notification.setId(newNotificationRef.getId());
        return newNotificationRef.set(notification)
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        return newNotificationRef.getId();
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }

    public Task<List<Notification>> getNotifications(){
        return notificationCollection.get().continueWith(task -> {
            if (task.isSuccessful()) {
                List<Notification> notifications = new ArrayList<>();
                QuerySnapshot querySnapshot = task.getResult();
                if (querySnapshot != null) {
                    for (DocumentSnapshot document : querySnapshot) {
                        Notification notification = document.toObject(Notification.class);
                        if (notification != null) {
                            notifications.add(notification);
                        }
                    }
                }
                return notifications;
            } else {
                throw Objects.requireNonNull(task.getException());
            }
        });
    }
    public Task<Void> addNotification(Notification notification){
        DocumentReference newNotificationRef = notificationCollection.document();
        notification.setId(newNotificationRef.getId());
        return newNotificationRef.set(notification);
    }
    public Task<List<Notification>> getNotificationsByUser(String userId){
        return notificationCollection.get()
                .continueWith(task -> {
                    List<Notification> notifications = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Notification notification = document.toObject(Notification.class);
                        assert notification != null;
                        if(notification.getUserId()==null)continue;
                        if(notification.getUserId().equals(userId))
                            notifications.add(notification);
                    }
                    return notifications;
                });
    }
}
