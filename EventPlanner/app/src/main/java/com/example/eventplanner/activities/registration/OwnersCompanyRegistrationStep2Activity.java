package com.example.eventplanner.activities.registration;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import com.example.eventplanner.R;
import java.util.Objects;

public class OwnersCompanyRegistrationStep2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owners_company_registration_step2);

        Intent intent = getIntent();
        String role = intent.getStringExtra("role");
        String email = intent.getStringExtra("email");
        String firstName = intent.getStringExtra("firstName");
        String lastName = intent.getStringExtra("lastName");
        String address = intent.getStringExtra("address");
        String phoneNumber = intent.getStringExtra("phoneNumber");
        String password = intent.getStringExtra("password");
        String companyType = intent.getStringExtra("companyType");
        String companyEmail = intent.getStringExtra("companyEmail");
        String companyName = intent.getStringExtra("companyName");
        String companyAddress = intent.getStringExtra("companyAddress");
        String companyPhoneNumber = intent.getStringExtra("companyPhoneNumber");
        String companyDescription = intent.getStringExtra("companyDescription");

        // Pass the owner's personal info to step 3 activity
        Button buttonNext = findViewById(R.id.nextButton);
        buttonNext.setOnClickListener(v -> {
            Intent intentStep3 = new Intent(OwnersCompanyRegistrationStep2Activity.this, OwnersCompanyRegistrationStep3Activity.class);
            intentStep3.putExtra("role", role);
            intentStep3.putExtra("email", email);
            intentStep3.putExtra("firstName", firstName);
            intentStep3.putExtra("lastName", lastName);
            intentStep3.putExtra("address", address);
            intentStep3.putExtra("phoneNumber", phoneNumber);
            intentStep3.putExtra("password", password);
            intentStep3.putExtra("companyType", companyType);
            intentStep3.putExtra("companyEmail", companyEmail);
            intentStep3.putExtra("companyName", companyName);
            intentStep3.putExtra("companyAddress", companyAddress);
            intentStep3.putExtra("companyPhoneNumber", companyPhoneNumber);
            intentStep3.putExtra("companyDescription", companyDescription);
            startActivity(intentStep3);
        });
    }

    public void navigateToStep3(View view) {
        Intent intent = new Intent(this, OwnersCompanyRegistrationStep3Activity.class);
        intent.putExtras(Objects.requireNonNull(getIntent().getExtras())); // Pass data received from previous activity
        startActivity(intent);
    }
}