package com.example.eventplanner.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.BudgetItem;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.services.EventService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class BudgetPlanActivity extends AppCompatActivity {
    private EventService eventService;
    private Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_plan);

        eventService = new EventService(new EventRepository());

        LinearLayout parentLayout = findViewById(R.id.main);

        int marginBetweenItems = getResources().getDimensionPixelSize(R.dimen.margin_between_items);

        String eventId = (String) getIntent().getSerializableExtra("EVENT");
        eventService.getById(eventId, new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                BudgetPlanActivity.this.event = events.get(0);

                for (int i = 0; i < BudgetPlanActivity.this.event.getBudgetItems().size(); i++) {
                    final String budgetItemId = BudgetPlanActivity.this.event.getBudgetItems().get(i).getName();

                    LinearLayout itemLayout = new LinearLayout(BudgetPlanActivity.this);
                    itemLayout.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    ));
                    itemLayout.setOrientation(LinearLayout.VERTICAL);

                    TextView itemNameTextView = new TextView(BudgetPlanActivity.this);
                    itemNameTextView.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    ));
                    itemNameTextView.setText(BudgetPlanActivity.this.event.getBudgetItems().get(i).getName() + " (" + BudgetPlanActivity.this.event.getBudgetItems().get(i).getCategory().getName()+ ")");
                    itemLayout.addView(itemNameTextView);

                    TextView amountTextView = new TextView(BudgetPlanActivity.this);
                    amountTextView.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    ));
                    amountTextView.setText("Ammount: " + BudgetPlanActivity.this.event.getBudgetItems().get(i).getAmmount());
                    itemLayout.addView(amountTextView);

                    Button viewButton = new Button(BudgetPlanActivity.this);
                    LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    );
                    viewParams.setMargins(0, marginBetweenItems / 4, 0, 0);
                    viewParams.gravity = Gravity.CENTER_HORIZONTAL;
                    viewButton.setLayoutParams(viewParams);
                    viewButton.setText("Details");
                    viewButton.setTextColor(Color.WHITE);
                    viewButton.setBackgroundColor(ContextCompat.getColor(BudgetPlanActivity.this, R.color.purple_btn));
                    viewButton.setOnClickListener(v -> {
                        Intent editIntent = new Intent(BudgetPlanActivity.this, ReservedServicesActivity.class);
                        editIntent.putExtra("EVENT", event.getId());
                        editIntent.putExtra("BUDGET_ITEM", budgetItemId);
                        BudgetPlanActivity.this.startActivity(editIntent);
                    });
                    itemLayout.addView(viewButton);

                    Button editButton = new Button(BudgetPlanActivity.this);
                    LinearLayout.LayoutParams editParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    );
                    editParams.setMargins(0, marginBetweenItems / 4, 0, 0);
                    editParams.gravity = Gravity.CENTER_HORIZONTAL;
                    editButton.setLayoutParams(editParams);
                    editButton.setText("Edit");
                    editButton.setTextColor(Color.WHITE);
                    editButton.setBackgroundColor(ContextCompat.getColor(BudgetPlanActivity.this, R.color.purple_btn));
                    editButton.setOnClickListener(v -> {
                        Intent editIntent = new Intent(BudgetPlanActivity.this, EditBudgetItemActivity.class);
                        editIntent.putExtra("EVENT", event.getId());
                        editIntent.putExtra("BUDGET_ITEM", budgetItemId);
                        BudgetPlanActivity.this.startActivity(editIntent);
                    });
                    itemLayout.addView(editButton);

                    Button deleteButton = new Button(BudgetPlanActivity.this);
                    LinearLayout.LayoutParams deleteParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    );
                    deleteParams.setMargins(0, marginBetweenItems / 4, 0, 0);
                    deleteParams.gravity = Gravity.CENTER_HORIZONTAL;
                    deleteButton.setLayoutParams(deleteParams);
                    deleteButton.setText("Delete");
                    deleteButton.setTextColor(Color.WHITE);
                    deleteButton.setBackgroundColor(ContextCompat.getColor(BudgetPlanActivity.this, R.color.purple_btn));
                    deleteButton.setOnClickListener(v -> {
                        deleteBudgetItem(budgetItemId);
                    });
                    itemLayout.addView(deleteButton);

                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) itemLayout.getLayoutParams();
                    params.setMargins(0, 0, 0, marginBetweenItems);
                    itemLayout.setLayoutParams(params);

                    parentLayout.addView(itemLayout);
                }

                TextView totalTextView = new TextView(BudgetPlanActivity.this);
                LinearLayout.LayoutParams totalParams = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );
                totalParams.gravity = Gravity.CENTER_HORIZONTAL;
                totalTextView.setLayoutParams(totalParams);
                double total = 0;
                for (BudgetItem budgetItem: BudgetPlanActivity.this.event.getBudgetItems()) {
                    total += budgetItem.getAmmount();
                }
                totalTextView.setText("Total: " + total);
                parentLayout.addView(totalTextView);

                TextView recommendedServicesTextView = findViewById(R.id.recommendedServices);
                String recommendedServices = "Recommended services: ";
                if(BudgetPlanActivity.this.event.getEventType().getRecommendedServices().size() != 0) {
                    for (Category category: BudgetPlanActivity.this.event.getEventType().getRecommendedServices()) {
                        recommendedServices += category.getName();
                        if(category.getSubcategories().size() != 0) {
                            recommendedServices += " (";
                            for (Category subcategory: category.getSubcategories()) {
                                recommendedServices += subcategory.getName() + ", ";
                            }
                            recommendedServices = recommendedServices.substring(0, recommendedServices.length() - 2);
                            recommendedServices += ")";
                        }
                        recommendedServices += ", ";
                    }
                    recommendedServices = recommendedServices.substring(0, recommendedServices.length() - 2);
                }
                recommendedServicesTextView.setText(recommendedServices);
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });

        FloatingActionButton addButton = findViewById(R.id.fab);
        addButton.setOnClickListener(v -> {
            Intent editIntent = new Intent(BudgetPlanActivity.this, BudgetItemCreationActivity.class);
            editIntent.putExtra("EVENT", event.getId());
            BudgetPlanActivity.this.startActivity(editIntent);
        });
    }

    private void deleteBudgetItem(String budgetItemId) {
        BudgetItem budgetItem = event.getBudgetItems()
                .stream()
                .filter(item -> item.getName().equals(budgetItemId))
                .findFirst()
                .orElse(null);

        showDeleteConfirmationDialog(budgetItem);
    }

    private void showDeleteConfirmationDialog(BudgetItem budgetItem) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete this budget item?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        boolean isItemRemoved = event.removeBudgetItem(budgetItem);
                        if(isItemRemoved) {
                            eventService.update(event);
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Can't delete budget items with reservated/bought services.", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }
}