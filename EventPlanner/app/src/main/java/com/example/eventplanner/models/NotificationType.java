package com.example.eventplanner.models;

public enum NotificationType {
    REGISTRATION,
    CHAT,
    GRADES,
    RESERVATION,
    ADMIN,
    REPORT
}
