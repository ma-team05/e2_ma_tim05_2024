package com.example.eventplanner.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Chat {
    private String id;
    private String participant1;
    private String participant2;
    private boolean readAllMessages1;
    private boolean readAllMessages2;
    private Date lastUpdated;
    private List<ChatMessage> messages = new ArrayList<>();

    public Chat() {
    }

    public Chat(String id, String participant1, String participant2, boolean readAllMessages1, boolean readAllMessages2, Date lastUpdated, List<ChatMessage> messages) {
        this.id = id;
        this.participant1 = participant1;
        this.participant2 = participant2;
        this.readAllMessages1 = readAllMessages1;
        this.readAllMessages2 = readAllMessages2;
        this.lastUpdated = lastUpdated;
        this.messages = messages;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParticipant1() {
        return participant1;
    }

    public void setParticipant1(String participant1) {
        this.participant1 = participant1;
    }

    public String getParticipant2() {
        return participant2;
    }

    public void setParticipant2(String participant2) {
        this.participant2 = participant2;
    }

    public boolean isReadAllMessages1() {
        return readAllMessages1;
    }

    public void setReadAllMessages1(boolean readAllMessages1) {
        this.readAllMessages1 = readAllMessages1;
    }

    public boolean isReadAllMessages2() {
        return readAllMessages2;
    }

    public void setReadAllMessages2(boolean readAllMessages2) {
        this.readAllMessages2 = readAllMessages2;
    }

    public List<ChatMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<ChatMessage> messages) {
        this.messages = messages;
    }

    public void addMessage(ChatMessage message) {
        this.messages.add(message);
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public void read(String participant) {
        if(participant1.equals(participant)) {
            readAllMessages1 = true;
        }
        else {
            readAllMessages2 = true;
        }

        for(ChatMessage message : messages) {
            if(message.getTo().equals(participant)) {
                message.setReadStatus(true);
            }
        }
    }
}
