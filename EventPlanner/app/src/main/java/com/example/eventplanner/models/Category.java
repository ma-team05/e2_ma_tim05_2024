package com.example.eventplanner.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Category {
    private String id;
    private String name;
    private String description;
    private SubcategoryType subcategoryType;
    private List<Category> subcategories;
    private List<Category> subcategoriesRequests;

    public Category() {}

    public Category(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.subcategories = new ArrayList<>();
        this.subcategoriesRequests = new ArrayList<>();
    }

    public Category(String name, String description) {
        this.name = name;
        this.description = description;
        this.subcategories = new ArrayList<>();
        this.subcategoriesRequests = new ArrayList<>();
    }

    public Category(String id, String name, String description, SubcategoryType subcategoryType) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.subcategories = new ArrayList<>();
        this.subcategoryType = subcategoryType;
        this.subcategoriesRequests = new ArrayList<>();
    }

    public Category(String name, String description, SubcategoryType subcategoryType) {
        this.name = name;
        this.description = description;
        this.subcategories = new ArrayList<>();
        this.subcategoryType = subcategoryType;
        this.subcategoriesRequests = new ArrayList<>();
    }

    public void setId(String id) { this.id = id; }
    public String getId() { return id; }
    public List<Category> getSubcategories() {
        return this.subcategories;
    }

    public void setSubcategories(List<Category> subcategories) {
        this.subcategories = subcategories;
    }
    public void setSubcategoriesRequests(List<Category> subcategoriesRequests) {
        this.subcategoriesRequests = subcategoriesRequests;
    }

    public List<Category> getSubcategoriesRequests() {
        return subcategoriesRequests;
    }

    public SubcategoryType getSubcategoryType() {
        return subcategoryType;
    }

    public void setSubcategoryType(SubcategoryType subcategoryType) {
        this.subcategoryType = subcategoryType;
    }
    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
    public boolean hasSubcategories() {
        return !subcategories.isEmpty();
    }

    public void addSubcategory(Category newSubcategory) {
        if (subcategories == null) {
            subcategories = new ArrayList<>();
        }
        subcategories.add(newSubcategory);
    }

    // The code is searching for a subcategory with the same old values and if it exists it updates it
    public void updateSubcategory(String oldName, String oldDescription, SubcategoryType oldSubcategoryType, Category newSubcategory) {
        for (Category subcategory : subcategories) {
            if (subcategory.getName().equals(oldName) &&
                    subcategory.getDescription().equals(oldDescription) &&
                    subcategory.getSubcategoryType() == oldSubcategoryType) {
                // Update the subcategory with new values
                subcategory.setName(newSubcategory.getName());
                subcategory.setDescription(newSubcategory.getDescription());
                subcategory.setSubcategoryType(newSubcategory.getSubcategoryType());
                return;
            }
        }
    }

    // Similar logic here
    public void deleteSubcategory(Category sub) {
        Iterator<Category> iterator = subcategories.iterator();
        while (iterator.hasNext()) {
            Category subcategory = iterator.next();
            if (subcategory.getName().equals(sub.getName()) &&
                    subcategory.getDescription().equals(sub.getDescription()) &&
                    subcategory.getSubcategoryType() == sub.getSubcategoryType()) {
                iterator.remove(); // Remove the selected subcategory
                return;
            }
        }
    }

    public void approveSubcategoryRequest(Category selectedSubcategoryRequest) {
        if (subcategoriesRequests == null) {
            subcategoriesRequests = new ArrayList<>();
        }
        Iterator<Category> iterator = subcategoriesRequests.iterator();
        while (iterator.hasNext()) {
            Category subcategoryRequest = iterator.next();
            if (subcategoryRequest.getName().equals(selectedSubcategoryRequest.getName()) &&
                    subcategoryRequest.getDescription().equals(selectedSubcategoryRequest.getDescription()) &&
                    subcategoryRequest.getSubcategoryType() == selectedSubcategoryRequest.getSubcategoryType()) {
                iterator.remove();
                addSubcategory(selectedSubcategoryRequest);
                return;
            }
        }
    }

    public void rejectSubcategoryRequest(Category selectedSubcategoryRequest) {
        if (subcategoriesRequests == null) {
            subcategoriesRequests = new ArrayList<>();
        }
        Iterator<Category> iterator = subcategoriesRequests.iterator();
        while (iterator.hasNext()) {
            Category subcategoryRequest = iterator.next();
            if (subcategoryRequest.getName().equals(selectedSubcategoryRequest.getName()) &&
                    subcategoryRequest.getDescription().equals(selectedSubcategoryRequest.getDescription()) &&
                    subcategoryRequest.getSubcategoryType() == selectedSubcategoryRequest.getSubcategoryType()) {
                iterator.remove();
                return;
            }
        }
    }
}