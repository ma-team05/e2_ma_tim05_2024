package com.example.eventplanner.services;

import com.example.eventplanner.models.Employee;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.google.android.gms.tasks.Task;

import java.util.List;

public class EmployeeService {
    private final EmployeeRepository repository;

    public EmployeeService(EmployeeRepository repository) {
        this.repository = repository;
    }
    public EmployeeService() {
        repository = new EmployeeRepository();
    }
    public void create(Employee employee, String userId) {
        repository.saveEmployee(employee, userId);
    }
    public Task<Void> update(Employee employee){
        return repository.updateEmployee(employee);
    }

    public Task<List<Employee>> getAll() {
        return repository.getAll();
    }
    public Task<List<Employee>> getAllByCompany(String companyId) {
        return repository.getAllByCompany(companyId);
    }

    public void delete(Employee employee) { repository.deleteEmployee(employee); }
    public Task<Employee> get(String id){ return repository.getById(id); }
    public void updateBatch(List<Employee> employees){
        repository.updateBatchOfEmployees(employees);
    }
}
