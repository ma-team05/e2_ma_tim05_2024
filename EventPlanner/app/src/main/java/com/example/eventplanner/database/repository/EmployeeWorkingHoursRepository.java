package com.example.eventplanner.database.repository;

import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.EmployeesWorkingHours;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class EmployeeWorkingHoursRepository {
    private FirebaseFirestore db;
    private CollectionReference employeesWorkingHoursCollection;

    public EmployeeWorkingHoursRepository(){
        db = FirebaseFirestore.getInstance();
        employeesWorkingHoursCollection = db.collection("employeesWorkingHours");
    }

    public Task<Void> saveEmployeesWorkingHours(EmployeesWorkingHours employeeWorkingHours){
        DocumentReference newEmployeesWorkingHoursRef = employeesWorkingHoursCollection.document();
        employeeWorkingHours.setId(newEmployeesWorkingHoursRef.getId());
        return newEmployeesWorkingHoursRef.set(employeeWorkingHours);
    }


    public Task<Void> updateEmployeesWorkingHours(EmployeesWorkingHours employeesWorkingHours){
        DocumentReference employeesWorkingHoursRef = employeesWorkingHoursCollection.document(employeesWorkingHours.getId());
        return employeesWorkingHoursRef.set(employeesWorkingHours);
    }

    public Task<Void> deleteEmployeesWorkingHours(EmployeesWorkingHours employeesWorkingHours){
        DocumentReference employeesWorkingHoursRef = employeesWorkingHoursCollection.document(employeesWorkingHours.getId());
        return employeesWorkingHoursRef.delete();
    }

    public Task<List<EmployeesWorkingHours>> getAllByEmployee(String employeeId){
        return employeesWorkingHoursCollection.get()
                .continueWith(task -> {
                    List<EmployeesWorkingHours> employeesWorkingHours = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        EmployeesWorkingHours employeesWorkingHour = document.toObject(EmployeesWorkingHours.class);
                        if(employeesWorkingHour.getEmployeeId().equals(employeeId))
                            employeesWorkingHours.add(employeesWorkingHour);
                    }
                    return employeesWorkingHours;
                });
    }

    public Task<EmployeesWorkingHours> getById(String id) {
        DocumentReference employeeRef = employeesWorkingHoursCollection.document(id);
        return employeeRef.get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            return document.toObject(EmployeesWorkingHours.class);
                        } else {
                            throw new Exception("Employees working hours with ID " + id + " not found");
                        }
                    } else {
                        throw task.getException();
                    }
                });
    }

}
