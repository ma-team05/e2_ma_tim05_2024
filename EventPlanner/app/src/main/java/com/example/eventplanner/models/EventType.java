package com.example.eventplanner.models;

import java.util.ArrayList;
import java.util.List;

public class EventType {
    private String id;
    private String name;
    private String description;
    private boolean activated;

    private List<Category> recommendedServices = new ArrayList<>();

    public EventType() { }
    public EventType(String name, String description, boolean activated) {
        this.name = name;
        this.description = description;
        this.activated = activated;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public void addRecommendedService(Category category) { recommendedServices.add(category); }

    public List<Category> getRecommendedServices() { return recommendedServices; }

    public void setRecommendedServices(List<Category> recommendedServices) { this.recommendedServices = recommendedServices; }
}
