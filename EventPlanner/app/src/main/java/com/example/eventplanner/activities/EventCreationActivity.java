package com.example.eventplanner.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EventAdapter;
import com.example.eventplanner.database.repository.CategoryRepository;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.database.repository.EventTypeRepository;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.EventStatus;
import com.example.eventplanner.models.EventType;

import com.example.eventplanner.R;
import com.example.eventplanner.models.SubcategoryType;
import com.example.eventplanner.services.EventService;
import com.example.eventplanner.services.EventTypeService;

public class EventCreationActivity extends AppCompatActivity {
    private EventService eventService;
    private EventTypeService eventTypeService;
    private ImageButton selectDateButton;
    private TextView dateLabel;
    private List<EventType> eventTypes = new ArrayList<>();

    Date from;
    Date to;
    String email;

    private Event event = new Event();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_creation);

        eventService = new EventService(new EventRepository());
        eventTypeService = new EventTypeService();

        selectDateButton = findViewById(R.id.selectDateButton);
        dateLabel = findViewById(R.id.dateLabel);

        Intent intent = getIntent();
        email = intent.getStringExtra("EMAIL");

        eventTypeService.getAll(new EventTypeService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<EventType> eventTypes) {
                eventTypes = eventTypes.stream().filter(t -> t.isActivated()).collect(Collectors.toList());

                List<EventType> types = new ArrayList<>();

                EventType select = new EventType("Select an event type", "Select an event type", true);
                EventType other = new EventType("Other", "Other", true);
                types.add(select);
                types.addAll(eventTypes);
                types.add(other);

                EventCreationActivity.this.eventTypes = types;

                List<String> names = new ArrayList<>();
                for (EventType type : types) {
                    names.add(type.getName());
                }

                Spinner typeSpinner = findViewById(R.id.type);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(EventCreationActivity.this, android.R.layout.simple_spinner_item, names);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                typeSpinner.setAdapter(adapter);

                typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position >= 0 && position <= types.size()) {
                            EventType selectedEventType = types.get(position);
                            onEventTypeSelection(selectedEventType);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }

            @Override
            public void onFailure(String errorMessage) {
                // Handle failure
            }
        });

        setDateLabel(Calendar.getInstance());

        Button createButton = findViewById(R.id.createButton);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields();
            }
        });

        selectDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        ImageButton startTimeButton = findViewById(R.id.timeButton);
        ImageButton endTimeButton = findViewById(R.id.endTimeButton);

        startTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog();
            }
        });

        endTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEndTimePickerDialog();
            }
        });
    }

    private void showTimePickerDialog() {
        // Get current time
        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        // Create TimePickerDialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        from = new Date();
                        from.setHours(hourOfDay);
                        from.setMinutes(minute);
                    }
                }, hour, minute, false);

        timePickerDialog.show();
    }

    private void showEndTimePickerDialog() {
        // Get current time
        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        // Create TimePickerDialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        to = new Date();
                        to.setHours(hourOfDay);
                        to.setMinutes(minute);
                    }
                }, hour, minute, false);

        timePickerDialog.show();
    }

    private void showDatePicker() {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                EventCreationActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(year, month, dayOfMonth);
                        setDateLabel(calendar);
                        String selectedDate = dayOfMonth + "/" + (month + 1) + "/" + year;
                        Toast.makeText(EventCreationActivity.this, "Selected date: " + selectedDate, Toast.LENGTH_SHORT).show();
                    }
                },
                year, month, dayOfMonth
        );

        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());

        datePickerDialog.show();
    }

    private void setDateLabel(Calendar calendar) {
        if (dateLabel != null) {
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH) + 1;
            int year = calendar.get(Calendar.YEAR);

            String dateString = "Date: " + String.format(Locale.getDefault(), "%02d/%02d/%d", dayOfMonth, month, year);

            dateLabel.setText(dateString);
        }
    }

    private List<String> getEventTypeNames(List<EventType> eventTypes) {
        List<String> names = new ArrayList<>();
        for (EventType type : eventTypes) {
            names.add(type.getName());
        }
        return names;
    }

    private void onEventTypeSelection(EventType selectedEventType) {
        TextView recommendedServicesTextView = findViewById(R.id.recommendedServices);
        recommendedServicesTextView.setText("");

        if (selectedEventType != null && !selectedEventType.getName().equals("Select an event type") && !selectedEventType.getName().equals("Other")) {
            String recommendedServices = "Recommended services: ";
            if(selectedEventType.getRecommendedServices().size() != 0) {
                for (Category category: selectedEventType.getRecommendedServices()) {
                    recommendedServices += category.getName();
                    if(category.getSubcategories().size() != 0) {
                        recommendedServices += " (";
                        for (Category subcategory: category.getSubcategories()) {
                            recommendedServices += subcategory.getName() + ", ";
                        }
                        recommendedServices = recommendedServices.substring(0, recommendedServices.length() - 2);
                        recommendedServices += ")";
                    }
                    recommendedServices += ", ";
                }
                recommendedServices = recommendedServices.substring(0, recommendedServices.length() - 2);
            }
            recommendedServicesTextView.setText(recommendedServices);
        } else {

        }
    }

    private boolean validateFields() {
        boolean isValid = true;

        EditText eventNameEditText;
        Spinner eventTypeSpinner;
        String field;
        String eventName;
        String evenType;

        eventNameEditText = findViewById(R.id.name);
        field = eventNameEditText.getText().toString().trim();
        eventName = field;
        if (TextUtils.isEmpty(field)) {
            eventNameEditText.setError("Please enter a name");
            eventNameEditText.requestFocus();
            isValid = false;
        }

        eventTypeSpinner = findViewById(R.id.type);
        field = eventTypeSpinner.getSelectedItem().toString().trim();
        String eventType = field;
        if (field.equals("Select an event type")) {
            Toast.makeText(this, "Please select an event type", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        eventNameEditText = findViewById(R.id.description);
        field = eventNameEditText.getText().toString().trim();
        if (TextUtils.isEmpty(field)) {
            eventNameEditText.setError("Please enter a description");
            eventNameEditText.requestFocus();
            isValid = false;
        }

        eventNameEditText = findViewById(R.id.maxGuests);
        field = eventNameEditText.getText().toString().trim();
        if (TextUtils.isEmpty(field)) {
            eventNameEditText.setError("Please enter the max. number of guests");
            eventNameEditText.requestFocus();
            isValid = false;
        } else {
            int maxGuests = Integer.parseInt(field);
            if (maxGuests < 1) {
                eventNameEditText.setError("Please enter a number bigger than 0 for max. number of guests");
                eventNameEditText.requestFocus();
                isValid = false;
            }
        }

        eventNameEditText = findViewById(R.id.city);
        field = eventNameEditText.getText().toString().trim();
        if (TextUtils.isEmpty(field)) {
            eventNameEditText.setError("Please enter the city");
            eventNameEditText.requestFocus();
            isValid = false;
        }

        eventNameEditText = findViewById(R.id.maxKm);
        field = eventNameEditText.getText().toString().trim();
        if (TextUtils.isEmpty(field)) {
            eventNameEditText.setError("Please enter the max. distance (km)");
            eventNameEditText.requestFocus();
            isValid = false;
        } else {
            double maxDistance = Double.parseDouble(field);
            if (maxDistance <= 0) {
                eventNameEditText.setError("Please enter a number greater than 0 for max. distance (km)");
                eventNameEditText.requestFocus();
                isValid = false;
            }
        }

        if(from == null) {
            Toast.makeText(this, "You must select start time", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(to == null) {
            Toast.makeText(this, "You must select end time", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(from.after(to)) {
            Toast.makeText(this, "Start time can't be after end time", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if(isValid) {
            createEvent();
        }

        return isValid;
    }

    private void createEvent() {
        EditText nameEditText = findViewById(R.id.name);
        EditText descriptionEditText = findViewById(R.id.description);
        EditText maxGuestsEditText = findViewById(R.id.maxGuests);
        EditText cityEditText = findViewById(R.id.city);
        EditText maxKmEditText = findViewById(R.id.maxKm);
        TextView dateLabel = findViewById(R.id.dateLabel);
        Switch openToPublicSwitch = findViewById(R.id.openToPublicSwitch);
        Spinner eventTypeSpinner = findViewById(R.id.type);

        String name = nameEditText.getText().toString().trim();
        String description = descriptionEditText.getText().toString().trim();
        String city = cityEditText.getText().toString().trim();
        String dateString = dateLabel.getText().toString().substring(6);
        boolean openToPublic = openToPublicSwitch.isChecked();
        int position = eventTypeSpinner.getSelectedItemPosition();
        EventType eventType = eventTypes.get(position);

        int maxGuests = Integer.parseInt(maxGuestsEditText.getText().toString().trim());
        double maxDistance = Double.parseDouble(maxKmEditText.getText().toString().trim());

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        Date date;
        try {
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }

        ImageButton startTimeButton = findViewById(R.id.timeButton);
        ImageButton endTimeButton = findViewById(R.id.endTimeButton);

        Event event = new Event();
        event.setName(name);
        event.setDescription(description);
        event.setMaxGuests(maxGuests);
        event.setOpenToPublic(openToPublic);
        event.setCity(city);
        event.setMaxDistance(maxDistance);
        event.setDate(date);
        from.setMonth(date.getMonth());
        from.setYear(date.getYear());
        from.setDate(date.getDate());

        to.setMonth(date.getMonth());
        to.setYear(date.getYear());
        to.setDate(date.getDate());
        event.setEventType(eventType);
        event.setFrom(from);
        event.setTo(to);
        event.setStatus(EventStatus.RESERVED);
        event.setUserEmail(email);

        eventService.create(event);

        String eventDetails = "Event created:\n" +
                "Name: " + event.getName() + "\n" +
                "Description: " + event.getDescription() + "\n" +
                "Max Guests: " + event.getMaxGuests() + "\n" +
                "Open to Public: " + event.isOpenToPublic() + "\n" +
                "City: " + event.getCity() + "\n" +
                "Max Distance: " + event.getMaxDistance() + "\n" +
                "Date: " + sdf.format(event.getDate());
        Toast.makeText(this, eventDetails, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, MyEventsActivity.class);
        intent.putExtra("email", email);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        finish();
    }
}