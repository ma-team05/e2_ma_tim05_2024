package com.example.eventplanner.services;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.eventplanner.activities.EventActivitiesActivity;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.EventActivity;
import com.example.eventplanner.models.Guest;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class PDFReportService {
    private static final String TAG = "PdfReportGenerator";

    public void generateGuestListReport(Context context, Event event) {
        android.graphics.pdf.PdfDocument document = new android.graphics.pdf.PdfDocument();
        android.graphics.pdf.PdfDocument.PageInfo pageInfo = new android.graphics.pdf.PdfDocument.PageInfo.Builder(1080, 1920, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        paint.setColor(android.graphics.Color.BLACK);
        paint.setTextSize(36);

        canvas.drawText("Guest list", 100, 100, paint);

        paint.setTextSize(30);
        canvas.drawText("Event: " + event.getName(), 100, 150, paint);

        paint.setTextSize(24);
        int startX = 100;
        int startY = 250;
        int rowHeight = 50;
        int columnWidth = 250;
        canvas.drawText("Full name", startX, startY, paint);
        canvas.drawText("Age", startX + columnWidth, startY, paint);
        canvas.drawText("Invited", startX + 2 * columnWidth, startY, paint);
        canvas.drawText("Special requests", startX + 3 * columnWidth, startY, paint);
        canvas.drawLine(startX, startY + 10, startX + 4 * columnWidth, startY + 10, paint);

        int currentY = startY + rowHeight;
        for (Guest guest : event.getGuests()) {
            canvas.drawText(guest.getFullName(), startX, currentY, paint);
            canvas.drawText(guest.getAgeAsString(), startX + columnWidth, currentY, paint);
            canvas.drawText(guest.getIsInvitedAsString(), startX + 2 * columnWidth, currentY, paint);
            canvas.drawText(guest.getSpecialRequestsShort(), startX + 3 * columnWidth, currentY, paint);
            currentY += rowHeight;
        }

        document.finishPage(page);

        String fileName = event.getName() + "_guest_list.pdf";
        download(context, document, fileName);

        document.close();
    }

    public void generateActivityListReport(Context context, Event event) {
        android.graphics.pdf.PdfDocument document = new android.graphics.pdf.PdfDocument();
        android.graphics.pdf.PdfDocument.PageInfo pageInfo = new android.graphics.pdf.PdfDocument.PageInfo.Builder(1080, 1920, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        paint.setColor(android.graphics.Color.BLACK);
        paint.setTextSize(36);

        canvas.drawText("Activity list", 100, 100, paint);

        paint.setTextSize(30);
        canvas.drawText("Event: " + event.getName(), 100, 150, paint);

        paint.setTextSize(24);
        int startX = 100;
        int startY = 250;
        int rowHeight = 50;
        int columnWidth = 200;
        canvas.drawText("Name", startX, startY, paint);
        canvas.drawText("Description", startX + columnWidth, startY, paint);
        canvas.drawText("Location", startX + 2 * columnWidth, startY, paint);
        canvas.drawText("Start time", startX + 3 * columnWidth, startY, paint);
        canvas.drawText("End time", startX + 4 * columnWidth, startY, paint);
        canvas.drawLine(startX, startY + 10, startX + 5 * columnWidth, startY + 10, paint);

        List<EventActivity> sortedActivities = new ArrayList<>(event.getActivities());
        Collections.sort(sortedActivities, new Comparator<EventActivity>() {
            @Override
            public int compare(EventActivity activity1, EventActivity activity2) {
                try {
                    long hours1 = activity1.getFrom().getHours();
                    long hours2 = activity2.getFrom().getHours();
                    if(hours1 < hours2) {
                        return -1;
                    }
                    else if (hours1 > hours2) {
                        return 1;
                    } else {
                        int minutes1 = activity1.getFrom().getMinutes();
                        int minutes2 = activity2.getFrom().getMinutes();
                        if(minutes1 < minutes2) {
                            return -1;
                        }
                        else if (minutes1 > minutes2) {
                            return 1;
                        } else {
                            long hours12 = activity1.getTo().getHours();
                            long hours22 = activity2.getTo().getHours();
                            if(hours12 < hours22) {
                                return -1;
                            }
                            else if (hours12 > hours22) {
                                return 1;
                            } else {
                                int minutes12 = activity1.getTo().getMinutes();
                                int minutes22 = activity2.getTo().getMinutes();
                                if(minutes12 < minutes22) {
                                    return -1;
                                }
                                else if (minutes12 > minutes22) {
                                    return 1;
                                } else {
                                    return -1;
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");

        int currentY = startY + rowHeight;
        for (EventActivity activity : sortedActivities) {
            canvas.drawText(activity.getName(), startX, currentY, paint);
            canvas.drawText(activity.getDescription(), startX + columnWidth, currentY, paint);
            canvas.drawText(activity.getLocation(), startX + 2 * columnWidth, currentY, paint);
            canvas.drawText(formatter.format(activity.getFrom()), startX + 3 * columnWidth, currentY, paint);
            canvas.drawText(formatter.format(activity.getTo()), startX + 4 * columnWidth, currentY, paint);
            currentY += rowHeight;
        }

        document.finishPage(page);

        String fileName = event.getName() + "_activity_list.pdf";
        download(context, document, fileName);

        document.close();
    }

    private void download(Context context, PdfDocument document, String fileName) {
        String directoryPath = Environment.getExternalStorageDirectory().getPath() + "/Download/";
        File file = new File(directoryPath, fileName);
        try {
            document.writeTo(new FileOutputStream(file));
            Toast.makeText(context, "Report downloaded", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context, "Report download failed", Toast.LENGTH_LONG).show();
        }
    }
}
