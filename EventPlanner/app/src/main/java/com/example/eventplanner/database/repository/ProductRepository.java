package com.example.eventplanner.database.repository;

import android.util.Log;

import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Product;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProductRepository {
    private final FirebaseFirestore db;
    public ProductRepository(){
        db = FirebaseFirestore.getInstance();
    }

    public void create(Product product){
        db.collection("products")
                .add(product)
                .addOnSuccessListener(documentReference -> {
                    String productId = documentReference.getId();
                    product.setId(productId);
                    documentReference.update("id", productId)
                            .addOnSuccessListener(aVoid -> {
                                Log.d("PRODUCT UPDATE", "Document updated with product id: " + productId);
                            })
                            .addOnFailureListener(e -> {
                                Log.w("PRODUCT UPDATE FAILED", "Error updating document", e);
                            });
                })
                .addOnFailureListener(e ->{
                    Log.w("PRODUCT CREATION FAILED", "Error adding document", e);
                });

    }

    public void update(Product product){
        String productId = product.getId();
        if (productId != null) {

            DocumentReference productRef = db.collection("products").document(productId);

            productRef.set(product)
                    .addOnSuccessListener(aVoid -> {
                        Log.d("PRODUCT UPDATE", "Product updated successfully: " + productId);
                    })
                    .addOnFailureListener(e -> {
                        Log.w("PRODUCT UPDATE FAILED", "Error updating product: " + productId, e);
                    });
        } else {
            Log.w("PRODUCT UPDATE FAILED", "Product ID is null");
        }

    }

    public void deleteLogucally(Product product){
        String productId = product.getId();
        product.setDeleted(true); // logical deletition
        if (productId != null) {

            DocumentReference productRef = db.collection("products").document(productId);

            productRef.set(product)
                    .addOnSuccessListener(aVoid -> {
                        Log.d("PRODUCT UPDATE", "Product updated successfully: " + productId);
                    })
                    .addOnFailureListener(e -> {
                        Log.w("PRODUCT UPDATE FAILED", "Error updating product: " + productId, e);
                    });
        } else {
            Log.w("PRODUCT UPDATE FAILED", "Product ID is null");
        }

    }

    public interface OnDataFetchListener {
        void onSuccess(List<Product> events);
        void onFailure(String errorMessage);
    }

    public Task<Product> getById(String id) {
        DocumentReference productsRef = db.collection("products").document(id);
        return productsRef.get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            return document.toObject(Product.class);
                        } else {
                            throw new Exception("Product with ID " + id + " not found");
                        }
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }

    public void getAll(final OnDataFetchListener listener){
        db.collection("products")

                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Product> productList = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Product product = documentSnapshot.toObject(Product.class);
                        productList.add(product);
                    }
                    // Process the list of products with categories here
                    processProductsWithCategories(productList);

                    listener.onSuccess(productList);
                })
                .addOnFailureListener(e -> {
                    Log.w("GET PRODUCTS FAILED", "Error getting products", e);
                    listener.onFailure(e.getMessage());
                });
    }

    public void getUndeletedOfPUP(String userId ,final OnDataFetchListener listener){
        db.collection("products")
                .whereEqualTo("companyId", userId)
                .whereEqualTo("deleted", false)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Product> productList = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Product product = documentSnapshot.toObject(Product.class);
                        productList.add(product);
                    }
                    // Process the list of products with categories here
                    processProductsWithCategories(productList);
                    listener.onSuccess(productList);
                })
                .addOnFailureListener(e -> {
                    Log.w("GET PRODUCTS FAILED", "Error getting products", e);
                    listener.onFailure(e.getMessage());
                });
    }

    private void processProductsWithCategories(List<Product> productList) {
        for (Product product : productList) {
            String categoryId = product.getCategoryId();
            if (categoryId != null) {

                db.collection("categories")
                        .document(categoryId)
                        .get()
                        .addOnSuccessListener(documentSnapshot -> {
                            if (documentSnapshot.exists()) {
                                Category category = documentSnapshot.toObject(Category.class);
                                // Set the category object to the product

                                product.setCathegory(category.getName());
                                //find subcategory
                                Category subcategory = findSubcategory(category,product.getSubcategoryId());
                                if(subcategory != null){
                                    product.setSubCathegory(subcategory.getName());
                                }
                                Log.d("PRODUCT WITH CATEGORY", product.toString());
                            } else {
                                Log.w("GET CATEGORY FAILED", "Category document not found for product: " + product.getId());
                            }
                        })
                        .addOnFailureListener(e -> {
                            Log.w("GET CATEGORY FAILED", "Error getting category for product: " + product.getId(), e);
                        });
            } else {
                Log.w("GET CATEGORY FAILED", "Category ID is null for product: " + product.getId());
            }
        }
    }

    private Category findSubcategory(Category category, String subcategoryId){
//        for (Category sc : category.getSubcategories()){
//            if( sc.getName() != null  sc.getId().equals(subcategoryId)){
//                return sc;
//            }else{
//                return null;
//            }
//        }
        return null;
    }

    public void getUndeletedOdPupByCategory(String companyId, String categoryId, final OnDataFetchListener listener){
        db.collection("products")
                .whereEqualTo("companyId", companyId)
                .whereEqualTo("categoryId", categoryId)
                .whereEqualTo("deleted", false)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Product> productList = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Product product = documentSnapshot.toObject(Product.class);
                        productList.add(product);
                    }
                    // Process the list of products with categories here
                    processProductsWithCategories(productList);
                    listener.onSuccess(productList);
                })
                .addOnFailureListener(e -> {
                    Log.w("GET PRODUCTS FAILED", "Error getting products", e);
                    listener.onFailure(e.getMessage());
                });
    }

    public void updateBatch(List<Product> updatedProducts) {
        // Create a write batch
        WriteBatch batch = db.batch();

        for (Product pkg : updatedProducts) {
            // Get the document reference for this package
            DocumentReference packageRef = db.collection("products").document(pkg.getId());

            // Update the package in the batch
            batch.set(packageRef, pkg);
        }

        // Commit the batch
        batch.commit().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d("BATCH", "Batch update successful!");
            } else {
                Log.w("BATCH", "Batch update failed.", task.getException());
            }
        });
    }


}
