package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.EventTypeService;
import com.example.eventplanner.services.PackageService;
import com.example.eventplanner.services.ProductService;

import org.bouncycastle.crypto.util.Pack;

import java.util.List;

public class ProductPricelistAdapter extends  RecyclerView.Adapter<ProductPricelistAdapter.ProductViewHolder>{
    private Context context;
    private List<Product> productList;
    private ProductService productService;
    private double packagePrice;
    private PackageService packageService;

    UserRole userRole;

    public ProductPricelistAdapter(Context context,List<Product> productList, ProductService productService,UserRole userRole) {
        this.context = context;
        this.productList = productList;
        this.productService = productService;
        this.userRole = userRole;

        PackageRepository packageRepository = new PackageRepository();
        packageService = new PackageService(packageRepository);
        Log.i("Frag","Napravio adapter");
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pricelist, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = productList.get(position);
        holder.bind(product);
    }
    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        ImageButton  editProductBtn;
        TextView productName, productPrice, productDiscount, productDiscountPrice, productOridinalNumber;
        private Product currentProduct;
        private int currentIndex;
        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            editProductBtn = itemView.findViewById(R.id.pricelist_item_edit_btn);

            productOridinalNumber = itemView.findViewById(R.id.pricelist_item_oridinalNumber);
            productName = itemView.findViewById(R.id.pricelist_item_name);
            productPrice = itemView.findViewById(R.id.pricelist_item_price);
            productDiscount = itemView.findViewById(R.id.pricelist_item_discount);
            productDiscountPrice = itemView.findViewById(R.id.pricelist_item_newPrice);


            editProductBtn.setOnClickListener(v -> {
                if(userRole == UserRole.OWNER ){
                    if(currentProduct.isPending()){
                        Toast.makeText(context, "Product creation is pending - new subcategory.", Toast.LENGTH_SHORT).show();
                    }else{
                        editProductPopup(currentProduct);
                    }

                }else{
                    Toast.makeText(context, "Your have no permission for this action", Toast.LENGTH_SHORT).show();
                }

            });
        }

        public void bind(Product product) {
            currentProduct = product;
            currentIndex = 0;
            boolean ind = product != null;
            currentIndex = productList.indexOf(product);
            currentIndex++;
            Log.i("binder",ind? "nije null": "null je");
            productName.setText(product.getName());
            productOridinalNumber.setText(String.valueOf(currentIndex));
            productPrice.setText(String.valueOf(product.getPrice()));
            productDiscount.setText(String.valueOf(product.getDiscount()));
            productDiscountPrice.setText(String.valueOf(product.getPrice() - product.getDiscount()*product.getPrice()/100));

        }

        @SuppressLint({"NotifyDataSetChanged"})
        private void editProductPopup(Product product) {
            @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.edit_pricelist, null);

            // Create a PopupWindow
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            boolean focusable = true;

            PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
            EditText price = popupView.findViewById(R.id.edit_pricelist_price);
            price.setText(String.valueOf(product.getPrice()));

            EditText discount = popupView.findViewById(R.id.edit_pricelist_discount);
            discount.setText(String.valueOf(product.getDiscount()));


            //show window
            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

            Button saveButton = popupView.findViewById(R.id.buttonSave_edit_pricelist);
            Button cancelButton = popupView.findViewById(R.id.buttonCancel_edit_pricelist);

            saveButton.setOnClickListener(v -> {
                //preuzimanje vrijesnosti spinera
                product.setPrice(Double.parseDouble(price.getText().toString()));
                product.setDiscount(Integer.parseInt(discount.getText().toString()));

                productService.update(product);

                // TODO update packages and their products
                packageService.getUndeletedWithProduct(product.getCompanyId(), product.getId(), new PackageService.OnDataFetchListener() {
                    @Override
                    public void onSuccess(List<Package> objects) {
                        Log.i("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa",String.valueOf(objects.size()));

                        for(Package obj : objects){
                            obj.getProductsHashMap().put(product.getId(), product);
                            setPackagePrice(obj);
                        }
                        packageService.updatePackagesInFirestore(objects);
                    }

                    @Override
                    public void onFailure(String errorMessage) {

                    }
                });
                //Updating packages
                notifyDataSetChanged();
                popupWindow.dismiss();

            });

            cancelButton.setOnClickListener(v -> {

                popupWindow.dismiss();

            });
        }

        private void setPackagePrice(Package newPackage) {
            newPackage.setPrice(0);
            packagePrice = 0;
            if (newPackage.getProductsHashMap() != null) {
                newPackage.getProductsHashMap().forEach((key, value) -> {
                    packagePrice += (value.getPrice() - ( value.getPrice() * value.getDiscount() / 100));
                });
            }

            if (newPackage.getServicesHashMap() != null) {
                newPackage.getServicesHashMap().forEach((key, value) -> {
                    if(value.isAppointment()){
                        double duration = Double.parseDouble(value.getAppointmentDuration());
                        packagePrice += (value.getPrice() - (value.getPrice() * value.getDiscount() / 100))*duration;
                    }else{
                        double duration = Double.parseDouble(value.getMinDuration());
                        packagePrice += (value.getPrice() - (value.getPrice() * value.getDiscount() / 100))*duration;
                    }

                });
            }
            newPackage.setPrice(packagePrice);

        }


    }

}
