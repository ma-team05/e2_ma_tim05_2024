package com.example.eventplanner.activities.registration;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Notification;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.User;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.UserService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegistrationActivity extends AppCompatActivity {

    private EditText editTextEmail, editTextPassword, editTextConfirmPassword,
            editTextFirstName, editTextLastName, editTextAddress, editTextPhoneNumber;
    private ImageView imageViewProfile;
    private static final int REQUEST_IMAGE_GET = 1;
    private String selectedRole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextConfirmPassword = findViewById(R.id.editTextConfirmPassword);
        editTextFirstName = findViewById(R.id.editTextFirstName);
        editTextLastName = findViewById(R.id.editTextLastName);
        editTextAddress = findViewById(R.id.editTextAddress);
        editTextPhoneNumber = findViewById(R.id.editTextPhoneNumber);
        imageViewProfile = findViewById(R.id.imageViewProfile);
        Button buttonSignup = findViewById(R.id.buttonSignup);
        Button buttonSelectImage = findViewById(R.id.buttonSelectImage);

        buttonSignup.setOnClickListener(v -> signUp());
        buttonSelectImage.setOnClickListener(v -> selectImage());

        Spinner spinnerRole = findViewById(R.id.spinnerRole);
        spinnerRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                selectedRole = parentView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Do nothing
            }
        });

        buttonSignup.setOnClickListener(v -> signUp());
    }

    String getTrimmedText(EditText text) {
        return text.getText().toString().trim();
    }

    private void handleRoleSelection(String selectedRole) {
        String email = getTrimmedText(editTextEmail);
        String firstName = getTrimmedText(editTextFirstName);
        String lastName = getTrimmedText(editTextLastName);
        String address = getTrimmedText(editTextAddress);
        String phoneNumber = getTrimmedText(editTextPhoneNumber);
        String password = getTrimmedText(editTextPassword);

        switch (selectedRole) {
            case "Owner":
                Intent intentOwnerStep1 = new Intent(RegistrationActivity.this, OwnersCompanyRegistrationStep1Activity.class);
                intentOwnerStep1.putExtra("role", selectedRole);
                intentOwnerStep1.putExtra("email", email);
                intentOwnerStep1.putExtra("firstName", firstName);
                intentOwnerStep1.putExtra("lastName", lastName);
                intentOwnerStep1.putExtra("address", address);
                intentOwnerStep1.putExtra("phoneNumber", phoneNumber);
                intentOwnerStep1.putExtra("password", password);
                startActivity(intentOwnerStep1);
                finish();
                break;
            case "Event Planner":
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                // Send verification email
                                sendVerificationEmail();

                                // Registration successful
                                User user = new User(firstName, lastName, email, phoneNumber, address, "", UserRole.EVENT_PLANNER, password);
                                UserService userService = new UserService();
                                userService.save(user);

                                Toast.makeText(this, "User registered successfully. Check your email for verification.", Toast.LENGTH_SHORT).show();

                                Notification notification = new Notification(NotificationType.REGISTRATION, "User " + firstName + " " + lastName + "has successfully registered as an event planner!");
                                NotificationService notificationService = new NotificationService();
                                notificationService.save(notification);
                            } else {
                                // Registration failed
                                if (!task.isSuccessful()) {
                                    Exception exception = task.getException();
                                    if (exception != null) {
                                        Log.e("RegistrationError", "Registration failed: " + exception.getMessage());
                                    }
                                    Toast.makeText(this, "Registration failed. Please try again.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                break;
            default:
                break;
        }
    }

    private void selectImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_IMAGE_GET);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_GET && resultCode == RESULT_OK && data != null) {
            Uri imageUri = data.getData();
            if (imageUri != null) {
                imageViewProfile.setImageURI(imageUri);
            }
        }
    }

    private void signUp() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String confirmPassword = editTextConfirmPassword.getText().toString().trim();
        String firstName = editTextFirstName.getText().toString().trim();
        String lastName = editTextLastName.getText().toString().trim();
        String address = editTextAddress.getText().toString().trim();
        String phoneNumber = editTextPhoneNumber.getText().toString().trim();

        if (checkValidity(email, password, confirmPassword, firstName, lastName, address, phoneNumber))
            return;

        handleRoleSelection(selectedRole);
    }

    private boolean checkValidity(String email, String password, String confirmPassword, String firstName, String lastName, String address, String phoneNumber) {
        // Check if any field is empty
        if (email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() ||
                firstName.isEmpty() || lastName.isEmpty() || address.isEmpty() || phoneNumber.isEmpty()) {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show();
            return true;
        }

        // Check if the email is valid
        if (!isValidEmail(email)) {
            Toast.makeText(this, "Please enter a valid email address", Toast.LENGTH_SHORT).show();
            return true;
        }

        // Check if the password meets the minimum length requirement
        if (password.length() < 6) {
            Toast.makeText(this, "Password must be at least 6 characters long", Toast.LENGTH_SHORT).show();
            return true;
        }

        // Check if the passwords match
        if (!password.equals(confirmPassword)) {
            editTextPassword.setTextColor(ContextCompat.getColor(this, R.color.error_color));
            editTextConfirmPassword.setTextColor(ContextCompat.getColor(this, R.color.error_color));
            Toast.makeText(this, "Passwords do not match", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            editTextPassword.setTextColor(ContextCompat.getColor(this, android.R.color.black));
            editTextConfirmPassword.setTextColor(ContextCompat.getColor(this, android.R.color.black));
        }

        return false;
    }


    private boolean isValidEmail(String email) {
        String emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";
        return email.matches(emailPattern);
    }


    private void sendVerificationEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            user.sendEmailVerification()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            // Email sent successfully
                            Toast.makeText(this, "Verification email sent", Toast.LENGTH_SHORT).show();
                        } else {
                            // Email sending failed
                            Toast.makeText(this, "Failed to send verification email", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }
}