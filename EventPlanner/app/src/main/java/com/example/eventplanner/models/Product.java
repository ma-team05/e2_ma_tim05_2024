package com.example.eventplanner.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class Product implements Parcelable {
    private String id;
    private String cathegory;
    private String subCathegory;
    private String name;
    private String description;
    private double price;
    private int discount;
    private String companyId;
    private String categoryId;
    private String subcategoryId;
    private List<Integer> images;
    private String eventType;
    private boolean visible;
    private boolean available;
    private boolean isDeleted;
    private boolean isPending;

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCathegory() {
        return cathegory;
    }

    public void setCathegory(String cathegory) {
        this.cathegory = cathegory;
    }

    public String getSubCathegory() {
        return subCathegory;
    }

    public void setSubCathegory(String subCathegory) {
        this.subCathegory = subCathegory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public List<Integer> getImages() {
        return images;
    }

    public void setImages(List<Integer> images) {
        this.images = images;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isPending() {
        return isPending;
    }

    public void setPending(boolean pending) {
        isPending = pending;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public Product(String cathegory, String subCathegory, String name, String description, double price, int discount, List<Integer> images, String eventType, boolean visible, boolean available) {
        this.cathegory = cathegory;
        this.subCathegory = subCathegory;
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.images = images;
        this.eventType = eventType;
        this.visible = visible;
        this.available = available;

    }

    public Product(String id, String name, String description, double price, int discount, String companyId, String categoryId, String subcategoryId, List<Integer> images, String eventType, boolean visible, boolean available, boolean isDeleted, boolean isPending) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.companyId = companyId;
        this.categoryId = categoryId;
        this.subcategoryId = subcategoryId;
        this.images = images;
        this.eventType = eventType;
        this.visible = visible;
        this.available = available;
        this.isDeleted = isDeleted;
        this.isPending = isPending;
    }

    public Product(){}

    public Product(Parcel in){
        id = in.readString();
        cathegory = in.readString();
        subCathegory = in.readString();
        name = in.readString();
        description = in.readString();
        price = in.readDouble();
        discount = in.readInt();
        images = new ArrayList<>();
        in.readList(images, Integer.class.getClassLoader());

        eventType = in.readString();

        visible = in.readByte() != 0;
        available = in.readByte() != 0;
        isDeleted = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeString(id);
        }
        dest.writeString(cathegory);
        dest.writeString(subCathegory);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeDouble(price);
        dest.writeInt(discount);
        dest.writeString(eventType);
        dest.writeByte((byte) (visible ? 1 : 0));
        dest.writeByte((byte) (available ? 1 : 0));
        dest.writeByte((byte) (isDeleted ? 1 : 0));
    }


}


