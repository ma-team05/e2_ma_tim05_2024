package com.example.eventplanner.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.PackageResultsAdapter;
import com.example.eventplanner.adapters.ProductResultsAdapter;
import com.example.eventplanner.adapters.ServiceResultsAdapter;
import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.database.repository.ProductRepository;
import com.example.eventplanner.database.repository.ServiceRepository;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.PackageService;
import com.example.eventplanner.services.ProductService;
import com.example.eventplanner.services.ServiceService;
import com.example.eventplanner.services.UserService;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ServiceSearchResultsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<Product> productList;
    private ProductResultsAdapter productAdapter;

    private ServiceResultsAdapter serviceAdapter;

    private PackageResultsAdapter packageAdapter;

    private ProductService productService;
    private ServiceService serviceService;
    private PackageService packageService;
    private UserService userService;

    private String selectedEventId = "";
    User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);

        productService = new ProductService(new ProductRepository());
        serviceService = new ServiceService(new ServiceRepository());
        packageService = new PackageService(new PackageRepository());
        userService = new UserService();

        String email = (String) getIntent().getSerializableExtra("email");
        selectedEventId = (String) getIntent().getSerializableExtra("EVENT");

        userService.getByEmail(email).addOnCompleteListener(this, task->{
            if(task.isSuccessful()){
                user = task.getResult();
            }

            setContentView(R.layout.activity_service_search_results);
            ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
                Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
                v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
                return insets;
            });

            String name = (String) getIntent().getSerializableExtra("NAME");
            String provider = (String) getIntent().getSerializableExtra("PROVIDER");
            double minPrice = getIntent().getDoubleExtra("MIN_PRICE", -1.0);;
            double maxPrice = getIntent().getDoubleExtra("MAX_PRICE", -1.0);;
            String city = (String) getIntent().getSerializableExtra("CITY");
            double maxDistance = getIntent().getDoubleExtra("MAX_DISTANCE", -1.0);;

            String eventType = (String) getIntent().getSerializableExtra("EVENT_TYPE");
            String serviceCategory = (String) getIntent().getSerializableExtra("CATEGORY");
            String serviceSubcategory = (String) getIntent().getSerializableExtra("SUBCATEGORY");
            String serviceType = (String) getIntent().getSerializableExtra("TYPE");

            boolean available = (boolean) getIntent().getBooleanExtra("AVAILABLE", false);
            boolean favourite = (boolean) getIntent().getBooleanExtra("FAVOURITE", false);

            Date startDate = (Date) getIntent().getSerializableExtra("START_DATE");
            Date endDate = (Date) getIntent().getSerializableExtra("END_DATE");

            Log.d("SEARCH: ", "\nNAME: " + name +
                    "\nNAME: " + name +
                    "\nPROVIDER: " + provider +
                    "\nMIN_PRICE: " + minPrice +
                    "\nMAX_PRICE: " + maxPrice +
                    "\nCITY: " + city +
                    "\nMAX_DISTANCE: " + maxDistance +
                    "\nEVENT_TYPE: " + eventType +
                    "\nCATEGORY: " + serviceCategory +
                    "\nSUBCATEGORY: " + serviceSubcategory +
                    "\nTYPE: " + serviceType +
                    "\nAVAILABLE: " + available +
                    "\nSTART_DATE: " + startDate +
                    "\nEND_DATE: " + endDate);

            Button filterButton = findViewById(R.id.product_filter_btn);

            Button yourButton = findViewById(R.id.product_filter_btn);

            if(serviceType.equals("Service")) {
                serviceService.search(name, provider, minPrice, maxPrice, city, maxDistance, eventType, serviceCategory, serviceSubcategory, available, new ServiceService.OnDataFetchListener() {
                    @Override
                    public void onSuccess(List<Service> events) {
                        events = events.stream().filter(e -> !favourite || user.getFavouriteServices().contains(e.getId())).collect(Collectors.toList());
                        events = events.stream().filter(e -> e.isVisible()).collect(Collectors.toList());
                        initializeServicesRecyclerView(events);
                    }

                    @Override
                    public void onFailure(String errorMessage) {
                    }
                });
            }
            else if(serviceType.equals("Product")) {
                productService.search(name, provider, minPrice, maxPrice, city, maxDistance, eventType, serviceCategory, serviceSubcategory, available, new ProductService.OnDataFetchListener() {
                    @Override
                    public void onSuccess(List<Product> events) {
                        events = events.stream().filter(e -> !favourite || user.getFavouriteProducts().contains(e.getId())).collect(Collectors.toList());
                        events = events.stream().filter(e -> e.isVisible()).collect(Collectors.toList());
                        initializeProductsRecyclerView(events);
                    }

                    @Override
                    public void onFailure(String errorMessage) {
                    }
                });
            }
            else if(serviceType.equals("Package")) {
                packageService.search(name, provider, minPrice, maxPrice, city, maxDistance, eventType, serviceCategory, serviceSubcategory, available, new PackageService.OnDataFetchListener() {
                    @Override
                    public void onSuccess(List<Package> events) {
                        events = events.stream().filter(e -> !favourite || user.getFavouritePackages().contains(e.getId())).collect(Collectors.toList());
                        events = events.stream().filter(e -> e.isVisible()).collect(Collectors.toList());
                        initializePackagesRecyclerView(events);
                    }

                    @Override
                    public void onFailure(String errorMessage) {
                    }
                });
            }

            filterButton.setOnClickListener(v -> finish());
        });



    }
    private void initializeProductsRecyclerView(List<Product> products){
        recyclerView = findViewById(R.id.products_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        productAdapter = new ProductResultsAdapter(this, products, user, selectedEventId);
        recyclerView.setAdapter(productAdapter);

    }

    private void initializeServicesRecyclerView(List<Service> services){
        recyclerView = findViewById(R.id.products_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        serviceAdapter = new ServiceResultsAdapter(this, services, user, selectedEventId);
        recyclerView.setAdapter(serviceAdapter);

    }

    private void initializePackagesRecyclerView(List<Package> packages){
        recyclerView = findViewById(R.id.products_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        packageAdapter = new PackageResultsAdapter(this, packages, user, selectedEventId);
        recyclerView.setAdapter(packageAdapter);

    }

    @SuppressLint({"NotifyDataSetChanged"})
    private void AddNewProductPopup() {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.product_add_form, null);

        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.buttonSave_add_product);
        Button cancelButton = popupView.findViewById(R.id.buttonCancel_add_product);

        saveButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });

        cancelButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });
    }

}