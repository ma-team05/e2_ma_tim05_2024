package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.PackagePricelistAdapter;
import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.PackageService;

import java.util.ArrayList;
import java.util.List;


public class PricelistPackagesFragment extends Fragment {

    private static final String ARG_USER_ROLE = "user_role";
    private static final String ARG_COMPANY_ID = "company_id";
    private RecyclerView recyclerView;
    private PackagePricelistAdapter packageAdapter;
    private List<Package> packageList;
    private PackageService packageService;
    private String companyId;
    private String userRole;

    public PricelistPackagesFragment() {
        // Required empty public constructor
    }

    public static PricelistPackagesFragment newInstance(String userRole, String companyId) {
        PricelistPackagesFragment fragment = new PricelistPackagesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USER_ROLE, userRole);
        args.putString(ARG_COMPANY_ID, companyId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userRole = getArguments().getString(ARG_USER_ROLE);
            companyId = getArguments().getString(ARG_COMPANY_ID);
        }

        PackageRepository productRepository = new PackageRepository();
        packageService = new PackageService(productRepository);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_pricelist_pacgages, container, false);
        recyclerView = view.findViewById(R.id.pricelist_packages);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        // Call the method to initialize the RecyclerView
        initializeRecyclerView();

        return view;
    }

    private void initializeRecyclerView(){

        packageService.getUndeletedOfPUP(companyId ,new PackageService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Package> events) {
                packageList = new ArrayList<>(events);
                if(userRole.equals("OWNER")){
                    packageAdapter = new PackagePricelistAdapter(getContext(), packageList, packageService, UserRole.OWNER);
                }else{
                    packageAdapter = new PackagePricelistAdapter(getContext(), packageList, packageService, UserRole.EMPLOYEE);
                }

                recyclerView.setAdapter(packageAdapter);

            }

            @Override
            public void onFailure(String errorMessage) {}
        });
    }
}