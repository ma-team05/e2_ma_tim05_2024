package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.PackageDetailsActivity;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.UserService;
import com.google.firebase.Timestamp;

import java.util.Calendar;
import java.util.List;

public class PackageResultsAdapter extends  RecyclerView.Adapter<PackageResultsAdapter.PackageViewHolder> {
    private Context context;
    private List<Package> packageList;
    Package package1;

    LinearLayout layoutPackage;
    private User user;

    private UserService userService = new UserService();
    private String selectedEventId = "";

    public PackageResultsAdapter(Context context,List<Package> packageList, User user, String selectedEventId) {
        this.context = context;
        this.packageList = packageList;
        this.user = user;
        this.selectedEventId = selectedEventId;
    }

    @NonNull
    @Override
    public PackageResultsAdapter.PackageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_package_result, parent, false);
        return new PackageResultsAdapter.PackageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PackageResultsAdapter.PackageViewHolder holder, int position) {
        Package product = packageList.get(position);
        holder.bind(product);
    }

    @Override
    public int getItemCount() {
        return packageList.size();
    }

    public class PackageViewHolder extends RecyclerView.ViewHolder {

        ImageButton btPrevious, btNext, btnFav, btnUnFav, btnInfo;
        ImageSwitcher packageImageSwitcher;
        TextView packageName, packageCategory, packageSubcategory, packageDescription,
                packagePrice, packageDiscount, packageDiscountPrice, packageAvailability, packageVisibility,  packageEventType,
                packageReserveIn, packageCancelIn, packageConfirm, packageProducts, packageServices;
        private Package currentPackage;
        private int currentIndex;
        public PackageViewHolder(@NonNull View itemView) {
            super(itemView);

            btPrevious = itemView.findViewById(R.id.bt_previous);
            btNext = itemView.findViewById(R.id.bt_next);
            btnFav = itemView.findViewById(R.id.btn_fav);
            btnUnFav = itemView.findViewById(R.id.btn_unfav);
            btnInfo = itemView.findViewById(R.id.btn_info);
            packageImageSwitcher = itemView.findViewById(R.id.package_image_switcher);
            packageImageSwitcher.setFactory(() -> {
                ImageView imageView = new ImageView(itemView.getContext());
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                return imageView;
            });
            packageName = itemView.findViewById(R.id.package_name);
            packageCategory = itemView.findViewById(R.id.package_category);
            packageSubcategory = itemView.findViewById(R.id.package_subcategory);
            packageDescription = itemView.findViewById(R.id.package_description);
            packagePrice = itemView.findViewById(R.id.package_price);
            packageDiscount = itemView.findViewById(R.id.package_discount);
            packageDiscountPrice = itemView.findViewById(R.id.package_discount_price);
            packageAvailability = itemView.findViewById(R.id.package_available);
            packageVisibility = itemView.findViewById(R.id.package_visibility);
            packageEventType = itemView.findViewById(R.id.package_event_type);
            packageReserveIn = itemView.findViewById(R.id.package_res_deadline);
            packageCancelIn = itemView.findViewById(R.id.package_cancel_in);
            packageConfirm = itemView.findViewById(R.id.package_confirmation);
            packageProducts = itemView.findViewById(R.id.package_products);
            packageServices = itemView.findViewById(R.id.package_services);
            LinearLayout service_layout = itemView.findViewById(R.id.layout_package_solution_helper);

            if(currentPackage != null){
                if(currentPackage.getServices().isEmpty()){
                    service_layout.setVisibility(View.GONE);
                }else{
                    service_layout.setVisibility(View.VISIBLE);
                }
            }


            btNext.setOnClickListener(v -> {
                //int currentIndex = packageImageSwitcher.getDisplayedChild();
                if (currentIndex < currentPackage.getImages().size() - 1) {
                    currentIndex++;
                    packageImageSwitcher.setImageResource(currentPackage.getImages().get(currentIndex));
                }
            });

            btPrevious.setOnClickListener(v -> {
                //int currentIndex = packageImageSwitcher.getDisplayedChild();
                if (currentIndex > 0) {
                    currentIndex--;
                    packageImageSwitcher.setImageResource(currentPackage.getImages().get(currentIndex));
                }
            });

            btnFav.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    user.addFavouritePackage(packageList.get(position).getId());
                    userService.update(user);
                    btnFav.setVisibility(View.GONE);
                    btnUnFav.setVisibility(View.VISIBLE);
                }
            });

            btnUnFav.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    user.removeFavouritePackage(packageList.get(position).getId());
                    userService.update(user);
                    btnFav.setVisibility(View.VISIBLE);
                    btnUnFav.setVisibility(View.GONE);
                }
            });

            btnInfo.setOnClickListener(v -> {
                int position = getAdapterPosition();
                Intent createIntent = new Intent(context, PackageDetailsActivity.class);
                createIntent.putExtra("EMAIL", user.getEmail());
                createIntent.putExtra("PACKAGE", packageList.get(position).getId());
                createIntent.putExtra("EVENT", selectedEventId);
                context.startActivity(createIntent);
            });
        }

        @SuppressLint("SetTextI18n")
        public void bind(Package aPackage) {
            currentPackage = aPackage;
            currentIndex = 0;

            packageName.setText(aPackage.getName());
            packageCategory.setText(aPackage.getCategory());
            packageSubcategory.setText(aPackage.getSubcategoriesString());
            packageDescription.setText(aPackage.getDescription());
            packagePrice.setText(String.valueOf(aPackage.getPrice()));
            packageDiscount.setText(String.valueOf(aPackage.getDiscount()));
            packageDiscountPrice.setText(String.valueOf(aPackage.getPrice() * (100 - aPackage.getDiscount()) / 100));
            packageEventType.setText(aPackage.getEventTypes());
            packageAvailability.setText(aPackage.isAvailable() ? "Yes" : "No");
            packageVisibility.setText(aPackage.isVisible() ? "Yes" : "No");
            packageProducts.setText(aPackage.getProductsString());
            packageServices.setText(aPackage.getServicesString());

            // Handle reservation deadline
            Timestamp reservationDeadline = aPackage.getReservationDeadline();
            if (reservationDeadline != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(reservationDeadline.getSeconds() * 1000);
                String stringBuilder = cal.get(Calendar.MONTH) + "m " +
                        cal.get(Calendar.DAY_OF_MONTH) + "d ";
                packageReserveIn.setText(stringBuilder);
            } else {
                packageReserveIn.setText("N/A");
            }

            // Handle cancellation deadline
            Timestamp cancellationDeadline = aPackage.getCancellationDeadline();
            if (cancellationDeadline != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(cancellationDeadline.getSeconds() * 1000);
                String stringBuilder = cal.get(Calendar.MONTH) + "m " +
                        cal.get(Calendar.DAY_OF_MONTH) + "d ";
                packageCancelIn.setText(stringBuilder);
            } else {
                packageCancelIn.setText("N/A");
            }

            // Handle confirmation type
            if (aPackage.getConfirmationType() == Service.ConfirmationType.AUTOMATIC) {
                packageConfirm.setText("Automatic");
            } else {
                packageConfirm.setText("By hand");
            }

            // Set the first image if available
            if (aPackage.getImages() != null && !aPackage.getImages().isEmpty()) {
                packageImageSwitcher.setImageResource(aPackage.getImages().get(0));
            } else {
                packageImageSwitcher.setImageResource(R.drawable.default_image);
            }

            // Set favorite button visibility
            if (user.getFavouritePackages().contains(aPackage.getId())) {
                btnUnFav.setVisibility(View.VISIBLE);
                btnFav.setVisibility(View.GONE);
            } else {
                btnFav.setVisibility(View.VISIBLE);
                btnUnFav.setVisibility(View.GONE);
            }
        }
    }

    private void showDeleteConfirmationDialog(Package package1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Delete Product");
        builder.setMessage("Are you sure you want to delete this product?");

        // Set up the buttons
        builder.setPositiveButton("OK", (dialog, which) -> {
            //delete operation here

            dialog.dismiss();
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @SuppressLint({"NotifyDataSetChanged"})
    private void EditPackagePopup() {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.package_edit_form, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.buttonSave_edit_package);
        Button cancelButton = popupView.findViewById(R.id.buttonCancel_edit_package);
        Button productButton = popupView.findViewById(R.id.package_buttonProducts_edit);
        Button serviceButton = popupView.findViewById(R.id.package_buttonServices_edit);

        layoutPackage = popupView.findViewById(R.id.package_help_layout);
        //inicijalna vidljivost paketa
        if(package1.getServices().isEmpty()){
            layoutPackage.setVisibility(View.GONE);
        }else{
            layoutPackage.setVisibility(View.VISIBLE);
        }




        saveButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });

        productButton.setOnClickListener(v->{
            AddProductsPopup();
        });
        serviceButton.setOnClickListener(v->{
            AddServicesPopup(package1);

        });

        cancelButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });
    }

    @SuppressLint({"NotifyDataSetChanged"})
    private void AddProductsPopup() {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.package_product_popup, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.package_product_add);

        saveButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });


    }

    @SuppressLint({"NotifyDataSetChanged"})
    private void AddServicesPopup(Package package1) {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.package_service_popup, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.package_service_add);
        CheckBox checkboxService1 = popupView.findViewById(R.id.checkbox_service1);
        CheckBox checkboxService2 = popupView.findViewById(R.id.checkbox_service2);
        CheckBox checkboxService3 = popupView.findViewById(R.id.checkbox_service3);

        saveButton.setOnClickListener(v -> {
            //package1.getServices().clear();

            if (checkboxService1.isChecked()) {
                //package1.getServices().add("Service 1");
            }
            if (checkboxService2.isChecked()) {
                //package1.getServices().add("Service 2");
            }
            if (checkboxService3.isChecked()) {
                //package1.getServices().add("Service 3");
            }

            popupWindow.dismiss();

        });

    }
}
