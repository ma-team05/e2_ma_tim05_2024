package com.example.eventplanner.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.BudgetItem;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.EventService;
import com.example.eventplanner.services.ProductService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BuyProductActivity extends AppCompatActivity {
    private List<Event> events;
    private Event selectedEvent;
    private String selectedEventId = "";
    private Product product;
    EventService eventService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_buy_service);

        eventService = new EventService(new EventRepository());
        ProductService productService = new ProductService();

        String email = (String) getIntent().getSerializableExtra("EMAIL");
        selectedEventId = (String) getIntent().getSerializableExtra("EVENT");
        String productId = (String) getIntent().getSerializableExtra("PRODUCT_ID");

        productService.get(productId).addOnSuccessListener(result -> {
            product = result;

            eventService.getAll(new EventService.OnDataFetchListener() {
                @Override
                public void onSuccess(List<Event> events1) {
                    events = events1.stream().filter(e -> e.getUserEmail().equals(email)).collect(Collectors.toList());

                    List<String> names = new ArrayList<>();
                    names.add("Select an event");
                    for (Event e : events) {
                        names.add("Selected event: " + e.getName());
                    }

                    Spinner eventSpinner = findViewById(R.id.event);
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(BuyProductActivity.this, android.R.layout.simple_spinner_item, names);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    eventSpinner.setAdapter(adapter);

                    for (int i = 0; i < events.size(); i++) {
                        if (events.get(i).getId().equals(selectedEventId)) {
                            eventSpinner.setSelection(i + 1);
                            selectedEvent = events.get(i);
                            break;
                        } else {
                            eventSpinner.setSelection(0);
                            selectedEvent = null;
                        }
                    }

                    eventSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String eventName = eventSpinner.getSelectedItem().toString().trim();
                            if (eventName.equals("Select an event")) {
                                selectedEventId = "";
                                selectedEvent = null;
                            } else {
                                selectedEvent = events.stream().filter(e -> e.getName().equals(eventName.substring(16))).findFirst().orElse(null);
                                assert selectedEvent != null;
                                selectedEventId = selectedEvent.getId();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                }

                @Override
                public void onFailure(String errorMessage) {
                }
            });
        }).addOnFailureListener(e -> {
            Log.e("PRODUCT_FETCH_ERROR", "Error fetching product", e);
        });

        Button buyButton = findViewById(R.id.createButton);

        buyButton.setOnClickListener(v -> buy());
    }

    private void buy() {
        Toast.makeText(this, "KLIKNUO SI ME", Toast.LENGTH_SHORT).show();
        if (selectedEvent == null) {
            Toast.makeText(this, "Please select an event", Toast.LENGTH_SHORT).show();
            return;
        }

        if (product == null) {
            Toast.makeText(this, "Error: Product not found", Toast.LENGTH_SHORT).show();
            return;
        }

        CategoryService categoryService = new CategoryService();
        categoryService.getById(product.getCategoryId()).addOnSuccessListener(category -> {
            BudgetItem matchingBudgetItem = null;

            // Find the matching budget item
            for (BudgetItem budgetItem : selectedEvent.getBudgetItems()) {
                if (budgetItem.getCategory().equals(category)) {
                    matchingBudgetItem = budgetItem;
                    break;
                }
            }

            if (matchingBudgetItem != null) {
                // Update existing budget item
                for (int i = 0; i < selectedEvent.getBudgetItems().size(); i++) {
                    if (selectedEvent.getBudgetItems().get(i).equals(matchingBudgetItem)) {
                        List<Product> products = matchingBudgetItem.getProducts();
                        products.add(product); // Add the new product to the list and update the database
                        matchingBudgetItem.setProducts(products);
                        selectedEvent.getBudgetItems().set(i, matchingBudgetItem);
                        eventService.update(selectedEvent);
                    }
                }
            } else {
                // Create a new budget item
                BudgetItem newBudgetItem = new BudgetItem();
                newBudgetItem.setEventId(selectedEvent.getId());
                // Set the category of the purchased product
                newBudgetItem.setCategory(category);
                // Add the new budget item to the selected event's budget items list
                selectedEvent.addBudgetItem(newBudgetItem);
                newBudgetItem.setName(category.getName());
                newBudgetItem.setAmmount(0);
                List<Product> products = new ArrayList<>();
                products.add(product);
                newBudgetItem.setProducts(products);

                // Save changes to the database
                eventService.update(selectedEvent);

                Toast.makeText(this, "New budget item created and product added", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(e -> {
            Toast.makeText(this, "Error fetching category", Toast.LENGTH_SHORT).show();
            Log.e("CATEGORY_FETCH_ERROR", "Error fetching category", e);
        });
    }
}