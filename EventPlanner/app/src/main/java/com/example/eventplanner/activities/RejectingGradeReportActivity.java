package com.example.eventplanner.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.models.GradeReport;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.ReportStatus;
import com.example.eventplanner.services.GradeReportService;
import com.example.eventplanner.services.NotificationService;

import java.util.Date;

public class RejectingGradeReportActivity extends AppCompatActivity {
    private GradeReportService reportService = new GradeReportService();
    private String reportId;
    private String ownerId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_rejecting_grade_report);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        Intent intent = getIntent();
        reportId = intent.getStringExtra("reportId");
        ownerId = intent.getStringExtra("ownerId");
        Button submitButton = this.findViewById(R.id.buttonSubmit);
        submitButton.setOnClickListener(v->{
            EditText commentEdittext = findViewById(R.id.rejectingReason);
            String reason = commentEdittext.getText().toString();
            if(reason==null || reason.isEmpty()){
                Toast.makeText(this, "No reason added", Toast.LENGTH_SHORT).show();
                return;
            }
            reportService.getById(reportId).addOnCompleteListener(task1->{
                if(task1.isSuccessful()){
                    reportService.RejectGradeReport(task1.getResult(), reason).addOnCompleteListener(task->{
                        if(task.isSuccessful()) {
                            NotificationService notificationService = new NotificationService();
                            notificationService.add(ownerId, "Report rejected by admin: "+reason, NotificationType.GRADES).addOnCompleteListener(t->{
                               if(t.isSuccessful()){
                                   Intent resultIntent = new Intent();
                                   resultIntent.putExtra("reportId", reportId);
                                   resultIntent.putExtra("reason", reason);
                                   setResult(Activity.RESULT_OK, resultIntent);
                                   finish();
                               }
                            });
                        }
                        else
                            Toast.makeText(this, "Error rejecting report", Toast.LENGTH_SHORT).show();
                    });
                }
            });
        });
    }
}