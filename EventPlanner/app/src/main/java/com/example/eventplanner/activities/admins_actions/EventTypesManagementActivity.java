package com.example.eventplanner.activities.admins_actions;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EventTypeAdapter;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.services.EventTypeService;

import java.util.ArrayList;
import java.util.List;

public class EventTypesManagementActivity extends AppCompatActivity implements EventTypeAdapter.OnEventTypeEditListener {

    private List<EventType> eventTypes;
    private EventTypeAdapter adapter;
    private EventTypeService eventTypeService;
    private RecyclerView recyclerView;
    @Override
    public void onEventTypeEdit(EventType eventType) {
        showEventTypeEditPopup(eventType);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_types_management);

        recyclerView = findViewById(R.id.recyclerViewEventTypes);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        eventTypeService = new EventTypeService();

        fetchEventTypes();

        adapter = new EventTypeAdapter(this, eventTypes);
        adapter.setOnEventTypeEditListener(this);

        recyclerView.setAdapter(adapter);

        Button addButton = findViewById(R.id.addButton);
        addButton.setOnClickListener(v -> showAddEventTypePopup());
    }
    @SuppressLint("NotifyDataSetChanged")
    private void showEventTypeEditPopup(EventType eventType) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_edit_event_type, null);

        // Initialize views
        EditText eventNameEditText = popupView.findViewById(R.id.editTextEventName);
        EditText eventDescriptionEditText = popupView.findViewById(R.id.editTextEventDescription);
        Button saveButton = popupView.findViewById(R.id.saveButton);

        // Set current values
        eventNameEditText.setText(eventType.getName());
        eventDescriptionEditText.setText(eventType.getDescription());

        // Create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // Allows tapping outside the popup to dismiss it
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // Set up a click listener for the save button
        saveButton.setOnClickListener(v -> {
            // Get the updated values
            String eventName = eventNameEditText.getText().toString().trim();
            String eventDescription = eventDescriptionEditText.getText().toString().trim();

            // Update the event type object
            eventType.setName(eventName);
            eventType.setDescription(eventDescription);

            EventTypeService eventTypeService = new EventTypeService();
            eventTypeService.update(eventType);

            // Notify the adapter of the change
            adapter.notifyDataSetChanged();

            popupWindow.dismiss();
        });

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void showAddEventTypePopup() {
        if (eventTypes == null) {
            eventTypes = new ArrayList<>();
        }

        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.popup_add_event_type, null);
        PopupWindow popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        EditText eventNameEditText = popupView.findViewById(R.id.editTextEventName);
        EditText eventDescriptionEditText = popupView.findViewById(R.id.editTextEventDescription);
        Button saveButton = popupView.findViewById(R.id.saveButton);

        saveButton.setOnClickListener(v -> {
            String eventName = eventNameEditText.getText().toString().trim();
            String eventDescription = eventDescriptionEditText.getText().toString().trim();

            if (!eventName.isEmpty() && !eventDescription.isEmpty()) {
                EventType newEventType = new EventType(eventName, eventDescription, true);
                EventTypeService eventTypeService = new EventTypeService();
                eventTypeService.save(newEventType);
                eventTypes.add(newEventType);
                adapter.notifyDataSetChanged();
                popupWindow.dismiss();
            } else {
                Toast.makeText(this, "Please fill both fields", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchEventTypes() {
        eventTypeService.getAll(new EventTypeService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<EventType> objects) {
                eventTypes = objects;
                adapter = new EventTypeAdapter(EventTypesManagementActivity.this, eventTypes);
                adapter.setOnEventTypeEditListener(EventTypesManagementActivity.this);
                recyclerView.setAdapter(adapter);
            }
            @Override
            public void onFailure(String errorMessage) {
                Toast.makeText(EventTypesManagementActivity.this, "Failed to fetch event types: " + errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
