package com.example.eventplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.models.EmployeesWorkingHours;

import java.util.List;

public class WorkingHoursAdapter extends RecyclerView.Adapter<WorkingHoursAdapter.WorkingHoursViewHolder> {

    private List<EmployeesWorkingHours> employeesWorkingHoursList;

    public WorkingHoursAdapter(List<EmployeesWorkingHours> employeeList) {
        this.employeesWorkingHoursList = employeeList;
    }

    @Override
    public void onBindViewHolder(@NonNull WorkingHoursAdapter.WorkingHoursViewHolder holder, int position) {
        EmployeesWorkingHours employee = employeesWorkingHoursList.get(position);
        holder.bind(employee);
    }
    @NonNull
    @Override
    public WorkingHoursAdapter.WorkingHoursViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.working_hours_item, parent, false);
        return new WorkingHoursAdapter.WorkingHoursViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return employeesWorkingHoursList.size();
    }

    static class WorkingHoursViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewMonday;
        private TextView textViewTuesday;
        private TextView textViewWednesday;
        private TextView textViewThursday;
        private TextView textViewFriday;
        private TextView textViewDateRange;
        private Button detailsButton;

        public WorkingHoursViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewMonday = itemView.findViewById(R.id.monday);
            textViewTuesday = itemView.findViewById(R.id.tuesday);
            textViewWednesday = itemView.findViewById(R.id.wednesday);
            textViewThursday = itemView.findViewById(R.id.thursday);
            textViewFriday = itemView.findViewById(R.id.friday);
            textViewDateRange = itemView.findViewById(R.id.dateRange);
        }

        public void bind(EmployeesWorkingHours workingHours) {
            textViewMonday.setText(workingHours.getTimeSchedule().mondayStart + "-" + workingHours.getTimeSchedule().mondayFinish);
            textViewTuesday.setText(workingHours.getTimeSchedule().tuesdayStart + "-" + workingHours.getTimeSchedule().tuesdayFinish);
            textViewWednesday.setText(workingHours.getTimeSchedule().wednesdayStart + "-" + workingHours.getTimeSchedule().wednesdayFinish);
            textViewThursday.setText(workingHours.getTimeSchedule().thursdayStart + "-" + workingHours.getTimeSchedule().thursdayFinish);
            textViewFriday.setText(workingHours.getTimeSchedule().fridayStart + "-" + workingHours.getTimeSchedule().fridayFinish);
            if(workingHours.getValidFrom()==null || workingHours.getValidTo()==null)
                textViewDateRange.setText("Regular working hours");
            else
                textViewDateRange.setText(workingHours.getValidFrom().getDate() + "." + (workingHours.getValidFrom().getMonth()+1) + "." +(workingHours.getValidFrom().getYear()+1900) + "-" +
                        workingHours.getValidTo().getDate() + "." + (workingHours.getValidTo().getMonth()+1) + "." +(workingHours.getValidTo().getYear()+1900) + ".");
        }
    }
}
