package com.example.eventplanner.models;

import java.util.List;

public class ServiceReservation {
    private String id;

    private String name;

    private double ammount;

    public ServiceReservation() {
    }

    public ServiceReservation(String id, String name, double ammount) {
        this.id = id;
        this.name = name;
        this.ammount = ammount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmmount() {
        return ammount;
    }

    public void setAmmount(double ammount) {
        this.ammount = ammount;
    }
}
