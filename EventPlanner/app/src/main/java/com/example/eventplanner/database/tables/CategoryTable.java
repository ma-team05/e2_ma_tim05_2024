package com.example.eventplanner.database.tables;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class CategoryTable {
    private CategoryTable() {}

    public static final class Table implements BaseColumns {
        public static final String TABLE_NAME = "categories";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_PARENT_ID = "parent_id";
        public static final String COLUMN_SUBCATEGORY_TYPE = "subcategory_type";
    }

    public static final class DDL {
        public static final String CREATE_TABLE = "CREATE TABLE " +
                Table.TABLE_NAME + " (" +
                Table._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Table.COLUMN_NAME + " TEXT NOT NULL, " +
                Table.COLUMN_DESCRIPTION + " TEXT NOT NULL, " +
                Table.COLUMN_PARENT_ID + " INTEGER, " +
                Table.COLUMN_SUBCATEGORY_TYPE + " INTEGER NOT NULL, " +
                "FOREIGN KEY(" + Table.COLUMN_PARENT_ID + ") REFERENCES " +
                Table.TABLE_NAME + "(" + Table._ID + ")" +
                ");";

        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " +
                Table.TABLE_NAME;
    }

    public static final class DML {
        public static void insertData(SQLiteDatabase db) {
            ContentValues values = new ContentValues();
            values.put(CategoryTable.Table.COLUMN_NAME, "Category 1");
            values.put(CategoryTable.Table.COLUMN_DESCRIPTION, "Description 1");
            values.putNull(CategoryTable.Table.COLUMN_PARENT_ID);
            values.put(CategoryTable.Table.COLUMN_SUBCATEGORY_TYPE, 2);
            db.insert(CategoryTable.Table.TABLE_NAME, null, values);

            values.clear();
            values.put(CategoryTable.Table.COLUMN_NAME, "Category 11");
            values.put(CategoryTable.Table.COLUMN_DESCRIPTION, "Description 2");
            values.put(CategoryTable.Table.COLUMN_PARENT_ID, 1);
            values.put(CategoryTable.Table.COLUMN_SUBCATEGORY_TYPE, 1);
            db.insert(CategoryTable.Table.TABLE_NAME, null, values);

            values.clear();
            values.put(CategoryTable.Table.COLUMN_NAME, "Category 12");
            values.put(CategoryTable.Table.COLUMN_DESCRIPTION, "Description 3");
            values.put(CategoryTable.Table.COLUMN_PARENT_ID, 1);
            values.put(CategoryTable.Table.COLUMN_SUBCATEGORY_TYPE, 0);
            db.insert(CategoryTable.Table.TABLE_NAME, null, values);

            values.clear();
            values.put(CategoryTable.Table.COLUMN_NAME, "Category 2");
            values.put(CategoryTable.Table.COLUMN_DESCRIPTION, "Description 1");
            values.putNull(CategoryTable.Table.COLUMN_PARENT_ID);
            values.put(CategoryTable.Table.COLUMN_SUBCATEGORY_TYPE, 2);
            db.insert(CategoryTable.Table.TABLE_NAME, null, values);

            values.clear();
            values.put(CategoryTable.Table.COLUMN_NAME, "Category 21");
            values.put(CategoryTable.Table.COLUMN_DESCRIPTION, "Description 2");
            values.put(CategoryTable.Table.COLUMN_PARENT_ID, 4);
            values.put(CategoryTable.Table.COLUMN_SUBCATEGORY_TYPE, 1);
            db.insert(CategoryTable.Table.TABLE_NAME, null, values);
        }
    }
}
