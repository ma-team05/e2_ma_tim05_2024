package com.example.eventplanner.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.EventStatus;

import java.text.SimpleDateFormat;
import java.util.List;

public class EmployeeEventAdapter extends RecyclerView.Adapter<EmployeeEventAdapter.EventViewHolder> {
    private List<Event> eventsList;
    public int selectedItem = RecyclerView.NO_POSITION;

    public EmployeeEventAdapter(List<Event> eventsList) {
        this.eventsList = eventsList;
    }

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event, parent, false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder holder, int position) {
        Event event = eventsList.get(position);
        holder.bind(event, position == selectedItem);
    }

    @Override
    public int getItemCount() {
        return eventsList.size();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView textViewName;
        private TextView textViewStatus;
        private TextView textViewDate;

        public EventViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.eventNameTextView);
            textViewStatus = itemView.findViewById(R.id.eventStatusTextView);
            textViewDate = itemView.findViewById(R.id.eventDateTextView);
            itemView.setOnClickListener(this);
        }

        public void bind(Event event, boolean isSelected) {
            textViewName.setText(event.getName());
            String fromString = new SimpleDateFormat("yyyy.MM.dd HH:mm").format(event.getFrom());
            String toString = new SimpleDateFormat("HH:mm").format(event.getTo());
            textViewDate.setText(fromString + " - " + toString);
            textViewStatus.setText(event.getStatus() == EventStatus.TAKEN ? "✔\uFE0F" : "\uD83D\uDD67");

            itemView.setBackgroundColor(isSelected ? Color.parseColor("#FFEE88") : Color.parseColor("#FFFFFF"));
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if(selectedItem==position){
                selectedItem = RecyclerView.NO_POSITION;
            }
            else if (position != RecyclerView.NO_POSITION) {
                selectedItem = position;
            }
            notifyDataSetChanged();
        }
    }
}
