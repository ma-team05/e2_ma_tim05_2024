package com.example.eventplanner.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CategorySpinnerAdapter;
import com.example.eventplanner.adapters.EventTypeSpinnerAdapter;
import com.example.eventplanner.adapters.ProductListAdapter;
import com.example.eventplanner.database.repository.CategoryRepository;
import com.example.eventplanner.database.repository.ProductRepository;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.SubcategoryType;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.EventTypeService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.ProductService;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductManagementActivity extends AppCompatActivity {

    private CategoryRepository categoryRepository;

    Spinner categorySpinner;
    Spinner filterCategorySpinner;
    Spinner filterEventTypeSpinner;
    EventTypeService eventTypeService;
    ProductService productService;
    String userId;
    UserRole userRole;
    ProductListAdapter productAdapter;
    List<Product> productList;

    String companyId;
    Category selectedCategoryNewProduct;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_product_management);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");
        userRole = UserRole.values()[intent.getIntExtra("userRole", 0)];

        categoryRepository = new CategoryRepository();
        eventTypeService = new EventTypeService();
        ProductRepository productRepository = new ProductRepository();
        productService = new ProductService(productRepository);

        OwnerService ownerService = new OwnerService();
        EmployeeService employeeService = new EmployeeService(new EmployeeRepository());

        if(userRole == UserRole.EMPLOYEE){
            employeeService.get(userId)
                    .addOnSuccessListener(new OnSuccessListener<Employee>() {
                        @Override
                        public void onSuccess(Employee employee) {
                            companyId = employee.getCompanyId();
                            initializeRecyclerView();
                            //TODO dodati za eventtypes dio preko kompanije a izbrisati stari dio
                        }
                    });
        }
        if(userRole == UserRole.OWNER){
            ownerService.get(userId)
                    .addOnSuccessListener(new OnSuccessListener<Owner>() {
                        @Override
                        public void onSuccess(Owner owner) {
                            companyId = owner.getCompanyId();
                            initializeRecyclerView();
                        }
                    });
        }


        Button filterButton = findViewById(R.id.product_filter_btn);
        Button searchButton = findViewById(R.id.product_search_btn);

        filterButton.setOnClickListener(v ->{
            handleFilterSearch();
        });

        // Kreiranje novog proizvoda
        FloatingActionButton newProductButton = findViewById(R.id.add_product_floatingActionButton);
        newProductButton.setOnClickListener(v-> {
            if(userRole == UserRole.OWNER){
                AddNewProductPopup();
            }else{
                Toast.makeText(this, "You have no permission for this action", Toast.LENGTH_SHORT).show();
            }

        } );

        //search
        EditText searchET = findViewById(R.id.product_search_text);
        searchButton.setOnClickListener(v ->{
            String name = searchET.getText().toString();
            handleSearch(name);
        });




    }
    private void initializeRecyclerView(){
        RecyclerView recyclerView = findViewById(R.id.products_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        productService.getUndeletedOfPUP(companyId ,new ProductService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Product> objects) {
                productList = new ArrayList<>(objects);
                productAdapter = new ProductListAdapter(ProductManagementActivity.this, productList,productService, userRole);
                recyclerView.setAdapter(productAdapter);

            }

            @Override
            public void onFailure(String errorMessage) {}
        });
    }

    private void AddNewProductPopup() {
        Product newProduct = new Product();
        newProduct.setPending(false); //at the beggining- assumption the subcategory will be something that exists
        newProduct.setImages(Arrays.asList(
                R.drawable.image_4,
                R.drawable.image_5,
                R.drawable.image_6
        ));
        newProduct.setDeleted(false);
        //ToDo CHANGE TO COMPANY ID
        newProduct.setCompanyId(companyId);
        View popupView = LayoutInflater.from(this).inflate(R.layout.product_add_form, null);

        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.buttonSave_add_product);
        Button cancelButton = popupView.findViewById(R.id.buttonCancel_add_product);
        ImageButton newSubCategoryBtn = popupView.findViewById(R.id.add_new_subcategory_btn_product);
        Button eventTypesbutton = popupView.findViewById(R.id.product_event_type_add);

        //fields
        EditText nameEditText = popupView.findViewById(R.id.edit_product_name);
        EditText descriptionEditView = popupView.findViewById(R.id.add_product_description);
        EditText priceEditView = popupView.findViewById(R.id.add_product_price);
        EditText discountEditView = popupView.findViewById(R.id.add_product_discount);
        RadioGroup availableRG = popupView.findViewById(R.id.add_available_product);
        RadioButton availableYES = popupView.findViewById(R.id.add_available_product_yes);
        RadioButton availableNO = popupView.findViewById(R.id.add_available_product_no);
        RadioGroup visibleRG = popupView.findViewById(R.id.add_visible_product_rg);
        RadioButton visibleYES = popupView.findViewById(R.id.add_visible_product_yes);
        RadioButton visibleNO = popupView.findViewById(R.id.add_visible_product_no);


         categorySpinner = popupView.findViewById(R.id.product_category_add_form);
        Spinner subCategorySpinner = popupView.findViewById(R.id.product_subcategory_add);

        setDataToCategorySpinner(categorySpinner);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCategoryNewProduct = (Category) parent.getItemAtPosition(position);

                subCategorySpinner.setAdapter(new CategorySpinnerAdapter(ProductManagementActivity.this, getSubcategories(selectedCategoryNewProduct)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        newSubCategoryBtn.setOnClickListener(v -> {
            if(newProduct.isPending()){
                Toast.makeText(this, "The request for new subcategory alredy has been made", Toast.LENGTH_SHORT).show();
            }else{
                showPopupCreateSubCategory(newProduct);
            }

        });

        saveButton.setOnClickListener(v -> {
            // mapping all values to the fields
            newProduct.setName(nameEditText.getText().toString());
            newProduct.setDescription(descriptionEditView.getText().toString());
            newProduct.setPrice(Double.parseDouble(priceEditView.getText().toString()));
            newProduct.setDiscount(Integer.parseInt(discountEditView.getText().toString()));

            int selectedRadioButtonId = availableRG.getCheckedRadioButtonId();
            if(selectedRadioButtonId != -1){
                if(selectedRadioButtonId == availableYES.getId()) {
                    newProduct.setAvailable(true);
                }
                if(selectedRadioButtonId == availableNO.getId()){
                    newProduct.setAvailable(false);
                }
            }else{
                Toast.makeText(this, "Select availability.", Toast.LENGTH_SHORT).show();
            }

            int selectedRadioButtonVisibleId = visibleRG.getCheckedRadioButtonId();
            if(selectedRadioButtonVisibleId != -1){
                if(selectedRadioButtonVisibleId == visibleYES.getId()) {
                    newProduct.setVisible(true);
                }
                if(selectedRadioButtonVisibleId == visibleNO.getId()){
                    newProduct.setVisible(false);
                }
            }else{
                Toast.makeText(this, "Select visibility.", Toast.LENGTH_SHORT).show();
            }
            //category and subcategory
            Category selectedCategory = (Category)categorySpinner.getSelectedItem();
            newProduct.setCathegory(selectedCategory.getName());
            newProduct.setCategoryId(selectedCategory.getId());

            Category selectedSubcategory = (Category)subCategorySpinner.getSelectedItem();
            newProduct.setSubCathegory(selectedSubcategory.getName());
            newProduct.setSubcategoryId(selectedSubcategory.getId());
            productList.add(newProduct);
            productAdapter.notifyItemInserted(productList.size()-1);
            productService.create(newProduct);
            popupWindow.dismiss();
        });

        cancelButton.setOnClickListener(v -> popupWindow.dismiss());
        eventTypesbutton.setOnClickListener(v -> handleEventTypeSelection(newProduct));
    }

    private void setDataToCategorySpinner(Spinner categorySpinner){
        CategoryService categoryService = new CategoryService();
        categoryService.getAll().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<Category> categories = task.getResult();
                CategorySpinnerAdapter adapter = new CategorySpinnerAdapter(this, categories);
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                categorySpinner.setAdapter(adapter);

            } else {
                // Handle the error
                Log.e("ProductManagementActivity", "Error getting categories", task.getException());
            }
        });
    }

    private void showPopupCreateSubCategory(Product product) {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.popup_add_subcategory, null);
        CategoryService categoryService = new CategoryService();
        PopupWindow popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.showAtLocation(popupView, 0, 0, 0);

            Spinner spinnerSubcategoryType = popupView.findViewById(R.id.spinnerSubcategoryType);
            Button saveButton = popupView.findViewById(R.id.buttonSave);
            // Set the adapter for the spinner
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.subcategory_types_product, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerSubcategoryType.setAdapter(adapter);
            EditText newSCname = popupView.findViewById(R.id.editTextSubcategoryName);
            EditText newSCdesc = popupView.findViewById(R.id.editTextSubcategoryDescription);

        saveButton.setOnClickListener(v->{
            Category newSubcategory = new Category();
            newSubcategory.setName(newSCname.getText().toString());
            newSubcategory.setDescription(newSCdesc.getText().toString());
            newSubcategory.setSubcategoryType(SubcategoryType.PRODUCT); //hardoced because we are working with products :)
            if(selectedCategoryNewProduct != null){
                if(selectedCategoryNewProduct.getSubcategoriesRequests() == null){
                    selectedCategoryNewProduct.setSubcategoriesRequests(new ArrayList<>());
                }
                selectedCategoryNewProduct.getSubcategoriesRequests().add(newSubcategory);
                //update of the category
                categoryService.update(selectedCategoryNewProduct).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        Toast.makeText(ProductManagementActivity.this, "Request succsessfully sent", Toast.LENGTH_SHORT).show();
                        product.setPending(true); //product is now pending
                    }
                });
            }
            popupWindow.dismiss();
        });

    }


    private List<Category> getSubcategories(Category category){
        return category.getSubcategories();
    }

    private void handleEventTypeSelection(Product product){
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.event_type_helper_layout, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        // dodavanje checkboxova
        LinearLayout checkboxContainer = popupView.findViewById(R.id.checkbox_container);
        eventTypeService.getAll(new EventTypeService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<EventType> objects) {
                //dodavanje checkboxova hehe
                for(EventType et: objects){
                    CheckBox checkBox = new CheckBox(ProductManagementActivity.this);
                    checkBox.setText(et.getName());
                    checkboxContainer.addView(checkBox);
                }
            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });
        Button saveButton = popupView.findViewById(R.id.button_save_event_type);

        saveButton.setOnClickListener(v -> {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < checkboxContainer.getChildCount(); i++) {
                View childView = checkboxContainer.getChildAt(i);
                if (childView instanceof CheckBox) {
                    CheckBox checkBox = (CheckBox) childView;
                    if (checkBox.isChecked()) {
                        sb.append(checkBox.getText().toString()).append(", ");
                    }
                }
            }
            //POSTAVLJANJE VRIJEDNOSTI!!!!!
            product.setEventType(sb.toString());

            popupWindow.dismiss();

        });
    }

    private void handleSearch(String name){
        List<Product> searchResult = new ArrayList<>();
            productService.getUndeletedOfPUP(companyId ,new ProductService.OnDataFetchListener() {
                @Override
                public void onSuccess(List<Product> objects) {
                    productList.clear();

                    if(name.isEmpty() || name.equals(" ")){
                        productList.addAll(objects);
                        productAdapter.notifyDataSetChanged();
                    }else{
                        searchResult.addAll(objects);
                        for(Product p : searchResult){
                           if(p.getName().contains(name)) {
                               productList.add(p);
                           }
                        }
                        productAdapter.notifyDataSetChanged();
                    }

                }

                @Override
                public void onFailure(String errorMessage) {

                }
            });


    }

    public void handleFilterSearch(){
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        View dialogView = getLayoutInflater().inflate(R.layout.product_filter_sheet, null);
        bottomSheetDialog.setContentView(dialogView);
        //setting information for spinners - categories and subcategories
        filterCategorySpinner = bottomSheetDialog.findViewById(R.id.product_category_filter);
        Spinner filterSubCategorySpinner = bottomSheetDialog.findViewById(R.id.product_subcategory_filter);
         filterEventTypeSpinner = bottomSheetDialog.findViewById(R.id.product_event_type_filter);
        //getting categories
        setDataToFilterSpinner();
        setDataToEventSpinner();
        filterCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Category selectedCategory = (Category) parent.getItemAtPosition(position);
                List<Category> subcategories = new ArrayList<>(getSubcategories(selectedCategory));
                Category emptySubcategory = new Category("No Selection","");
                subcategories.add(emptySubcategory);
                filterSubCategorySpinner.setAdapter(new CategorySpinnerAdapter(ProductManagementActivity.this, subcategories));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        bottomSheetDialog.show();

        //dobavljanje ostalih elemenata
        EditText pricefilter = bottomSheetDialog.findViewById(R.id.product_price_filter);
        EditText descriptionFilter = bottomSheetDialog.findViewById(R.id.product_description_filter);
        RadioGroup availableRG = bottomSheetDialog.findViewById(R.id.product_visible_filter);
        RadioButton availableYES = bottomSheetDialog.findViewById(R.id.product_visible_filter_yes);
        RadioButton availableNO = bottomSheetDialog.findViewById(R.id.product_visible_filter_no);


        bottomSheetDialog.setOnDismissListener(dialog ->{
            //spinneri i rg
            Category selectedCategory = (Category)filterCategorySpinner.getSelectedItem();
            Category selectedSubCategory = (Category)filterSubCategorySpinner.getSelectedItem();
            EventType selectedEventType = (EventType)filterEventTypeSpinner.getSelectedItem();
            int selectedRadioButtonId = availableRG.getCheckedRadioButtonId();
            int available = 0;
            if(selectedRadioButtonId != -1){
                if(selectedRadioButtonId == availableYES.getId()) {
                    available = 1;
                }
                if(selectedRadioButtonId == availableNO.getId()){
                    available = 2;
                }
            }

            //pretragica haha
            List<Product> filterResult = new ArrayList<>();
            boolean toAdd;
            for(Product p : productList){
                toAdd = true;
                //kategorija
                if(!selectedCategory.getName().equals("No Selection")){
                    if(!p.getCathegory().equals(selectedCategory.getName())){
                        toAdd = false;
                    }
                }

                if(!selectedSubCategory.getName().equals("No Selection")){
                    if(!p.getSubCathegory().equals(selectedSubCategory.getName())){
                        toAdd = false;
                    }
                }

                if(!selectedEventType.getName().equals("No Selection")){
                    if(!p.getEventType().contains(selectedEventType.getName())){
                        toAdd = false;
                    }
                }

                if(!descriptionFilter.getText().toString().isEmpty() || descriptionFilter.getText().toString().equals(" ")){
                    if(!p.getDescription().contains(descriptionFilter.getText().toString())){
                        toAdd = false;
                    }
                }
                if(!pricefilter.getText().toString().isEmpty() || descriptionFilter.getText().toString().equals(" ")){
                    double price = Double.parseDouble(pricefilter.getText().toString());
                    if(!(p.getPrice() == price)){
                        toAdd = false;
                    }
                }

                if(available != 0){
                    if(available == 1){
                        if(!p.isAvailable()){
                            toAdd = false;
                        }
                    }else{
                        if(p.isAvailable()){
                            toAdd = false;
                        }
                    }
                }
                if(toAdd){
                    filterResult.add(p);
                }
            }

            productList.clear();
            productList.addAll(filterResult);
            productAdapter.notifyDataSetChanged();

        });
    }


    private void setDataToFilterSpinner(){
        CategoryService categoryService = new CategoryService();
        categoryService.getAll().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<Category> categories = new ArrayList<>();

                Category emptyCategory = new Category();
                emptyCategory.setName("No Selection");
                Category emptySubcategory = new Category("No Selection","");
                List<Category> subcategoriesList = new ArrayList<>();
                subcategoriesList.add(emptySubcategory);
                emptyCategory.setSubcategories(subcategoriesList);
                categories.add(0,emptyCategory);
                categories.addAll(task.getResult());

                CategorySpinnerAdapter adapter = new CategorySpinnerAdapter(this, categories);
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                filterCategorySpinner.setAdapter(adapter);

            } else {
                // Handle the error
                Log.e("ProductManagementActivity", "Error getting categories", task.getException());
            }
        });
    }

    private void setDataToEventSpinner(){
        eventTypeService.getAll(new EventTypeService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<EventType> objects) {
                // Dodavanje checkboxova hehe
                EventType emptyEventType = new EventType();
                emptyEventType.setName("No Selection");
                emptyEventType.setId("000");
                List<EventType> eventTypes = new ArrayList<>();
                eventTypes.add(0,emptyEventType);
                eventTypes.addAll(objects);
                EventTypeSpinnerAdapter adapter = new EventTypeSpinnerAdapter(ProductManagementActivity.this, eventTypes);
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                filterEventTypeSpinner.setAdapter(adapter);
            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });
    }


}
