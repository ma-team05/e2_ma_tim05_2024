package com.example.eventplanner.activities;

import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.Utilities.PDFGenerator;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.database.repository.ProductRepository;
import com.example.eventplanner.database.repository.ServiceRepository;
import com.example.eventplanner.fragments.PricelistPackagesFragment;
import com.example.eventplanner.fragments.PricelistProductsFragment;
import com.example.eventplanner.fragments.PricelistServicesFragment;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.UserRole;

import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.PackageService;
import com.example.eventplanner.services.ProductService;
import com.example.eventplanner.services.ServiceService;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class PriceListActivity extends AppCompatActivity {

    String userId;
    UserRole userRole;
    String companyId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_price_list);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");
        userRole = UserRole.values()[intent.getIntExtra("userRole", 0)];

        OwnerService ownerService = new OwnerService();
        EmployeeService employeeService = new EmployeeService(new EmployeeRepository());

        // getting buttons
        Button productsButton = findViewById(R.id.pricelist_products_btn);
        Button servicesButton = findViewById(R.id.pricelist_services_btn);
        Button packagesButton = findViewById(R.id.pricelist_packages_btn);
        Button pdfButton = findViewById(R.id.pricelist_pdf);


        if(userRole == UserRole.EMPLOYEE){
            employeeService.get(userId)
                    .addOnSuccessListener(new OnSuccessListener<Employee>() {

                        @Override
                        public void onSuccess(Employee employee) {
                            Log.i("Frag","Dobavio Usera");
                            companyId = employee.getCompanyId();
                            setOnCLickListeners(productsButton,servicesButton,packagesButton,pdfButton);
                        }
                    });
        }
        if(userRole == UserRole.OWNER){
            ownerService.get(userId)
                    .addOnSuccessListener(new OnSuccessListener<Owner>() {
                        @Override
                        public void onSuccess(Owner owner) {
                            companyId = owner.getCompanyId();
                            setOnCLickListeners(productsButton,servicesButton,packagesButton,  pdfButton);
                        }
                    });
        }



    }

    private void setOnCLickListeners(Button productsButton, Button servicesButton, Button packagesButton,Button pdfButton){
        Log.i("Frag","Usao u metodu");
        productsButton.setOnClickListener(v ->{

            PricelistProductsFragment fragment = PricelistProductsFragment.newInstance(userRole.toString(),companyId);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.pricelist_fragment_container, fragment)
                    .commit();
        });

        servicesButton.setOnClickListener(v -> {
            PricelistServicesFragment fragment = PricelistServicesFragment.newInstance(userRole.toString(),companyId);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.pricelist_fragment_container, fragment)
                    .commit();
        });

        packagesButton.setOnClickListener( v ->{
            PricelistPackagesFragment fragment = PricelistPackagesFragment.newInstance(userRole.toString(),companyId);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.pricelist_fragment_container, fragment)
                    .commit();
        });

        pdfButton.setOnClickListener(v ->{
            createPdf();
        });
    }

    private void createPdf(){
        ProductService packageService = new ProductService(new ProductRepository());
        ServiceService serviceService = new ServiceService(new ServiceRepository());
        PackageService productService = new PackageService(new PackageRepository());
        PDFGenerator pdfGenerator = new PDFGenerator();
        packageService.getUndeletedOfPUP(companyId, new ProductService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Product> products) {
                serviceService.getUndeletedOfPUP(companyId, new ServiceService.OnDataFetchListener() {
                    @Override
                    public void onSuccess(List<Service> services) {
                        productService.getUndeletedOfPUP(companyId, new PackageService.OnDataFetchListener() {
                            @Override
                            public void onSuccess(List<Package> packages) {
                                PdfDocument pdfDocument = pdfGenerator.generatePDF(products,services,packages);
                                downloadPDF(pdfDocument);
                            }

                            @Override
                            public void onFailure(String errorMessage) {

                            }
                        });
                    }

                    @Override
                    public void onFailure(String errorMessage) {

                    }
                });
            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });
    }

    public void downloadPDF(PdfDocument pdfDocument) {
        File downloadsFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        if (!downloadsFolder.exists()) {
            downloadsFolder.mkdirs(); // Create if it doesn't exist
        }
        File file = new File(downloadsFolder, "pricelist.pdf");
        try {

            FileOutputStream outputStream = new FileOutputStream(file);
            pdfDocument.writeTo(outputStream);
            pdfDocument.close();

            // Notify the system that a new file was created
            MediaScannerConnection.scanFile(this, new String[]{file.getAbsolutePath()}, null, null);

            Toast.makeText(this, "PDF saved to Downloads folder", Toast.LENGTH_SHORT).show();
            pdfDocument.close();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Failed to save PDF: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}