package com.example.eventplanner.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Grade;
import com.example.eventplanner.models.GradeReport;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.ReportStatus;
import com.example.eventplanner.services.GradeReportService;
import com.example.eventplanner.services.GradeService;
import com.example.eventplanner.services.NotificationService;

import java.util.Date;

public class ReportingGradeActivity extends AppCompatActivity {

    private String gradeId;
    private GradeReportService gradeReportService = new GradeReportService();
    private String ownerId;
    private GradeService gradeService = new GradeService();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_reporting_grade);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        Intent intent = getIntent();
        gradeId = intent.getStringExtra("gradeId");
        ownerId = intent.getStringExtra("ownerId");
        Button submitButton = this.findViewById(R.id.buttonSubmit);
        submitButton.setOnClickListener(v->{
            EditText commentEdittext = findViewById(R.id.editTextComment);
            String reason = commentEdittext.getText().toString();
            if(reason==null || reason.isEmpty()){
                Toast.makeText(this, "No reason added", Toast.LENGTH_SHORT).show();
                return;
            }
            gradeService.getById(gradeId).addOnCompleteListener(task1->{
               if(task1.isSuccessful()){
                   gradeReportService.addGradeReport(new GradeReport("", gradeId, reason, "", new Date(), ReportStatus.REPORTED, ownerId, task1.getResult())).addOnCompleteListener(task->{
                       if(task.isSuccessful()) {
                           NotificationService ns = new NotificationService();
                           ns.add(null, "New grade report", NotificationType.ADMIN).addOnCompleteListener(task2 -> {
                               Toast.makeText(this, "Grade reported successfully", Toast.LENGTH_SHORT).show();
                               Intent resultIntent = new Intent();
                               setResult(Activity.RESULT_OK, resultIntent);
                               finish();
                           });
                       }
                       else
                           Toast.makeText(this, "Error reporting grade", Toast.LENGTH_SHORT).show();
                   });
               }
            });
        });
    }
}