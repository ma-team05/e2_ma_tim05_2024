package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.ServiceDetailsActivity;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.UserService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ServiceResultsAdapter extends RecyclerView.Adapter<ServiceResultsAdapter.ServiceViewHolder> {

    private Context context;
    private List<Service> serviceList;

    private User user;

    private UserService userService = new UserService();

    private String selectedEventId = "";


    public ServiceResultsAdapter(Context context, List<Service> serviceList, User user, String selectedEventId) {
        this.context = context;
        this.serviceList = serviceList;
        this.user = user;
        this.selectedEventId = selectedEventId;


        Log.i("Frag", "Napravio adapter");
    }

    @NonNull
    @Override
    public ServiceResultsAdapter.ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service_result, parent, false);
        return new ServiceResultsAdapter.ServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceViewHolder holder, int position) {
        Service service = serviceList.get(position);
        holder.bind(service);
    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }

    public class ServiceViewHolder extends RecyclerView.ViewHolder {

        ImageButton btPrevious, btNext, btnFav, btnUnFav, btnInfo;
        ImageSwitcher solutionImageSwitcher;
        TextView serviceName, serviceCategory, serviceSubcategory, serviceDescription,
                servicePrice, serviceDiscount, serviceDiscountPrice, serviceAvailable, serviceVisibility, serviceEventType,
                serviceDuration, serviceReserveIn, serviceCancelIn, serviceConfirm, serviceEmployees, serviceSpecifities;
        private Service currentService;  // Changed to Service
        private int currentIndex;

        public ServiceViewHolder(@NonNull View itemView) {
            super(itemView);

            btPrevious = itemView.findViewById(R.id.bt_previous);
            btNext = itemView.findViewById(R.id.bt_next);
            btnFav = itemView.findViewById(R.id.btn_fav);
            btnUnFav = itemView.findViewById(R.id.btn_unfav);
            btnInfo = itemView.findViewById(R.id.btn_info);
            solutionImageSwitcher = itemView.findViewById(R.id.solution_image_switcher);
            solutionImageSwitcher.setFactory(() -> {
                ImageView imageView = new ImageView(itemView.getContext());
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                return imageView;
            });
            serviceName = itemView.findViewById(R.id.solution_name);
            serviceCategory = itemView.findViewById(R.id.solution_category);
            serviceSubcategory = itemView.findViewById(R.id.solution_subcategory);
            serviceDescription = itemView.findViewById(R.id.solution_description);
            servicePrice = itemView.findViewById(R.id.solution_price);
            serviceDiscount = itemView.findViewById(R.id.solution_discount);
            serviceDiscountPrice = itemView.findViewById(R.id.solution_discount_price);
            serviceAvailable = itemView.findViewById(R.id.solution_available);
            serviceVisibility = itemView.findViewById(R.id.solution_visibility);
            serviceEventType = itemView.findViewById(R.id.solution_event_type);
            serviceDuration = itemView.findViewById(R.id.solution_duration);
            serviceReserveIn = itemView.findViewById(R.id.solution_res_deadline);
            serviceCancelIn = itemView.findViewById(R.id.solution_cancel_in);
            serviceConfirm = itemView.findViewById(R.id.solution_confirmation);
            serviceEmployees = itemView.findViewById(R.id.solution_employees);
            serviceSpecifities = itemView.findViewById(R.id.solution_specifities);

            btNext.setOnClickListener(v -> {
                if (currentIndex < currentService.getImages().size() - 1) {
                    currentIndex++;
                    solutionImageSwitcher.setImageResource(currentService.getImages().get(currentIndex));
                }
            });

            btPrevious.setOnClickListener(v -> {
                if (currentIndex > 0) {
                    currentIndex--;
                    solutionImageSwitcher.setImageResource(currentService.getImages().get(currentIndex));
                }
            });

            btnFav.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    user.addFavouriteService(serviceList.get(position).getId());
                    userService.update(user);
                    btnFav.setVisibility(View.GONE);
                    btnUnFav.setVisibility(View.VISIBLE);
                }
            });

            btnUnFav.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    user.removeFavouriteService(serviceList.get(position).getId());
                    userService.update(user);
                    btnFav.setVisibility(View.VISIBLE);
                    btnUnFav.setVisibility(View.GONE);
                }
            });

            btnInfo.setOnClickListener(v -> {
                int position = getAdapterPosition();
                Intent createIntent = new Intent(context, ServiceDetailsActivity.class);
                createIntent.putExtra("EMAIL", user.getEmail());
                createIntent.putExtra("SERVICE", serviceList.get(position).getId());
                createIntent.putExtra("EVENT", selectedEventId);
                context.startActivity(createIntent);
            });
        }

        public void bind(Service service) {  // Changed to Service
            currentService = service;
            boolean ind = service != null;
            currentIndex = 0;
            Log.i("binder", ind ? "nije null" : "null je");
            serviceName.setText(service.getName());
            serviceCategory.setText(service.getCategory());
            serviceSubcategory.setText(service.getSubCategory());
            serviceDescription.setText(service.getDescription());
            servicePrice.setText(String.valueOf(service.getPrice()));
            serviceDiscount.setText(String.valueOf(service.getDiscount()));

            serviceEventType.setText(service.getEventType());
            serviceAvailable.setText(service.isAvailable() ? "Yes" : "No");
            serviceVisibility.setText(service.isVisible() ? "Yes" : "No");
            serviceEmployees.setText(service.getEmployeesAsString());

            if (currentService.isAppointment()) {
                serviceDuration.setText(currentService.getAppointmentDuration() + "h");
            } else {
                StringBuilder builder = new StringBuilder();
                builder.append("min ").append(currentService.getMinDuration()).append("h ").append("max ").append(currentService.getMaxDuration()).append("h ");
                serviceDuration.setText(builder.toString());
            }

            Calendar cal = Calendar.getInstance();

            // Reservation deadline
            if (currentService.getReservationDeadline() != null) {
                Date reservationDate = currentService.getReservationDeadline().toDate();
                cal.setTimeInMillis(reservationDate.getTime());
                StringBuilder reservationBuilder = new StringBuilder();
                reservationBuilder.append(cal.get(Calendar.MONTH)).append("m "); // Adding 1 to get 1-based month
                reservationBuilder.append(cal.get(Calendar.DAY_OF_MONTH)).append("d ");
                serviceReserveIn.setText(reservationBuilder.toString());
            } else {
                serviceReserveIn.setText("N/A");
            }

            // Cancellation deadline
            if (currentService.getCancelationDeadline() != null) {
                Date cancellationDate = currentService.getCancelationDeadline().toDate();
                cal.setTimeInMillis(cancellationDate.getTime());
                StringBuilder cancellationBuilder = new StringBuilder();
                cancellationBuilder.append(cal.get(Calendar.MONTH)).append("m "); // Adding 1 to get 1-based month
                cancellationBuilder.append(cal.get(Calendar.DAY_OF_MONTH)).append("d ");
                serviceCancelIn.setText(cancellationBuilder.toString());
            } else {
                serviceCancelIn.setText("N/A");
            }

            if (currentService.getSpecifities().isEmpty()) {
                serviceSpecifities.setText("/");
            } else {
                serviceSpecifities.setText(currentService.getSpecifities());
            }

            if (currentService.getConfirmationType() == Service.ConfirmationType.AUTOMATIC) {
                serviceConfirm.setText("Automatic");
            } else {
                serviceConfirm.setText("By hand");
            }

            if (currentService.isAppointment()) {
                double finalprice = service.getPrice() - service.getDiscount();
                double hours = Double.parseDouble(currentService.getAppointmentDuration());
                finalprice = finalprice * hours;
                serviceDiscountPrice.setText(String.valueOf(finalprice));
            } else {
                serviceDiscountPrice.setText("will be calculated");
            }

            if (service.getImages() != null && !service.getImages().isEmpty()) {
                solutionImageSwitcher.setImageResource(service.getImages().get(0));
            } else {
                solutionImageSwitcher.setImageResource(R.drawable.default_image);
            }

            if (user.getFavouriteServices().contains(service.getId())) {
                btnUnFav.setVisibility(View.VISIBLE);
                btnFav.setVisibility(View.GONE);
            } else {
                btnFav.setVisibility(View.VISIBLE);
                btnUnFav.setVisibility(View.GONE);
            }
        }
    }

    @SuppressLint({"NotifyDataSetChanged"})
    private void editServicePopup(Service service) {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.service_edit_form, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        LinearLayout layoutServiceAppointment = popupView.findViewById(R.id.layout_service_appointment);
        LinearLayout layoutServiceNoAppoitnment = popupView.findViewById(R.id.layout_service_no_appointment);

        RadioButton automaticRadioButotn = popupView.findViewById(R.id.automatic_radio_button);

        if (service.isAppointment()) {
            layoutServiceNoAppoitnment.setVisibility(View.GONE);
            layoutServiceAppointment.setVisibility(View.VISIBLE);
            automaticRadioButotn.setVisibility(View.VISIBLE);
        } else {
            layoutServiceNoAppoitnment.setVisibility(View.VISIBLE);
            layoutServiceAppointment.setVisibility(View.GONE);
            automaticRadioButotn.setVisibility(View.GONE);
        }
        //show window
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.buttonSave_edit_service);
        Button cancelButton = popupView.findViewById(R.id.buttonCancel_edit_service);

        saveButton.setOnClickListener(v -> popupWindow.dismiss());

        cancelButton.setOnClickListener(v -> popupWindow.dismiss());
    }

    private void showDeleteConfirmationDialog(Service service) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Delete Service");
        builder.setMessage("Are you sure you want to delete this service?");

        // Set up the buttons
        builder.setPositiveButton("OK", (dialog, which) -> {
            //delete operation here
            dialog.dismiss();
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}