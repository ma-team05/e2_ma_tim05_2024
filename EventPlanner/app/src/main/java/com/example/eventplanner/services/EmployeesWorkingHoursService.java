package com.example.eventplanner.services;

import com.example.eventplanner.models.EmployeesWorkingHours;
import com.example.eventplanner.database.repository.EmployeeWorkingHoursRepository;
import com.google.android.gms.tasks.Task;

import java.util.List;

public class EmployeesWorkingHoursService {
    private EmployeeWorkingHoursRepository repository;

    public EmployeesWorkingHoursService() {
        this.repository = new EmployeeWorkingHoursRepository();
    }

    public void create(EmployeesWorkingHours employee) {
        repository.saveEmployeesWorkingHours(employee);
    }

    public Task<List<EmployeesWorkingHours>> getAllByEmployee(String employeeId) {
        return repository.getAllByEmployee(employeeId);
    }

    public void delete(EmployeesWorkingHours employeesWorkingHours) { repository.deleteEmployeesWorkingHours(employeesWorkingHours); }
    public Task<EmployeesWorkingHours> get(String id){return repository.getById(id);}
}
