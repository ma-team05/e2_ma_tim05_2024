package com.example.eventplanner.Utilities;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import androidx.core.content.FileProvider;

import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

public class PDFGenerator {
    public PdfDocument generatePDF(List<Product> products, List<Service> services, List<Package> packages) {
        PdfDocument pdfDocument = new PdfDocument();
        Paint paint = new Paint();
        Paint titlePaint = new Paint();
        Paint subtitlePaint = new Paint();
        Paint headerPaint = new Paint();

        titlePaint.setTextSize(20);
        titlePaint.setFakeBoldText(true);

        subtitlePaint.setTextSize(16);
        subtitlePaint.setFakeBoldText(true);

        headerPaint.setTextSize(12);
        headerPaint.setFakeBoldText(true);

        int pageWidth = 300;
        int pageHeight = 600;
        int rowSpacing = 20;
        int startXPosition = 20;

        class PageContext {
            int yPosition = 90;  // initial position after title
            PdfDocument.Page page;
            Canvas canvas;

            PageContext() {
                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(pageWidth, pageHeight, 1).create();
                this.page = pdfDocument.startPage(pageInfo);
                this.canvas = page.getCanvas();
                // Add the date
                String date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                canvas.drawText("Date: " + date, 20, 30, paint);
                // Add the title
                canvas.drawText("PRICELIST", 20, 60, titlePaint);
            }

            void startNewPage() {
                pdfDocument.finishPage(this.page);
                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(pageWidth, pageHeight, pdfDocument.getPages().size() + 1).create();
                this.page = pdfDocument.startPage(pageInfo);
                this.canvas = this.page.getCanvas();
                this.yPosition = 0;  // reset yPosition for the new page
            }
        }

        PageContext context = new PageContext();

        // Add the subtitle for products
        context.canvas.drawText("Products", 20, context.yPosition, subtitlePaint);
        context.yPosition += rowSpacing;

        // Add the table headers
        context.canvas.drawText("Name", startXPosition, context.yPosition, headerPaint);
        context.canvas.drawText("Price", startXPosition + 80, context.yPosition, headerPaint);
        context.canvas.drawText("Discount", startXPosition + 140, context.yPosition, headerPaint);
        context.canvas.drawText("New Price", startXPosition + 200, context.yPosition, headerPaint);
        context.yPosition += rowSpacing;

        // Add the list of products
        for (Product item : products) {
            if (context.yPosition > pageHeight - rowSpacing) context.startNewPage();
            context.canvas.drawText(item.getName(), startXPosition, context.yPosition, paint);
            context.canvas.drawText(String.valueOf(item.getPrice()), startXPosition + 80, context.yPosition, paint);
            context.canvas.drawText(String.valueOf(item.getDiscount()), startXPosition + 140, context.yPosition, paint);
            context.canvas.drawText(String.valueOf(item.getPrice() - item.getPrice() * item.getDiscount() / 100), startXPosition + 200, context.yPosition, paint);
            context.yPosition += rowSpacing;
        }

        context.yPosition += rowSpacing;
        if (context.yPosition > pageHeight - rowSpacing) context.startNewPage();
        context.canvas.drawText("Services", 20, context.yPosition, subtitlePaint);
        context.yPosition += rowSpacing;

        context.canvas.drawText("Name", startXPosition, context.yPosition, headerPaint);
        context.canvas.drawText("Price", startXPosition + 80, context.yPosition, headerPaint);
        context.canvas.drawText("Discount", startXPosition + 140, context.yPosition, headerPaint);
        context.canvas.drawText("New Price", startXPosition + 200, context.yPosition, headerPaint);
        context.yPosition += rowSpacing;

        for (Service item : services) {
            if (context.yPosition > pageHeight - rowSpacing) context.startNewPage();
            context.canvas.drawText(item.getName(), startXPosition, context.yPosition, paint);
            context.canvas.drawText(String.valueOf(item.getPrice()), startXPosition + 80, context.yPosition, paint);
            context.canvas.drawText(String.valueOf(item.getDiscount()), startXPosition + 140, context.yPosition, paint);
            if (item.isAppointment()) {
                context.canvas.drawText(String.valueOf((item.getPrice() - item.getPrice() * item.getDiscount() / 100) * Double.parseDouble(item.getAppointmentDuration())), startXPosition + 200, context.yPosition, paint);
            } else {
                context.canvas.drawText(String.valueOf((item.getPrice() - item.getPrice() * item.getDiscount() / 100) * Double.parseDouble(item.getMinDuration())), startXPosition + 200, context.yPosition, paint);
            }
            context.yPosition += rowSpacing;
        }

        context.yPosition += rowSpacing;
        if (context.yPosition > pageHeight - rowSpacing) context.startNewPage();
        context.canvas.drawText("Packages", 20, context.yPosition, subtitlePaint);
        context.yPosition += rowSpacing;

        context.canvas.drawText("Name", startXPosition, context.yPosition, headerPaint);
        context.canvas.drawText("Price", startXPosition + 80, context.yPosition, headerPaint);
        context.canvas.drawText("Discount", startXPosition + 140, context.yPosition, headerPaint);
        context.canvas.drawText("New Price", startXPosition + 200, context.yPosition, headerPaint);
        context.yPosition += rowSpacing;

        for (Package item : packages) {
            if (context.yPosition > pageHeight - rowSpacing) context.startNewPage();
            context.canvas.drawText(item.getName(), startXPosition, context.yPosition, paint);
            context.canvas.drawText(String.valueOf(item.getPrice()), startXPosition + 80, context.yPosition, paint);
            context.canvas.drawText(String.valueOf(item.getDiscount()), startXPosition + 140, context.yPosition, paint);
            context.canvas.drawText(String.valueOf(item.getPrice() - item.getPrice() * item.getDiscount() / 100), startXPosition + 200, context.yPosition, paint);
            context.yPosition += rowSpacing;
        }

        pdfDocument.finishPage(context.page);
        return pdfDocument;
    }


}
