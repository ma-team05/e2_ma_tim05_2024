package com.example.eventplanner.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CategorySpinnerAdapter;
import com.example.eventplanner.adapters.PackageAdapter;
import com.example.eventplanner.database.repository.CategoryRepository;
import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.database.repository.ProductRepository;
import com.example.eventplanner.database.repository.ServiceRepository;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.PackageService;
import com.example.eventplanner.services.ProductService;
import com.example.eventplanner.services.ServiceService;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.google.firebase.Timestamp;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class PackageManagementActivity extends AppCompatActivity {

    private CategoryRepository categoryRepository;
    Spinner categorySpinner;
    Spinner filterCategorySpinner;
    Spinner filterEventTypeSpinner;
    String userId;
    UserRole userRole;
    String companyId;
    Category selectedCategoryNewProduct;
    private RecyclerView recyclerView;
    Package package1;
    LinearLayout layoutPackage;
    private PackageAdapter packageAdapter;
    List<Package> packageList;
    private PackageService packageService;
    private ProductService productService;
    private ServiceService serviceService;
    RadioButton automaticConfirmationRB;
    boolean visibleAutomaticConf;
    double packagePrice;
    LocalDate smallestTimestamp;
    LocalDate smallestTimestamp1;
    boolean serviceSearch;
    boolean productSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_package_management);

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");
        userRole = UserRole.values()[intent.getIntExtra("userRole", 0)];
        categoryRepository = new CategoryRepository();
        PackageRepository packageRepository = new PackageRepository();
        packageService = new PackageService(packageRepository);
        serviceService = new ServiceService(new ServiceRepository());
        productService = new ProductService(new ProductRepository());

        OwnerService ownerService = new OwnerService();
        EmployeeService employeeService = new EmployeeService(new EmployeeRepository());

        if (userRole == UserRole.EMPLOYEE) {
            employeeService.get(userId)
                    .addOnSuccessListener(new OnSuccessListener<Employee>() {
                        @Override
                        public void onSuccess(Employee employee) {
                            companyId = employee.getCompanyId();
                            initializeRecyclerView();
                            //TODO dodati za eventtypes dio preko kompanije a izbrisati stari dio
                        }
                    });
        }
        if (userRole == UserRole.OWNER) {
            ownerService.get(userId)
                    .addOnSuccessListener(new OnSuccessListener<Owner>() {
                        @Override
                        public void onSuccess(Owner owner) {
                            companyId = owner.getCompanyId();
                            initializeRecyclerView();
                        }
                    });
        }


        List<Integer> emptyImages = new ArrayList<>();
        List<String> emptyList = new ArrayList<>();

        package1 = new Package("",
                "",                 // category
                emptyList,          // subcategories
                "",                 // description
                0.0,                // price
                0.0,                // discount
                emptyImages,        // images
                true,               // visible
                true,               // available
                emptyList,          // products
                emptyList,          // services
                "",                 // eventTypes
                new Timestamp(0,0),   // reservationDeadline
                new Timestamp(0,0),   // cancelationDeadline
                null                // confirmationType
        );

        Button searchButton = findViewById(R.id.package_search_btn);
        Button filterButton = findViewById(R.id.package_filter_btn);
        filterButton.setOnClickListener(v -> {
//            Log.i("ShopApp", "Bottom Sheet Dialog");
//            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
//            View dialogView = getLayoutInflater().inflate(R.layout.package_filter_sheet, null);
//            bottomSheetDialog.setContentView(dialogView);
//            bottomSheetDialog.show();
            //todo uncomment
            //handleFilterSearch();
        });

        EditText searchET = findViewById(R.id.package_search_text);
        searchButton.setOnClickListener(v -> {
            String name = searchET.getText().toString();
            //TODO uncomment
            handleSearch(name);
        });

        FloatingActionButton newProductButton = findViewById(R.id.add_package_floatingActionButton);
        newProductButton.setOnClickListener(v -> {
            if (userRole == UserRole.OWNER) {
                AddNewPackagePopup();
            } else {
                Toast.makeText(this, "Your have no permission for this action", Toast.LENGTH_SHORT).show();
            }

        });


    }


    @SuppressLint({"NotifyDataSetChanged"})
    private void AddNewPackagePopup() {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.package_add_form, null);
        Package newPackage = new Package();
        newPackage.setImages(Arrays.asList(
                R.drawable.image_4,
                R.drawable.image_5,
                R.drawable.image_6
        ));
        newPackage.setDeleted(false);
        newPackage.setCompanyId(companyId);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.buttonSave_add_package);
        Button cancelButton = popupView.findViewById(R.id.buttonCancel_add_package);
        Button productButton = popupView.findViewById(R.id.package_buttonProducts_add);
        Button serviceButton = popupView.findViewById(R.id.package_buttonServices_add);

        layoutPackage = popupView.findViewById(R.id.package_help_layout);
        layoutPackage.setVisibility(View.GONE);

        automaticConfirmationRB = popupView.findViewById(R.id.package_automatic_radio_button);

        //setting up spinner data and employee data
        categorySpinner = popupView.findViewById(R.id.package_category_add);
        //fields
        TextView eventType = popupView.findViewById(R.id.package_event_types_add);
        TextView priceText = popupView.findViewById(R.id.package_price_add);
        TextView suncategorytext = popupView.findViewById(R.id.package_subcategory_add);
        TextView resMonth = popupView.findViewById(R.id.package_res_month);
        TextView resday = popupView.findViewById(R.id.package_res_day);
        TextView cancMonth = popupView.findViewById(R.id.package_cancel_month);
        TextView cancDay = popupView.findViewById(R.id.package_cancel_day);

        EditText discount = popupView.findViewById(R.id.add_package_discount);
        EditText description = popupView.findViewById(R.id.add_package_description);
        EditText name = popupView.findViewById(R.id.add_package_name);
        RadioGroup confirmationRG = popupView.findViewById(R.id.package_conformation);
        RadioButton byHandRadioButton = popupView.findViewById(R.id.package_confirmation_bh);
        RadioGroup availableRG = popupView.findViewById(R.id.package_available_rg);
        RadioButton availableYES = popupView.findViewById(R.id.package_available_rg_yes);
        RadioButton availableNO = popupView.findViewById(R.id.package_available_rg_no);
        RadioGroup visibleRG = popupView.findViewById(R.id.package_visible_rg);
        RadioButton visibleYES = popupView.findViewById(R.id.package_visible_rg_yes);
        RadioButton visibleNO = popupView.findViewById(R.id.package_visible_rg_no);

        setDataToCategorySpinner(categorySpinner);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCategoryNewProduct = (Category) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        saveButton.setOnClickListener(v -> {
            newPackage.setDescription(description.getText().toString());
            newPackage.setDiscount(Integer.parseInt(discount.getText().toString()));
            newPackage.setName(name.getText().toString());

            int selectedRadioButtonId = availableRG.getCheckedRadioButtonId();
            if(selectedRadioButtonId != -1){
                if(selectedRadioButtonId == availableYES.getId()) {
                    newPackage.setAvailable(true);
                }
                if(selectedRadioButtonId == availableNO.getId()){
                    newPackage.setAvailable(false);
                }
            }else{
                Toast.makeText(this, "Select availability.", Toast.LENGTH_SHORT).show();
            }

            int selectedRadioButtonVisibleId = visibleRG.getCheckedRadioButtonId();
            if(selectedRadioButtonVisibleId != -1){
                if(selectedRadioButtonVisibleId == visibleYES.getId()) {
                    newPackage.setVisible(true);
                }
                if(selectedRadioButtonVisibleId == visibleNO.getId()){
                    newPackage.setVisible(false);
                }
            }else{
                Toast.makeText(this, "Select visibility.", Toast.LENGTH_SHORT).show();
            }

            //finally check for confirmation
            if(!newPackage.getServices().isEmpty()){
                if(automaticConfirmationRB.getVisibility() == View.VISIBLE){
                    int selectedRadioButtonConfirmationId = confirmationRG.getCheckedRadioButtonId();
                    if(selectedRadioButtonConfirmationId != -1){
                        if(selectedRadioButtonConfirmationId == byHandRadioButton.getId()) {
                            newPackage.setConfirmationType(Service.ConfirmationType.AUTOMATIC);
                        }else{
                            newPackage.setConfirmationType(Service.ConfirmationType.BYHAND);
                        }

                    }else{
                        Toast.makeText(this, "Select confirmation Type.", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    newPackage.setConfirmationType(Service.ConfirmationType.BYHAND);
                }
            }

            packageService.create(newPackage);
            packageList.add(newPackage);
            packageAdapter.notifyDataSetChanged();

            popupWindow.dismiss();

        });

        productButton.setOnClickListener(v -> {
            AddProductsPopup(newPackage, eventType, priceText, suncategorytext);
        });
        serviceButton.setOnClickListener(v -> {
            AddServicesPopup(newPackage, eventType, priceText, suncategorytext,resMonth,resday,cancMonth,cancDay);

        });

        cancelButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });
    }

    @SuppressLint({"NotifyDataSetChanged"})
    private void AddProductsPopup(Package newPackage, TextView eventType, TextView priceText, TextView subcategoryText) {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.event_type_helper_layout, null);
        List<Product> categoryProducts = new ArrayList<>();
        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        // dodavanje checkboxova
        LinearLayout checkboxContainer = popupView.findViewById(R.id.checkbox_container);
        productService.getUndeletedOfPupByCategory(companyId, selectedCategoryNewProduct.getId(), new ProductService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Product> objects) {
                categoryProducts.clear();
                categoryProducts.addAll(objects);
                //dodavanje checkboxova hehe
                for (Product et : objects) {
                    CheckBox checkBox = new CheckBox(PackageManagementActivity.this);
                    checkBox.setText(et.getName());
                    checkBox.setTag(et.getId());
                    checkboxContainer.addView(checkBox);

                }
            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });
        Button saveButton = popupView.findViewById(R.id.button_save_event_type);

        saveButton.setOnClickListener(v -> {
            newPackage.setProducts(new ArrayList<>());
            newPackage.setProductIds(new ArrayList<>());
            newPackage.setProductsHashMap(new HashMap<>());
            for (int i = 0; i < checkboxContainer.getChildCount(); i++) {
                View childView = checkboxContainer.getChildAt(i);
                if (childView instanceof CheckBox) {
                    CheckBox checkBox = (CheckBox) childView;
                    if (checkBox.isChecked()) {
                        newPackage.getProducts().add(checkBox.getText().toString());
                        newPackage.getProductIds().add(checkBox.getTag().toString());
                        newPackage.getProductsHashMap().put(checkBox.getTag().toString(), findProduct(categoryProducts, checkBox.getTag().toString()));
                    }
                }
            }

            //UPDATING eventtye, categories and price
            setEventType(newPackage, eventType);
            setPrice(newPackage, priceText);
            setSubcategories(newPackage, subcategoryText);

            popupWindow.dismiss();

        });


    }

    @SuppressLint({"NotifyDataSetChanged"})
    private void AddServicesPopup(Package newPackage, TextView eventType, TextView priceText, TextView subcategoryText,TextView resMonth, TextView resDay, TextView cancMonth, TextView cancDay) {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.event_type_helper_layout, null);
        List<Service> categoryServices = new ArrayList<>();
        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        // dodavanje checkboxova
        LinearLayout checkboxContainer = popupView.findViewById(R.id.checkbox_container);
        serviceService.getUndeletedOfPUPByCategory(companyId, selectedCategoryNewProduct.getId(), new ServiceService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Service> objects) {
                categoryServices.clear();
                categoryServices.addAll(objects);
                //dodavanje checkboxova hehe
                for (Service et : objects) {
                    CheckBox checkBox = new CheckBox(PackageManagementActivity.this);
                    checkBox.setText(et.getName());
                    checkBox.setTag(et.getId());
                    checkboxContainer.addView(checkBox);

                }
            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });
        Button saveButton = popupView.findViewById(R.id.button_save_event_type);

        saveButton.setOnClickListener(v -> {
            newPackage.setServices(new ArrayList<>());
            newPackage.setServiceIds(new ArrayList<>());
            newPackage.setServicesHashMap(new HashMap<>());
            for (int i = 0; i < checkboxContainer.getChildCount(); i++) {
                View childView = checkboxContainer.getChildAt(i);
                if (childView instanceof CheckBox) {
                    CheckBox checkBox = (CheckBox) childView;
                    if (checkBox.isChecked()) {
                        newPackage.getServices().add(checkBox.getText().toString());
                        newPackage.getServiceIds().add(checkBox.getTag().toString());
                        newPackage.getServicesHashMap().put(checkBox.getTag().toString(), findService(categoryServices, checkBox.getTag().toString()));
                    }
                }
            }

            //UPDATING eventtye, categories and price
            setEventType(newPackage, eventType);
            handleLayoutVisibility(newPackage);
            setPrice(newPackage, priceText);
            setSubcategories(newPackage, subcategoryText);
            setReservationAndCancelation(newPackage,resMonth,resDay,cancMonth,cancDay);

            popupWindow.dismiss();

        });


    }

    private void initializeRecyclerView() {
        recyclerView = findViewById(R.id.package_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        packageService.getUndeletedOfPUP(companyId, new PackageService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Package> events) {
                packageList = new ArrayList<>(events);
                packageAdapter = new PackageAdapter(PackageManagementActivity.this, packageList, userRole);
                recyclerView.setAdapter(packageAdapter);
            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });

    }

    private void setDataToCategorySpinner(Spinner categorySpinner) {
        CategoryService categoryService = new CategoryService();
        categoryService.getAll().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<Category> categories = task.getResult();
                CategorySpinnerAdapter adapter = new CategorySpinnerAdapter(this, categories);
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                categorySpinner.setAdapter(adapter);


            } else {

                Log.e("PackageManagementActivity", "Error getting categories", task.getException());
            }
        });
    }

    private Product findProduct(List<Product> products, String productId) {
        for (Product p : products) {
            if (p.getId().equals(productId)) {
                return p;
            }
        }
        return null;
    }

    private Service findService(List<Service> products, String productId) {
        for (Service p : products) {
            if (p.getId().equals(productId)) {
                return p;
            }
        }
        return null;
    }

    private void setEventType(Package newPackage, TextView eventType) {
        newPackage.setEventTypes("");
        StringBuilder sb = new StringBuilder();
        if (newPackage.getProductsHashMap() != null) {
            newPackage.getProductsHashMap().forEach((key, value) -> {
                sb.append(value.getEventType()).append(" ");
            });
        }

        if (newPackage.getServicesHashMap() != null) {
            newPackage.getServicesHashMap().forEach((key, value) -> {
                sb.append(value.getEventType()).append(" ");
            });
        }

        newPackage.setEventTypes(sb.toString());
        eventType.setText(sb.toString());

    }

    private void handleLayoutVisibility(Package newPackage) {
        if (newPackage.getServiceIds().isEmpty()) {
            layoutPackage.setVisibility(View.GONE);
            visibleAutomaticConf = false;
            newPackage.getServicesHashMap().forEach((key, value) -> {
                if (value.isAppointment()) {
                    visibleAutomaticConf = true;
                }
            });
            if (visibleAutomaticConf) {
                automaticConfirmationRB.setVisibility(View.VISIBLE);

            }
        } else {
            layoutPackage.setVisibility(View.VISIBLE);
        }


    }

    private void setPrice(Package newPackage, TextView price) {
        newPackage.setPrice(0);
        packagePrice = 0;
        if (newPackage.getProductsHashMap() != null) {
            newPackage.getProductsHashMap().forEach((key, value) -> {
                packagePrice += (value.getPrice() - ( value.getPrice() * value.getDiscount() / 100));
            });
        }

        if (newPackage.getServicesHashMap() != null) {
            newPackage.getServicesHashMap().forEach((key, value) -> {
                if(value.isAppointment()){
                    double duration = Double.parseDouble(value.getAppointmentDuration());
                    packagePrice += (value.getPrice() - (value.getPrice() * value.getDiscount() / 100))*duration;
                }else{
                    double duration = Double.parseDouble(value.getMinDuration());
                    packagePrice += (value.getPrice() - (value.getPrice() * value.getDiscount() / 100))*duration;
                }

            });
        }
        newPackage.setPrice(packagePrice);
        price.setText(String.valueOf(packagePrice));

    }

    private void setSubcategories(Package newPackage, TextView subcategories) {
        newPackage.setSubcategories(new ArrayList<>());

        if (newPackage.getProductsHashMap() != null) {
            newPackage.getProductsHashMap().forEach((key, value) -> {
                if (!containsSubcategory(newPackage.getSubcategories(), value.getSubCathegory())) {
                    newPackage.getSubcategories().add(value.getSubCathegory());
                }
            });
        }

        if (newPackage.getServicesHashMap() != null) {
            newPackage.getServicesHashMap().forEach((key, value) -> {
                if (!containsSubcategory(newPackage.getSubcategories(), value.getSubCategory())) {
                    newPackage.getSubcategories().add(value.getSubCategory());
                }
            });
        }


        subcategories.setText(newPackage.getSubcategoriesString());

    }

    private void setReservationAndCancelation(Package newPackage, TextView resMonts, TextView resDay, TextView cancelMonth, TextView cancelDay) {


        if (newPackage.getServicesHashMap() != null) {
            newPackage.setReservationDeadline(new com.google.firebase.Timestamp(0, 0));
            newPackage.setCancellationDeadline(new com.google.firebase.Timestamp(0, 0));
            smallestTimestamp = null;
            newPackage.getServicesHashMap().forEach((key, value) -> {
                com.google.firebase.Timestamp firestoreTimestamp = value.getReservationDeadline(); // Firestore Timestamp
                LocalDate localDate = firestoreTimestamp.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(); // Convert Timestamp to LocalDate
                if (smallestTimestamp == null || (localDate.getMonthValue() < smallestTimestamp.getMonthValue()) ||
                        (localDate.getMonthValue() == smallestTimestamp.getMonthValue() && localDate.getDayOfMonth() < smallestTimestamp.getDayOfMonth())) {
                    smallestTimestamp = localDate;
                }
            });

            if (smallestTimestamp != null) {

                resDay.setText(smallestTimestamp.getDayOfMonth() + "d");
                resMonts.setText((smallestTimestamp.getMonthValue())%12 + "m");
                Date date = Date.from(smallestTimestamp.atStartOfDay(ZoneId.systemDefault()).toInstant());
                com.google.firebase.Timestamp firestoreTimestamp = new Timestamp(date);
                newPackage.setReservationDeadline(firestoreTimestamp);
            }

            smallestTimestamp1 = null;
            newPackage.getServicesHashMap().forEach((key, value) -> {
                com.google.firebase.Timestamp firestoreTimestamp = value.getCancelationDeadline(); // Firestore Timestamp
                LocalDate localDate = firestoreTimestamp.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(); // Convert Timestamp to LocalDate
                if (smallestTimestamp1 == null || (localDate.getMonthValue() < smallestTimestamp1.getMonthValue()) ||
                        (localDate.getMonthValue() == smallestTimestamp1.getMonthValue() && localDate.getDayOfMonth() < smallestTimestamp.getDayOfMonth())) {
                    smallestTimestamp1 = localDate;
                }
            });
            if (smallestTimestamp1 != null) {

                cancelDay.setText(smallestTimestamp1.getDayOfMonth() + "d");
                cancelMonth.setText((smallestTimestamp1.getMonthValue())%12 + "m");
                Date date = Date.from(smallestTimestamp1.atStartOfDay(ZoneId.systemDefault()).toInstant());
                com.google.firebase.Timestamp firestoreTimestamp = new Timestamp(date);
                newPackage.setCancellationDeadline(firestoreTimestamp);
            }

        }


    }

    private boolean containsSubcategory(List<String> subcategories, String subcateString) {
        for (String d : subcategories) {
            if (d.equals(subcateString)) {
                return true;
            }
        }
        return false;

    }

    private void handleSearch(String name){
        List<Package> searchResult = new ArrayList<>();
        packageService.getUndeletedOfPUP(companyId ,new PackageService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Package> objects) {
                packageList.clear();

                if(name.isEmpty() || name.equals(" ")){
                    packageList.addAll(objects);
                    packageAdapter.notifyDataSetChanged();
                }else{
                    searchResult.addAll(objects);
                    for(Package p : searchResult){
                        if(p.getName().contains(name) || serviceNamesSearch(p.getServicesHashMap(),name) || productNameSearch(p.getProductsHashMap(),name)) {
                            packageList.add(p);
                        }
                    }
                    packageAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });


    }

    private boolean serviceNamesSearch(HashMap<String,Service> serviceList, String name){
        serviceSearch = false;
        if(serviceList != null){
            serviceList.forEach((key,value)->{
                if(value.getName().contains(name)){
                    serviceSearch = true;
                }
            });
        }
        return serviceSearch;
    }

    private boolean productNameSearch(HashMap<String,Product> serviceList, String name){
        productSearch = false;
        if(serviceList != null){
            serviceList.forEach((key,value)->{
                if(value.getName().contains(name)){
                    productSearch = true;
                }
            });
        }
        return productSearch;
    }

}


