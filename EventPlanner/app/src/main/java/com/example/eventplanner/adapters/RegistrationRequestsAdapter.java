package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Company;
import com.example.eventplanner.models.User;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.services.CompanyService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RegistrationRequestsAdapter extends RecyclerView.Adapter<RegistrationRequestsAdapter.ViewHolder> implements Filterable {

    private final List<User> users;
    private final List<User> usersFull;
    private final OnRequestActionListener listener;
    private final Map<String, String> userCompanyMap;
    private final CompanyService companyService;
    private Category selectedCategory;

    private final Context context;

    public RegistrationRequestsAdapter(Context context, List<User> users, OnRequestActionListener listener, Map<String, String> userCompanyMap) {
        this.context = context;
        this.users = users;
        this.usersFull = new ArrayList<>(users);
        this.listener = listener;
        this.userCompanyMap = userCompanyMap;
        companyService = new CompanyService();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_registration_request, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = users.get(position);
        String companyName = userCompanyMap.get(user.getId());

        holder.textViewFirstName.setText(user.getFirstName());
        holder.textViewLastName.setText(user.getLastName());
        holder.textViewEmail.setText(user.getEmail());

        if (companyName != null) {
            holder.textViewCompanyName.setText(companyName);
            companyService.getByName(companyName).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Company company = task.getResult();
                    if (company != null) {
                        holder.textViewCompanyEmail.setText(company.getEmail());
                    } else {
                        holder.textViewCompanyEmail.setText("N/A");
                    }
                } else {
                    holder.textViewCompanyEmail.setText("N/A");
                }
            });
        } else {
            holder.textViewCompanyName.setText("N/A");
            holder.textViewCompanyEmail.setText("N/A");
        }

        holder.buttonAccept.setOnClickListener(v -> listener.onAccept(user));
        holder.buttonReject.setOnClickListener(v -> showRejectionDialog(user));
    }
    private void showRejectionDialog(User user) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Reason for Rejection");

        // Set up the input
        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", (dialog, which) -> {
            String reason = input.getText().toString();
            if (!TextUtils.isEmpty(reason)) {
                listener.onReject(user, reason);
            } else {
                // Handle empty reason
                Toast.makeText(context, "Please enter a reason for rejection", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        builder.show();
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public void filterByCategory(Category category) {
        this.selectedCategory = category;
        getFilter().filter("");
    }

    private final Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<User> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(usersFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (User user : usersFull) {
                    String companyName = userCompanyMap.get(user.getId());
                    if (user.getFirstName().toLowerCase().contains(filterPattern) ||
                            user.getLastName().toLowerCase().contains(filterPattern) ||
                            user.getEmail().toLowerCase().contains(filterPattern) ||
                            (companyName != null && companyName.toLowerCase().contains(filterPattern))
                    ) {
                        filteredList.add(user);
                    }
                }
            }

            // Filter by category if one is selected
            if (selectedCategory != null) {
                List<User> categoryFilteredList = new ArrayList<>();
                for (User user : filteredList) {
                    String companyName = userCompanyMap.get(user.getId());
                    if (companyName != null) {
                        companyService.getByName(companyName).addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Company company = task.getResult();
                                if (company != null && company.getCategories() != null && company.getCategories().contains(selectedCategory)) {
                                    categoryFilteredList.add(user);
                                }
                            }
                        });
                    }
                }
                filteredList = categoryFilteredList;
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @SuppressLint("NotifyDataSetChanged")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            users.clear();
            users.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewFirstName;
        public TextView textViewLastName;
        public TextView textViewEmail;
        public TextView textViewCompanyName;
        public TextView textViewCompanyEmail;
        public Button buttonAccept;
        public Button buttonReject;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewFirstName = itemView.findViewById(R.id.textViewFirstName);
            textViewLastName = itemView.findViewById(R.id.textViewLastName);
            textViewEmail = itemView.findViewById(R.id.textViewEmail);
            textViewCompanyName = itemView.findViewById(R.id.textViewCompanyName);
            textViewCompanyEmail = itemView.findViewById(R.id.textViewCompanyEmail);
            buttonAccept = itemView.findViewById(R.id.buttonAccept);
            buttonReject = itemView.findViewById(R.id.buttonReject);
        }
    }

    private boolean isAscendingOrder = true;
    @SuppressLint("NotifyDataSetChanged")
    public void filterByDate() {
        isAscendingOrder = !isAscendingOrder;

        // Sort the list by createdAt attribute
        users.sort((u1, u2) -> {
            if (isAscendingOrder) {
                return u1.getCreatedAt().compareTo(u2.getCreatedAt());
            } else {
                return u2.getCreatedAt().compareTo(u1.getCreatedAt());
            }
        });
        notifyDataSetChanged();
    }

    public interface OnRequestActionListener {
        void onAccept(User user);
        void onReject(User user, String reason);
    }
}