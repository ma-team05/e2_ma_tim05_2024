package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.ChatMessage;
import com.example.eventplanner.models.Company;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.EventStatus;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.Notification;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.services.ChatService;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.OwnerService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class StartChatOwnerActivity extends AppCompatActivity {

    private String email;
    private String recepientEmail;
    private Owner owner;

    private OwnerService ownerService = new OwnerService();
    private ChatService chatService = new ChatService();
    private NotificationService notificationService = new NotificationService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_start_chat_owner);

        Intent intent = getIntent();
        email = intent.getStringExtra("EMAIL");
        String companyId = intent.getStringExtra("COMPANY");

        ownerService.getByComopanyId(companyId).addOnCompleteListener(ownerTask -> {
            if (ownerTask.isSuccessful()) {
                owner = ownerTask.getResult();
                bind();
            }
        });

        Button createButton = findViewById(R.id.createButton);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields();
            }
        });
    }

    private void bind() {
        TextView recepient;

        recepient = findViewById(R.id.recepient);

        recepient.setText("To: " + owner.getEmail());
    }

    private boolean validateFields() {
        boolean isValid = true;

        EditText messageEditText;
        String message;

        messageEditText = findViewById(R.id.message);
        message = messageEditText.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            messageEditText.setError("Please enter a message");
            messageEditText.requestFocus();
            isValid = false;
        }

        if(isValid) {
            startChat();
        }

        return isValid;
    }

    private void startChat() {
        EditText messageEditText;
        String message;

        messageEditText = findViewById(R.id.message);
        message = messageEditText.getText().toString().trim();

        ChatMessage chatMessage = new ChatMessage();

        chatMessage.setFrom(email);
        chatMessage.setTo(owner.getEmail());
        chatMessage.setMessage(message);
        chatMessage.setDate(new Date());
        chatMessage.setReadStatus(false);

        chatService.start(chatMessage);

        Notification notification = new Notification();
        notification.setUserId(recepientEmail);
        notification.setDescription("You received a message from: " + email);
        notification.setRead(false);
        notification.setSent(true);
        notification.setType(NotificationType.CHAT);

        notificationService.add(owner.getId(), "You received a message from: " + email, NotificationType.CHAT);

        String eventDetails = "Message sent:\n";
        Toast.makeText(this, eventDetails, Toast.LENGTH_LONG).show();

        finish();
    }
}