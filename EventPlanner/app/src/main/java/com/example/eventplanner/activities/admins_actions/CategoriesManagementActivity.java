package com.example.eventplanner.activities.admins_actions;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CategoryAdapter;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.services.CategoryService;

import java.util.ArrayList;
import java.util.List;

public class CategoriesManagementActivity extends AppCompatActivity implements CategoryAdapter.OnCategoryEditListener {

    private List<Category> categories;
    private CategoryAdapter adapter;
    private CategoryService categoryService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories_management);

        RecyclerView recyclerView = findViewById(R.id.recyclerViewCategories);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        categoryService = new CategoryService();

        categories = new ArrayList<>();
        adapter = new CategoryAdapter(this, categories);
        adapter.setOnCategoryEditListener(this);
        recyclerView.setAdapter(adapter);

        Button addButton = findViewById(R.id.buttonAddCategory);
        addButton.setOnClickListener(v -> showCategoryPopup(null));

        fetchCategories();
    }

    @Override
    public void onCategoryEdit(Category category) {
        showCategoryPopup(category);
    }
    
    private void showCategoryPopup(Category category) {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.popup_add_category, null);

        PopupWindow popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.showAtLocation(popupView, 0, 0, 0);

        EditText categoryNameEditText = popupView.findViewById(R.id.editTextCategoryName);
        EditText categoryDescriptionEditText = popupView.findViewById(R.id.editTextCategoryDescription);
        Button saveButton = popupView.findViewById(R.id.buttonSave);

        if (category != null) {
            categoryNameEditText.setText(category.getName());
            categoryDescriptionEditText.setText(category.getDescription());
        }

        saveButtonHandler(category, popupWindow, categoryNameEditText, categoryDescriptionEditText, saveButton);
    }
    @SuppressLint("NotifyDataSetChanged")
    private void saveButtonHandler(Category category, PopupWindow popupWindow, EditText categoryNameEditText, EditText categoryDescriptionEditText, Button saveButton) {
        saveButton.setOnClickListener(v -> {
            String categoryName = categoryNameEditText.getText().toString().trim();
            String categoryDescription = categoryDescriptionEditText.getText().toString().trim();

            if (!categoryName.isEmpty() && !categoryDescription.isEmpty()) {
                if (category != null) {
                    // Editing existing category
                    category.setName(categoryName);
                    category.setDescription(categoryDescription);
                    categoryService.update(category);
                } else {
                    // Adding new category
                    Category newCategory = new Category(categoryName, categoryDescription);
                    categoryService.create(newCategory);
                    categories.add(newCategory);
                }

                adapter.notifyDataSetChanged();
                popupWindow.dismiss();
            } else {
                Toast.makeText(this, "Please fill both fields", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    private void fetchCategories() {
        categoryService.getAll()
                .addOnSuccessListener(categories -> {
                    this.categories.clear();
                    this.categories.addAll(categories);
                    adapter.notifyDataSetChanged();
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(this, "Failed to fetch categories", Toast.LENGTH_SHORT).show();
                });
    }
}