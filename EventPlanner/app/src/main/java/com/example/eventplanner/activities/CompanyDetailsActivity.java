package com.example.eventplanner.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Company;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.Report;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.ReportService;
import com.example.eventplanner.services.UserService;
import com.google.firebase.Timestamp;

import java.util.List;

public class CompanyDetailsActivity extends AppCompatActivity {
    private User user;
    private Company company;
    private UserService userService = new UserService();
    private CompanyService companyService = new CompanyService();
    private ReportService reportService = new ReportService();
    private OwnerService ownerService = new OwnerService();
    private NotificationService notificationService = new NotificationService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_company_details);

        Intent intent = getIntent();
        String email = intent.getStringExtra("EMAIL");
        String companyId = intent.getStringExtra("COMPANY");
        userService.getByEmail(email).addOnCompleteListener(this, task->{
            if(task.isSuccessful()){
                user = task.getResult();
            }

            companyService.get(companyId).addOnCompleteListener(companyTask -> {
                if (companyTask.isSuccessful()) {
                    company = companyTask.getResult();
                    bind();
                }
            });
        });


        Button contactButton = findViewById(R.id.contact);

        contactButton.setOnClickListener(v -> {
            Intent createIntent = new Intent(this, StartChatOwnerActivity.class);
            createIntent.putExtra("EMAIL", email);
            createIntent.putExtra("COMPANY", companyId);
            this.startActivity(createIntent);
        });

        Button reportButton = findViewById(R.id.report);
        reportButton.setOnClickListener(v ->{
            showReportPopup();
        });
    }

    private void bind() {
        TextView name, description, type, email, phone, categories, eventTypes, address;

        name = findViewById(R.id.name);
        description = findViewById(R.id.description);
        type = findViewById(R.id.type);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        categories = findViewById(R.id.categories);
        eventTypes = findViewById(R.id.eventTypes);
        address = findViewById(R.id.address);

        name.setText("Name: " + company.getName());
        description.setText("Description: " + company.getDescription());
        type.setText("Type: " + company.getCompanyType().toLowerCase());
        email.setText("Email: " + company.getEmail());
        phone.setText("Phone number: " + company.getPhoneNumber());
        address.setText("Address: " + company.getAddress());

        String categoriesString = "";
        for(Category category : company.getCategories()) {
            categoriesString += ", " + category.getName();
        }
        if(!categoriesString.isEmpty()) {
            categoriesString = categoriesString.substring(2);
        }
        categories.setText("Categories: " + categoriesString);

        String eventTypesString = "";
        for(EventType eventType : company.getEventTypes()) {
            eventTypesString += ", " + eventType.getName();
        }
        if(!eventTypesString.isEmpty()) {
            eventTypesString = eventTypesString.substring(2);
        }
        eventTypes.setText("Event types: " + eventTypesString);
    }

    private void showReportPopup(){
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.report_form, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;

        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        EditText reason = popupView.findViewById(R.id.report_reason);

        //show window
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.report_save_btn);
        Button cancelButton = popupView.findViewById(R.id.report_cancel_btn);

        saveButton.setOnClickListener(v -> {
            // getting owner of the company

            ownerService.getAll().addOnCompleteListener(employeeTask -> {
                        if (employeeTask.isSuccessful()) {
                            Owner owner = employeeTask.getResult().stream().filter(o -> o.getCompanyId().equals(company.getId())).findFirst().orElse(null);

                            Report report  = new Report();
                            report.setReason(reason.getText().toString());
                            report.setReportedId(owner.getId());
                            report.setReporterId(user.getId());
                            report.setReportedUsername(owner.getEmail());
                            report.setReporterUsername(user.getEmail());
                            report.setReportTimestamp(Timestamp.now());
                            report.setForCompany(true);
                            report.setReportType(Report.REPORT_TYPE.Reported);

                            reportService.getExisting(report.getReporterId(), report.getReportedId(), new ReportService.OnDataFetchListener() {
                                @Override
                                public void onSuccess(List<Report> reports) {
                                    if(reports.isEmpty()){
                                        reportService.create(report);
                                        sendNotification(report.getReporterUsername(), report.getReportedUsername());
                                        popupWindow.dismiss();
                                    }else{
                                        Toast.makeText(CompanyDetailsActivity.this, "You have already reported this user.", Toast.LENGTH_SHORT).show();
                                        popupWindow.dismiss();
                                    }
                                }

                                @Override
                                public void onFailure(String errorMessage) {
                                    Toast.makeText(CompanyDetailsActivity.this, "Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                                    popupWindow.dismiss();
                                }
                            });
                        }
            });




        });

        cancelButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });
    }

    private void sendNotification(String reporterUsername, String reportedUsername){
        StringBuilder sb = new StringBuilder();
        sb.append(reporterUsername).append(" has just reported ").append(reportedUsername).append(".");
        notificationService.add(null, sb.toString(), NotificationType.ADMIN);
    }
}