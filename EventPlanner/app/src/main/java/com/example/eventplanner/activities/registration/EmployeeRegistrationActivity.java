package com.example.eventplanner.activities.registration;

import android.content.Intent;
import android.graphics.Bitmap;

import com.example.eventplanner.models.Company;
import com.example.eventplanner.services.CompanyService;
import com.google.firebase.auth.FirebaseAuth;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.TimeSchedule;
import com.example.eventplanner.models.User;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.UserService;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;

public class EmployeeRegistrationActivity extends AppCompatActivity {

    private Company company;
    private String companyId;
    private CompanyService companyService = new CompanyService();
    private EmployeeService service;
    private Employee newEmployee;
    private EditText editTextEmail, editTextPassword, editTextConfirmPassword,
            editTextFirstName, editTextLastName, editTextAddress, editTextPhoneNumber;
    private static final int PICK_IMAGE_REQUEST = 1;
    private ImageView imageViewProfile;
    private Button buttonSelectImage;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        service = new EmployeeService(new EmployeeRepository());
        setContentView(R.layout.activity_employee_registration);
        Intent intent = getIntent();
        companyId = intent.getStringExtra("companyId");


        editTextEmail = findViewById(R.id.editTextEmail1);
        editTextPassword = findViewById(R.id.editTextPassword1);
        editTextConfirmPassword = findViewById(R.id.editTextConfirmPassword1);
        editTextFirstName = findViewById(R.id.editTextFirstName1);
        editTextLastName = findViewById(R.id.editTextLastName1);
        editTextAddress = findViewById(R.id.editTextAddress1);
        editTextPhoneNumber = findViewById(R.id.editTextPhoneNumber1);

        // Initialize ImageView and Button
        imageViewProfile = findViewById(R.id.imageProfilePicture);
        buttonSelectImage = findViewById(R.id.buttonProfileImage);
        registerButton = findViewById(R.id.buttonRegister);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                register();
            }
        });
        buttonSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });
        companyService.get(companyId).addOnCompleteListener(task->{
           if(task.isSuccessful()){
               company = task.getResult();
               if(company.getTimeSchedule()!=null) {
                   setWorkingHours(R.id.timeRangePicker1, 0);
                   setWorkingHours(R.id.timeRangePicker2, 1);
                   setWorkingHours(R.id.timeRangePicker3, 2);
                   setWorkingHours(R.id.timeRangePicker4, 3);
                   setWorkingHours(R.id.timeRangePicker5, 4);
               }
           }
           else{
               Exception e = task.getException();
               e.printStackTrace();
           }
        });
    }

    private void register(){
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String confirmPassword = editTextConfirmPassword.getText().toString().trim();
        String firstName = editTextFirstName.getText().toString().trim();
        String lastName = editTextLastName.getText().toString().trim();
        String address = editTextAddress.getText().toString().trim();
        String phoneNumber = editTextPhoneNumber.getText().toString().trim();
        if (email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() ||
                firstName.isEmpty() || lastName.isEmpty() || address.isEmpty() || phoneNumber.isEmpty()) {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!isValidEmail(email)) {
            Toast.makeText(this, "Please enter a valid email address", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!password.equals(confirmPassword)) {
            editTextPassword.setTextColor(ContextCompat.getColor(this, R.color.error_color));
            editTextConfirmPassword.setTextColor(ContextCompat.getColor(this, R.color.error_color));
            Toast.makeText(this, "Passwords do not match", Toast.LENGTH_SHORT).show();
            return;
        } else {
            editTextPassword.setTextColor(ContextCompat.getColor(this, android.R.color.black));
            editTextConfirmPassword.setTextColor(ContextCompat.getColor(this, android.R.color.black));
        }
        TimeSchedule schedule = isWorkingHoursValid();
        if(schedule==null)
            Toast.makeText(this, "Invalid working hours", Toast.LENGTH_SHORT).show();
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        sendPasswordResetEmail();
                        User user = new User(firstName, lastName, email, phoneNumber, address, "", UserRole.EMPLOYEE, password);
                        UserService userService = new UserService();
                        userService.save(user).addOnCompleteListener(task2->{
                            if(task2.isSuccessful()){
                                String id = task2.getResult();
                                newEmployee = new Employee(id, firstName, lastName, email, phoneNumber, address, imageViewProfile.toString(), password, schedule, companyId);
                                service.create(newEmployee, id);
                                Toast.makeText(this, "Employee registered successfully. Check your email for verification.", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });

                    } else {
                        Exception exception = task.getException();
                        if (exception != null) {
                            Log.e("RegistrationError", "Registration failed: " + exception.getMessage());
                        }
                        Toast.makeText(this, "Registration failed. Please try again.", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    // Method to open gallery and select an image
    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    // Method to handle the result of image selection
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri imageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                imageViewProfile.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Method to set 24-hour format for TimePicker in the specified layout


    private void setWorkingHours(int timeRangePickerId, int dayOfTheWeek){
        TimePicker start = findViewById(timeRangePickerId).findViewById(R.id.startTimePicker);
        start.setIs24HourView(true);
        TimePicker end = findViewById(timeRangePickerId).findViewById(R.id.endTimePicker);
        end.setIs24HourView(true);
        switch(dayOfTheWeek){
            case 0:{
                start.setHour(Integer.parseInt(company.getTimeSchedule().mondayStart.split(":")[0]));
                start.setMinute(Integer.parseInt(company.getTimeSchedule().mondayStart.split(":")[1]));
                end.setHour(Integer.parseInt(company.getTimeSchedule().mondayFinish.split(":")[0]));
                end.setMinute(Integer.parseInt(company.getTimeSchedule().mondayFinish.split(":")[1]));
                break;
            }
            case 1:{
                start.setHour(Integer.parseInt(company.getTimeSchedule().tuesdayStart.split(":")[0]));
                start.setMinute(Integer.parseInt(company.getTimeSchedule().tuesdayStart.split(":")[1]));
                end.setHour(Integer.parseInt(company.getTimeSchedule().tuesdayFinish.split(":")[0]));
                end.setMinute(Integer.parseInt(company.getTimeSchedule().tuesdayFinish.split(":")[1]));
                break;
            }
            case 2:{
                start.setHour(Integer.parseInt(company.getTimeSchedule().wednesdayStart.split(":")[0]));
                start.setMinute(Integer.parseInt(company.getTimeSchedule().wednesdayStart.split(":")[1]));
                end.setHour(Integer.parseInt(company.getTimeSchedule().wednesdayFinish.split(":")[0]));
                end.setMinute(Integer.parseInt(company.getTimeSchedule().wednesdayFinish.split(":")[1]));
                break;
            }
            case 3:{
                start.setHour(Integer.parseInt(company.getTimeSchedule().thursdayStart.split(":")[0]));
                start.setMinute(Integer.parseInt(company.getTimeSchedule().thursdayStart.split(":")[1]));
                end.setHour(Integer.parseInt(company.getTimeSchedule().thursdayFinish.split(":")[0]));
                end.setMinute(Integer.parseInt(company.getTimeSchedule().thursdayFinish.split(":")[1]));
                break;
            }
            case 4:{
                start.setHour(Integer.parseInt(company.getTimeSchedule().fridayStart.split(":")[0]));
                start.setMinute(Integer.parseInt(company.getTimeSchedule().fridayStart.split(":")[1]));
                end.setHour(Integer.parseInt(company.getTimeSchedule().fridayFinish.split(":")[0]));
                end.setMinute(Integer.parseInt(company.getTimeSchedule().fridayFinish.split(":")[1]));
                break;
            }
            default:break;
        }
    }

    private TimeSchedule isWorkingHoursValid(){
        TimePicker start, end;
        TimeSchedule schedule = new TimeSchedule();
        start = findViewById(R.id.timeRangePicker1).findViewById(R.id.startTimePicker);
        end = findViewById(R.id.timeRangePicker1).findViewById(R.id.endTimePicker);
        schedule.setStartTime("mondayStart", String.valueOf(start.getHour())+" : "+String.valueOf(start.getMinute()));
        schedule.setFinishTime("mondayFinish", String.valueOf(end.getHour())+" : "+String.valueOf(end.getMinute()));
        if (end.getHour() < start.getHour() || (end.getHour() == start.getHour() && end.getMinute() < start.getMinute()))
            return null;
        start = findViewById(R.id.timeRangePicker2).findViewById(R.id.startTimePicker);
        end = findViewById(R.id.timeRangePicker2).findViewById(R.id.endTimePicker);
        schedule.setStartTime("tuesdayStart", String.valueOf(start.getHour())+" : "+String.valueOf(start.getMinute()));
        schedule.setFinishTime("tuesdayFinish", String.valueOf(end.getHour())+" : "+String.valueOf(end.getMinute()));
        if (end.getHour() < start.getHour() || (end.getHour() == start.getHour() && end.getMinute() < start.getMinute()))
            return null;
        start = findViewById(R.id.timeRangePicker3).findViewById(R.id.startTimePicker);
        end = findViewById(R.id.timeRangePicker3).findViewById(R.id.endTimePicker);
        schedule.setStartTime("wednesdayStart", String.valueOf(start.getHour())+" : "+String.valueOf(start.getMinute()));
        schedule.setFinishTime("wednesdayFinish", String.valueOf(end.getHour())+" : "+String.valueOf(end.getMinute()));
        if (end.getHour() < start.getHour() || (end.getHour() == start.getHour() && end.getMinute() < start.getMinute()))
            return null;
        start = findViewById(R.id.timeRangePicker4).findViewById(R.id.startTimePicker);
        end = findViewById(R.id.timeRangePicker4).findViewById(R.id.endTimePicker);
        schedule.setStartTime("thursdayStart", String.valueOf(start.getHour())+" : "+String.valueOf(start.getMinute()));
        schedule.setFinishTime("thursdayFinish", String.valueOf(end.getHour())+" : "+String.valueOf(end.getMinute()));
        if (end.getHour() < start.getHour() || (end.getHour() == start.getHour() && end.getMinute() < start.getMinute()))
            return null;
        start = findViewById(R.id.timeRangePicker5).findViewById(R.id.startTimePicker);
        end = findViewById(R.id.timeRangePicker5).findViewById(R.id.endTimePicker);
        schedule.setStartTime("fridayStart", String.valueOf(start.getHour())+" : "+String.valueOf(start.getMinute()));
        schedule.setFinishTime("fridayFinish", String.valueOf(end.getHour())+" : "+String.valueOf(end.getMinute()));
        if (end.getHour() < start.getHour() || (end.getHour() == start.getHour() && end.getMinute() < start.getMinute()))
            return null;
        return schedule;
    }
    private boolean isValidEmail(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        return email.matches(emailPattern);
    }
    private void sendPasswordResetEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            FirebaseAuth.getInstance().sendPasswordResetEmail(user.getEmail())
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Toast.makeText(this, "Password reset email sent. Please check your email to reset your password and verify your account.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, "Failed to send password reset email", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

}
