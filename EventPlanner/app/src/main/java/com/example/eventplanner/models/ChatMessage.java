package com.example.eventplanner.models;

import java.util.Date;

public class ChatMessage {
    private String from;
    private String to;
    private String message;
    private Date date;
    private boolean readStatus;

    public ChatMessage() {
    }

    public ChatMessage(String from, String to, String message, Date date, boolean readStatus) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
        this.readStatus = readStatus;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isReadStatus() {
        return readStatus;
    }

    public void setReadStatus(boolean readStatus) {
        this.readStatus = readStatus;
    }
}
