package com.example.eventplanner.models;

public class TimeSchedule {
    public String mondayStart = "", mondayFinish = "";
    public String tuesdayStart = "", tuesdayFinish = "";
    public String wednesdayStart = "", wednesdayFinish = "";
    public String thursdayStart = "", thursdayFinish = "";
    public String fridayStart = "", fridayFinish = "";
    public String saturdayStart = "", saturdayFinish = "";
    public String sundayStart = "", sundayFinish = "";
    public TimeSchedule(){}

    public String getStartTime(String tag) {
        switch (tag) {
            case "mondayStart":
                return mondayStart;
            case "tuesdayStart":
                return tuesdayStart;
            case "wednesdayStart":
                return wednesdayStart;
            case "thursdayStart":
                return thursdayStart;
            case "fridayStart":
                return fridayStart;
            case "saturdayStart":
                return saturdayStart;
            case "sundayStart":
                return sundayStart;
            default:
                return "";
        }
    }

    public String getFinishTime(String tag) {
        switch (tag) {
            case "mondayFinish":
                return mondayFinish;
            case "tuesdayFinish":
                return tuesdayFinish;
            case "wednesdayFinish":
                return wednesdayFinish;
            case "thursdayFinish":
                return thursdayFinish;
            case "fridayFinish":
                return fridayFinish;
            case "saturdayFinish":
                return saturdayFinish;
            case "sundayFinish":
                return sundayFinish;
            default:
                return "";
        }
    }

    public void setStartTime(String tag, String time) {
        switch (tag) {
            case "mondayStart":
                mondayStart = time;
                break;
            case "tuesdayStart":
                tuesdayStart = time;
                break;
            case "wednesdayStart":
                wednesdayStart = time;
                break;
            case "thursdayStart":
                thursdayStart = time;
                break;
            case "fridayStart":
                fridayStart = time;
                break;
            case "saturdayStart":
                saturdayStart = time;
                break;
            case "sundayStart":
                sundayStart = time;
                break;
        }
    }

    public void setFinishTime(String tag, String time) {
        switch (tag) {
            case "mondayFinish":
                mondayFinish = time;
                break;
            case "tuesdayFinish":
                tuesdayFinish = time;
                break;
            case "wednesdayFinish":
                wednesdayFinish = time;
                break;
            case "thursdayFinish":
                thursdayFinish = time;
                break;
            case "fridayFinish":
                fridayFinish = time;
                break;
            case "saturdayFinish":
                saturdayFinish = time;
                break;
            case "sundayFinish":
                sundayFinish = time;
                break;
        }
    }
}