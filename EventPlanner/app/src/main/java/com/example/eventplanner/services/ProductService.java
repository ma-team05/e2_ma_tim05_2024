package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.ProductRepository;
import com.example.eventplanner.models.Product;
import com.google.android.gms.tasks.Task;

import java.util.List;
import java.util.stream.Collectors;

public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository){
        this.productRepository = productRepository;
    }
    public ProductService() {
        productRepository = new ProductRepository();
    }
    public Task<Product> get(String id){ return productRepository.getById(id); }

    public interface OnDataFetchListener {
        void onSuccess(List<Product> objects);
        void onFailure(String errorMessage);
    }
    public void create(Product product){
        productRepository.create(product);
    }

    public void update(Product product){
        productRepository.update(product);
    }

    public void deleteLogically(Product product){
        productRepository.deleteLogucally(product);
    }

    public void getAll(final OnDataFetchListener listener){
        productRepository.getAll(new ProductRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Product> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void getUndeletedOfPUP(String userId,final OnDataFetchListener listener){
        productRepository.getUndeletedOfPUP(userId,new ProductRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Product> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void search(String name, String provider, double minPrice, double maxPrice, String city, double maxDistance, String eventType, String serviceCategory, String serviceSubcategory, boolean available,final OnDataFetchListener listener){
        productRepository.getAll(new ProductRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Product> events) {
                List<Product> searched = events;

                if(name != null && !name.isEmpty()) {
                    searched = events.stream().filter(e ->
                            e.getName().toLowerCase().contains(name.toLowerCase())).collect(Collectors.toList());
                }

                if(minPrice != -1) {
                    searched = searched.stream().filter(e ->
                            e.getPrice() >= minPrice).collect(Collectors.toList());
                }

                if(maxPrice != -1) {
                    searched = searched.stream().filter(e ->
                            e.getPrice() <= maxPrice).collect(Collectors.toList());
                }

                if(eventType != null && !eventType.isEmpty()) {
                    searched = searched.stream().filter(e ->
                            e.getEventType().toLowerCase().contains(eventType.toLowerCase())).collect(Collectors.toList());
                }

                if(serviceCategory != null && !serviceCategory.isEmpty()) {
                    searched = searched.stream().filter(e ->
                            e.getCathegory().toLowerCase().contains(serviceCategory.toLowerCase())).collect(Collectors.toList());
                }

                if(serviceSubcategory != null && !serviceSubcategory.isEmpty()) {

                    searched = searched.stream().filter(e ->
                            e.getSubCathegory().toLowerCase().contains(serviceSubcategory.toLowerCase())).collect(Collectors.toList());
                }

                if(available) {
                    searched = searched.stream().filter(e ->
                            e.isAvailable() == available).collect(Collectors.toList());
                }

                listener.onSuccess(searched);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void getUndeletedOfPupByCategory(String companyId, String categoryId, final OnDataFetchListener listener){
        productRepository.getUndeletedOdPupByCategory(companyId,categoryId,new ProductRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Product> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void updateBatch(List<Product> products){
        productRepository.updateBatch(products);
    }
}
