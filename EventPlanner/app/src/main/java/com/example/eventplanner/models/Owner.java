package com.example.eventplanner.models;

public class Owner extends User {
    private String companyId;
    public Owner(){super();}

    public Owner(String id, String firstName, String lastName, String email, String phone, String address, String picture, String password, String companyId) {
        super(id, firstName, lastName, email, phone, address, picture, UserRole.OWNER, password, true);
        this.companyId = companyId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}

