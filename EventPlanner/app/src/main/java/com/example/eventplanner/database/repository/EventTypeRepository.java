package com.example.eventplanner.database.repository;


import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.EventType;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EventTypeRepository {
    private final FirebaseFirestore db;
    private final String collection;

    public EventTypeRepository() {

        db = FirebaseFirestore.getInstance();
        collection = "event_types";
    }

    public Task<Void> update(EventType eventType) {
        return db.collection(collection).document(eventType.getId()).set(eventType);
    }

    public interface OnDataFetchListener {
        void onSuccess(List<EventType> eventTypes);
        void onFailure(String errorMessage);
    }

    public Task<String> save(EventType eventType) {
        DocumentReference newEventTypeRef = db.collection(collection).document();
        eventType.setId(newEventTypeRef.getId());
        return newEventTypeRef.set(eventType)
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        return newEventTypeRef.getId();
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }

    public void getAll(final EventTypeRepository.OnDataFetchListener listener) {
        db.collection(collection).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                List<EventType> objects = new ArrayList<>();
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    EventType eventType = documentSnapshot.toObject(EventType.class);
                    eventType.setId(documentSnapshot.getId());
                    objects.add(eventType);
                    //documentSnapshot.getReference().delete();
                }
                listener.onSuccess(objects);
            }
        });
    }

    public void deleteAll() {
        db.collection(collection).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    documentSnapshot.getReference().delete();
                }
            }
        });
    }
}
