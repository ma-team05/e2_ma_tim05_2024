package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.ServiceRepository;
import com.example.eventplanner.models.Service;
import com.google.android.gms.tasks.Task;

import java.util.List;
import java.util.stream.Collectors;

public class ServiceService {
    private final ServiceRepository serviceRepository;

    public ServiceService(ServiceRepository serviceRepository){
        this.serviceRepository = serviceRepository;
    }

    public ServiceService() {
        serviceRepository = new ServiceRepository();
    }

    public Task<Service> getService(String serviceId) {
        return serviceRepository.get(serviceId);
    }

    public interface OnDataFetchListener {
        void onSuccess(List<Service> objects);
        void onFailure(String errorMessage);
    }

    public void search(String name, String provider, double minPrice, double maxPrice, String city, double maxDistance, String eventType, String serviceCategory, String serviceSubcategory, boolean available,final ServiceService.OnDataFetchListener listener){
        serviceRepository.getAll(new ServiceRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Service> events) {
                List<Service> searched = events;

                if(name != null && !name.isEmpty()) {
                    searched = events.stream().filter(e ->
                            e.getName().toLowerCase().contains(name.toLowerCase())).collect(Collectors.toList());
                }

                if(minPrice != -1) {
                    searched = searched.stream().filter(e ->
                            e.getPrice() >= minPrice).collect(Collectors.toList());
                }

                if(maxPrice != -1) {
                    searched = searched.stream().filter(e ->
                            e.getPrice() <= maxPrice).collect(Collectors.toList());
                }

                if(eventType != null && !eventType.isEmpty()) {
                    searched = searched.stream().filter(e ->
                            e.getEventType().toLowerCase().contains(eventType.toLowerCase())).collect(Collectors.toList());
                }

                if(serviceCategory != null && !serviceCategory.isEmpty()) {
                    searched = searched.stream().filter(e ->
                            e.getCategory().toLowerCase().contains(serviceCategory.toLowerCase())).collect(Collectors.toList());
                }

                if(serviceSubcategory != null && !serviceSubcategory.isEmpty()) {

                    searched = searched.stream().filter(e ->
                            e.getSubCategory().toLowerCase().contains(serviceSubcategory.toLowerCase())).collect(Collectors.toList());
                }

                if(available) {
                    searched = searched.stream().filter(e ->
                            e.isAvailable() == available).collect(Collectors.toList());
                }

                listener.onSuccess(searched);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void create(Service service){
        serviceRepository.create(service);
    }

    public void update(Service service){
        serviceRepository.update(service);
    }

    public void deleteLogically(Service service){
        serviceRepository.deleteLogically(service);
    }
    public void getAll(final OnDataFetchListener listener){
        serviceRepository.getAll(new ServiceRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Service> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void getUndeletedOfPUP(String userId,final OnDataFetchListener listener){
        serviceRepository.getUndeletedOfPUP(userId,new ServiceRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Service> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void getUndeletedOfPUPByCategory(String companyId,String categoryId,final OnDataFetchListener listener){
        serviceRepository.getUndeletedOfPUPByCategory(companyId,categoryId,new ServiceRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Service> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void updateBatch(List<Service> services){
        serviceRepository.updateBatch(services);
    }
}
