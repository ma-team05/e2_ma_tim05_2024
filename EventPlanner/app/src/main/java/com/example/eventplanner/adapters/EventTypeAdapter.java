package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.services.EventTypeService;

import java.util.List;

public class EventTypeAdapter extends RecyclerView.Adapter<EventTypeAdapter.EventTypeViewHolder> {

    private static List<EventType> eventTypes;
    private static OnEventTypeEditListener editListener;

    public EventTypeAdapter(Context context, List<EventType> eventTypes) {
        EventTypeAdapter.eventTypes = eventTypes;
    }
    public interface OnEventTypeEditListener {
        void onEventTypeEdit(EventType eventType);
    }

    public void setOnEventTypeEditListener(OnEventTypeEditListener listener) {
        editListener = listener;
    }

    @NonNull
    @Override
    public EventTypeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_type, parent, false);
        return new EventTypeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventTypeViewHolder holder, int position) {
        EventType eventType = eventTypes.get(position);
        holder.bind(eventType);
        holder.editIcon.setOnClickListener(v -> {
            editEventType(eventType);
        });
    }
    private void editEventType(EventType eventType) {
        // Notify the listener when the edit button is clicked
        if (editListener != null) {
            editListener.onEventTypeEdit(eventType);
        }
    }
    @Override
    public int getItemCount() {
        return eventTypes != null ? eventTypes.size() : 0;
    }


    static class EventTypeViewHolder extends RecyclerView.ViewHolder {
        private final TextView textViewEventTypeName;
        private final TextView textViewEventTypeDescription;
        private final CheckBox checkBoxActivated;
        private final ImageView editIcon;

        public EventTypeViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewEventTypeName = itemView.findViewById(R.id.textViewEventTypeName);
            textViewEventTypeDescription = itemView.findViewById(R.id.textViewEventTypeDescription);
            checkBoxActivated = itemView.findViewById(R.id.checkBoxActivated);
            editIcon = itemView.findViewById(R.id.editIcon);

            editIcon.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    EventType eventType = eventTypes.get(position);
                    if (editListener != null) {
                        editListener.onEventTypeEdit(eventType);
                    }
                }
            });
        }

        public void bind(EventType eventType) {
            textViewEventTypeName.setText(eventType.getName());
            textViewEventTypeDescription.setText(eventType.getDescription());
            checkBoxActivated.setChecked(eventType.isActivated());

            // Update activation state of EventType when checkbox is clicked
            checkBoxActivated.setOnCheckedChangeListener((buttonView, isChecked) -> {
                eventType.setActivated(isChecked);
                EventTypeService eventTypeService = new EventTypeService();
                eventTypeService.update(eventType);
            });
        }
    }
}
