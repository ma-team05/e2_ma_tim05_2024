package com.example.eventplanner.activities.registration;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Company;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.TimeSchedule;
import com.example.eventplanner.models.User;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.UserService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class OwnersCompanyRegistrationStep3Activity extends AppCompatActivity {

    private final TimeSchedule timeSchedule = new TimeSchedule();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owners_company_registration_step3);

        Button finishButton = findViewById(R.id.finishButton);
        finishButton.setOnClickListener(v -> onFinishButtonClick());
    }
    private void onFinishButtonClick() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String email = extras.getString("email");
            String firstName = extras.getString("firstName");
            String lastName = extras.getString("lastName");
            String address = extras.getString("address");
            String phoneNumber = extras.getString("phoneNumber");
            String password = extras.getString("password");
            String companyType = extras.getString("companyType");
            String companyEmail = extras.getString("companyEmail");
            String companyName = extras.getString("companyName");
            String companyAddress = extras.getString("companyAddress");
            String companyPhoneNumber = extras.getString("companyPhoneNumber");
            String companyDescription = extras.getString("companyDescription");

            // Create the company first
            Company company = new Company(companyName, companyEmail, companyAddress, companyPhoneNumber, companyDescription, companyType, timeSchedule);
            CompanyService companyService = new CompanyService();
            companyService.create(company)
                    .addOnCompleteListener(companyTask -> {
                        String companyId = companyTask.getResult();
                        if (companyTask.isSuccessful()) {
                            // Company created successfully, now create the user
                            assert email != null && password != null;
                            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                                    .addOnCompleteListener(userTask -> {
                                        if (userTask.isSuccessful()) {
                                            // Send verification email
                                            // sendVerificationEmail();

                                            // Registration successful
                                            User user = new User(firstName, lastName, email, phoneNumber, address, "", UserRole.OWNER, password);
                                            user.setActivated(false);
                                            UserService userService = new UserService();
                                            userService.save(user).addOnCompleteListener(task -> {
                                                String userId = task.getResult();

                                                // Create the owner and associate it with the company
                                                Owner owner = new Owner(userId, firstName, lastName, email, phoneNumber, address, "", password, company.getId());
                                                OwnerService ownerService = new OwnerService();
                                                ownerService.create(owner, userId, companyId)
                                                        .addOnCompleteListener(ownerTask -> {
                                                            if (ownerTask.isSuccessful()) {
                                                                // Owner created successfully
                                                                Toast.makeText(this, "Owner registered successfully. Check your email for verification.", Toast.LENGTH_SHORT).show();
                                                            } else {
                                                                // Owner creation failed
                                                                Toast.makeText(this, "Failed to create owner. Please try again.", Toast.LENGTH_SHORT).show();
                                                            }
                                                        });
                                            });
                                        } else {
                                            // User creation failed
                                            if (!userTask.isSuccessful()) {
                                                Exception exception = userTask.getException();
                                                if (exception != null) {
                                                    Log.e("RegistrationError", "Registration failed: " + exception.getMessage());
                                                }
                                                Toast.makeText(this, "User registration failed. Please try again.", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        } else {
                            // Company creation failed
                            Toast.makeText(this, "Failed to create company. Please try again.", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void sendVerificationEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            user.sendEmailVerification()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Toast.makeText(this, "Verification email sent", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, "Failed to send verification email", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }


    public void showTimePickerDialog(View view) {
        Button button = (Button) view;
        String tag = button.getTag().toString();

        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                (view1, hourOfDay, minute1) -> {
                    @SuppressLint("DefaultLocale") String selectedTime = String.format("%02d:%02d", hourOfDay, minute1);


                    if (validateTime(tag)) {     // Validate start and finish time
                        button.setText(selectedTime);
                        saveTime(tag, selectedTime);
                    } else {
                        Toast.makeText(getApplicationContext(), "Finish time must be later than start time", Toast.LENGTH_SHORT).show();
                    }
                }, hour, minute, true);

        timePickerDialog.show();
    }

    private boolean isTimeBefore(String startTime, String finishTime) {
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            Date startTimeDate = format.parse(startTime);
            Date finishTimeDate = format.parse(finishTime);
            assert startTimeDate != null;
            return startTimeDate.before(finishTimeDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean validateTime(String tag) {
        String startTag = tag.endsWith("Start") ? tag : tag.substring(0, tag.length() - 5) + "Start";
        String finishTag = tag.endsWith("Finish") ? tag : tag.substring(0, tag.length() - 6) + "Finish";

        String startTime = timeSchedule.getStartTime(startTag);
        String finishTime = timeSchedule.getFinishTime(finishTag);

        // Check if both start and finish times are empty
        if (startTime.isEmpty() && finishTime.isEmpty()) {
            return true;
        }

        // Check if both start and finish times are not empty and validate
        if (!startTime.isEmpty() && !finishTime.isEmpty()) {
            return isTimeBefore(startTime, finishTime);
        }

        return false; // Return false if either start or finish time is empty while the other is not
    }
    private void saveTime(String tag, String selectedTime) {
        if (tag.endsWith("Start")) {
            timeSchedule.setStartTime(tag, selectedTime);
        } else if (tag.endsWith("Finish")) {
            timeSchedule.setFinishTime(tag, selectedTime);
        }
    }
}