package com.example.eventplanner.models;

public class Guest {
    private String id;

    private String fullName;

    private GuestAge age;

    private boolean isInvited;

    private boolean acceptedInvitation;

    private String specialRequests;

    public Guest() {
    }

    public Guest(String fullName, GuestAge age, boolean isInvited, boolean acceptedInvitation, String specialRequests) {
        this.fullName = fullName;
        this.age = age;
        this.isInvited = isInvited;
        this.acceptedInvitation = acceptedInvitation;
        this.specialRequests = specialRequests;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public GuestAge getAge() {
        return age;
    }

    public String getAgeAsString() {switch (age) {
        case AGE_0_3:
            return "0-3";
        case AGE_3_10:
            return "3-10";
        case AGE_10_18:
            return "10-18";
        case AGE_18_30:
            return "18-30";
        case AGE_30_50:
            return "30-50";
        case AGE_50_70:
            return "50-70";
        case AGE_70_PLUS:
            return "70+";
        default:
            return "Unknown";
    }

    }

    public void setAge(GuestAge age) {
        this.age = age;
    }

    public boolean isInvited() {
        return isInvited;
    }

    public String getIsInvitedAsString() {
        if(!this.isInvited) {
            return "No";
        }
        else if(this.acceptedInvitation) {
            return "Yes (accepted)";
        }
        return "Yes (not accepted)";
    }

    public void setInvited(boolean invited) {
        isInvited = invited;
    }

    public boolean isAcceptedInvitation() {
        return acceptedInvitation;
    }

    public void setAcceptedInvitation(boolean acceptedInvitation) {
        this.acceptedInvitation = acceptedInvitation;
    }

    public String getSpecialRequests() {
        return specialRequests;
    }

    public String getSpecialRequestsShort() {
        if(this.specialRequests.contains("Vegan")) {
            return "Vegan";
        }
        else if (this.specialRequests.isEmpty()) {
            return "None";
        }
        return this.specialRequests;
    }

    public void setSpecialRequests(String specialRequests) {
        this.specialRequests = specialRequests;
    }
}
