package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ProductListViewAdapter;
import com.example.eventplanner.adapters.ProductResultsAdapter;
import com.example.eventplanner.adapters.ServiceResultsAdapter;
import com.example.eventplanner.adapters.ServicesListViewAdapter;
import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.PackageService;
import com.example.eventplanner.services.UserService;

import java.util.List;
import java.util.stream.Collectors;

public class PackageServicesAndProductsActivity extends AppCompatActivity {
    private User user;
    private Package aPackage;
    private UserService userService = new UserService();
    private PackageService packageService = new PackageService(new PackageRepository());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_package_services_and_products);

        Intent intent = getIntent();
        String email = intent.getStringExtra("EMAIL");
        String packageId = intent.getStringExtra("PACKAGE");
        userService.getByEmail(email).addOnCompleteListener(this, task->{
            if(task.isSuccessful()){
                user = task.getResult();
            }

            packageService.getAll(new PackageService.OnDataFetchListener() {
                @Override
                public void onSuccess(List<Package> packages) {
                    aPackage = packages.stream().filter(p -> p.getId().equals(packageId)).findFirst().orElse(null);

                    ListView listView1 = findViewById(R.id.listView1);
                    ListView listView2 = findViewById(R.id.listView2);

                    List<Service> services = aPackage.getServicesHashMap().values().stream().filter(s -> s.isVisible()).collect(Collectors.toList());
                    List<Product> products = aPackage.getProductsHashMap().values().stream().filter(p -> p.isVisible()).collect(Collectors.toList());

                    initializeServicesRecyclerView(services);
                    initializeProductsRecyclerView(products);

                    RadioButton servicesButton = findViewById(R.id.radio_services);
                    RadioGroup radioGroup = findViewById(R.id.rad);

                    servicesButton.setChecked(true);
                    listView1.setVisibility(View.VISIBLE);
                    listView2.setVisibility(View.GONE);

                    radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                    {
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            RadioButton rb=(RadioButton)findViewById(checkedId);
                            if(rb.getText().equals("Services")) {
                                listView1.setVisibility(View.VISIBLE);
                                listView2.setVisibility(View.GONE);
                            }
                            else if(rb.getText().equals("Products")) {
                                listView2.setVisibility(View.VISIBLE);
                                listView1.setVisibility(View.GONE);
                            }
                        }
                    });
                }

                @Override
                public void onFailure(String errorMessage) {
                }
            });
        });
    }

    private void initializeServicesRecyclerView(List<Service> services){
        ListView listView;
        listView = findViewById(R.id.listView1);
        ServicesListViewAdapter serviceAdapter = new ServicesListViewAdapter(PackageServicesAndProductsActivity.this, services, user);
        listView.setAdapter(serviceAdapter);

    }

    private void initializeProductsRecyclerView(List<Product> products){
        ListView listView;
        listView = findViewById(R.id.listView2);
        ProductListViewAdapter productAdapter = new ProductListViewAdapter(PackageServicesAndProductsActivity.this, products, user);
        listView.setAdapter(productAdapter);
    }
}