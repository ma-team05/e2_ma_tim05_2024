package com.example.eventplanner.models;


import java.util.ArrayList;
import java.util.List;

public class Company {
    private String Id;
    private String Email;
    private String Name;
    private String Address;
    private String PhoneNumber;
    private String Description;
    private List<Category> Categories;
    private List<EventType> EventTypes;
    private TimeSchedule TimeSchedule;
    private String CompanyType;

    public Company() { }
    public Company(String name, String email, String address, String phoneNumber, String description, String companyType, TimeSchedule timeSchedule) {
        Name = name;
        Email = email;
        Address = address;
        PhoneNumber = phoneNumber;
        Description = description;
        Categories = new ArrayList<>();
        EventTypes = new ArrayList<>();
        CompanyType = companyType;
        TimeSchedule = timeSchedule;
    }

    public void setCompanyType(String companyType) {
        CompanyType = companyType;
    }
    public String getCompanyType() {
        return CompanyType;
    }
    public void setEventTypes(List<EventType> eventTypes) { EventTypes = eventTypes; }
    public List<EventType> getEventTypes() {
        return EventTypes;
    }
    public void setId(String id) {
        Id = id;
    }
    public void setEmail(String email) {
        Email = email;
    }
    public void setName(String name) {
        Name = name;
    }
    public void setAddress(String address) {
        Address = address;
    }
    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }
    public void setDescription(String description) {
        Description = description;
    }
    public void setCategories(List<Category> categories) {
        Categories = categories;
    }
    public void setTimeSchedule(TimeSchedule timeSchedule) {
        TimeSchedule = timeSchedule;
    }
    public String getId() {
        return Id;
    }
    public String getEmail() {
        return Email;
    }
    public String getName() {
        return Name;
    }
    public String getAddress() {
        return Address;
    }
    public String getPhoneNumber() {
        return PhoneNumber;
    }
    public String getDescription() {
        return Description;
    }
    public List<Category> getCategories() { return Categories; }
    public TimeSchedule getTimeSchedule() {
        return TimeSchedule;
    }


}
