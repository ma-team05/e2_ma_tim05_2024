package com.example.eventplanner.activities;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.Button;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.NotificationAdapter;
import com.example.eventplanner.models.Notification;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.utils.ShakeDetector;

import java.util.ArrayList;
import java.util.List;

public class NotificationsActivity extends AppCompatActivity {

    private NotificationService notificationService;
    private NotificationAdapter notificationAdapter;
    private List<Notification> allNotifications;
    private List<Notification> readNotifications;
    private List<Notification> unreadNotifications;
    private int currentTab = 0;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_notifications);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");

        notificationService = new NotificationService();
        notificationAdapter = new NotificationAdapter(new ArrayList<>());
        allNotifications = new ArrayList<>();
        readNotifications = new ArrayList<>();
        unreadNotifications = new ArrayList<>();

        RecyclerView recyclerView = findViewById(R.id.recycler_view_notifications);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(notificationAdapter);

        Button btnAll = findViewById(R.id.btn_all);
        Button btnRead = findViewById(R.id.btn_read);
        Button btnUnread = findViewById(R.id.btn_unread);

        btnAll.setOnClickListener(v -> {
            currentTab = 0;
            notificationAdapter.setNotifications(allNotifications);
            updateButtonStates(btnAll, btnRead, btnUnread);
        });

        btnRead.setOnClickListener(v -> {
            currentTab = 1;
            notificationAdapter.setNotifications(readNotifications);
            updateButtonStates(btnAll, btnRead, btnUnread);
        });

        btnUnread.setOnClickListener(v -> {
            currentTab = 2;
            notificationAdapter.setNotifications(unreadNotifications);
            updateButtonStates(btnAll, btnRead, btnUnread);
        });

        fetchNotifications();

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(count -> {
            switch (currentTab) {
                case 0:
                    currentTab = 1;
                    notificationAdapter.setNotifications(readNotifications);
                    break;
                case 1:
                    currentTab = 2;
                    notificationAdapter.setNotifications(unreadNotifications);
                    break;
                case 2:
                    currentTab = 0;
                    notificationAdapter.setNotifications(allNotifications);
                    break;
            }
            updateButtonStates(btnAll, btnRead, btnUnread);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(mShakeDetector);
    }

    private void fetchNotifications() {
        notificationService.getNotificationsByUser(userId).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                allNotifications = task.getResult();
                readNotifications.clear();
                unreadNotifications.clear();
                for (Notification notification : allNotifications) {
                    if (notification.isSent()) {
                        readNotifications.add(notification);
                    } else {
                        unreadNotifications.add(notification);
                    }
                }
                notificationAdapter.setNotifications(allNotifications);
                updateButtonStates(findViewById(R.id.btn_all), findViewById(R.id.btn_read), findViewById(R.id.btn_unread));
            }
        });
    }

    private void updateButtonStates(Button btnAll, Button btnRead, Button btnUnread) {
        btnAll.setEnabled(currentTab != 0);
        btnRead.setEnabled(currentTab != 1);
        btnUnread.setEnabled(currentTab != 2);

        int activeColor = getResources().getColor(R.color.active_tab_color);
        int inactiveColor = getResources().getColor(R.color.inactive_tab_color);

        btnAll.setBackgroundColor(currentTab == 0 ? activeColor : inactiveColor);
        btnRead.setBackgroundColor(currentTab == 1 ? activeColor : inactiveColor);
        btnUnread.setBackgroundColor(currentTab == 2 ? activeColor : inactiveColor);
    }
}