package com.example.eventplanner.database.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.eventplanner.models.Chat;
import com.example.eventplanner.models.Event;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChatRepository {
    private final FirebaseFirestore db;
    private final String collection;

    public ChatRepository() {

        db = FirebaseFirestore.getInstance();
        collection = "chats";
    }

    public interface OnDataFetchListener {
        void onSuccess(List<Chat> events);
        void onFailure(String errorMessage);
    }

    public void save(Chat chat) {
        chat.setLastUpdated(new Date());
        db.collection(collection)
                .add(chat)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("CHAT CREATION SUCCESS", "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("CHAT CREATION FAILED", "Error adding document", e);
                    }
                });
    }

    public void getAll(final ChatRepository.OnDataFetchListener listener) {
        db.collection(collection).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                List<Chat> objects = new ArrayList<>();
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    Chat event = documentSnapshot.toObject(Chat.class);
                    event.setId(documentSnapshot.getId());
                    objects.add(event);
                    //documentSnapshot.getReference().delete();
                }
                listener.onSuccess(objects);
            }
        });
    }

    public void update(Chat chat, boolean updateDate) {
        if(updateDate) {
            chat.setLastUpdated(new Date());
        }
        db.collection(collection)
                .document(chat.getId())
                .set(chat)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("CHAT UPDATE SUCCESS", "Chat updated successfully with ID: " + chat.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("CHAT UPDATE FAILED", "Error updating chat with ID: " + chat.getId(), e);
                    }
                });
    }
}
