package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.database.repository.ServiceRepository;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Service;
import com.google.android.gms.tasks.Task;

import java.util.List;
import java.util.stream.Collectors;

public class PackageService {
    private final PackageRepository packageRepository;

    public PackageService(PackageRepository packageRepository){
        this.packageRepository = packageRepository;
    }

    public Task<Package> get(String id){ return packageRepository.get(id); }

    public interface OnDataFetchListener {
        void onSuccess(List<Package> objects);
        void onFailure(String errorMessage);
    }

    public void search(String name, String provider, double minPrice, double maxPrice, String city, double maxDistance, String eventType, String serviceCategory, String serviceSubcategory, boolean available,final PackageService.OnDataFetchListener listener){
        packageRepository.getAll(new PackageRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Package> events) {
                List<Package> searched = events;

                if(name != null && !name.isEmpty()) {
                    searched = events.stream().filter(e ->
                            e.getName().toLowerCase().contains(name.toLowerCase())).collect(Collectors.toList());
                }

                if(minPrice != -1) {
                    searched = searched.stream().filter(e ->
                            e.getPrice() >= minPrice).collect(Collectors.toList());
                }

                if(maxPrice != -1) {
                    searched = searched.stream().filter(e ->
                            e.getPrice() <= maxPrice).collect(Collectors.toList());
                }

                if(eventType != null && !eventType.isEmpty()) {
                    searched = searched.stream().filter(e ->
                            e.getEventTypes().toLowerCase().contains(eventType.toLowerCase())).collect(Collectors.toList());
                }

                if(serviceCategory != null && !serviceCategory.isEmpty()) {
                    searched = searched.stream().filter(e ->
                            e.getCategory().toLowerCase().contains(serviceCategory.toLowerCase())).collect(Collectors.toList());
                }

                if(serviceSubcategory != null && !serviceSubcategory.isEmpty()) {
                }

                if(available) {
                    searched = searched.stream().filter(e ->
                            e.isAvailable() == available).collect(Collectors.toList());
                }

                listener.onSuccess(searched);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }
    public void create(Package service){
        packageRepository.create(service);
    }

    public void update(Package service){
        packageRepository.update(service);
    }

    public void deleteLogically(Package service){
        packageRepository.deleteLogically(service);
    }
    public void getAll(final OnDataFetchListener listener){
        packageRepository.getAll(new PackageRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Package> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void getUndeletedOfPUP(String userId,final OnDataFetchListener listener){
        packageRepository.getUndeletedOfPUP(userId,new PackageRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Package> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void getUndeletedWithProduct(String userId,String productId,final OnDataFetchListener listener){
        packageRepository.getUndeletedWithProduct(userId,productId,new PackageRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Package> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void getUndeletedWithService(String userId,String serviceId,final OnDataFetchListener listener){
        packageRepository.getUndeletedWithService(userId,serviceId,new PackageRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Package> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void updatePackagesInFirestore(List<Package> updatedPackages){
        packageRepository.updatePackagesInFirestore(updatedPackages);
    }

}
