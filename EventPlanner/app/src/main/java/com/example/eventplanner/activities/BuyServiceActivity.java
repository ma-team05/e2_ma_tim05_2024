package com.example.eventplanner.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.services.EventService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BuyServiceActivity extends AppCompatActivity {
    private EventService eventService;
    private List<Event> events;
    private Event selectedEvent;
    private String selectedEventId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_buy_service);

        eventService = new EventService(new EventRepository());

        String email = (String) getIntent().getSerializableExtra("EMAIL");
        selectedEventId = (String) getIntent().getSerializableExtra("EVENT");

        eventService.getAll(new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events1) {
                events = events1.stream().filter(e -> e.getUserEmail().equals(email)).collect(Collectors.toList());

                List<String> names = new ArrayList<>();
                names.add("Select an event");
                for (Event e : events) {
                    names.add("Selected event: " + e.getName());
                }

                Spinner eventSpinner = findViewById(R.id.event);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(BuyServiceActivity.this, android.R.layout.simple_spinner_item, names);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                eventSpinner.setAdapter(adapter);

                for (int i = 0; i < events.size(); i++) {
                    if (events.get(i).getId().equals(selectedEventId)) {
                        eventSpinner.setSelection(i + 1);
                        selectedEvent = events.get(i);
                        break;
                    } else {
                        eventSpinner.setSelection(0);
                        selectedEvent = null;
                    }
                }

                eventSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String eventName = eventSpinner.getSelectedItem().toString().trim();
                        if (eventName.equals("Select an event")) {
                            selectedEventId = "";
                            selectedEvent = null;
                        } else {
                            selectedEvent = events.stream().filter(e -> e.getName().equals(eventName.substring(16))).findFirst().orElse(null);
                            selectedEventId = selectedEvent.getId();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });

        Button buyButton = findViewById(R.id.createButton);

        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buy();
            }
        });
    }

    private void buy() {
        if (selectedEvent == null) {
            Toast.makeText(this, "Please select an event", Toast.LENGTH_SHORT).show();
            return;
        }
    }
}