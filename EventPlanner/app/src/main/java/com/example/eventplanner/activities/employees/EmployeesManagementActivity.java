package com.example.eventplanner.activities.employees;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.registration.EmployeeRegistrationActivity;
import com.example.eventplanner.adapters.EmployeeAdapter;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.EmployeeService;

import java.util.ArrayList;
import java.util.List;

public class EmployeesManagementActivity extends AppCompatActivity {//implements EmployeeAdapter.OnDetailsButtonClickListener {

    private static final int REQUEST_STORAGE_PERMISSION = 1;

    private CompanyService companyService = new CompanyService();
    private String companyId;
    private EmployeeService service;
    private RecyclerView recyclerView;
    private List<Employee> employeeList;
    private EmployeeAdapter employeeAdapter;

    @Override
    protected void onResume() {
        super.onResume();
        initializeRecyclerView();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employees_management);
        Intent intent = getIntent();
        companyId = intent.getStringExtra("userId");

        Button addButton = findViewById(R.id.addButton);
        Button searchButton = findViewById(R.id.searchButton);
        service = new EmployeeService(new EmployeeRepository());
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registrationIntent = new Intent(EmployeesManagementActivity.this, EmployeeRegistrationActivity.class);
                registrationIntent.putExtra("companyId", companyId);
                startActivity(registrationIntent);
            }
        });
        searchButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                search();
            }
        });
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE_PERMISSION);
        } else {
            initializeRecyclerView();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_STORAGE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initializeRecyclerView();
            } else {
                initializeRecyclerView();
            }
        }
    }

    private void initializeRecyclerView() {

        recyclerView = findViewById(R.id.employeeRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        service.getAllByCompany(companyId).addOnCompleteListener(task2 -> {
            if (task2.isSuccessful()) {
                employeeList = task2.getResult();
                employeeAdapter = new EmployeeAdapter(employeeList);
                recyclerView.setAdapter(employeeAdapter);
            } else {
                Exception exception = task2.getException();
                exception.printStackTrace();
            }
        });
    }
    private void search(){
        EditText search = findViewById(R.id.searchEditText);
        List<Employee> ret = new ArrayList<>();
        String input = search.getText().toString().trim().toLowerCase();
        for(Employee e: employeeList) {
            String fullName = e.getFirstName() + " " + e.getLastName();
            String email = e.getEmail();
            String name = e.getFirstName();
            String lastname = e.getLastName();
            if(email.toLowerCase().contains(input) || name.toLowerCase().contains(input)|| lastname.toLowerCase().contains(input)||fullName.toLowerCase().contains(input)){
                ret.add(e);
            }
        }
        recyclerView = findViewById(R.id.employeeRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new EmployeeAdapter(ret));
    }
}
