package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.ServiceRepository;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.ServiceService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ServiceAdapter extends  RecyclerView.Adapter<ServiceAdapter.ServiceViewHolder>{

    private Context context;
    private List<Service> serviceList;
    ServiceService serviceservice;
    UserRole userRole;

    public ServiceAdapter(Context context,List<Service> serviceList, UserRole userRole) {
        this.context = context;
        this.serviceList = serviceList;
        serviceservice = new ServiceService(new ServiceRepository());
        this.userRole = userRole;

        Log.i("Frag","Napravio adapter");
    }

    @NonNull
    @Override
    public ServiceAdapter.ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service, parent, false);
        return new ServiceAdapter.ServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceViewHolder holder, int position) {
        Service service = serviceList.get(position);
        holder.bind(service);
    }



    @Override
    public int getItemCount() {
        return serviceList.size();
    }



    public class ServiceViewHolder extends RecyclerView.ViewHolder {

        ImageButton btPrevious, btNext, editServiceBtn, deleteServiceBtn;
        ImageSwitcher solutionImageSwitcher;
        TextView serviceName, serviceCategory, serviceSubcategory, serviceDescription,
                servicePrice, serviceDiscount, serviceDiscountPrice, serviceAvailable, serviceVisibility, serviceEventType,
                serviceDuration, serviceReserveIn, serviceCancelIn, serviceConfirm, serviceEmployees, serviceSpecifities;
        private Service currentService;  // Changed to Service
        private int currentIndex;
        public ServiceViewHolder(@NonNull View itemView) {
            super(itemView);

            btPrevious = itemView.findViewById(R.id.bt_previous);
            btNext = itemView.findViewById(R.id.bt_next);
            editServiceBtn = itemView.findViewById(R.id.edit_solution_btn);
            deleteServiceBtn = itemView.findViewById(R.id.delete_solution_btn);
            solutionImageSwitcher = itemView.findViewById(R.id.solution_image_switcher);
            solutionImageSwitcher.setFactory(() -> {
                ImageView imageView = new ImageView(itemView.getContext());
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                return imageView;
            });
            serviceName = itemView.findViewById(R.id.solution_name);
            serviceCategory = itemView.findViewById(R.id.solution_category);
            serviceSubcategory = itemView.findViewById(R.id.solution_subcategory);
            serviceDescription = itemView.findViewById(R.id.solution_description);
            servicePrice = itemView.findViewById(R.id.solution_price);
            serviceDiscount = itemView.findViewById(R.id.solution_discount);
            serviceDiscountPrice = itemView.findViewById(R.id.solution_discount_price);
            serviceAvailable = itemView.findViewById(R.id.solution_available);
            serviceVisibility = itemView.findViewById(R.id.solution_visibility);
            serviceEventType = itemView.findViewById(R.id.solution_event_type);
            serviceDuration = itemView.findViewById(R.id.solution_duration);
            serviceReserveIn = itemView.findViewById(R.id.solution_res_deadline);
            serviceCancelIn = itemView.findViewById(R.id.solution_cancel_in);
            serviceConfirm = itemView.findViewById(R.id.solution_confirmation);
            serviceEmployees = itemView.findViewById(R.id.solution_employees);
            serviceSpecifities = itemView.findViewById(R.id.solution_specifities);


            btNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //int currentIndex = solutionImageSwitcher.getDisplayedChild();
                    if (currentIndex < currentService.getImages().size() - 1) {
                        currentIndex++;
                        solutionImageSwitcher.setImageResource(currentService.getImages().get(currentIndex));
                    }
                }
            });

            btPrevious.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //int currentIndex = solutionImageSwitcher.getDisplayedChild();
                    if (currentIndex > 0) {
                        currentIndex--;
                        solutionImageSwitcher.setImageResource(currentService.getImages().get(currentIndex));
                    }
                }
            });

            editServiceBtn.setOnClickListener(v -> {
                if(userRole == UserRole.OWNER){
                    editServicePopup(currentService);
                }else{
                    Toast.makeText(context, "You have no peri=mission for this action", Toast.LENGTH_SHORT).show();
                }


            });
            deleteServiceBtn.setOnClickListener(v -> {
                if(userRole == UserRole.OWNER){
                    showDeleteConfirmationDialog(currentService);
                }else{
                    Toast.makeText(context, "You have no peri=mission for this action", Toast.LENGTH_SHORT).show();
                }

            });
        }

        public void bind(Service service) {  // Changed to Service
            currentService = service;
            boolean ind = service != null;
            currentIndex = 0;
            Log.i("binder", ind ? "nije null" : "null je");
            serviceName.setText(service.getName());
            serviceCategory.setText(service.getCategory());
            serviceSubcategory.setText(service.getSubCategory());
            serviceDescription.setText(service.getDescription());
            servicePrice.setText(String.valueOf(service.getPrice()));
            serviceDiscount.setText(String.valueOf(service.getDiscount()));

            serviceEventType.setText(service.getEventType());
            serviceAvailable.setText(service.isAvailable() ? "Yes" : "No");
            serviceVisibility.setText(service.isVisible() ? "Yes" : "No");
            serviceEmployees.setText(service.getEmployeesAsString());

            if(currentService.isAppointment()){
                serviceDuration.setText(currentService.getAppointmentDuration() + "h");
            }else{
                StringBuilder builder = new StringBuilder();
                builder.append("min ").append(currentService.getMinDuration()).append("h ").append("max ").append(currentService.getMaxDuration()).append("h ");
                serviceDuration.setText(builder.toString());
            }
            //for time stamp
            Calendar cal = Calendar.getInstance();

// Reservation deadline
            Date reservationDate = currentService.getReservationDeadline().toDate();
            cal.setTimeInMillis(reservationDate.getTime());
            StringBuilder reservationBuilder = new StringBuilder();
            reservationBuilder.append((cal.get(Calendar.MONTH) + 1)%12).append("m "); // Adding 1 to get 1-based month
            reservationBuilder.append(cal.get(Calendar.DAY_OF_MONTH)).append("d ");
            serviceReserveIn.setText(reservationBuilder.toString());

// Cancellation deadline
            if(currentService.getCancelationDeadline() != null){
                Date cancellationDate = currentService.getCancelationDeadline().toDate();
                cal.setTimeInMillis(cancellationDate.getTime());
                StringBuilder cancellationBuilder = new StringBuilder();

                cancellationBuilder.append((cal.get(Calendar.MONTH ) + 1)%12).append("m "); // Adding 1 to get 1-based month
                cancellationBuilder.append(cal.get(Calendar.DAY_OF_MONTH )).append("d ");
                serviceCancelIn.setText(cancellationBuilder.toString());
            }


            if(currentService.getSpecifities().isEmpty()){
                serviceSpecifities.setText("/");
            }else{
                serviceSpecifities.setText(currentService.getSpecifities());
            }

            if(currentService.getConfirmationType() == Service.ConfirmationType.AUTOMATIC){
                serviceConfirm.setText("Automatic");
            }else{
                serviceConfirm.setText("By hand");
            }

            if(currentService.isAppointment()){
                double finalprice = service.getPrice() - service.getDiscount()*service.getPrice()/100;
                double hours = Double.parseDouble(currentService.getAppointmentDuration());
                finalprice = finalprice*hours;
                serviceDiscountPrice.setText(String.valueOf(finalprice));
            }else{
                serviceDiscountPrice.setText("will be calculated");
            }


            //serviceDuration, serviceReserveIn, serviceCancelIn, serviceConfirm
            if (service.getImages() != null && !service.getImages().isEmpty()) {
                solutionImageSwitcher.setImageResource(service.getImages().get(0));
            } else {
                solutionImageSwitcher.setImageResource(R.drawable.default_image);
            }
        }
    }

    @SuppressLint({"NotifyDataSetChanged"})
    private void editServicePopup(Service service) {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.service_edit_form, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        LinearLayout layoutServiceAppointment = popupView.findViewById(R.id.layout_service_appointment);
        LinearLayout layoutServiceNoAppoitnment = popupView.findViewById(R.id.layout_service_no_appointment);

        RadioButton automaticRadioButotn = popupView.findViewById(R.id.automatic_radio_button);

        if(service.isAppointment()){
            layoutServiceNoAppoitnment.setVisibility(View.GONE);
            layoutServiceAppointment.setVisibility(View.VISIBLE);
            automaticRadioButotn.setVisibility(View.VISIBLE);
        }else{
            layoutServiceNoAppoitnment.setVisibility(View.VISIBLE);
            layoutServiceAppointment.setVisibility(View.GONE);
            automaticRadioButotn.setVisibility(View.GONE);
        }
        //show window
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.buttonSave_edit_service);
        Button cancelButton = popupView.findViewById(R.id.buttonCancel_edit_service);

        saveButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });

        cancelButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });
    }

    private void showDeleteConfirmationDialog(Service service) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Delete Service");
        builder.setMessage("Are you sure you want to delete this service?");

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //delete operation here
                service.setDeleted(true);
                serviceservice.deleteLogically(service);
                serviceList.remove(service);
                notifyDataSetChanged();


                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
