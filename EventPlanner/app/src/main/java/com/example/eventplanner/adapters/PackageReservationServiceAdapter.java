package com.example.eventplanner.adapters;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Reservation;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.ReservationService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class PackageReservationServiceAdapter extends ArrayAdapter<Service> {
    private final List<Service> services;
    private final List<Long> startDates;
    private final List<Long> endDates;
    private final List<Long> startTimes;
    private final List<Long> endTimes;
    private final Context context;
    private final EmployeeService employeeService;
    private final Map<Integer, Employee> selectedEmployees = new HashMap<>();

    public PackageReservationServiceAdapter(Context context, List<Service> services) {
        super(context, 0, services);
        this.context = context;
        this.services = services;
        this.employeeService = new EmployeeService();
        this.startDates = new ArrayList<>();
        this.endDates = new ArrayList<>();
        this.startTimes = new ArrayList<>();
        this.endTimes = new ArrayList<>();

        for (int i = 0; i < services.size(); i++) {
            startDates.add(null);
            endDates.add(null);
            startTimes.add(null);
            endTimes.add(null);
        }
    }

    public void setStartTime(int position, Long startTime) {
        startTimes.set(position, startTime);
    }

    public void setEndTime(int position, Long endTime) {
        endTimes.set(position, endTime);
    }
    public boolean areAllDatesSelected() {
        for (int i = 0; i < services.size(); i++) {
            if (startDates.get(i) == null || endDates.get(i) == null) {
                return false;
            }
        }
        return true;
    }

    public void createReservations(User user, String packageId, String eventId) {
        ReservationService reservationService = new ReservationService();
        for (int i = 0; i < services.size(); i++) {
            Long startDate = startDates.get(i);
            Long endDate = endDates.get(i);
            Employee employee = selectedEmployees.get(i);
            if (startDate != null && endDate != null && employee != null) {
                Reservation reservation = new Reservation(user, employee, new Date(startDate), new Date(endDate), services.get(i), packageId);
                reservation.setEventId(eventId);
                reservationService.save(reservation).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Toast.makeText(context, "Package was successfully reserved!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "An error occurred. Please try again.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    public void setStartDate(int position, Long startDate) {
        startDates.set(position, startDate);
    }

    public void setEndDate(int position, Long endDate) {
        endDates.set(position, endDate);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_service_card, parent, false);
            holder = new ViewHolder();
            holder.textName = convertView.findViewById(R.id.textName);
            holder.startDateEditText = convertView.findViewById(R.id.startDateEditText);
            holder.startTimeEditText = convertView.findViewById(R.id.startTimeEditText);
            holder.endDateEditText = convertView.findViewById(R.id.endDateEditText);
            holder.endTimeEditText = convertView.findViewById(R.id.endTimeEditText);
            holder.spinnerEmployees = convertView.findViewById(R.id.spinnerEmployees);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Service service = services.get(position);
        holder.textName.setText(service.getName());

        holder.startTimeEditText.setOnClickListener(v -> showTimePicker(holder.startTimeEditText, position, true));
        holder.endTimeEditText.setOnClickListener(v -> showTimePicker(holder.endTimeEditText, position, false));
        holder.startDateEditText.setOnClickListener(v -> showDatePicker(holder.startDateEditText, position));
        holder.endDateEditText.setOnClickListener(v -> showDatePicker(holder.endDateEditText, position));

        employeeService.getAll().addOnSuccessListener(employees -> {
            List<String> employeeNames = new ArrayList<>();
            for (Employee employee : employees) {
                employeeNames.add(employee.getFullName());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, employeeNames);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.spinnerEmployees.setAdapter(adapter);

            holder.spinnerEmployees.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    Employee selectedEmployee = employees.get(position);
                    selectedEmployees.put(position, selectedEmployee);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {

                }
            });
        }).addOnFailureListener(e -> {

        });

        return convertView;
    }

    static class ViewHolder {
        TextView textName;
        EditText startDateEditText, startTimeEditText, endDateEditText, endTimeEditText;
        Spinner spinnerEmployees;
    }

    private void showDatePicker(EditText editText, int position) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                (view, selectedYear, selectedMonth, selectedDay) -> {
                    String selectedDate = (selectedMonth + 1) + "/" + selectedDay + "/" + selectedYear;
                    editText.setText(selectedDate);
                    if (editText.getId() == R.id.startDateEditText) {
                        setStartDate(position, calendar.getTimeInMillis());
                    } else {
                        setEndDate(position, calendar.getTimeInMillis());
                    }
                }, year, month, day);

        datePickerDialog.show();
    }

    private void showTimePicker(EditText editText, int position, boolean isStartTime) {
        Calendar currentTime = Calendar.getInstance();
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                (view, selectedHour, selectedMinute) -> {
                    Calendar selectedTime = Calendar.getInstance();
                    selectedTime.set(Calendar.HOUR_OF_DAY, selectedHour);
                    selectedTime.set(Calendar.MINUTE, selectedMinute);
                    selectedTime.set(Calendar.SECOND, 0);
                    selectedTime.set(Calendar.MILLISECOND, 0);
                    long timeInMillis = selectedTime.getTimeInMillis();
                    editText.setText(String.format(Locale.getDefault(), "%02d:%02d", selectedHour, selectedMinute));
                    if (isStartTime) {
                        setStartTime(position, timeInMillis);
                    } else {
                        setEndTime(position, timeInMillis);
                    }
                }, hour, minute, true);

        timePickerDialog.show();
    }


}