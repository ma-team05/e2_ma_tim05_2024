package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EventAdapter;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.services.EventService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MyEventsActivity extends AppCompatActivity {
    private EventService eventService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_events);

        eventService = new EventService(new EventRepository());

        ListView listView = findViewById(R.id.listView);

        Intent intent = getIntent();
        String email = intent.getStringExtra("email");

        eventService.getAll(new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                events = events.stream().filter(e -> e.getUserEmail().equals(email)).collect(Collectors.toList());
                EventAdapter adapter = new EventAdapter(MyEventsActivity.this, events, email);
                listView.setAdapter(adapter);
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });

        FloatingActionButton addButton = findViewById(R.id.fab);
        addButton.setOnClickListener(v -> startActivity(new Intent(MyEventsActivity.this, EventCreationActivity.class)));

        addButton.setOnClickListener(v -> {
            Intent createIntent = new Intent(this, EventCreationActivity.class);
            createIntent.putExtra("EMAIL", email);
            this.startActivity(createIntent);
        });
    }
}