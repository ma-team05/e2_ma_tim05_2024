package com.example.eventplanner.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CategorySpinnerAdapter;
import com.example.eventplanner.adapters.EventAdapter;
import com.example.eventplanner.database.repository.CategoryRepository;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.database.repository.EventTypeRepository;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.EventService;
import com.example.eventplanner.services.EventTypeService;
import com.example.eventplanner.services.UserService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class ServiceSearchActivity extends AppCompatActivity {

    private CategoryService categoryService;
    private EventTypeService eventTypeService;
    private UserService userService;
    private EventService eventService;
    private List<EventType> types;
    private List<Event> events;
    List<EventType> typesList;
    int selectedTypePosition;
    EventType selectedType;
    Category selectedCategory;

    private String selectedEventId = "";

    User user;

    Event event = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_service_search);

        categoryService = new CategoryService();
        eventTypeService = new EventTypeService();
        eventService = new EventService(new EventRepository());
        userService = new UserService();

        String email = (String) getIntent().getSerializableExtra("email");
        selectedEventId = (String) getIntent().getSerializableExtra("EVENT");

        eventService.getAll(new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events1) {
                events = events1.stream().filter(e -> e.getUserEmail().equals(email)).collect(Collectors.toList());

                List<String> names = new ArrayList<>();
                names.add("Select an event");
                for (Event e : events) {
                    names.add("Selected event: " + e.getName());
                }

                Spinner eventSpinner = findViewById(R.id.event);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(ServiceSearchActivity.this, android.R.layout.simple_spinner_item, names);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                eventSpinner.setAdapter(adapter);

                for (int i = 0; i < events.size(); i++) {
                    if (events.get(i).getId().equals(selectedEventId)) {
                        eventSpinner.setSelection(i + 1);
                        break;
                    }
                    else {
                        eventSpinner.setSelection(0);
                    }
                }

                eventSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String eventName = eventSpinner.getSelectedItem().toString().trim();
                        if(eventName.equals("Select an event")) {
                            selectedEventId = "";
                        }
                        else {
                            Event selectedEvent = events.stream().filter(e -> e.getName().equals(eventName.substring(16))).findFirst().orElse(null);
                            assert selectedEvent != null;
                            selectedEventId = selectedEvent.getId();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });

        userService.getByEmail(email).addOnCompleteListener(this, task->{
            if(task.isSuccessful()){
                user = task.getResult();
            }

            eventTypeService.getAll(new EventTypeService.OnDataFetchListener() {
                @Override
                public void onSuccess(List<EventType> eventTypes) {

                    ServiceSearchActivity.this.types = eventTypes;

                    ServiceSearchActivity.this.typesList = new ArrayList<>();
                    List<Category> categoriesList = new ArrayList<>();

                    EventType select = new EventType("Select an event type", "Select an event type", true);
                    EventType other = new EventType("Other", "Other", true);
                    ServiceSearchActivity.this.typesList.add(select);
                    ServiceSearchActivity.this.typesList.addAll(eventTypes);
                    ServiceSearchActivity.this.typesList.add(other);

                    List<String> names = new ArrayList<>();
                    for (EventType type : ServiceSearchActivity.this.typesList) {
                        names.add(type.getName());
                    }

                    selectedTypePosition = 0;
                    List<String> names1 = new ArrayList<>();
                    names1.add("Select a service category");

                    List<String> names2 = new ArrayList<>();
                    names2.add("Select a service subcategory");

                    Spinner typeSpinner = findViewById(R.id.type);
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(ServiceSearchActivity.this, android.R.layout.simple_spinner_item, names);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    typeSpinner.setAdapter(adapter);

                    Spinner typeSpinner1 = findViewById(R.id.type1);
                    ArrayAdapter<String> adapter1 = new ArrayAdapter<>(ServiceSearchActivity.this, android.R.layout.simple_spinner_item, names1);
                    adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    typeSpinner1.setAdapter(adapter1);

                    Spinner typeSpinner2 = findViewById(R.id.type2);
                    ArrayAdapter<String> adapter2 = new ArrayAdapter<>(ServiceSearchActivity.this, android.R.layout.simple_spinner_item, names2);
                    adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    typeSpinner2.setAdapter(adapter2);

                    typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (ServiceSearchActivity.this.typesList.get(position).getName().equals("Select an event type") || ServiceSearchActivity.this.typesList.get(position).getName().equals("Other")) {
                                List<String> names1 = new ArrayList<>();
                                names1.add("Select a service category");

                                List<String> names2 = new ArrayList<>();
                                names2.add("Select a service subcategory");

                                Spinner typeSpinner1 = findViewById(R.id.type1);
                                ArrayAdapter<String> adapter1 = new ArrayAdapter<>(ServiceSearchActivity.this, android.R.layout.simple_spinner_item, names1);
                                adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                typeSpinner1.setAdapter(adapter1);

                                Spinner typeSpinner2 = findViewById(R.id.type2);
                                ArrayAdapter<String> adapter2 = new ArrayAdapter<>(ServiceSearchActivity.this, android.R.layout.simple_spinner_item, names2);
                                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                typeSpinner2.setAdapter(adapter2);
                            }
                            else {
                                selectedType = ServiceSearchActivity.this.typesList.get(position);
                                List<String> names1 = new ArrayList<>();
                                names1.add("Select a service category");
                                for (Category c: selectedType.getRecommendedServices()) {
                                    names1.add(c.getName());
                                }

                                List<String> names2 = new ArrayList<>();
                                names2.add("Select a service subcategory");

                                Spinner typeSpinner1 = findViewById(R.id.type1);
                                ArrayAdapter<String> adapter1 = new ArrayAdapter<>(ServiceSearchActivity.this, android.R.layout.simple_spinner_item, names1);
                                adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                typeSpinner1.setAdapter(adapter1);

                                Spinner typeSpinner2 = findViewById(R.id.type2);
                                ArrayAdapter<String> adapter2 = new ArrayAdapter<>(ServiceSearchActivity.this, android.R.layout.simple_spinner_item, names2);
                                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                typeSpinner2.setAdapter(adapter2);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                    typeSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position == 0 || selectedType.getRecommendedServices().get(position - 1).equals("Select a service category")) {
                                List<String> names2 = new ArrayList<>();
                                names2.add("Select a service subcategory");

                                Spinner typeSpinner2 = findViewById(R.id.type2);
                                ArrayAdapter<String> adapter2 = new ArrayAdapter<>(ServiceSearchActivity.this, android.R.layout.simple_spinner_item, names2);
                                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                typeSpinner2.setAdapter(adapter2);
                            }
                            else {
                                selectedCategory = selectedType.getRecommendedServices().get(position - 1);

                                List<String> names2 = new ArrayList<>();
                                names2.add("Select a service subcategory");
                                for (Category c: selectedCategory.getSubcategories()) {
                                    names2.add(c.getName());
                                }

                                Spinner typeSpinner2 = findViewById(R.id.type2);
                                ArrayAdapter<String> adapter2 = new ArrayAdapter<>(ServiceSearchActivity.this, android.R.layout.simple_spinner_item, names2);
                                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                typeSpinner2.setAdapter(adapter2);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                    String eventId = (String) getIntent().getSerializableExtra("EVENT");

                    if(eventId != null && !eventId.isEmpty()) {
                        eventService.getById(eventId, new EventService.OnDataFetchListener() {
                            @Override
                            public void onSuccess(List<Event> events) {
                                ServiceSearchActivity.this.event = events.get(0);

                                EditText cityEditText = findViewById(R.id.city);
                                EditText maxKmEditText = findViewById(R.id.maxKm);

                                cityEditText.setText(event.getCity());
                                maxKmEditText.setText(String.valueOf(event.getMaxDistance()));

                                setEventTypeSpinnerSelection(ServiceSearchActivity.this.event.getEventType());
                            }

                            @Override
                            public void onFailure(String errorMessage) {
                            }
                        });
                    }

                }

                @Override
                public void onFailure(String errorMessage) {
                    // Handle failure
                }
            });
        });


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });


        String[] options3 = {"Select a service type", "Service", "Product", "Package"};

        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, options3);

        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner type3Spinner = findViewById(R.id.type3);
        type3Spinner.setAdapter(adapter3);

        Calendar calendar = Calendar.getInstance();
        int initialYear = calendar.get(Calendar.YEAR);
        int initialMonth = calendar.get(Calendar.MONTH);
        int initialDay = calendar.get(Calendar.DAY_OF_MONTH);

        TextView dateLabel = findViewById(R.id.dateLabel);
        ImageButton selectDateButton = findViewById(R.id.selectDateButton);

        selectDateButton.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    ServiceSearchActivity.this,
                    (view, year, month, dayOfMonth) -> {
                        String selectedDate = dayOfMonth + "/" + (month + 1) + "/" + year;
                        dateLabel.setText("Start date: " + selectedDate);
                    },
                    initialYear, initialMonth, initialDay
            );
            datePickerDialog.show();
        });

        TextView dateLabel1 = findViewById(R.id.dateLabel1);
        ImageButton selectDateButton1 = findViewById(R.id.selectDateButton1);

        selectDateButton1.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    ServiceSearchActivity.this,
                    (view, year, month, dayOfMonth) -> {
                        String selectedDate = dayOfMonth + "/" + (month + 1) + "/" + year;
                        dateLabel1.setText("End date: " + selectedDate);
                    },
                    initialYear, initialMonth, initialDay
            );
            datePickerDialog.show();
        });

        String selectedDate = initialDay + "/" + initialMonth + "/" + initialYear;
        dateLabel.setText("Start date: " + selectedDate);
        dateLabel1.setText("End date: " + selectedDate);

        TextView timeTitle = findViewById(R.id.timeTitle);

        type3Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Button createButton = findViewById(R.id.createButton);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) createButton.getLayoutParams();

                if (!parent.getItemAtPosition(position).equals("Service")) {
                    dateLabel.setVisibility(View.GONE);
                    selectDateButton.setVisibility(View.GONE);
                    dateLabel1.setVisibility(View.GONE);
                    selectDateButton1.setVisibility(View.GONE);
                    timeTitle.setVisibility(View.GONE);
                    params.addRule(RelativeLayout.BELOW, R.id.maxKm);
                } else {
                    dateLabel.setVisibility(View.VISIBLE);
                    selectDateButton.setVisibility(View.VISIBLE);
                    dateLabel1.setVisibility(View.VISIBLE);
                    selectDateButton1.setVisibility(View.VISIBLE);
                    timeTitle.setVisibility(View.VISIBLE);
                    params.addRule(RelativeLayout.BELOW, R.id.selectDateButton1);
                }
                createButton.setLayoutParams(params);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Button yourButton = findViewById(R.id.createButton);

        yourButton.setOnClickListener(v -> search());
    }

    private void search() {
        String name = ((EditText) findViewById(R.id.name)).getText().toString();
        String provider = ((EditText) findViewById(R.id.provider)).getText().toString();

        double minPrice;
        if(((EditText) findViewById(R.id.min)).getText().toString().isEmpty() || ((EditText) findViewById(R.id.min)).getText().toString() == null) {
            minPrice = -1;
        }
        else {
            minPrice = Double.parseDouble(((EditText) findViewById(R.id.min)).getText().toString());
        }

        double maxPrice;
        if(((EditText) findViewById(R.id.max)).getText().toString().isEmpty() || ((EditText) findViewById(R.id.max)).getText().toString() == null) {
            maxPrice = -1;
        }
        else {
            maxPrice = Double.parseDouble(((EditText) findViewById(R.id.max)).getText().toString());
        }

        String city = ((EditText) findViewById(R.id.city)).getText().toString();

        double maxDistance;
        if(((EditText) findViewById(R.id.maxKm)).getText().toString().isEmpty() || ((EditText) findViewById(R.id.maxKm)).getText().toString() == null) {
            maxDistance = -1;
        }
        else {
            maxDistance = Double.parseDouble(((EditText) findViewById(R.id.maxKm)).getText().toString());
        }

        String eventType;
        if(((Spinner) findViewById(R.id.type)).getSelectedItem().toString().equals("Select an event type")) {
            eventType = "";
        }
        else {
            eventType = ((Spinner) findViewById(R.id.type)).getSelectedItem().toString();
        }

        String serviceCategory;
        if(((Spinner) findViewById(R.id.type1)).getSelectedItem().toString().equals("Select a service category")) {
            serviceCategory = "";
        }
        else {
            serviceCategory = ((Spinner) findViewById(R.id.type1)).getSelectedItem().toString();
        }

        String serviceSubcategory;
        if(((Spinner) findViewById(R.id.type2)).getSelectedItem().toString().equals("Select a service subcategory")) {
            serviceSubcategory = "";
        }
        else {
            serviceSubcategory = ((Spinner) findViewById(R.id.type2)).getSelectedItem().toString();
        }

        String serviceType;
        if(((Spinner) findViewById(R.id.type3)).getSelectedItem().toString().equals("Select a service type")) {
            Toast.makeText(this, "Please select a service type", Toast.LENGTH_LONG).show();
            return;
        }
        else {
            serviceType = ((Spinner) findViewById(R.id.type3)).getSelectedItem().toString();
        }

        boolean available = ((Switch) findViewById(R.id.openToPublicSwitch)).isChecked();

        boolean favourite = ((Switch) findViewById(R.id.favSwitch)).isChecked();

        String startDateString = ((TextView) findViewById(R.id.dateLabel)).getText().toString().replace("Start date: ", "");
        String endDateString = ((TextView) findViewById(R.id.dateLabel1)).getText().toString().replace("End date: ", "");

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        Date startDate;
        Date endDate;
        try {
            startDate = sdf.parse(startDateString);
            endDate = sdf.parse(endDateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }

        Intent searchIntent = new Intent(this, ServiceSearchResultsActivity.class);
        searchIntent.putExtra("FAVOURITE", favourite);
        searchIntent.putExtra("NAME", name);
        searchIntent.putExtra("PROVIDER", provider);
        searchIntent.putExtra("MIN_PRICE", minPrice);
        searchIntent.putExtra("MAX_PRICE", maxPrice);
        searchIntent.putExtra("CITY", city);
        searchIntent.putExtra("MAX_DISTANCE", maxDistance);
        searchIntent.putExtra("EVENT_TYPE", eventType);
        searchIntent.putExtra("CATEGORY", serviceCategory);
        searchIntent.putExtra("SUBCATEGORY", serviceSubcategory);
        searchIntent.putExtra("TYPE", serviceType);
        searchIntent.putExtra("AVAILABLE", available);
        searchIntent.putExtra("START_DATE", startDate);
        searchIntent.putExtra("END_DATE", endDate);
        searchIntent.putExtra("email", user.getEmail());
        searchIntent.putExtra("userId", user.getId());
        searchIntent.putExtra("EVENT", selectedEventId);
        this.startActivity(searchIntent);
    }

    private void setEventTypeSpinnerSelection(EventType eventType) {
        Spinner typeSpinner = findViewById(R.id.type);
        for (int i = 0; i < typesList.size(); i++) {
            if (typesList.get(i).getName().equals(eventType.getName())) {
                typeSpinner.setSelection(i);
                break;
            }
        }
    }
}