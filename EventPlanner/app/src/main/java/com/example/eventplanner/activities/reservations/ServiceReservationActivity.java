package com.example.eventplanner.activities.reservations;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.Reservation;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.EventService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.ReservationService;
import com.example.eventplanner.services.ServiceService;
import com.example.eventplanner.services.UserService;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

public class ServiceReservationActivity extends AppCompatActivity {

    private TextInputEditText startDateInput, endDateInput, startTimeInput, endTimeInput;
    private Employee selectedEmployee;
    private List<Employee> employeeList;
    private Spinner employeeSpinner;
    private Service selectedService;
    private String selectedEventId = "";
    private Event selectedEvent;
    private List<Event> events;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_reservation);
        initViews();
        setupWindowInsets();
        setupDatePickers();
        setupTimePickers();
        setupSubmitButton();
        fetchEmployeeList();
        fetchEventData();
    }

    private void initViews() {
        startDateInput = findViewById(R.id.start_date_input);
        endDateInput = findViewById(R.id.end_date_input);
        startTimeInput = findViewById(R.id.start_time_input);
        endTimeInput = findViewById(R.id.end_time_input);
        employeeSpinner = findViewById(R.id.employee_spinner);
        selectedService = new Service();
        ServiceService serviceService = new ServiceService();
        String serviceId = (String) getIntent().getSerializableExtra("serviceId");
        serviceService.getService(serviceId).addOnCompleteListener(serviceTask -> {
            if (serviceTask.isSuccessful()) {
                selectedService = serviceTask.getResult();
            } else {
                Log.w("GET SERVICE", "get failed with ", serviceTask.getException());
            }
        });
    }

    private void setupWindowInsets() {
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void setupDatePickers() {
        startDateInput.setOnClickListener(v -> showDatePickerDialog(startDateInput, endDateInput));
        endDateInput.setOnClickListener(v -> showDatePickerDialog(endDateInput, null));
    }

    private void setupTimePickers() {
        startTimeInput.setOnClickListener(v -> showTimePickerDialog(startTimeInput));

        ServiceService serviceService = new ServiceService();
        String serviceId = (String) getIntent().getSerializableExtra("serviceId");
        serviceService.getService(serviceId).addOnCompleteListener(serviceTask -> {
            if (serviceTask.isSuccessful()) {
                selectedService = serviceTask.getResult();
                if (selectedService != null && selectedService.isAppointment()) {
                    endTimeInput.setVisibility(View.GONE);
                } else {
                    endTimeInput.setOnClickListener(v -> showTimePickerDialog(endTimeInput));
                }
            } else {
                Log.w("GET SERVICE", "get failed with ", serviceTask.getException());
            }
        });
    }

    private void setupSubmitButton() {
        MaterialButton submitButton = findViewById(R.id.submit_button);
        submitButton.setOnClickListener(v -> {
            String startDateText = Objects.requireNonNull(startDateInput.getText()).toString();
            String endDateText = Objects.requireNonNull(endDateInput.getText()).toString();
            String startTimeText = Objects.requireNonNull(startTimeInput.getText()).toString();
            String endTimeText = Objects.requireNonNull(endTimeInput.getText()).toString();

            if (startDateText.isEmpty() || endDateText.isEmpty() || startTimeText.isEmpty()) {
                Toast.makeText(this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    saveReservation(startDateText, endDateText, startTimeText, endTimeText);
                } catch (Exception e) {
                    Toast.makeText(this, "Error saving reservation", Toast.LENGTH_SHORT).show();
                    Log.e("SAVE RESERVATION", "Error saving reservation", e);
                }
            }
        });
    }

    private void saveReservation(String startDateText, String endDateText, String startTimeText, String endTimeText) {
        UserService userService = new UserService();
        String email = (String) getIntent().getSerializableExtra("EMAIL");
        String serviceId = (String) getIntent().getSerializableExtra("serviceId");

        userService.getByEmail(email).addOnCompleteListener(userTask -> {
            if (userTask.isSuccessful() && userTask.getResult() != null) {
                User user = userTask.getResult();
                try {
                    ServiceService serviceService = new ServiceService();
                    serviceService.getService(serviceId).addOnCompleteListener(serviceTask -> {
                        if (serviceTask.isSuccessful()) {
                            Service service = serviceTask.getResult();
                            if (service != null) {
                                selectedService = service;
                                makeReservation(user, startDateText, endDateText, startTimeText, endTimeText);
                                NotificationService notificationService = new NotificationService();
                                notificationService.add(selectedEmployee.getId(), user.getFullName() + " has successfully reserved a service with id: " + service.getId(), NotificationType.RESERVATION);
                            } else {
                                Log.w("GET SERVICE", "No such service");
                            }
                        } else {
                            Log.w("GET SERVICE", "get failed with ", serviceTask.getException());
                        }
                    });
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            } else {
                Toast.makeText(this, "Failed to retrieve user", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void makeReservation(User user, String startDateText, String endDateText, String startTimeText, String endTimeText) {
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        try {
            if (selectedEvent == null) {
                Toast.makeText(this, "Please select an event", Toast.LENGTH_SHORT).show();
                return;
            }

            String startDateTimeStr = startDateText + " " + startTimeText;
            String endDateTimeStr;

            if (selectedService.isAppointment()) {
                long startTimeMillis = Objects.requireNonNull(dateTimeFormat.parse(startDateTimeStr)).getTime();
                long endTimeMillis = startTimeMillis + (long) Integer.parseInt(selectedService.getAppointmentDuration()) * 60 * 60 * 1000; // Convert duration from hours to milliseconds
                java.util.Date endTime = new java.util.Date(endTimeMillis);
                endDateTimeStr = dateTimeFormat.format(endTime);
            } else {
                endDateTimeStr = endDateText + " " + endTimeText;
            }

            java.util.Date startDate = dateTimeFormat.parse(startDateTimeStr);
            java.util.Date endDate = dateTimeFormat.parse(endDateTimeStr);

            Reservation reservation = new Reservation(user, selectedEmployee, startDate, endDate, selectedService);
            reservation.setEventId(selectedEvent.getId());
            ReservationService reservationService = new ReservationService();
            reservationService.save(reservation).addOnCompleteListener(saveTask -> {
                if (saveTask.isSuccessful()) {
                    Toast.makeText(this, "Reservation submitted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Failed to save reservation", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            Toast.makeText(this, "Error making reservation", Toast.LENGTH_SHORT).show();
            Log.e("MAKE RESERVATION", "Error making reservation", e);
        }
    }

    private void fetchEmployeeList() {
        EmployeeService employeeService = new EmployeeService();
        employeeService.getAll().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                employeeList = task.getResult();
                setupEmployeeSpinner();
            } else {
                Toast.makeText(this, "Failed to fetch employee list", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupEmployeeSpinner() {
        List<String> employeeNames = new ArrayList<>();
        for (Employee employee : employeeList) {
            employeeNames.add(employee.getFullName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, employeeNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        employeeSpinner.setAdapter(adapter);

        employeeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                selectedEmployee = employeeList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Do nothing
            }
        });
    }

    private void showDatePickerDialog(TextInputEditText startDateInput, TextInputEditText endDateInput) {
        CalendarConstraints.Builder constraintsBuilder = new CalendarConstraints.Builder();
        MaterialDatePicker.Builder<androidx.core.util.Pair<Long, Long>> builder = MaterialDatePicker.Builder.dateRangePicker();
        builder.setTitleText("Select Date Range");
        builder.setCalendarConstraints(constraintsBuilder.build());

        MaterialDatePicker<androidx.core.util.Pair<Long, Long>> datePicker = builder.build();
        datePicker.show(getSupportFragmentManager(), datePicker.toString());

        datePicker.addOnPositiveButtonClickListener(selection -> {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            startDateInput.setText(sdf.format(selection.first));
            if (endDateInput != null) {
                endDateInput.setText(sdf.format(selection.second));
            }
        });
    }

    private void showTimePickerDialog(TextInputEditText timeInput) {
        int hour = 12;
        int minute = 0;
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, (view, hourOfDay, minuteOfHour) -> {
            String time = String.format(Locale.getDefault(), "%02d:%02d", hourOfDay, minuteOfHour);
            timeInput.setText(time);

            if (selectedService != null && !selectedService.isAppointment()) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    String startTimeText = Objects.requireNonNull(timeInput.getText()).toString();
                    long startTimeMillis = Objects.requireNonNull(sdf.parse(startTimeText)).getTime();
                    long endTimeMillis = startTimeMillis + Integer.parseInt(selectedService.getAppointmentDuration()) * 60000L; // Convert duration from minutes to milliseconds
                    String endTimeText = sdf.format(endTimeMillis);
                    endTimeInput.setText(endTimeText);
                } catch (Exception e) {
                    Log.e("TIME PICKER", "Error calculating end time", e);
                }
            }
        }, hour, minute, true);
        timePickerDialog.show();
    }

    private void fetchEventData() {
        EventService eventService = new EventService(new EventRepository());

        String email = (String) getIntent().getSerializableExtra("EMAIL");
        selectedEventId = (String) getIntent().getSerializableExtra("EVENT");

        eventService.getAll(new EventService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events1) {
                events = events1.stream().filter(e -> e.getUserEmail().equals(email)).collect(Collectors.toList());

                List<String> names = new ArrayList<>();
                names.add("Select an event");
                for (Event e : events) {
                    names.add("Selected event: " + e.getName());
                }

                Spinner eventSpinner = findViewById(R.id.event_spinner);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(ServiceReservationActivity.this, android.R.layout.simple_spinner_item, names);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                eventSpinner.setAdapter(adapter);

                for (int i = 0; i < events.size(); i++) {
                    if (events.get(i).getId().equals(selectedEventId)) {
                        eventSpinner.setSelection(i + 1);
                        selectedEvent = events.get(i);
                        break;
                    } else {
                        eventSpinner.setSelection(0);
                        selectedEvent = null;
                    }
                }

                eventSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String eventName = eventSpinner.getSelectedItem().toString().trim();
                        if (eventName.equals("Select an event")) {
                            selectedEventId = "";
                            selectedEvent = null;
                        } else {
                            selectedEvent = events.stream().filter(e -> e.getName().equals(eventName.substring(16))).findFirst().orElse(null);
                            assert selectedEvent != null;
                            selectedEventId = selectedEvent.getId();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });
    }
}
