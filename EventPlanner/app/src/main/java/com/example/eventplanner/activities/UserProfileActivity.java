package com.example.eventplanner.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.MainActivity;
import com.example.eventplanner.R;
import com.example.eventplanner.activities.employees.EmployeeDetailsActivity;
import com.example.eventplanner.adapters.EmployeeAdapter;
import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.NotificationType;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Product;
import com.example.eventplanner.models.Report;
import com.example.eventplanner.models.Reservation;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.NotificationService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.PackageService;
import com.example.eventplanner.services.ProductService;
import com.example.eventplanner.services.ReportService;
import com.example.eventplanner.services.ReservationService;
import com.example.eventplanner.services.ServiceService;
import com.example.eventplanner.services.UserService;
import com.google.firebase.Timestamp;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class UserProfileActivity extends AppCompatActivity {

    private UserService userService = new UserService();
    private OwnerService ownerService = new OwnerService();
    private ReportService reportService = new ReportService();
    private ReservationService reservationService = new ReservationService();
    private EmployeeService employeeService = new EmployeeService();
    private ServiceService serviceService = new ServiceService();
    private ProductService productService = new ProductService();
    private PackageService packageService = new PackageService(new PackageRepository());
    private User user;
    private String forReport;
    private NotificationService notificationService = new NotificationService();

    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_user_profile);

        Intent intent = getIntent();
        email = intent.getStringExtra("EMAIL");
        forReport = intent.getStringExtra("ForReport");

        userService.getByEmail(email).addOnCompleteListener(this, task->{
            if(task.isSuccessful()){
                user = task.getResult();
                bind();

                Button editButton = findViewById(R.id.edit);
                Button reportButton = findViewById(R.id.report);
                Intent editIntent = new Intent(this, EditProfileActivity.class);
                editIntent.putExtra("EMAIL", email);
                editButton.setOnClickListener(v -> startActivity(editIntent));

                Button passwordButton = findViewById(R.id.edit_password);
                Intent passwordIntent = new Intent(this, ChangePasswordActivity.class);
                passwordIntent.putExtra("EMAIL", email);
                passwordButton.setOnClickListener(v -> startActivity(passwordIntent));
                if(forReport.equals("yes")){
                    editButton.setVisibility(View.GONE);
                    passwordButton.setVisibility(View.GONE);
                    //getting reporting entity data
                    String reportingId = intent.getStringExtra("ReportingId");
                    reportButton.setOnClickListener(v ->{
                        reportUser(reportingId);
                    });
                } else if (forReport.equals("view")) {
                    editButton.setVisibility(View.GONE);
                    passwordButton.setVisibility(View.GONE);
                    //getting reporting entity data
                    reportButton.setVisibility(View.GONE);
                } else{
                    reportButton.setVisibility(View.GONE);
                }

                ownerService.getAll().addOnCompleteListener(employeeTask -> {
                    if (employeeTask.isSuccessful()) {
                        Owner owner = employeeTask.getResult().stream().filter(o -> o.getEmail().equals(email)).findFirst().orElse(null);

                        Button companyButton = findViewById(R.id.company);

                        if(owner == null) {
                            companyButton.setVisibility(View.GONE);
                        }
                        else {
                            Intent companyIntent = new Intent(this, CompanyDetailsOwnerActivity.class);
                            companyIntent.putExtra("EMAIL", email);
                            companyIntent.putExtra("COMPANY", owner.getCompanyId());
                            companyButton.setOnClickListener(v -> startActivity(companyIntent));
                        }
                    }
                });

                Button deactivateButton = findViewById(R.id.deactivate);
                deactivateButton.setOnClickListener(l -> {
                    deactivateAccount();
                });
                if(user.getRole().equals(UserRole.ADMIN) || user.getRole().equals(UserRole.EMPLOYEE)) {
                    deactivateButton.setVisibility(View.GONE);
                }
            }
        });
    }

    private void bind() {
        TextView firstName, lastName, email, phone, address;

        firstName = findViewById(R.id.first_name);
        lastName = findViewById(R.id.last_name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        address = findViewById(R.id.address);

        firstName.setText("First name: " + user.getFirstName());
        lastName.setText("Last name: " + user.getLastName());
        email.setText("Email: " + user.getEmail());
        phone.setText("Phone number: " + user.getPhone());
        address.setText("Address: " + user.getAddress());
    }

    private void reportUser(String reportingId){
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.report_form, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;

        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        EditText reason = popupView.findViewById(R.id.report_reason);

        //show window
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.report_save_btn);
        Button cancelButton = popupView.findViewById(R.id.report_cancel_btn);

        saveButton.setOnClickListener(v -> {
            userService.get(reportingId).addOnCompleteListener(this, task ->{
                if(task.isSuccessful()){
                    User reportingUser = task.getResult();
                    Report report  = new Report();
                    report.setReason(reason.getText().toString());
                    report.setReportedId(user.getId());
                    report.setReporterId(reportingId);
                    report.setReportedUsername(user.getEmail());
                    report.setReporterUsername(reportingUser.getEmail());
                    report.setReportTimestamp(Timestamp.now());
                    report.setForCompany(false);
                    report.setReportType(Report.REPORT_TYPE.Reported);

                    reportService.getExisting(report.getReporterId(), report.getReportedId(), new ReportService.OnDataFetchListener() {
                        @Override
                        public void onSuccess(List<Report> reports) {
                            if(reports.isEmpty()){
                                reportService.create(report);
                                sendNotification(report.getReporterUsername(), report.getReportedUsername());
                                popupWindow.dismiss();
                            }else{
                                Toast.makeText(UserProfileActivity.this, "You have already reported this user.", Toast.LENGTH_SHORT).show();
                                popupWindow.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(String errorMessage) {
                            Toast.makeText(UserProfileActivity.this, "Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                            popupWindow.dismiss();
                        }
                    });
                }
            });




        });

        cancelButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });
    }

    private void sendNotification(String reporterUsername, String reportedUsername){
        StringBuilder sb = new StringBuilder();
        sb.append(reporterUsername).append(" has just reported ").append(reportedUsername).append(".");
        notificationService.add(null, sb.toString(), NotificationType.ADMIN);
    }

    private void deactivateAccount() {
        if(user.getRole().equals(UserRole.EVENT_PLANNER)) {
            reservationService.getAllByEventPlanner(user.getId()).addOnCompleteListener(task->{
                if(task.isSuccessful()) {
                    List<Reservation> reservations = task.getResult();
                    reservations = reservations.stream().filter(r -> r.getTo().after(new Date())).collect(Collectors.toList());

                    if(reservations.isEmpty()) {
                        user.setDeactivated(true);
                        userService.update(user);

                        Intent intent = new Intent(UserProfileActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Toast.makeText(this, "You can't deactivate your account because of your reservations", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else if(user.getRole().equals(UserRole.OWNER)) {
            ownerService.get(user.getId()).addOnCompleteListener(task1 -> {
                if (task1.isSuccessful()) {
                    Owner owner = task1.getResult();

                    reservationService.getActiveByCompanyId(owner.getCompanyId()).addOnCompleteListener(task2->{
                        if(task2.isSuccessful()) {
                            List<Reservation> reservations = task2.getResult();
                            reservations = reservations.stream().filter(r -> r.getTo().after(new Date())).collect(Collectors.toList());

                            if(reservations.isEmpty()) {
                                user.setDeactivated(true);
                                userService.update(user);

                                employeeService.getAllByCompany(owner.getCompanyId()).addOnCompleteListener(task3 -> {
                                    if (task3.isSuccessful()) {
                                        List<Employee> employees = task3.getResult();

                                        for(Employee employee : employees) {
                                            userService.getByEmail(email).addOnCompleteListener(this, task4->{
                                                if(task4.isSuccessful()){
                                                    User employeeToDeactivate = task4.getResult();
                                                    employeeToDeactivate.setDeactivated(true);
                                                    userService.update(employeeToDeactivate);
                                                }
                                            });
                                        }
                                    }
                                });

                                serviceService.getAll(new ServiceService.OnDataFetchListener() {
                                    @Override
                                    public void onSuccess(List<Service> services) {
                                        for (Service service : services) {
                                            service.setVisible(false);
                                            serviceService.update(service);
                                        }
                                    }

                                    @Override
                                    public void onFailure(String errorMessage) {
                                    }
                                });

                                productService.getAll(new ProductService.OnDataFetchListener() {
                                    @Override
                                    public void onSuccess(List<Product> products) {
                                        for (Product product : products) {
                                            product.setVisible(false);
                                            productService.update(product);
                                        }
                                    }

                                    @Override
                                    public void onFailure(String errorMessage) {
                                    }
                                });

                                packageService.getAll(new PackageService.OnDataFetchListener() {
                                    @Override
                                    public void onSuccess(List<Package> packages) {
                                        for (Package aPackage : packages) {
                                            aPackage.setVisible(false);
                                            packageService.update(aPackage);
                                        }
                                    }

                                    @Override
                                    public void onFailure(String errorMessage) {
                                    }
                                });

                                Intent intent = new Intent(UserProfileActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                Toast.makeText(this, "You can't deactivate your account because of your reservations", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            });
        }
    }
}