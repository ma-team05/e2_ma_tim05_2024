package com.example.eventplanner.activities.registration;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.R;

import java.util.ArrayList;
import java.util.List;

public class OwnersCompanyRegistrationStep1Activity extends AppCompatActivity {

    private Spinner spinnerCompanyType;
    private EditText editTextCompanyEmail, editTextCompanyName, editTextCompanyAddress,
            editTextCompanyPhoneNumber, editTextCompanyDescription;

    private EditText editTextCategories;
    private List<String> enteredTextList;
    private ArrayAdapter<String> spinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owners_company_registration_step1);

        initializeViews();
        setupEventTypesCheckboxes();
        setupCompanyTypeSpinner();
        setupAddCategoriesButton();

        // Retrieve owner's personal info from intent extras
        Intent intent = getIntent();
        String role = intent.getStringExtra("role");
        String email = intent.getStringExtra("email");
        String firstName = intent.getStringExtra("firstName");
        String lastName = intent.getStringExtra("lastName");
        String address = intent.getStringExtra("address");
        String phoneNumber = intent.getStringExtra("phoneNumber");
        String password = intent.getStringExtra("password");

        // Pass the owner's personal info to step 2 activity
        Button buttonNext = findViewById(R.id.buttonRegisterCompany);
        buttonNext.setOnClickListener(v -> {
            Intent intentStep2 = new Intent(OwnersCompanyRegistrationStep1Activity.this, OwnersCompanyRegistrationStep2Activity.class);
            intentStep2.putExtra("role", role);
            intentStep2.putExtra("email", email);
            intentStep2.putExtra("firstName", firstName);
            intentStep2.putExtra("lastName", lastName);
            intentStep2.putExtra("address", address);
            intentStep2.putExtra("phoneNumber", phoneNumber);
            intentStep2.putExtra("password", password);
            intentStep2.putExtra("companyType", spinnerCompanyType.getSelectedItem().toString().trim());
            intentStep2.putExtra("companyEmail", editTextCompanyEmail.getText().toString().trim());
            intentStep2.putExtra("companyName", editTextCompanyName.getText().toString().trim());
            intentStep2.putExtra("companyAddress", editTextCompanyAddress.getText().toString().trim());
            intentStep2.putExtra("companyPhoneNumber", editTextCompanyPhoneNumber.getText().toString().trim());
            intentStep2.putExtra("companyDescription", editTextCompanyDescription.getText().toString().trim());
            startActivity(intentStep2);
        });
    }


    private void initializeViews() {
        spinnerCompanyType = findViewById(R.id.spinnerCompanyType);
        editTextCompanyEmail = findViewById(R.id.editTextCompanyEmail);
        editTextCompanyName = findViewById(R.id.editTextCompanyName);
        editTextCompanyAddress = findViewById(R.id.editTextCompanyAddress);
        editTextCompanyPhoneNumber = findViewById(R.id.editTextCompanyPhoneNumber);
        editTextCompanyDescription = findViewById(R.id.editTextCompanyDescription);
        editTextCategories = findViewById(R.id.editTextCategories);
    }

    private void setupEventTypesCheckboxes() {
        LinearLayout layoutCheckboxes = findViewById(R.id.layoutCheckboxes);
        String[] eventTypes = getResources().getStringArray(R.array.event_types_array);
        for (String eventType : eventTypes) {
            CheckBox checkBox = new CheckBox(this);
            checkBox.setText(eventType);
            layoutCheckboxes.addView(checkBox);
        }
    }

    private void setupCompanyTypeSpinner() {
        Spinner spinnerEnteredText = findViewById(R.id.spinnerEnteredText);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,
                R.array.company_types_array,
                android.R.layout.simple_spinner_item
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCompanyType.setAdapter(adapter);

        enteredTextList = new ArrayList<>();
        spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, enteredTextList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEnteredText.setAdapter(spinnerAdapter);
    }

    private void setupAddCategoriesButton() {
        Button buttonAddText = findViewById(R.id.buttonAddText);
        buttonAddText.setOnClickListener(v -> {
            String enteredText = editTextCategories.getText().toString().trim();
            if (!enteredText.isEmpty()) {
                enteredTextList.add(enteredText);
                spinnerAdapter.notifyDataSetChanged();
                editTextCategories.setText("");
            }
        });
    }
}