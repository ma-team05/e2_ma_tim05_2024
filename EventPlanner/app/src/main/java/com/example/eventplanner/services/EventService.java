package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.EventRepository;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Event;
import com.example.eventplanner.models.EventStatus;

import java.util.Date;
import java.util.List;

public class EventService {
    private final EventRepository repository;

    public EventService(EventRepository repository) {
        this.repository = repository;
    }

    public EventService() {
        repository = new EventRepository();
    }

    public interface OnDataFetchListener {
        void onSuccess(List<Event> objects);
        void onFailure(String errorMessage);
    }

    public void create(Event event) {
        repository.save(event);
    }

    public void getAll(final OnDataFetchListener listener) {
        repository.getAll(new EventRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }
    public void getAllUnassigned(final OnDataFetchListener listener) {
        repository.getAllUnassigned(new EventRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }
    public void getAllUnassignedInWeek(Date start, Date end, final OnDataFetchListener listener) {
        repository.getAllUnassignedInWeek(start, end, new EventRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void getAllByEmployee(String employeeId, final OnDataFetchListener listener) {
        repository.getAllByEmployee(employeeId, new EventRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }
    public void getAllByEmployeeInWeek(String employeeId, Date start, Date end, final OnDataFetchListener listener) {
        repository.getAllByEmployeeInWeek(employeeId, start, end,new EventRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void getById(String eventId, final OnDataFetchListener listener) {
        repository.getById(eventId, new EventRepository.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Event> events) {
                listener.onSuccess(events);
            }

            @Override
            public void onFailure(String errorMessage) {
                listener.onFailure(errorMessage);
            }
        });
    }

    public void update(Event event) {
        repository.update(event);
    }
    public void assignToEmployee(Event event, String employeeId){
        event.setEmployeeId(employeeId);
        repository.update(event);
    }
    public void assignAndTake(Event event, String employeeId){
        if(event.getStatus()== EventStatus.TAKEN) return;
        if(event.getEmployeeId()!=null) return;
        event.setStatus(EventStatus.TAKEN);
        event.setEmployeeId(employeeId);
        repository.update(event);
    }
    public void take(Event event){
        if(event.getEmployeeId()==null) return;
        if(event.getStatus()== EventStatus.RESERVED)
            event.setStatus(EventStatus.TAKEN);
        else return;
        repository.update(event);
    }

    public void delete(String eventId) { repository.delete(eventId); }
}
