package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Company;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.UserService;

public class EditCompanyActivity extends AppCompatActivity {
    private Company company;
    private String email;
    private CompanyService companyService = new CompanyService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_edit_company);

        Intent intent = getIntent();
        String companyId = intent.getStringExtra("COMPANY");
        email = intent.getStringExtra("EMAIL");

        companyService.get(companyId).addOnCompleteListener(companyTask -> {
            if (companyTask.isSuccessful()) {
                company = companyTask.getResult();
                bind();
            }
        });

        Button editButton = findViewById(R.id.edit);

        editButton.setOnClickListener(v -> {
            validateFields();
        });
    }

    private void bind() {
        EditText descriptionET, addressET, phoneET;

        descriptionET = findViewById(R.id.description);
        addressET = findViewById(R.id.address);
        phoneET = findViewById(R.id.phone);

        descriptionET.setText(company.getDescription());
        addressET.setText(company.getAddress());
        phoneET.setText(company.getPhoneNumber());
    }

    private boolean validateFields() {
        boolean isValid = true;

        EditText descriptionET, addressET, phoneET;

        descriptionET = findViewById(R.id.description);
        addressET = findViewById(R.id.address);
        phoneET = findViewById(R.id.phone);

        String descriptioon = descriptionET.getText().toString().trim();
        String address = addressET.getText().toString().trim();
        String phone = phoneET.getText().toString().trim();

        if (TextUtils.isEmpty(descriptioon)) {
            descriptionET.setError("Please enter a descriptioon");
            descriptionET.requestFocus();
            isValid = false;
        }

        if (TextUtils.isEmpty(address)) {
            addressET.setError("Please enter an address");
            addressET.requestFocus();
            isValid = false;
        }

        if (TextUtils.isEmpty(phone)) {
            phoneET.setError("Please enter a phone number");
            phoneET.requestFocus();
            isValid = false;
        }
        if(isValid) {
            editEvent();
        }

        return isValid;
    }

    private void editEvent() {
        EditText descriptionET, addressET, phoneET;

        descriptionET = findViewById(R.id.description);
        addressET = findViewById(R.id.address);
        phoneET = findViewById(R.id.phone);

        String descriptioon = descriptionET.getText().toString().trim();
        String address = addressET.getText().toString().trim();
        String phone = phoneET.getText().toString().trim();

        company.setDescription(descriptioon);
        company.setAddress(address);
        company.setPhoneNumber(phone);

        companyService.update(company);

        String eventDetails = "Company updated";
        Toast.makeText(this, eventDetails, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, CompanyDetailsOwnerActivity.class);
        intent.putExtra("email", email);
        intent.putExtra("EMAIL", email);
        intent.putExtra("COMPANY", company.getId());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        finish();
    }
}