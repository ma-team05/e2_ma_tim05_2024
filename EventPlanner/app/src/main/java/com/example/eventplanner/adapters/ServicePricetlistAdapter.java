package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.PackageService;
import com.example.eventplanner.services.ServiceService;

import java.util.List;

public class ServicePricetlistAdapter extends  RecyclerView.Adapter<ServicePricetlistAdapter.ProductViewHolder>{
    private Context context;
    private List<Service> services;
    private ServiceService serviceService;
    private double packagePrice;
    private PackageService packageService;

    UserRole userRole;

    public ServicePricetlistAdapter(Context context, List<Service> services, ServiceService serviceService, UserRole userRole) {
        this.context = context;
        this.services = services;
        this.serviceService = serviceService;
        this.userRole = userRole;
        packageService = new PackageService(new PackageRepository());


        Log.i("Frag","Napravio adapter");
    }

    @NonNull
    @Override
    public ServicePricetlistAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pricelist, parent, false);
        return new ServicePricetlistAdapter.ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicePricetlistAdapter.ProductViewHolder holder, int position) {
        Service product = services.get(position);
        holder.bind(product);
    }
    @Override
    public int getItemCount() {
        return services.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        ImageButton editServiceBtn;
        TextView serviceName, servicePrice, serviceDiscount, serviceDiscountPrice, serviceOridinalNumber;
        private Service currentService;
        private int currentIndex;
        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            editServiceBtn = itemView.findViewById(R.id.pricelist_item_edit_btn);

            serviceOridinalNumber = itemView.findViewById(R.id.pricelist_item_oridinalNumber);
            serviceName = itemView.findViewById(R.id.pricelist_item_name);
            servicePrice = itemView.findViewById(R.id.pricelist_item_price);
            serviceDiscount = itemView.findViewById(R.id.pricelist_item_discount);
            serviceDiscountPrice = itemView.findViewById(R.id.pricelist_item_newPrice);


            editServiceBtn.setOnClickListener(v -> {
                if(userRole == UserRole.OWNER ){
                    if(currentService.isPending()){
                        Toast.makeText(context, "Product creation is pending - new subcategory.", Toast.LENGTH_SHORT).show();
                    }else{
                        editProductPopup(currentService);
                    }

                }else{
                    Toast.makeText(context, "Your have no permission for this action", Toast.LENGTH_SHORT).show();
                }

            });
        }

        public void bind(Service service) {
            currentService = service;
            currentIndex = 0;
            boolean ind = service != null;
            currentIndex = services.indexOf(service);
            currentIndex++;
            Log.i("binder",ind? "nije null": "null je");
            serviceName.setText(service.getName());
            serviceOridinalNumber.setText(String.valueOf(currentIndex));
            servicePrice.setText(String.valueOf(service.getPrice()));
            serviceDiscount.setText(String.valueOf(service.getDiscount()));

            if(currentService.isAppointment()){
                double finalprice = service.getPrice() - service.getDiscount()*service.getPrice()/100;
                double hours = Double.parseDouble(currentService.getAppointmentDuration());
                finalprice = finalprice*hours;
                serviceDiscountPrice.setText(String.valueOf(finalprice));
            }else{
                serviceDiscountPrice.setText("Depends on duration");
            }

        }

        @SuppressLint({"NotifyDataSetChanged"})
        private void editProductPopup(Service service) {
            @SuppressLint("InflateParams") View popupView = LayoutInflater.from(context).inflate(R.layout.edit_pricelist, null);

            // Create a PopupWindow
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            boolean focusable = true;

            PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
            EditText price = popupView.findViewById(R.id.edit_pricelist_price);
            price.setText(String.valueOf(service.getPrice()));

            EditText discount = popupView.findViewById(R.id.edit_pricelist_discount);
            discount.setText(String.valueOf(service.getDiscount()));


            //show window
            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

            Button saveButton = popupView.findViewById(R.id.buttonSave_edit_pricelist);
            Button cancelButton = popupView.findViewById(R.id.buttonCancel_edit_pricelist);

            saveButton.setOnClickListener(v -> {
                //preuzimanje vrijesnosti spinera
                service.setPrice(Double.parseDouble(price.getText().toString()));
                service.setDiscount(Double.parseDouble(discount.getText().toString()));

                serviceService.update(service);

                packageService.getUndeletedWithService(service.getCompanyId(), service.getId(), new PackageService.OnDataFetchListener() {
                    @Override
                    public void onSuccess(List<Package> objects) {
                        Log.i("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa",String.valueOf(objects.size()));

                        for(Package obj : objects){
                            obj.getServicesHashMap().put(service.getId(), service);
                            setPackagePrice(obj);
                        }
                        packageService.updatePackagesInFirestore(objects);
                    }

                    @Override
                    public void onFailure(String errorMessage) {

                    }
                });
                notifyDataSetChanged();
                popupWindow.dismiss();

            });

            cancelButton.setOnClickListener(v -> {

                popupWindow.dismiss();

            });
        }

        private void setPackagePrice(Package newPackage) {
            newPackage.setPrice(0);
            packagePrice = 0;
            if (newPackage.getProductsHashMap() != null) {
                newPackage.getProductsHashMap().forEach((key, value) -> {
                    packagePrice += (value.getPrice() - ( value.getPrice() * value.getDiscount() / 100));
                });
            }

            if (newPackage.getServicesHashMap() != null) {
                newPackage.getServicesHashMap().forEach((key, value) -> {
                    if(value.isAppointment()){
                        double duration = Double.parseDouble(value.getAppointmentDuration());
                        packagePrice += (value.getPrice() - (value.getPrice() * value.getDiscount() / 100))*duration;
                    }else{
                        double duration = Double.parseDouble(value.getMinDuration());
                        packagePrice += (value.getPrice() - (value.getPrice() * value.getDiscount() / 100))*duration;
                    }

                });
            }
            newPackage.setPrice(packagePrice);

        }


    }

}
