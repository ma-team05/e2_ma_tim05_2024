package com.example.eventplanner.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Company;
import com.example.eventplanner.models.Grade;
import com.example.eventplanner.services.CompanyService;
import com.example.eventplanner.services.GradeService;

import java.util.Date;

public class GradingCompanyActivity extends AppCompatActivity {

    private String companyId;
    private Company company;
    private CompanyService companyService = new CompanyService();
    private GradeService gradeService = new GradeService();
    private String userId;
    private String reservationId;
    @Override //"userId" i "companyId"
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_grading_company);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.gradingCompany), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        Intent intent = getIntent();
        companyId = intent.getStringExtra("companyId");
        userId = intent.getStringExtra("userId");
        reservationId = intent.getStringExtra("reservationId");
        //temporary
        if(companyId==null || companyId.isEmpty()){
            companyId = "QFVNleTxOAXdvrDCx7F5";
        }
        if(userId==null || userId.isEmpty()){
            userId = "JrZKxjHLXGDKZDmMEeYG";
        }
        companyService.get(companyId).addOnCompleteListener(task->{
            if(task.isSuccessful()) {
                company = task.getResult();
                TextView label = this.findViewById(R.id.labelGradeCompany);
                label.setText("Grade "+company.getName()+" company");
            }
        });
        Button submitButton = this.findViewById(R.id.buttonSubmit);
        submitButton.setOnClickListener(v->{
            RadioGroup rg = findViewById(R.id.radioGroupGrades);
            int selectedId = rg.getCheckedRadioButtonId();
            if (selectedId != -1) {
                RadioButton selectedRadioButton = findViewById(selectedId);
                int selectedGrade = Integer.parseInt(selectedRadioButton.getText().toString());
                EditText commentEdittext = findViewById(R.id.editTextComment);
                String comment = commentEdittext.getText().toString();
                if(comment==null || comment.isEmpty()){
                    Toast.makeText(this, "No comment added", Toast.LENGTH_SHORT).show();
                    return;
                }
                gradeService.addGrade(new Grade("", companyId, selectedGrade, comment, new Date(), userId)).addOnCompleteListener(task->{
                    if(task.isSuccessful()) {
                        Toast.makeText(this, "Rating added successfully", Toast.LENGTH_SHORT).show();
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("reservationId", reservationId);
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                    }
                    else
                        Toast.makeText(this, "Error adding grade", Toast.LENGTH_SHORT).show();
                });
            } else {
                Toast.makeText(this, "No rating selected", Toast.LENGTH_SHORT).show();
            }
        });
    }
}