package com.example.eventplanner.models;

public enum ReservationStatus {
    NEW,
    CANCELED_BY_PUP,
    CANCELED_BY_EVENT_PLANNER,
    CANCELED_BY_ADMIN,
    ACCEPTED,
    FINISHED,
    REJECTED
}
