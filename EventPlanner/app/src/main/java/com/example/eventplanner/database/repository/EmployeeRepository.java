package com.example.eventplanner.database.repository;

import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.Package;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EmployeeRepository {
    private final CollectionReference employeesCollection;

    public EmployeeRepository(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        employeesCollection = db.collection("employees");
    }

    public Task<Void> saveEmployee(Employee employee, String userId){
        DocumentReference newEmployeeRef = employeesCollection.document(userId);
        employee.setId(userId);
        return newEmployeeRef.set(employee);
    }


    public Task<Void> updateEmployee(Employee employee){
        DocumentReference employeeRef = employeesCollection.document(employee.getId());
        return employeeRef.set(employee);
    }

    public Task<Void> deleteEmployee(Employee employee){
        DocumentReference employeeRef = employeesCollection.document(employee.getId());
        return employeeRef.delete();
    }

    public Task<List<Employee>> getAll(){
        return employeesCollection.get()
                .continueWith(task -> {
                    List<Employee> employees = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Employee employee = document.toObject(Employee.class);
                        employees.add(employee);
                    }
                    return employees;
                });
    }

    public Task<List<Employee>> getAllByCompany(String companyId){
        return employeesCollection.get()
                .continueWith(task -> {
                    List<Employee> employees = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Employee employee = document.toObject(Employee.class);
                        assert employee != null;
                        if(employee.getCompanyId()==null)
                            continue;
                        if(employee.getCompanyId().equals(companyId))
                            employees.add(employee);
                    }
                    return employees;
                });
    }

    public Task<Employee> getById(String id) {
        DocumentReference employeeRef = employeesCollection.document(id);
        return employeeRef.get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            return document.toObject(Employee.class);
                        } else {
                            throw new Exception("Employee with ID " + id + " not found");
                        }
                    } else {
                        throw Objects.requireNonNull(task.getException()); // Propagate the exception
                    }
                });
    }

    public void updateBatchOfEmployees(List<Employee> employees) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        WriteBatch batch = db.batch();

        for (Employee pkg : employees) {
            // Get the document reference for this package
            DocumentReference packageRef = db.collection("employees").document(pkg.getId());

            // Update the package in the batch
            batch.set(packageRef, pkg);
        }

        batch.commit().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                // Batch update succeeded
                System.out.println("Batch update succeeded.");
            } else {
                // Batch update failed
                System.err.println("Batch update failed: " + task.getException());
            }
        });
    }

}
