package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.NotificationRepository;
import com.example.eventplanner.models.Notification;
import com.example.eventplanner.models.NotificationType;
import com.google.android.gms.tasks.Task;

import java.util.List;

public class NotificationService {
    private final NotificationRepository notificationRepository;

    public NotificationService() { notificationRepository = new NotificationRepository(); }

    public Task<String> save(Notification notification) { return notificationRepository.save(notification); }
    public Task<Void> add(String userId, String description, NotificationType type){
        Notification notification = new Notification(userId,description,type);
        return notificationRepository.addNotification(notification);
    }
    public Task<Void> update(Notification notification) { return notificationRepository.update(notification); }
    public Task<List<Notification>> getNotifications() {
        return notificationRepository.getNotifications();
    }
    public Task<List<Notification>> getNotificationsByUser(String userId){
        return notificationRepository.getNotificationsByUser(userId);
    }
}
