package com.example.eventplanner.models;

import java.util.List;

public class Employee extends User{
    private List<EventType> Utilities;
    private List<Event> Events;
    private TimeSchedule timeSchedule;
    private String companyId;
    public Employee(){super();}
    public Employee(String id, String firstName, String lastName, String email, String phone, String address, String picture, String password, TimeSchedule timeschedule, String companyId){
        super(id, firstName, lastName, email, phone, address, picture, UserRole.EMPLOYEE, password, true);
        timeSchedule = timeschedule;
        this.companyId = companyId;
    }

    public List<EventType> getUtilities() {
        return Utilities;
    }

    public void setUtilities(List<EventType> utilities) {
        Utilities = utilities;
    }

    public List<Event> getEvents() {
        return Events;
    }

    public void setEvents(List<Event> events) {
        Events = events;
    }

    public TimeSchedule getTimeSchedule() {
        return timeSchedule;
    }

    public void setTimeSchedule(TimeSchedule timeSchedule) {
        this.timeSchedule = timeSchedule;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getName(){
        return getFirstName();
    }
    public String getFullName() { return getFirstName() + " " + getLastName(); }
}
