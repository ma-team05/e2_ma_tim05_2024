package com.example.eventplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.models.Category;

import java.util.List;

public class SubcategoryRequestsAdapter extends RecyclerView.Adapter<SubcategoryRequestsAdapter.SubcategoryRequestViewHolder> {

    private List<Category> subcategoryRequests;
    private OnApproveRejectClickListener listener;

    public SubcategoryRequestsAdapter(List<Category> subcategoryRequests, OnApproveRejectClickListener listener) {
        this.subcategoryRequests = subcategoryRequests;
        this.listener = listener;
    }

    public interface OnApproveRejectClickListener {
        void onApproveClick(Category category);
        void onRejectClick(Category category);
    }

    @NonNull
    @Override
    public SubcategoryRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_subcategory_request, parent, false);
        return new SubcategoryRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubcategoryRequestViewHolder holder, int position) {
        Category subcategoryRequest = subcategoryRequests.get(position);
        holder.bind(subcategoryRequest);
    }

    @Override
    public int getItemCount() {
        return subcategoryRequests.size();
    }

    public class SubcategoryRequestViewHolder extends RecyclerView.ViewHolder {

        private final TextView textViewSubcategoryName;
        private final TextView textViewSubcategoryDescription;

        public SubcategoryRequestViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewSubcategoryName = itemView.findViewById(R.id.textViewSubcategoryName);
            textViewSubcategoryDescription = itemView.findViewById(R.id.textViewSubcategoryDescription);

            Button approveButton = itemView.findViewById(R.id.buttonApprove);
            Button rejectButton = itemView.findViewById(R.id.buttonReject);

            approveButton.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Category selectedCategory = subcategoryRequests.get(position);
                    listener.onApproveClick(selectedCategory);
                }
            });

            rejectButton.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Category selectedCategory = subcategoryRequests.get(position);
                    listener.onRejectClick(selectedCategory);
                }
            });
        }

        public void bind(Category subcategoryRequest) {
            textViewSubcategoryName.setText(subcategoryRequest.getName());
            textViewSubcategoryDescription.setText(subcategoryRequest.getDescription());
        }
    }
}