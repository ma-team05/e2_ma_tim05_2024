package com.example.eventplanner.services;

import com.example.eventplanner.database.repository.GradeReportRepository;
import com.example.eventplanner.models.GradeReport;
import com.example.eventplanner.models.ReportStatus;
import com.google.android.gms.tasks.Task;
import java.util.List;

public class GradeReportService {
    private GradeReportRepository repo;
    public GradeReportService(){
        repo = new GradeReportRepository();
    }
    public Task<Void> addGradeReport(GradeReport gradeReport){
        gradeReport.setStatus(ReportStatus.REPORTED);
        return repo.saveGradeReport(gradeReport);
    }
    public Task<Void> RejectGradeReport(GradeReport gr, String rejection){
        gr.setRejectionReason(rejection);
        gr.setStatus(ReportStatus.REJECTED);
        return repo.updateGradeReport(gr);
    }
    public Task<Void> AcceptGradeReport(GradeReport gr){
        gr.setStatus(ReportStatus.ACCEPTED);
        return repo.updateGradeReport(gr);
    }
    public Task<List<GradeReport>> getAllByGrade(String gradeId){
        return repo.getAllByGrade(gradeId);
    }
    public Task<List<GradeReport>> getAll(){
        return repo.getAll();
    }
    public Task<GradeReport> getById(String id) {
        return repo.getById(id);
    }
}
