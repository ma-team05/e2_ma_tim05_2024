package com.example.eventplanner.models;

public class Notification {
    private String id;
    private NotificationType type;
    private Boolean isSent;
    private Boolean isRead;
    private String description;
    private String userId;

    public Notification() { }
    public Notification(NotificationType type, String description) {
        this.type = type;
        this.description = description;
        this.isSent = false;
        this.isRead = false;
        this.userId = null;
    }
    public Notification(String userId, String description, NotificationType type){
        this.type = type;
        this.userId = userId;
        this.description = description;
        this.isRead = false;
        this.isSent = false;
    }

    public Notification(String id, boolean sent, NotificationType type) {
        this.id = id;
        this.isSent = sent;
        this.type = type;
    }

    public Notification(String id, NotificationType type, Boolean isSent, Boolean isRead, String description, String userId) {
        this.id = id;
        this.type = type;
        this.isSent = isSent;
        this.isRead = isRead;
        this.description = description;
        this.userId = userId;
    }

    public NotificationType getType() {
        return type;
    }

    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }
    public void setType(NotificationType type) {
        this.type = type;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public Boolean isSent() {
        return isSent;
    }
    public void setSent(Boolean sent) {
        isSent = sent;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
