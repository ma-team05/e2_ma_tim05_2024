package com.example.eventplanner;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.activities.LoggedInUserActivity;
import com.example.eventplanner.activities.admins_actions.LoggedInAdminActivity;
import com.example.eventplanner.activities.registration.RegistrationActivity;
import com.example.eventplanner.models.User;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.NotificationListenerService;
import com.example.eventplanner.services.UserService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    private EditText editTextEmail;
    private EditText editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);

        Button loginButton = findViewById(R.id.buttonLogin);
        loginButton.setOnClickListener(v -> login());

        TextView signUpLinkTextView = findViewById(R.id.textViewSignUpLink);
        signUpLinkTextView.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, RegistrationActivity.class);
            startActivity(intent);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        stopService(new Intent(this, NotificationListenerService.class));
    }

    private void login() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser currentUser = mAuth.getCurrentUser();
                        if (currentUser != null && currentUser.isEmailVerified()) {
                            UserService service = new UserService();
                            service.getByEmail(currentUser.getEmail()).addOnCompleteListener(
                                    this, task2 -> {
                                        if (task2.isSuccessful() && task2.getResult() != null) {
                                            if (!task2.getResult().isActivated()) {
                                                Toast.makeText(MainActivity.this, "Access denied.", Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                            if (task2.getResult().isDeactivated()) {
                                                Toast.makeText(MainActivity.this, "Access denied.", Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                            User user = task2.getResult();
                                            Intent intent;
                                            if (user.getRole() == UserRole.ADMIN) {
                                                intent = new Intent(MainActivity.this, LoggedInAdminActivity.class);
                                            } else {
                                                intent = new Intent(MainActivity.this, LoggedInUserActivity.class);
                                            }
                                            intent.putExtra("userId", user.getId());
                                            intent.putExtra("userRole", user.getRole().ordinal());
                                            intent.putExtra("email", user.getEmail());
                                            startActivity(intent);

                                            Intent serviceIntent = new Intent(this, NotificationListenerService.class);
                                            serviceIntent.putExtra("NOTIFICATIONS_USER_ID", user.getId());
                                            startService(serviceIntent);
                                        }
                                    }
                            );
                        } else {
                            Toast.makeText(MainActivity.this, "Please verify your email before signing in.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}