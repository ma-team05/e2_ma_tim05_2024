package com.example.eventplanner.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CategorySpinnerAdapter;
import com.example.eventplanner.adapters.ServiceAdapter;
import com.example.eventplanner.database.repository.CategoryRepository;
import com.example.eventplanner.database.repository.ServiceRepository;
import com.example.eventplanner.models.Category;
import com.example.eventplanner.models.Employee;
import com.example.eventplanner.models.EventType;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.SubcategoryType;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.example.eventplanner.services.CategoryService;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.EventTypeService;
import com.example.eventplanner.services.OwnerService;
import com.example.eventplanner.services.ServiceService;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.Timestamp;


import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class ServiceManagementActivity extends AppCompatActivity {
    private CategoryRepository categoryRepository;
    Spinner categorySpinner;
    Spinner filterCategorySpinner;
    Spinner filterEventTypeSpinner;
    EventTypeService eventTypeService;
    ServiceService serviceService;
    String userId;
    UserRole userRole;
    private RecyclerView recyclerView;
    private List<Service> serviceList;
    private ServiceAdapter serviceAdapter;
    String companyId;
    Category selectedCategoryNewProduct;
    String serviceAppoitmentDuration;
    String serviceAppoitmentMaxDuration;
    String serviceAppoitmentMinDuration;
    String serviceMinDuration;
    String serviceMaxDuration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_service_management);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");
        userRole = UserRole.values()[intent.getIntExtra("userRole", 0)];
        categoryRepository = new CategoryRepository();
        eventTypeService = new EventTypeService();
        ServiceRepository serviceRepository = new ServiceRepository();
        serviceService = new ServiceService(serviceRepository);

        OwnerService ownerService = new OwnerService();
        EmployeeService employeeService = new EmployeeService(new EmployeeRepository());

        if(userRole == UserRole.EMPLOYEE){
            employeeService.get(userId)
                    .addOnSuccessListener(new OnSuccessListener<Employee>() {
                        @Override
                        public void onSuccess(Employee employee) {
                            companyId = employee.getCompanyId();
                            initializeRecyclerView();
                            //TODO dodati za eventtypes dio preko kompanije a izbrisati stari dio
                        }
                    });
        }
        if(userRole == UserRole.OWNER){
            ownerService.get(userId)
                    .addOnSuccessListener(new OnSuccessListener<Owner>() {
                        @Override
                        public void onSuccess(Owner owner) {
                            companyId = owner.getCompanyId();
                            initializeRecyclerView();
                        }
                    });
        }


        Button searchButton = findViewById(R.id.service_search_btn);
        Button filterButton = findViewById(R.id.service_filter_btn);

        filterButton.setOnClickListener(v ->{
//            Log.i("ShopApp", "Bottom Sheet Dialog");
//            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
//            View dialogView = getLayoutInflater().inflate(R.layout.service_filter_sheet, null);
//            bottomSheetDialog.setContentView(dialogView);
//            bottomSheetDialog.show();
            //todo uncomment
            //handleFilterSearch();
        });

        EditText searchET = findViewById(R.id.service_search);
        searchButton.setOnClickListener(v ->{
            String name = searchET.getText().toString();
            //TODO uncomment
            handleSearch(name);
        });

        FloatingActionButton newProductButton = findViewById(R.id.add_service_floatingActionButton);
        newProductButton.setOnClickListener(v-> {
            if(userRole == UserRole.OWNER) {
                AddNewServicePopup();
            }else{
                Toast.makeText(this, "Your have no permission for this action", Toast.LENGTH_SHORT).show();
            }
        } );
    }

    private void initializeRecyclerView(){
        recyclerView = findViewById(R.id.service_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        serviceService.getUndeletedOfPUP(companyId, new ServiceService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Service> objects) {
                serviceList = new ArrayList<>(objects);
                serviceAdapter = new ServiceAdapter(ServiceManagementActivity.this,serviceList, userRole);
                recyclerView.setAdapter(serviceAdapter);
            }

            @Override
            public void onFailure(String errorMessage) {}
        });
    }

    @SuppressLint({"NotifyDataSetChanged"})
    private void AddNewServicePopup() {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.service_add_form, null);
        Service newService = new Service();
        newService.setPending(false); //at the beggining- assumption the subcategory will be something that exists
        newService.setImages(Arrays.asList(
                R.drawable.image_4,
                R.drawable.image_5,
                R.drawable.image_6
        ));
        newService.setEmployees(new ArrayList<>());
        newService.setDeleted(false);
        //ToDo CHANGE TO COMPANY ID
        newService.setCompanyId(companyId);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);


        //setting both layputs to be invisible
        LinearLayout layoutServiceAppointment = popupView.findViewById(R.id.layout_service_appointment);
        LinearLayout layoutServiceNoAppoitnment = popupView.findViewById(R.id.layout_service_no_appointment);
        layoutServiceNoAppoitnment.setVisibility(View.GONE);
        layoutServiceAppointment.setVisibility(View.GONE);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        Button saveButton = popupView.findViewById(R.id.buttonSave_add_product);
        Button cancelButton = popupView.findViewById(R.id.buttonCancel_add_product);
        ImageButton addNewSubcategory = popupView.findViewById(R.id.add_new_subcategory_btn);
        Button eventTypesbutton = popupView.findViewById(R.id.service_event_type_add);
        //handling choice on appoitment
        RadioGroup rg = popupView.findViewById(R.id.is_appoitment_rg);
        RadioButton automaticRadioButotn = popupView.findViewById(R.id.automatic_radio_button);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.is_appointment_yes){
                    layoutServiceNoAppoitnment.setVisibility(View.GONE);
                    layoutServiceAppointment.setVisibility(View.VISIBLE);
                    automaticRadioButotn.setVisibility(View.VISIBLE);
                }else if(checkedId == R.id.is_appoitment_no){
                    layoutServiceNoAppoitnment.setVisibility(View.VISIBLE);
                    layoutServiceAppointment.setVisibility(View.GONE);
                    automaticRadioButotn.setVisibility(View.GONE);
                }
            }
        });

        //fields in layout
        EditText nameEditText = popupView.findViewById(R.id.edit_product_name);
        EditText descriptionEditView = popupView.findViewById(R.id.add_product_description);
        EditText specifitiesEditView = popupView.findViewById(R.id.product_specifities);
        EditText priceEditView = popupView.findViewById(R.id.add_product_price);
        EditText discountEditView = popupView.findViewById(R.id.add_product_discount);
        RadioGroup availableRG = popupView.findViewById(R.id.add_available_service);
        RadioButton availableYES = popupView.findViewById(R.id.add_available_service_yes);
        RadioButton availableNO = popupView.findViewById(R.id.add_available_service_no);
        RadioGroup visibleRG = popupView.findViewById(R.id.add_visible_service);
        RadioButton visibleYES = popupView.findViewById(R.id.add_visible_service_yes);
        RadioButton visibleNO = popupView.findViewById(R.id.add_visible_service_no);

        Button employeesbutton = popupView.findViewById(R.id.button_Employees_Service);

        //reservation deadlines
        Spinner reservation_days = popupView.findViewById(R.id.res_day);
        Spinner reservation_months = popupView.findViewById(R.id.res_month);
        Spinner cancelation_days = popupView.findViewById(R.id.cancel_day);
        Spinner cancelation_months = popupView.findViewById(R.id.cancel_month);
        RadioGroup confirmationRG = popupView.findViewById(R.id.confirmation_rg);
        RadioButton byHandRadioButton = popupView.findViewById(R.id.by_hand_radio_button);

        Spinner serviceDurationSpinner = popupView.findViewById(R.id.service_duration_spinner);
        Spinner serviceMinDurationSpinner = popupView.findViewById(R.id.service_duration_min_spinner);
        Spinner serviceMaxDurationSpinner = popupView.findViewById(R.id.service_duration_max_spinner);

        //setting up spinner data and employee data
        categorySpinner = popupView.findViewById(R.id.product_category_add);
        Spinner subCategorySpinner = popupView.findViewById(R.id.product_subcategory_add);

        setDataToCategorySpinner(categorySpinner);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCategoryNewProduct = (Category)parent.getItemAtPosition(position);

                subCategorySpinner.setAdapter(new CategorySpinnerAdapter(ServiceManagementActivity.this, getSubcategories(selectedCategoryNewProduct)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        eventTypesbutton.setOnClickListener(v -> handleEventTypeSelection(newService));
        employeesbutton.setOnClickListener(v -> handleEmployeeSelection(newService));

        addNewSubcategory.setOnClickListener(v->{
            if(newService.isPending()){
                Toast.makeText(this, "The request for new subcategory alredy has been made", Toast.LENGTH_SHORT).show();
            }else{
                showPopupCreateSubCategory(newService);
            }
        });
        //handling layout changes
        serviceDurationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                serviceAppoitmentDuration =  (String) parent.getItemAtPosition(position);
                Log.i("STRIIIIIIIIIIIIIING",serviceAppoitmentDuration);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        serviceMinDurationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                serviceMinDuration =  (String) parent.getItemAtPosition(position);
                Log.i("STRIIIIIIIIIIIIIING",serviceMinDuration);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        serviceMaxDurationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                serviceMaxDuration =  (String) parent.getItemAtPosition(position);
                Log.i("STRIIIIIIIIIIIIIING",serviceMaxDuration);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        saveButton.setOnClickListener(v -> {
            newService.setName(nameEditText.getText().toString());
            newService.setDescription(descriptionEditView.getText().toString());
            newService.setPrice(Double.parseDouble(priceEditView.getText().toString()));
            newService.setDiscount(Integer.parseInt(discountEditView.getText().toString()));
            newService.setSpecifities(specifitiesEditView.getText().toString());

            int selectedRadioButtonId = availableRG.getCheckedRadioButtonId();
            if(selectedRadioButtonId != -1){
                if(selectedRadioButtonId == availableYES.getId()) {
                    newService.setAvailable(true);
                }
                if(selectedRadioButtonId == availableNO.getId()){
                    newService.setAvailable(false);
                }
            }else{
                Toast.makeText(this, "Select availability.", Toast.LENGTH_SHORT).show();
            }

            int selectedRadioButtonVisibleId = visibleRG.getCheckedRadioButtonId();
            if(selectedRadioButtonVisibleId != -1){
                if(selectedRadioButtonVisibleId == visibleYES.getId()) {
                    newService.setVisible(true);
                }
                if(selectedRadioButtonVisibleId == visibleNO.getId()){
                    newService.setVisible(false);
                }
            }else{
                Toast.makeText(this, "Select visibility.", Toast.LENGTH_SHORT).show();
            }

            //both appoitment andconfirmation type
            int selectedRadioButtonAppoitmentId = rg.getCheckedRadioButtonId();
            if(selectedRadioButtonAppoitmentId != -1){
                if(selectedRadioButtonAppoitmentId == R.id.is_appointment_yes) {
                    newService.setAppointment(true);
                    int selectedRadioButtonConfirmationId = confirmationRG.getCheckedRadioButtonId();
                    if(selectedRadioButtonId != -1){
                        if(selectedRadioButtonId == automaticRadioButotn.getId()) {
                            newService.setConfirmationType(Service.ConfirmationType.AUTOMATIC);
                        }
                        if(selectedRadioButtonId == byHandRadioButton.getId()){
                            newService.setConfirmationType(Service.ConfirmationType.BYHAND);
                        }
                    }else{
                        Toast.makeText(this, "Select confirmation.", Toast.LENGTH_SHORT).show();
                    }

                }
                if(selectedRadioButtonAppoitmentId == R.id.is_appoitment_no){
                    newService.setAppointment(false);
                    newService.setConfirmationType(Service.ConfirmationType.BYHAND);
                }
            }else{
                Toast.makeText(this, "fill in Appoitment questionary.", Toast.LENGTH_SHORT).show();
            }

            Category selectedCategory = (Category)categorySpinner.getSelectedItem();
            newService.setCategory(selectedCategory.getName());
            newService.setCategoryId(selectedCategory.getId());

            Category selectedSubcategory = (Category)subCategorySpinner.getSelectedItem();
            newService.setSubCategory(selectedSubcategory.getName());
            newService.setSubcategoryId(selectedSubcategory.getId());



            int selectedMonthIndex = Integer.parseInt((String) reservation_months.getSelectedItem()); // Adjust for 0-based indexing
            selectedMonthIndex = (selectedMonthIndex) % 12;

            Timestamp reservationDeadline = getTimeStamp(String.valueOf(selectedMonthIndex), (String)reservation_days.getSelectedItem());
            selectedMonthIndex = Integer.parseInt((String) cancelation_months.getSelectedItem()); // Adjust for 0-based indexing
            selectedMonthIndex = (selectedMonthIndex) % 12;
            Timestamp cancelationDeadline = getTimeStamp((String)cancelation_months.getSelectedItem(), (String)cancelation_days.getSelectedItem());
            newService.setReservationDeadline(reservationDeadline);
            newService.setCancelationDeadline(cancelationDeadline);

            if(newService.isAppointment()){
                //service_duration_spinner

                newService.setAppointmentDuration(this.serviceAppoitmentDuration);
            }else{

                if(((String)serviceMinDurationSpinner.getSelectedItem()).equals("Hour")){
                    newService.setMinDuration("0");
                }else{
                    newService.setMinDuration(serviceMinDuration);
                }



                if(((String)serviceMaxDurationSpinner.getSelectedItem()).equals("Hour")){
                    newService.setMaxDuration("0");
                }else{
                    newService.setMaxDuration(serviceMaxDuration);;
                }


            }

            serviceService.create(newService);
            serviceList.add(newService);
            serviceAdapter.notifyDataSetChanged();;

            popupWindow.dismiss();

        });

        cancelButton.setOnClickListener(v -> {

            popupWindow.dismiss();

        });
    }

    private void showPopupCreateSubCategory(Service service) {
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.popup_add_subcategory, null);

        PopupWindow popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.showAtLocation(popupView, 0, 0, 0);

        Spinner spinnerSubcategoryType = popupView.findViewById(R.id.spinnerSubcategoryType);
        Button saveButton = popupView.findViewById(R.id.buttonSave);
        // Set the adapter for the spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.subcategory_types_service, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSubcategoryType.setAdapter(adapter);

        EditText newSCname = popupView.findViewById(R.id.editTextSubcategoryName);
        EditText newSCdesc = popupView.findViewById(R.id.editTextSubcategoryDescription);

        saveButton.setOnClickListener(v->{
            Category newSubcategory = new Category();
            newSubcategory.setName(newSCname.getText().toString());
            newSubcategory.setDescription(newSCdesc.getText().toString());
            newSubcategory.setSubcategoryType(SubcategoryType.PRODUCT); //hardoced because we are working with products :)
            if(selectedCategoryNewProduct != null){
                if(selectedCategoryNewProduct.getSubcategoriesRequests() == null){
                    selectedCategoryNewProduct.setSubcategoriesRequests(new ArrayList<>());
                }
                selectedCategoryNewProduct.getSubcategoriesRequests().add(newSubcategory);
                //update of the category
                CategoryService categorySercive = new CategoryService();
                categorySercive.update(selectedCategoryNewProduct).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        Toast.makeText(ServiceManagementActivity.this, "Request succsessfully sent", Toast.LENGTH_SHORT).show();
                        service.setPending(true); //product is now pending
                    }
                });
            }
            popupWindow.dismiss();
        });


    }

    private void setDataToCategorySpinner(Spinner categorySpinner){
        CategoryService categoryService = new CategoryService();
        categoryService.getAll().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<Category> categories = task.getResult();
                CategorySpinnerAdapter adapter = new CategorySpinnerAdapter(this, categories);
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                categorySpinner.setAdapter(adapter);

            } else {

                Log.e("ServiceManagementActivity", "Error getting categories", task.getException());
            }
        });
    }
    private List<Category> getSubcategories(Category category){
        return category.getSubcategories();
    }

    private void handleEventTypeSelection(Service service){
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.event_type_helper_layout, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        // dodavanje checkboxova
        LinearLayout checkboxContainer = popupView.findViewById(R.id.checkbox_container);
        eventTypeService.getAll(new EventTypeService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<EventType> objects) {
                //dodavanje checkboxova hehe
                for(EventType et: objects){
                    CheckBox checkBox = new CheckBox(ServiceManagementActivity.this);
                    checkBox.setText(et.getName());
                    checkboxContainer.addView(checkBox);
                }
            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });
        Button saveButton = popupView.findViewById(R.id.button_save_event_type);

        saveButton.setOnClickListener(v -> {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < checkboxContainer.getChildCount(); i++) {
                View childView = checkboxContainer.getChildAt(i);
                if (childView instanceof CheckBox) {
                    CheckBox checkBox = (CheckBox) childView;
                    if (checkBox.isChecked()) {
                        sb.append(checkBox.getText().toString()).append(" ");
                    }
                }
            }
            //POSTAVLJANJE VRIJEDNOSTI!!!!!
            service.setEventType(sb.toString());

            popupWindow.dismiss();

        });
    }

    private Timestamp getTimeStamp(String month, String day){
        int selectedMonth = Integer.parseInt(month);
        int selectedDay = Integer.parseInt(day);

        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);

        Calendar selectedCalendar = Calendar.getInstance();
        selectedCalendar.set(currentYear, selectedMonth - 1, selectedDay); // Months are 0-based, so subtract 1 from selectedMonth

        return new Timestamp(selectedCalendar.getTimeInMillis() / 1000, 0);
    }

    private LocalTime getLocalTime(String hour){
        int selectedHour = Integer.parseInt(hour);
        LocalTime localTime = LocalTime.now();

// Set the selected hour to the LocalTime object
        return localTime.withHour(selectedHour);
    }

    private void handleEmployeeSelection(Service service){
        @SuppressLint("InflateParams") View popupView = LayoutInflater.from(this).inflate(R.layout.event_type_helper_layout, null);

        // Create a PopupWindow
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        // dodavanje checkboxova
        LinearLayout checkboxContainer = popupView.findViewById(R.id.checkbox_container);
        EmployeeService employeeService = new EmployeeService(new EmployeeRepository());
        employeeService.getAllByCompany(companyId)
                .addOnSuccessListener(new OnSuccessListener<List<Employee>>() {
                    @Override
                    public void onSuccess(List<Employee> employees) {
                        for(Employee et: employees){
                            CheckBox checkBox = new CheckBox(ServiceManagementActivity.this);
                            checkBox.setText(et.getName());
                            checkboxContainer.addView(checkBox);
                        }
                    }
                });
        Button saveButton = popupView.findViewById(R.id.button_save_event_type);

        saveButton.setOnClickListener(v -> {
            service.setEmployees(new ArrayList<>());
            List<String> employees = service.getEmployees();
            for (int i = 0; i < checkboxContainer.getChildCount(); i++) {
                View childView = checkboxContainer.getChildAt(i);
                if (childView instanceof CheckBox) {
                    CheckBox checkBox = (CheckBox) childView;
                    if (checkBox.isChecked()) {
                        employees.add(checkBox.getText().toString());
                    }
                }
            }
           popupWindow.dismiss();

        });
    }

    private void handleSearch(String name){
        List<Service> searchResult = new ArrayList<>();
        serviceService.getUndeletedOfPUP(companyId ,new ServiceService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Service> objects) {
                serviceList.clear();

                if(name.isEmpty() || name.equals(" ")){
                    serviceList.addAll(objects);
                    serviceAdapter.notifyDataSetChanged();
                }else{
                    searchResult.addAll(objects);
                    for(Service p : searchResult){
                        if(p.getName().contains(name)) {
                            serviceList.add(p);
                        }
                    }
                    serviceAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(String errorMessage) {

            }
        });


    }

}