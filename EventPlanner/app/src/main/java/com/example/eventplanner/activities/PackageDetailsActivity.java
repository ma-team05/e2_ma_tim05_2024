package com.example.eventplanner.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.reservations.PackageReservationActivity;
import com.example.eventplanner.database.repository.PackageRepository;
import com.example.eventplanner.models.Package;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.PackageService;
import com.example.eventplanner.services.UserService;
import com.google.firebase.Timestamp;

import java.util.Calendar;
import java.util.List;

public class PackageDetailsActivity extends AppCompatActivity {
    private User user;
    private Package aPackage;
    private final UserService userService = new UserService();
    private final PackageService packageService = new PackageService(new PackageRepository());
    private int currentIndex;
    private String selectedEventId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_package_details);

        ImageButton btnReserve = findViewById(R.id.btn_reserve);

        btnReserve.setOnClickListener(v -> {
            String packageId = getIntent().getStringExtra("PACKAGE");
            Intent intent = new Intent(PackageDetailsActivity.this, PackageReservationActivity.class);
            intent.putExtra("PACKAGE_ID", packageId);
            intent.putExtra("USER_ID", user.getId());
            startActivity(intent);
        });

        fetchIntentData();
        fetchUserAndPackageData();
    }

    private void fetchIntentData() {
        Intent intent = getIntent();
        selectedEventId = intent.getStringExtra("EVENT");
    }

    private void fetchUserAndPackageData() {
        String email = getIntent().getStringExtra("EMAIL");
        String packageId = getIntent().getStringExtra("PACKAGE");

        userService.getByEmail(email).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                user = task.getResult();
                fetchPackageData(packageId);
            }
        });
    }

    private void fetchPackageData(String packageId) {
        packageService.getAll(new PackageService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Package> packages) {
                aPackage = packages.stream()
                        .filter(p -> p.getId().equals(packageId))
                        .findFirst()
                        .orElse(null);
                if (aPackage != null) {
                    bind();
                }
            }

            @Override
            public void onFailure(String errorMessage) {
                // Handle failure
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void bind() {
        initializeUIComponents();
        populatePackageDetails();
        setupButtonListeners();
    }

    private void initializeUIComponents() {
        currentIndex = 0;

        ImageSwitcher packageImageSwitcher = findViewById(R.id.package_image_switcher);
        packageImageSwitcher.setFactory(() -> {
            ImageView imageView = new ImageView(PackageDetailsActivity.this);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            return imageView;
        });

        if (aPackage.getImages() != null && !aPackage.getImages().isEmpty()) {
            packageImageSwitcher.setImageResource(aPackage.getImages().get(0));
        } else {
            packageImageSwitcher.setImageResource(R.drawable.default_image);
        }

        updateFavoriteButtons();
    }

    private void populatePackageDetails() {
        ((TextView) findViewById(R.id.package_name)).setText(aPackage.getName());
        ((TextView) findViewById(R.id.package_category)).setText(aPackage.getCategory());
        ((TextView) findViewById(R.id.package_subcategory)).setText(aPackage.getSubcategoriesString());
        ((TextView) findViewById(R.id.package_description)).setText(aPackage.getDescription());
        ((TextView) findViewById(R.id.package_price)).setText(String.valueOf(aPackage.getPrice()));
        ((TextView) findViewById(R.id.package_discount)).setText(String.valueOf(aPackage.getDiscount()));
        ((TextView) findViewById(R.id.package_discount_price)).setText(String.valueOf(aPackage.getPrice() * (100 - aPackage.getDiscount()) / 100));
        ((TextView) findViewById(R.id.package_event_type)).setText(aPackage.getEventTypes());
        ((TextView) findViewById(R.id.package_available)).setText(aPackage.isAvailable() ? "Yes" : "No");
        ((TextView) findViewById(R.id.package_visibility)).setText(aPackage.isVisible() ? "Yes" : "No");
        ((TextView) findViewById(R.id.package_products)).setText(aPackage.getProductsString());
        ((TextView) findViewById(R.id.package_services)).setText(aPackage.getServicesString());
        ((TextView) findViewById(R.id.package_res_deadline)).setText(formatTimestamp(aPackage.getReservationDeadline()));
        ((TextView) findViewById(R.id.package_cancel_in)).setText(formatTimestamp(aPackage.getCancellationDeadline()));
        ((TextView) findViewById(R.id.package_confirmation)).setText(aPackage.getConfirmationType() == Service.ConfirmationType.AUTOMATIC ? "Automatic" : "By hand");

        if (!aPackage.isAvailable()) {
            findViewById(R.id.btn_buy).setVisibility(View.GONE);
            findViewById(R.id.btn_reserve).setVisibility(View.GONE);
        }
    }

    private String formatTimestamp(Timestamp timestamp) {
        if (timestamp == null) {
            return "N/A";
        }
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp.getSeconds() * 1000);
        return cal.get(Calendar.MONTH) + "m " + cal.get(Calendar.DAY_OF_MONTH) + "d ";
    }

    private void setupButtonListeners() {
        findViewById(R.id.bt_previous).setOnClickListener(v -> showPreviousImage());
        findViewById(R.id.bt_next).setOnClickListener(v -> showNextImage());
        findViewById(R.id.btn_fav).setOnClickListener(v -> addToFavorites());
        findViewById(R.id.btn_unfav).setOnClickListener(v -> removeFromFavorites());
        findViewById(R.id.btn_info).setOnClickListener(v -> startPackageServicesAndProductsActivity());
        findViewById(R.id.btn_company_info).setOnClickListener(v -> startCompanyDetailsActivity());
        findViewById(R.id.btn_buy).setOnClickListener(v -> startBuyPackageActivity());
    }

    private void showPreviousImage() {
        if (currentIndex > 0) {
            currentIndex--;
            ((ImageSwitcher) findViewById(R.id.package_image_switcher)).setImageResource(aPackage.getImages().get(currentIndex));
        }
    }

    private void showNextImage() {
        if (currentIndex < aPackage.getImages().size() - 1) {
            currentIndex++;
            ((ImageSwitcher) findViewById(R.id.package_image_switcher)).setImageResource(aPackage.getImages().get(currentIndex));
        }
    }

    private void updateFavoriteButtons() {
        if (user.getFavouritePackages().contains(aPackage.getId())) {
            findViewById(R.id.btn_unfav).setVisibility(View.VISIBLE);
            findViewById(R.id.btn_fav).setVisibility(View.GONE);
        } else {
            findViewById(R.id.btn_fav).setVisibility(View.VISIBLE);
            findViewById(R.id.btn_unfav).setVisibility(View.GONE);
        }
    }

    private void addToFavorites() {
        user.addFavouritePackage(aPackage.getId());
        userService.update(user);
        updateFavoriteButtons();
    }

    private void removeFromFavorites() {
        user.removeFavouritePackage(aPackage.getId());
        userService.update(user);
        updateFavoriteButtons();
    }

    private void startPackageServicesAndProductsActivity() {
        Intent intent = new Intent(this, PackageServicesAndProductsActivity.class);
        intent.putExtra("EMAIL", user.getEmail());
        intent.putExtra("PACKAGE", aPackage.getId());
        startActivity(intent);
    }

    private void startCompanyDetailsActivity() {
        Intent intent = new Intent(this, CompanyDetailsActivity.class);
        intent.putExtra("EMAIL", user.getEmail());
        intent.putExtra("COMPANY", aPackage.getCompanyId());
        startActivity(intent);
    }

    private void startBuyPackageActivity() {
        Intent intent = new Intent(this, PackageReservationActivity.class);
        intent.putExtra("EMAIL", user.getEmail());
        intent.putExtra("COMPANY", aPackage.getCompanyId());
        intent.putExtra("EVENT", selectedEventId);
        intent.putExtra("PACKAGE_ID", aPackage.getId());
        intent.putExtra("USER_ID", user.getId());
        startActivity(intent);
    }
}