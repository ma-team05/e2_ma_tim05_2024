package com.example.eventplanner.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.GradeAdapter;
import com.example.eventplanner.database.repository.EmployeeRepository;
import com.example.eventplanner.models.Grade;
import com.example.eventplanner.models.Owner;
import com.example.eventplanner.models.UserRole;
import com.example.eventplanner.services.EmployeeService;
import com.example.eventplanner.services.GradeService;
import com.example.eventplanner.services.OwnerService;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class GradesActivity extends AppCompatActivity {

    TextView cancelFilter;
    Date from;
    Date to;
    TextView openDatePickerButton;
    TextView openDatePickerButton2;
    TextView toTextView;
    TextView fromTextView;
    TextView filterButton;
    private Owner owner;
    private GradeService gradeService = new GradeService();
    private OwnerService ownerService = new OwnerService();
    private EmployeeService employeeService = new EmployeeService(new EmployeeRepository());
    private List<Grade> grades = new ArrayList<>();
    private List<Grade> filteredGrades = new ArrayList<>();
    private GradeAdapter adapter;
    private String userId;
    private UserRole userRole;
    private LinearLayout filtersContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_grades);

        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");
        userRole = UserRole.valueOf(intent.getStringExtra("userRole"));
        setFilters();
        setUpRecyclerView();
        loadGrades();
    }

    private void setFilters(){
        filtersContainer = findViewById(R.id.filtersContainer);
        if(userRole==UserRole.OWNER)
            filtersContainer.setVisibility(View.VISIBLE);
        fromTextView = findViewById(R.id.fromTextView);
        toTextView = findViewById(R.id.toTextView);
        openDatePickerButton = findViewById(R.id.openDatePickerButton);
        openDatePickerButton2 = findViewById(R.id.openDatePickerButton2);
        cancelFilter = findViewById(R.id.cancelFilter);
        filterButton = findViewById(R.id.filterButton);
        openDatePickerButton.setOnClickListener(v->{
            showDatePicker(fromTextView, "Grades from");
        });
        openDatePickerButton2.setOnClickListener(v->{
            showDatePicker(toTextView, "to");
        });
        filterButton.setOnClickListener(v->{
            filter();
        });
        cancelFilter.setOnClickListener(v->{
            setFilteredGrades();
            updateRecyclerView();
        });
    }
    private void filter() {
        if (from == null || to == null) {
            Toast.makeText(this, "Please select both dates", Toast.LENGTH_SHORT).show();
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        Log.d("Filter", "From: " + sdf.format(from));
        Log.d("Filter", "To: " + sdf.format(to));

        filteredGrades.clear();
        for (Grade g : grades) {
            Log.d("Filter", "CreatedOn: " + sdf.format(g.getCreatedOn()));

            boolean after = g.getCreatedOn().after(from);
            boolean before = g.getCreatedOn().before(to);

            Log.d("Filter", "After: " + after + ", Before: " + before);

            if (after && before) {
                filteredGrades.add(g);
            }
        }
        updateRecyclerView();
    }

    private void loadGrades() {
        switch (userRole) {
            case ADMIN: {
                gradeService.getAll().addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        grades = task.getResult();
                        setFilteredGrades();
                        updateRecyclerView();
                    }
                });
                break;
            }
            case EVENT_PLANNER: {
                gradeService.getAll().addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        grades = task.getResult();
                        setFilteredGrades();
                        updateRecyclerView();
                    }
                });
                break;
            }
            case OWNER: {
                ownerService.get(userId).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        owner = task.getResult();
                        gradeService.getAllByCompany(owner.getCompanyId()).addOnCompleteListener(task2 -> {
                            if (task2.isSuccessful()) {
                                grades = task2.getResult();
                                setFilteredGrades();
                                updateRecyclerView();
                            } else {
                                Exception e = task2.getException();
                                e.printStackTrace();
                            }
                        });
                    }
                });
                break;
            }
            case EMPLOYEE: {
                employeeService.get(userId).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        gradeService.getAllByCompany(task.getResult().getCompanyId()).addOnCompleteListener(task2 -> {
                            if (task2.isSuccessful()) {
                                grades = task2.getResult();
                                setFilteredGrades();
                                updateRecyclerView();
                            }
                        });
                    }
                });
                break;
            }
        }
    }

    private void setFilteredGrades(){
        fromTextView.setText("Grades from");
        toTextView.setText("to");
        from = null;
        to = null;
        filteredGrades.clear();
        for(Grade g: grades){
            filteredGrades.add(g);
        }
    }
    private void updateRecyclerView() {
        adapter.updateGrades(filteredGrades);
        Log.d("GradesActivity", "RecyclerView updated with adapter and " + grades.size() + " grades.");
    }

    private void setUpRecyclerView() {
        RecyclerView rv = findViewById(R.id.gradesRecyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adapter = new GradeAdapter(grades, userRole == UserRole.OWNER, userId, this);
        rv.setAdapter(adapter);
        Log.d("GradesActivity", "RecyclerView set up with adapter and " + grades.size() + " grades.");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            adapter.notifyDataSetChanged();
        }
    }

    private void showDatePicker(TextView datePicker, String defaultText) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                (view, year1, monthOfYear, dayOfMonth1) -> {
                    datePicker.setText(defaultText+" "+String.format(Locale.getDefault(), "%04d-%02d-%02d", year1, monthOfYear + 1, dayOfMonth1));
                    if(defaultText.equals("to"))
                        to = new Date(year1-1900, monthOfYear, dayOfMonth1);
                    else
                        from = new Date(year1-1900, monthOfYear, dayOfMonth1);
                },
                year,
                month,
                dayOfMonth
        );
        datePickerDialog.show();
    }
}
