package com.example.eventplanner.database.repository;

import com.example.eventplanner.models.Company;
import com.example.eventplanner.models.Employee;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Objects;

public class CompanyRepository {
    private final CollectionReference companiesCollection;

    public CompanyRepository() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        companiesCollection = db.collection("companies");
    }

    public Task<String> save(Company company) {
        DocumentReference newCompanyRef = companiesCollection.document();
        company.setId(newCompanyRef.getId());
        return newCompanyRef.set(company)
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        return newCompanyRef.getId();
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }
    public Task<Company> get(String id){
        DocumentReference companyRef = companiesCollection.document(id);
        return companyRef.get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            return document.toObject(Company.class);
                        } else {
                            throw new Exception("Company with ID " + id + " not found");
                        }
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }

    public Task<Company> getByName(String companyName) {
        return companiesCollection.whereEqualTo("name", companyName).get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (!querySnapshot.isEmpty()) {
                            DocumentSnapshot document = querySnapshot.getDocuments().get(0);
                            return document.toObject(Company.class);
                        } else {
                            throw new Exception("Company with name " + companyName + " not found");
                        }
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }

    public Task<Void> update(Company company){
        DocumentReference employeeRef = companiesCollection.document(company.getId());
        return employeeRef.set(company);
    }
}
