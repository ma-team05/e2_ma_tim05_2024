package com.example.eventplanner.models;

public enum EventStatus {
    RESERVED,
    TAKEN
}
