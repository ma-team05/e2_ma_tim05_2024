package com.example.eventplanner.models;

public enum SubcategoryType {
    PRODUCT(0),
    SERVICE(1),
    CATEGORY(2);

    private final int value;

    SubcategoryType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static SubcategoryType fromValue(int value) {
        for (SubcategoryType type : SubcategoryType.values()) {
            if (type.value == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("Invalid SubcategoryType value: " + value);
    }
}
