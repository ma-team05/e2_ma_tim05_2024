package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.models.ChatMessage;

import java.text.SimpleDateFormat;
import java.util.List;

public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.ChatMessageViewHolder> {
    private Context context;
    private List<ChatMessage> messages;
    private String currentUserId;

    public ChatMessageAdapter(Context context, List<ChatMessage> messages, String currentUserId) {
        this.context = context;
        this.messages = messages;
        this.currentUserId = currentUserId;
    }

    @NonNull
    @Override
    public ChatMessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_message_card, parent, false);
        return new ChatMessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatMessageViewHolder holder, int position) {
        ChatMessage message = messages.get(position);
        holder.bind(message, currentUserId);
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static class ChatMessageViewHolder extends RecyclerView.ViewHolder {
        LinearLayout rootLayout;
        TextView messageText;
        TextView messageDate;

        public ChatMessageViewHolder(@NonNull View itemView) {
            super(itemView);
            rootLayout = itemView.findViewById(R.id.root_layout);
            messageText = itemView.findViewById(R.id.message_text);
            messageDate = itemView.findViewById(R.id.message_date);
        }

        public void bind(ChatMessage message, String currentUserId) {
            messageText.setText(message.getMessage());
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy. HH:mm");
            String date = formatter.format(message.getDate());
            messageDate.setText(date);

            if (message.getFrom().equals(currentUserId)) {
                rootLayout.setBackgroundResource(R.color.purple);
            } else {
                rootLayout.setBackgroundResource(R.color.purple_washed);
            }
        }
    }
}
