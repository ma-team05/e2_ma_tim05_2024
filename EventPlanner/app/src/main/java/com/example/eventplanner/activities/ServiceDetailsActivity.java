package com.example.eventplanner.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.reservations.ServiceReservationActivity;
import com.example.eventplanner.database.repository.ServiceRepository;
import com.example.eventplanner.models.Service;
import com.example.eventplanner.models.User;
import com.example.eventplanner.services.ServiceService;
import com.example.eventplanner.services.UserService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ServiceDetailsActivity extends AppCompatActivity {
    private User user;
    private Service service;
    private final UserService userService = new UserService();
    private final ServiceService serviceService = new ServiceService(new ServiceRepository());
    private int currentIndex;
    private String selectedEventId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_service_details);

        Intent intent = getIntent();
        String email = intent.getStringExtra("EMAIL");
        String serviceId = intent.getStringExtra("SERVICE");
        selectedEventId = intent.getStringExtra("EVENT");

        userService.getByEmail(email).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                user = task.getResult();
                fetchServiceDetails(serviceId);
            }
        });
    }

    private void fetchServiceDetails(String serviceId) {
        serviceService.getAll(new ServiceService.OnDataFetchListener() {
            @Override
            public void onSuccess(List<Service> services) {
                service = services.stream()
                        .filter(s -> s.getId().equals(serviceId))
                        .findFirst()
                        .orElse(null);
                if (service != null) {
                    bindServiceDetails();
                }
            }

            @Override
            public void onFailure(String errorMessage) {
                // Handle failure
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void bindServiceDetails() {
        currentIndex = 0;

        ImageButton btPrevious = findViewById(R.id.bt_previous);
        ImageButton btNext = findViewById(R.id.bt_next);
        ImageButton btnFav = findViewById(R.id.btn_fav);
        ImageButton btnUnFav = findViewById(R.id.btn_unfav);
        ImageButton btnCompanyInfo = findViewById(R.id.btn_company_info);
        ImageButton btnChat = findViewById(R.id.btn_chat);
        ImageButton btnBuy = findViewById(R.id.btn_buy);
        ImageSwitcher solutionImageSwitcher = findViewById(R.id.solution_image_switcher);
        ImageButton btnReserveService = findViewById(R.id.btn_reserve);

        solutionImageSwitcher.setFactory(() -> {
            ImageView imageView = new ImageView(ServiceDetailsActivity.this);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            return imageView;
        });

        TextView serviceName = findViewById(R.id.solution_name);
        TextView serviceCategory = findViewById(R.id.solution_category);
        TextView serviceSubcategory = findViewById(R.id.solution_subcategory);
        TextView serviceDescription = findViewById(R.id.solution_description);
        TextView servicePrice = findViewById(R.id.solution_price);
        TextView serviceDiscount = findViewById(R.id.solution_discount);
        TextView serviceDiscountPrice = findViewById(R.id.solution_discount_price);
        TextView serviceAvailable = findViewById(R.id.solution_available);
        TextView serviceVisibility = findViewById(R.id.solution_visibility);
        TextView serviceEventType = findViewById(R.id.solution_event_type);
        TextView serviceDuration = findViewById(R.id.solution_duration);
        TextView serviceReserveIn = findViewById(R.id.solution_res_deadline);
        TextView serviceCancelIn = findViewById(R.id.solution_cancel_in);
        TextView serviceConfirm = findViewById(R.id.solution_confirmation);
        TextView serviceEmployees = findViewById(R.id.solution_employees);
        TextView serviceSpecifities = findViewById(R.id.solution_specifities);

        serviceName.setText(service.getName());
        serviceCategory.setText(service.getCategory());
        serviceSubcategory.setText(service.getSubCategory());
        serviceDescription.setText(service.getDescription());
        servicePrice.setText(String.valueOf(service.getPrice()));
        serviceDiscount.setText(String.valueOf(service.getDiscount()));
        serviceEventType.setText(service.getEventType());
        serviceAvailable.setText(service.isAvailable() ? "Yes" : "No");
        serviceVisibility.setText(service.isVisible() ? "Yes" : "No");
        serviceEmployees.setText(service.getEmployeesAsString());

        setServiceDuration(serviceDuration);
        setServiceDeadlines(serviceReserveIn, serviceCancelIn);
        setServiceSpecifities(serviceSpecifities);
        setServiceConfirmationType(serviceConfirm);
        setServiceDiscountPrice(serviceDiscountPrice);

        setInitialImage(solutionImageSwitcher);
        setFavoriteButtonVisibility(btnFav, btnUnFav);
        setImageSwitcherListeners(btPrevious, btNext, solutionImageSwitcher);
        setFavoriteButtonListeners(btnFav, btnUnFav);
        setInfoButtonListeners(btnCompanyInfo, btnChat, btnBuy, btnReserveService);

        if (!service.isAvailable()) {
            btnBuy.setVisibility(View.GONE);
            btnReserveService.setVisibility(View.GONE);
        }
    }

    @SuppressLint("SetTextI18n")
    private void setServiceDuration(TextView serviceDuration) {
        if (service.isAppointment()) {
            serviceDuration.setText(service.getAppointmentDuration() + "h");
        } else {
            serviceDuration.setText("min " + service.getMinDuration() + "h " + "max " + service.getMaxDuration() + "h");
        }
    }

    private void setServiceDeadlines(TextView serviceReserveIn, TextView serviceCancelIn) {
        Calendar cal = Calendar.getInstance();

        if (service.getReservationDeadline() != null) {
            Date reservationDate = service.getReservationDeadline().toDate();
            cal.setTimeInMillis(reservationDate.getTime());
            String reservationBuilder = cal.get(Calendar.MONTH) + "m " + cal.get(Calendar.DAY_OF_MONTH) + "d ";
            serviceReserveIn.setText(reservationBuilder);
        } else {
            serviceReserveIn.setText("N/A");
        }

        if (service.getCancelationDeadline() != null) {
            Date cancellationDate = service.getCancelationDeadline().toDate();
            cal.setTimeInMillis(cancellationDate.getTime());
            String cancellationBuilder = cal.get(Calendar.MONTH) + "m " + cal.get(Calendar.DAY_OF_MONTH) + "d ";
            serviceCancelIn.setText(cancellationBuilder);
        } else {
            serviceCancelIn.setText("N/A");
        }
    }


    private void setServiceSpecifities(TextView serviceSpecifities) {
        if (service.getSpecifities().isEmpty()) {
            serviceSpecifities.setText("/");
        } else {
            serviceSpecifities.setText(service.getSpecifities());
        }
    }

    @SuppressLint("SetTextI18n")
    private void setServiceConfirmationType(TextView serviceConfirm) {
        if (service.getConfirmationType() == Service.ConfirmationType.AUTOMATIC) {
            serviceConfirm.setText("Automatic");
        } else {
            serviceConfirm.setText("By hand");
        }
    }

    @SuppressLint("SetTextI18n")
    private void setServiceDiscountPrice(TextView serviceDiscountPrice) {
        if (service.isAppointment()) {
            double finalPrice = service.getPrice() - service.getDiscount();
            double hours = Double.parseDouble(service.getAppointmentDuration());
            finalPrice *= hours;
            serviceDiscountPrice.setText(String.valueOf(finalPrice));
        } else {
            serviceDiscountPrice.setText("will be calculated");
        }
    }

    private void setInitialImage(ImageSwitcher solutionImageSwitcher) {
        if (service.getImages() != null && !service.getImages().isEmpty()) {
            solutionImageSwitcher.setImageResource(service.getImages().get(0));
        } else {
            solutionImageSwitcher.setImageResource(R.drawable.default_image);
        }
    }

    private void setFavoriteButtonVisibility(ImageButton btnFav, ImageButton btnUnFav) {
        if (user.getFavouriteServices().contains(service.getId())) {
            btnUnFav.setVisibility(View.VISIBLE);
            btnFav.setVisibility(View.GONE);
        } else {
            btnFav.setVisibility(View.VISIBLE);
            btnUnFav.setVisibility(View.GONE);
        }
    }

    private void setImageSwitcherListeners(ImageButton btPrevious, ImageButton btNext, ImageSwitcher solutionImageSwitcher) {
        btNext.setOnClickListener(v -> {
            if (currentIndex < service.getImages().size() - 1) {
                currentIndex++;
                solutionImageSwitcher.setImageResource(service.getImages().get(currentIndex));
            }
        });

        btPrevious.setOnClickListener(v -> {
            if (currentIndex > 0) {
                currentIndex--;
                solutionImageSwitcher.setImageResource(service.getImages().get(currentIndex));
            }
        });
    }

    private void setFavoriteButtonListeners(ImageButton btnFav, ImageButton btnUnFav) {
        btnFav.setOnClickListener(v -> {
            user.addFavouriteService(service.getId());
            userService.update(user);
            btnFav.setVisibility(View.GONE);
            btnUnFav.setVisibility(View.VISIBLE);
        });

        btnUnFav.setOnClickListener(v -> {
            user.removeFavouriteService(service.getId());
            userService.update(user);
            btnFav.setVisibility(View.VISIBLE);
            btnUnFav.setVisibility(View.GONE);
        });
    }

    private void setInfoButtonListeners(ImageButton btnCompanyInfo, ImageButton btnChat, ImageButton btnBuy, ImageButton btnReserve) {
        btnCompanyInfo.setOnClickListener(v -> {
            Intent createIntent = new Intent(ServiceDetailsActivity.this, CompanyDetailsActivity.class);
            createIntent.putExtra("EMAIL", user.getEmail());
            createIntent.putExtra("COMPANY", service.getCompanyId());
            startActivity(createIntent);
        });

        btnChat.setOnClickListener(v -> {
            Intent createIntent = new Intent(ServiceDetailsActivity.this, StartChatEmployeeActivity.class);
            createIntent.putExtra("EMAIL", user.getEmail());
            createIntent.putExtra("email", user.getEmail());
            createIntent.putExtra("COMPANY", service.getCompanyId());
            startActivity(createIntent);
        });

        btnBuy.setOnClickListener(v -> {
            Intent createIntent = new Intent(ServiceDetailsActivity.this, ServiceReservationActivity.class);
            createIntent.putExtra("EMAIL", user.getEmail());
            createIntent.putExtra("email", user.getEmail());
            createIntent.putExtra("serviceId", service.getId());
            createIntent.putExtra("COMPANY", service.getCompanyId());
            createIntent.putExtra("EVENT", selectedEventId);
            startActivity(createIntent);
        });

        btnReserve.setOnClickListener(v -> {
            Intent createIntent = new Intent(ServiceDetailsActivity.this, ServiceReservationActivity.class);
            createIntent.putExtra("EMAIL", user.getEmail());
            createIntent.putExtra("email", user.getEmail());
            createIntent.putExtra("serviceId", service.getId());
            createIntent.putExtra("COMPANY", service.getCompanyId());
            createIntent.putExtra("EVENT", selectedEventId);
            startActivity(createIntent);
        });
    }
}